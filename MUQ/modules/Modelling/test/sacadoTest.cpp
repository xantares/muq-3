
#include <iostream>
#include <vector>

#include <Eigen/Dense>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/SacadoModPiece.h"
#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"


using namespace muq::Modelling;
using namespace std;


template<typename T>
const T func1(const Eigen::Matrix<T, Eigen::Dynamic, 1>& x) // sample function of n variables
{
  return x.squaredNorm();
}

template<typename T>
const T func2(const Eigen::Matrix<T, Eigen::Dynamic, 1>& x) // sample function of n variables
{
  return x.cwiseSqrt().squaredNorm();
}

class SacadoTestMod : public SacadoModPiece {
public:

  SacadoTestMod(int dim) : SacadoModPiece(dim * Eigen::VectorXi::Ones(2), 2) {}

  // define a templated version of the EvaluateImpl function.  This can be called anything as long as the appropriate
  // name is passed to the SACADO_EVALUATORS macro
  template<typename derived>
  Eigen::Matrix<derived, Eigen::Dynamic, 1> run(const std::vector < Eigen::Matrix < derived, Eigen::Dynamic, 1 >>
                                                & input)
  {
    Eigen::Matrix<derived, Eigen::Dynamic, 1> output(2);

    output << func1(input[0]) + func2(input[1]), func2(input[0]) + func1(input[1]);
    return output;
  }

  // include the SACADO_EVALUATORS macro to fill in all the virtual members from the SacadoModPiece class
  SACADO_EVALUATORS(run)
};


TEST(ModellingSacadoTest, SacadoModPieceWorks)
{
  auto testPiece = make_shared<SacadoTestMod>(2);
  std::vector<Eigen::VectorXd> inputs(2);

  inputs[0] = Eigen::VectorXd::Ones(2);
  inputs[1] = 0.1 * Eigen::VectorXd::Ones(2);

  Eigen::MatrixXd Jac = testPiece->Jacobian(inputs, 1);
  EXPECT_DOUBLE_EQ(1.0, Jac(0, 0));
  EXPECT_DOUBLE_EQ(1.0, Jac(0, 1));
  EXPECT_DOUBLE_EQ(0.2, Jac(1, 0));
  EXPECT_DOUBLE_EQ(0.2, Jac(1, 1));

  Eigen::VectorXd grad = testPiece->Gradient(inputs, Eigen::VectorXd::Ones(2), 0);
  EXPECT_DOUBLE_EQ(3.0, grad(0));
  EXPECT_DOUBLE_EQ(3.0, grad(1));

  Eigen::VectorXd Jv = testPiece->JacobianAction(inputs, Eigen::VectorXd::Ones(2), 0);
  EXPECT_DOUBLE_EQ(4.0, Jv(0));
  EXPECT_DOUBLE_EQ(2.0, Jv(1));

  Eigen::MatrixXd Hess = testPiece->Hessian(inputs, Eigen::VectorXd::Ones(2), 1);
  EXPECT_DOUBLE_EQ(2.0, Hess(1, 1));
  EXPECT_DOUBLE_EQ(2.0, Hess(0, 0));
  EXPECT_DOUBLE_EQ(0.0, Hess(0, 1));
  EXPECT_DOUBLE_EQ(0.0, Hess(1, 0));
}


