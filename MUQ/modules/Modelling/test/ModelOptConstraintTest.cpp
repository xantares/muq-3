
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/Model.h"
#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Optimization/OptProblem.h"
#include "MUQ/Optimization/Optimizer.h"
#include "MUQ/Modelling/ModelOptConstraint.h"

//#include "MUQ/Modelling/AnalyticFunctions/ExpModel.h"
#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"
#include "MUQ/Modelling/LinearModel.h"

using namespace muq::Modelling;
using namespace muq::Optimization;
using namespace muq::utilities;


/** Define an isotropic quadratic problem as a simple test optimization problem. */
class IsoQuadFunc : public OptProblem {
public:

  /** Evaluate the objective. */
  virtual double eval(const Eigen::VectorXd& xc)
  {
    return 0.5 * xc.squaredNorm();
  }

  /** Evaluate the gradient and return the objective value*/
  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient)
  {
    gradient = xc;
    return 0.5 * xc.squaredNorm();
  }
};


TEST(ModellingOptConstraintTest, QuadBFGS_NonlinearInequality)
{
  // create a parameter list declaring the solver, etc...
  ParameterList params;

  params.Set("Opt.Method", "BFGS-Line");
  params.Set("Opt.xtol", 0.0);
  params.Set("Opt.ftol", 0.0);
  params.Set("Opt.gtol", 1e-7);
  params.Set("Opt.ctol", 1e-4);

  OptimizerPtr Solver = OptimizerEngine::setSolver(params);

  // define the problem
  OptProbPtr prob(new IsoQuadFunc());

  // add a nonlinear equality constraint using the Modelling framework
  Model theta = std::make_shared<ModParameter>(2, 1.0);

  Eigen::MatrixXd A = Eigen::MatrixXd::Zero(2, 2);
  A(0, 0) = -1;
  A(1, 1) = -1;

  Model g_of_theta = A * exp(theta);

  // define the constraint
  OptConstraint consts = new  ModelOptConstraint(false, theta, g_of_theta, -2.0 * Eigen::VectorXd::Ones(2));

  // add the constraint to the optimization problem
  (*prob) += consts;

  // create an initial point for the optimization
  Eigen::VectorXd xc = Eigen::VectorXd::Random(2);

  Solver->min(prob, xc);

  // get the results
  xc = Solver->getState();


  EXPECT_NEAR(0.69314724830906682, xc[0], 1e-4);
  EXPECT_NEAR(0.69314724830906682, xc[1], 1e-4);
}

TEST(ModellingOptConstraintTest, QuadBFGS_NonlinearEquality)
{
  // create a parameter list declaring the solver, etc...
  ParameterList params;

  params.Set("Opt.Method", "BFGS-Line");
  params.Set("Opt.xtol", 0.0);
  params.Set("Opt.ftol", 0.0);
  params.Set("Opt.gtol", 1e-8);
  params.Set("Opt.ctol", 1e-4);

  OptimizerPtr Solver = OptimizerEngine::setSolver(params);

  // define the problem
  OptProbPtr prob(new IsoQuadFunc());

  // add a nonlinear equality constraint using the Modelling framework
  Model theta = std::make_shared<ModParameter>(2, 1.0);

  Eigen::MatrixXd A = Eigen::MatrixXd::Zero(1, 2);
  A(0, 0) = 1;

  Model g_of_theta = A * exp(theta);

  // define the constraint
  OptConstraint consts = new  ModelOptConstraint(true, theta, g_of_theta, 2.0 * Eigen::VectorXd::Ones(1));

  // add the constraint to the optimization problem
  (*prob) += consts;

  // create an initial point for the optimization
  Eigen::VectorXd xc = Eigen::VectorXd::Random(2);

  Solver->min(prob, xc);

  // get the results
  xc = Solver->getState();


  EXPECT_NEAR(0.69314724830906682, xc[0], 1e-4);
  EXPECT_NEAR(0.0,                 xc[1], 1e-4);
}