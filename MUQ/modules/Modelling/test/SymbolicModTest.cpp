
#include <iostream>

// include the google testing header
#include "gtest/gtest.h"

// MUQ-related includes
#include "MUQ/Modelling/Model.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"


using namespace muq::Modelling;
using namespace std;

TEST(ModellingAnalyticFunctionsTest, SinTest)
{
  Model Input = make_shared<ModParameter>(3, 3.14);
  Model Sin   = make_shared<SinModel>(3);

  Sin.SetInput(Input);

  Eigen::VectorXd result(3);
  Sin.Eval(result);

  for (int i = 0; i < 3; ++i) {
    EXPECT_EQ(sin(3.14), result[i]);
  }
}

TEST(ModellingAnalyticFunctionsTest, ExpTest)
{
  Model Input = make_shared<ModParameter>(3, 3.14);
  Model Exp;

  Exp = exp(Input);

  Eigen::VectorXd result(3);
  Exp.Eval(result);

  Input.Update(result);
  Exp.Eval(result);

  for (int i = 0; i < 3; ++i) {
    EXPECT_EQ(exp(exp(3.14)), result[i]);
  }
}


TEST(ModellingAnalyticFunctionsTest, AbsTest)
{
  Model Input = make_shared<ModParameter>(3, -3.14);
  Model Abs;

  Abs = abs(Input);

  Eigen::VectorXd result(3);
  Abs.Eval(result);

  Input.Update(result);
  Abs.Eval(result);

  for (int i = 0; i < 3; ++i) {
    EXPECT_EQ(fabs(-3.14), result[i]);
  }
}

TEST(ModellingAnalyticFunctionsTest, LogTest)
{
  Model Input = make_shared<ModParameter>(3, 1e-4);
  Model Log;

  Log = log(Input);

  Eigen::VectorXd result(3);
  Log.Eval(result);

  for (int i = 0; i < 3; ++i) {
    EXPECT_EQ(log(1e-4), result[i]);
  }
}