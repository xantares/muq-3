
#include <iostream>
#include <math.h>
#include <vector>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/Model.h"
#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"
#include "MUQ/Modelling/PceModel.h"


using namespace muq::Modelling;
using namespace muq::polychaos;

using namespace std;

TEST(ModellingPolychaosModelTest, BasicLegendreModel)
{
  // first, create a model
  Model theta      = make_shared<ModParameter>(3, 1.0);
  Model f_of_theta = cos(theta);

  // now create a polynomial chaos approximation to the model
  Model PceSurr = make_shared<PceModel>(theta, f_of_theta, "Legendre", 1e-4);

  // make sure the approximation is accurate
  Eigen::VectorXd Input = 0.8 * Eigen::VectorXd::Ones(3);
  Eigen::VectorXd TrueOut(3), ApproxOut(3);
  Eigen::VectorXd ApproxGrad = Eigen::VectorXd::Zero(3);

  PceSurr.SetInput(theta);
  theta.Update(Input);

  f_of_theta.Eval(TrueOut);
  PceSurr.Eval(ApproxOut);

  Eigen::VectorXd sensIn(3);
  sensIn << 1, 0, 0;

  PceSurr.Grad(theta, sensIn, ApproxGrad);

  EXPECT_NEAR(TrueOut[0],    ApproxOut[0],   1e-5);
  EXPECT_NEAR(ApproxGrad(0), -1.0 * sin(.8), .01);
}


TEST(ModellingPolychaosModelTest, BasicHermite)
{
  // first, create a model
  Model theta      = make_shared<ModParameter>(3, 1.0);
  Model f_of_theta = cos(theta);

  // now create a polynomial chaos approximation to the model
  Model PceSurr = make_shared<PceModel>(theta, f_of_theta, "Hermite", 1e-4);

  // make sure the approximation is accurate
  Eigen::VectorXd Input = 0.8 * Eigen::VectorXd::Ones(3);
  Eigen::VectorXd TrueOut(3), ApproxOut(3);
  Eigen::VectorXd ApproxGrad = Eigen::VectorXd::Zero(3);


  PceSurr.SetInput(theta);
  theta.Update(Input);

  f_of_theta.Eval(TrueOut);
  PceSurr.Eval(ApproxOut);

  Eigen::VectorXd sensIn(3);
  sensIn << 1, 0, 0;

  PceSurr.Grad(theta, sensIn, ApproxGrad);

  EXPECT_NEAR(TrueOut[0],    ApproxOut[0],   1e-5);
  EXPECT_NEAR(ApproxGrad(0), -1.0 * sin(.8), .01);
}
