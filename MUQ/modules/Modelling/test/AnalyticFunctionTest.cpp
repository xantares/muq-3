
#include <iostream>
#include <math.h>
#include <vector>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/ModGraphPiece.h"

using namespace muq::Modelling;
using namespace std;


TEST(ModellingAnalyticFunctions, SinTest)
{
  auto mod = make_shared<SinModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(sin(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, CosTest)
{
  auto mod = make_shared<CosModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(cos(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, CosOperatorTest)
{
  auto mod = Cos(make_shared<VectorPassthroughModel>(3));

  Eigen::VectorXd Input  = Eigen::VectorXd::Random(3);
  Eigen::VectorXd Output = ModGraphPiece::Create(mod)->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(cos(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, TanTest)
{
  auto mod = make_shared<TanModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(tan(Input[i]), Output[i]);
  }
}


TEST(ModellingAnalyticFunctions, ExpTest)
{
  auto mod = make_shared<ExpModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(exp(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, ExpOperatorTest)
{
  auto mod = Exp(make_shared<VectorPassthroughModel>(3));

  Eigen::VectorXd Input  = Eigen::VectorXd::Random(3);
  Eigen::VectorXd Output = ModGraphPiece::Create(mod)->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(exp(Input[i]), Output[i]);
  }
}


TEST(ModellingAnalyticFunctions, MultiOperatorTest)
{
  auto mod = Sin(Exp(Cos(make_shared<VectorPassthroughModel>(3))));

  Eigen::VectorXd Input  = Eigen::VectorXd::Random(3);
  Eigen::VectorXd Output = ModGraphPiece::Create(mod)->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(sin(exp(cos(Input[i]))), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, CscTest)
{
  auto mod = make_shared<CscModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(1.0 / sin(Input[i]), Output[i]);
  }
}


TEST(ModellingAnalyticFunctions, SecTest)
{
  auto mod = make_shared<SecModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(1.0 / cos(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, CotTest)
{
  auto mod = make_shared<CotModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(1.0 / tan(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, Pow10Test)
{
  auto mod = make_shared<Pow10Model>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(pow(10.0, Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, AbsTest)
{
  auto mod = make_shared<AbsModel>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(fabs(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, Pow2Test)
{
  auto mod = make_shared<Pow2Model>(3);

  Eigen::VectorXd Input = Eigen::VectorXd::Random(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(pow2(Input[i]), Output[i]);
  }
}

TEST(ModellingAnalyticFunctions, LogTest)
{
  auto mod = make_shared<LogModel>(3);

  Eigen::VectorXd Input = 3.0 * Eigen::VectorXd::Ones(3);

  Eigen::VectorXd Output = mod->Evaluate(Input);

  for (int i = 0; i < 3; ++i) {
    EXPECT_DOUBLE_EQ(log(Input[i]), Output[i]);
  }
}
