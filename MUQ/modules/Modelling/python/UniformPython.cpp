#include "MUQ/Modelling/python/UniformPython.h"


using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

UniformBoxPython::UniformBoxPython(boost::python::list const& lb,
                                   boost::python::list const& ub) : UniformBox(GetEigenVector<Eigen::VectorXd>(
                                                                                 lb),
                                                                               GetEigenVector<
                                                                                 Eigen::VectorXd>(ub))
{}
	
boost::python::list UniformBoxPython::PySample()
{
  return GetPythonVector<Eigen::VectorXd>(Sample());
}

double UniformBoxPython::PyPredicateFn(boost::python::list const& input)
{
  return PredicateFn(GetEigenVector<Eigen::VectorXd>(input));
}

double UniformBoxPython::PyDensity(boost::python::list const& input)
{
  return Density(GetEigenVector<Eigen::VectorXd>(input));
}

void muq::Modelling::ExportUniform()
{
  // expose specification
  boost::python::class_<UniformBoxPython, std::shared_ptr<UniformBoxPython>, boost::noncopyable> exportUniformBox(
    "UniformBox",
    boost::python::init<boost::python::list const&, boost::python::list const&>());

  // expose functions
  exportUniformBox.def("Sample", &UniformBoxPython::PySample);
  exportUniformBox.def("PredicateFn", &UniformBoxPython::PyPredicateFn);
  exportUniformBox.def("Density", &UniformBoxPython::PyDensity);

  // tell python about UniformSpecification
  boost::python::register_ptr_to_python<std::shared_ptr<UniformSpecification> >();

  // allow conversion to UniformSpecification
  boost::python::implicitly_convertible<std::shared_ptr<UniformBoxPython>, std::shared_ptr<UniformSpecification> >();

  // expose density
  boost::python::class_<UniformDensity, std::shared_ptr<UniformDensity>,
                        boost::python::bases<Density, ModPiece>, boost::noncopyable> exportUniformDens("UniformDensity",
                                                                                                       boost::python::init<std::shared_ptr<UniformSpecification> >());

  // convert to ModPiece and Density
  boost::python::implicitly_convertible<std::shared_ptr<UniformDensity>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<UniformDensity>, std::shared_ptr<Density> >();
}
