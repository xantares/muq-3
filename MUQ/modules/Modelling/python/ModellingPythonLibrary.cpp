#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Modelling/python/ModPiecePython.h"
#include "MUQ/Modelling/python/ModPieceTemplatesPython.h"
#include "MUQ/Modelling/python/VectorPassthroughModelPython.h"
#include "MUQ/Modelling/python/ModGraphPython.h"
#include "MUQ/Modelling/python/ModGraphPiecePython.h"
#include "MUQ/Modelling/python/RandVarPython.h"
#include "MUQ/Modelling/python/GaussianDensityPython.h"
#include "MUQ/Modelling/python/GaussianRVPython.h"
#include "MUQ/Modelling/python/GaussianPairPython.h"
#include "MUQ/Modelling/python/RosenbrockRVPython.h"
#include "MUQ/Modelling/python/RosenbrockDensityPython.h"
#include "MUQ/Modelling/python/RosenbrockPairPython.h"

#include "MUQ/Modelling/python/DensityPython.h"
#include "MUQ/Modelling/python/ModPieceDensityPython.h"
#include "MUQ/Modelling/python/OdeModPiecePython.h"
#include "MUQ/Modelling/python/LinearModelPython.h"
#include "MUQ/Modelling/python/DensityProductPython.h"
#include "MUQ/Modelling/python/EmpiricalRandVarPython.h"
#include "MUQ/Modelling/python/UniformPython.h"
#include "MUQ/Modelling/python/CachedModPiecePython.h"
#include "MUQ/Modelling/python/PointCachePython.h"
#include "MUQ/Modelling/python/ComponentwiseModelPython.h"
#include "MUQ/Modelling/python/SumModelPython.h"
#include "MUQ/Modelling/python/SliceModelPython.h"
#include "MUQ/Modelling/python/MultiplicationModPiecePython.h"
#include "MUQ/Modelling/ModGraphOperators.h"
#include "MUQ/Modelling/python/ConcatenateModelPython.h"
#include "MUQ/Modelling/python/ModParameterPython.h"

using namespace muq::Modelling;
using namespace boost::python;

// std::shared_ptr<ModGraph> PyAdd(std::shared_ptr<ModPiece> a, std::shared_ptr<ModPiece> b){
// return std::make_shared<ModGraph>();
	// return a+b;
	// return a.GetPointer()+b.GetPointer();	
// }
  BOOST_PYTHON_MODULE(libmuqModelling)
{
  // generic modelling ModPiece
  ExportModPiece();
  ExportModPieceTemplates();
  ExportVectorPassthroughModel();
  ExportModGraph();
  ExportModGraphPiece();
  ExportLinearModel();
  ExportCachedModPiece();
  ExportPointCache();
  ExportComponentwiseModel();
  ExportOdeModPiece();

  // probability densities
  ExportDensity();
  ExportModPieceDensity();
  ExportGaussianDensity();
  ExportDensityProduct();
  ExportUniform();
  ExportModGraphDensity();

  // pairs 
  ExportGaussianPair();

  // random variables
  ExportRandVar();
  ExportGaussianRV();
  ExportEmpiricalRandVar();
  ExportRosenbrockRV();
  ExportRosenbrockDensity();
  ExportRosenbrockPair();

  // sum model
  ExportSumModel();

  // slice model
  ExportSliceModel();

  // other models
  ExportMultiplicationModPiece();
  ExportModParameter();
  ExportConcatenateModel();
}
