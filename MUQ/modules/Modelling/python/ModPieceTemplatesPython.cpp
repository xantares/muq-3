#include "MUQ/Modelling/python/ModPieceTemplatesPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

OneInputNoDerivModPiecePython::OneInputNoDerivModPiecePython(int const inputSize, int const outputSize) : 
  ModPiecePython(GetPythonVector<Eigen::VectorXi>(inputSize*Eigen::VectorXi::Ones(1)),outputSize,false,false,false,false,false)
{
  return; 
}

Eigen::VectorXd OneInputNoDerivModPiecePython::EvaluateImpl(std::vector<Eigen::VectorXd> const& input) {
  boost::python::list inputPython = GetPythonVector<Eigen::VectorXd>(input[0]);
  return GetEigenVector<Eigen::VectorXd>(PyEvaluateImpl(inputPython));
}

boost::python::list OneInputNoDerivModPiecePython::PyGradientByFD(boost::python::list const& inputs,boost::python::list const& sensIn, int const inputDimWrt) {
  return PyGradientByFDOneInput(inputs,sensIn);
}

boost::python::list OneInputNoDerivModPiecePython::PyHessianByFD(boost::python::list const& inputs,boost::python::list const& sensIn, int const inputDimWrt) {
  return PyHessianByFDOneInput(inputs,sensIn);
}

boost::python::list OneInputNoDerivModPiecePython::PyGradientByFDOneInput(boost::python::list const& inputs,boost::python::list const& sensIn) {
  std::vector<Eigen::VectorXd> eigenInputs(1);
  eigenInputs[0] = GetEigenVector<Eigen::VectorXd>(inputs);
  assert((int)eigenInputs.size()==inputSizes.size());
  
  Eigen::VectorXd sens = GetEigenVector<Eigen::VectorXd>(sensIn);
  
  return GetPythonVector<Eigen::VectorXd>(GradientByFD(eigenInputs,sens,0));
}

boost::python::list OneInputNoDerivModPiecePython::PyHessianByFDOneInput(boost::python::list const& inputs,boost::python::list const& sensIn) {
  std::vector<Eigen::VectorXd> eigenInputs(1);
  eigenInputs[0] = GetEigenVector<Eigen::VectorXd>(inputs);
  assert((int)eigenInputs.size()==inputSizes.size());
  
  Eigen::VectorXd sens = GetEigenVector<Eigen::VectorXd>(sensIn);
  
  return GetPythonMatrix(HessianByFD(eigenInputs,sens,0));
}

OneInputAdjointModPiecePython::OneInputAdjointModPiecePython(int const inputSize, int const outputSize) : 
  ModPiecePython(GetPythonVector<Eigen::VectorXi>(inputSize*Eigen::VectorXi::Ones(1)),outputSize,true,false,false,false,false),
  OneInputNoDerivModPiecePython(inputSize,outputSize)
{
  return; 
}

Eigen::VectorXd OneInputAdjointModPiecePython::GradientImpl(std::vector<Eigen::VectorXd> const& input, Eigen::VectorXd const& sens, int const inputDimWrt) {
  boost::python::list inputPython = GetPythonVector<Eigen::VectorXd>(input[0]);
  boost::python::list sensPython = GetPythonVector<Eigen::VectorXd>(sens);
  
  return GetEigenVector<Eigen::VectorXd>(PyGradientImpl(inputPython,sensPython,inputDimWrt));
}

OneInputJacobianModPiecePython::OneInputJacobianModPiecePython(int const inputSize, int const outputSize) : 
  ModPiecePython(GetPythonVector<Eigen::VectorXi>(inputSize*Eigen::VectorXi::Ones(1)),outputSize,false,true,false,false,false),
  OneInputNoDerivModPiecePython(inputSize,outputSize)
{
  return;
}

Eigen::MatrixXd OneInputJacobianModPiecePython::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int inputDimWrt) {
  boost::python::list inputPython = GetPythonVector<Eigen::VectorXd>(input[0]);
  
  return GetEigenMatrix(PyJacobianImpl(inputPython,inputDimWrt));
}

OneInputAdjointJacobianModPiecePython::OneInputAdjointJacobianModPiecePython(int const inputSize, int const outputSize) : 
  ModPiecePython(GetPythonVector<Eigen::VectorXi>(inputSize*Eigen::VectorXi::Ones(1)),outputSize,true,true,true,false,false),
  OneInputNoDerivModPiecePython(inputSize,outputSize),
  OneInputAdjointModPiecePython(inputSize,outputSize),
  OneInputJacobianModPiecePython(inputSize,outputSize)
{
  return; 
}

Eigen::VectorXd OneInputAdjointJacobianModPiecePython::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input, Eigen::VectorXd const& target, int const inputDimWrt) {
  boost::python::list inputPython = GetPythonVector<Eigen::VectorXd>(input[0]);
  boost::python::list targetPython = GetPythonVector<Eigen::VectorXd>(target);

  return GetEigenVector<Eigen::VectorXd>(PyJacobianActionImpl(inputPython,targetPython,inputDimWrt));
}

boost::python::list OneInputAdjointJacobianModPiecePython::PyJacobianActionImplDefault(boost::python::list const& inputs,boost::python::list const& target,int const inputDimWrt) {
  return GetPythonVector<Eigen::VectorXd>(Jacobian(GetEigenVector<Eigen::VectorXd>(inputs),inputDimWrt)*GetEigenVector<Eigen::VectorXd>(target));
}

OneInputFullModPiecePython::OneInputFullModPiecePython(int const inputSize, int const outputSize) : 
  ModPiecePython(GetPythonVector<Eigen::VectorXi>(inputSize*Eigen::VectorXi::Ones(1)),outputSize,true,true,true,true,false),
  OneInputNoDerivModPiecePython(inputSize,outputSize),
  OneInputAdjointModPiecePython(inputSize,outputSize),
  OneInputJacobianModPiecePython(inputSize,outputSize),
  OneInputAdjointJacobianModPiecePython(inputSize,outputSize)
{
  return; 
}

Eigen::MatrixXd OneInputFullModPiecePython::HessianImpl(std::vector<Eigen::VectorXd> const& input, Eigen::VectorXd const& sens, int const inputDimWrt) {
  boost::python::list inputPython = GetPythonVector<Eigen::VectorXd>(input[0]);
  boost::python::list sensPython = GetPythonVector<Eigen::VectorXd>(sens);
  
  return GetEigenMatrix(PyHessianImpl(inputPython,sensPython,inputDimWrt));
}

void muq::Modelling::ExportModPieceTemplates()
{
  // one input no deriv
  boost::python::class_<OneInputNoDerivModPiecePython, std::shared_ptr<OneInputNoDerivModPiecePython>,
                        boost::python::bases<ModPiecePython>, boost::noncopyable> exportNoDeriv(
    "OneInputNoDerivModPiece",
    boost::python::init<int const,
                        int const>());

  // one input adjoint
  boost::python::class_<OneInputAdjointModPiecePython, std::shared_ptr<OneInputAdjointModPiecePython>,
                        boost::python::bases<OneInputNoDerivModPiecePython>, boost::noncopyable> exportAdjoint(
    "OneInputAdjointModPiece",
    boost::python::init<int const,
                        int const>());

  exportAdjoint.def("Gradient", &OneInputAdjointModPiecePython::PyGradientDefaultInputDim);

  // one input jacobian
  boost::python::class_<OneInputJacobianModPiecePython, std::shared_ptr<OneInputJacobianModPiecePython>,
                        boost::python::bases<OneInputNoDerivModPiecePython>, boost::noncopyable> exportJacobian(
    "OneInputJacobianModPiece",
    boost::python::init<int const, int const>());

  exportJacobian.def("Jacobian", &OneInputJacobianModPiecePython::PyJacobianDefaultInputDim);

  // one input adjoint jacobian
  boost::python::class_<OneInputAdjointJacobianModPiecePython, std::shared_ptr<OneInputAdjointJacobianModPiecePython>,
                        boost::python::bases<OneInputAdjointModPiecePython,
                                             OneInputJacobianModPiecePython>, boost::noncopyable> exportAdjointJacobian(
    "OneInputAdjointJacobianModPiece",
    boost::python::init<int const,
                        int const>());

  exportAdjointJacobian.def("JacobianAction", &OneInputAdjointJacobianModPiecePython::PyJacobianActionDefaultInputDim);

  // one input full
  boost::python::class_<OneInputFullModPiecePython, std::shared_ptr<OneInputFullModPiecePython>,
                        boost::python::bases<OneInputAdjointJacobianModPiecePython>, boost::noncopyable> exportFull(
    "OneInputFullModPiece",
    boost::python::init<int const, int const>());

  exportFull.def("Hessian", &OneInputFullModPiecePython::PyHessianDefaultInputDim);

  // allow conversion between one input classes and mod piece pointers
  boost::python::implicitly_convertible<std::shared_ptr<OneInputNoDerivModPiecePython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputAdjointModPiecePython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputJacobianModPiecePython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputAdjointJacobianModPiecePython>,
                                        std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputFullModPiecePython>, std::shared_ptr<ModPiece> >();
}
