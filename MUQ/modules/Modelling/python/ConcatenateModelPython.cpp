#include "MUQ/Modelling/python/ConcatenateModelPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

void muq::Modelling::ExportConcatenateModel() 
{
  py::class_<ConcatenateModel, shared_ptr<ConcatenateModel>, py::bases<ModPiece>, boost::noncopyable> exportConcat("ConcatenateModel", py::init<py::list const&>());

  py::implicitly_convertible<shared_ptr<ConcatenateModel>, shared_ptr<ModPiece> >();
}
