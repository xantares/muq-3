#include "MUQ/Modelling/python/GaussianRVPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

GaussianRV::GaussianRV(py::list const& mu, double const scaledCov) :
  GaussianRV(GetEigenVector<Eigen::VectorXd>(mu), scaledCov) {}

GaussianRV::GaussianRV(py::list const& mu, py::list const& diag) :
  GaussianRV(GetEigenVector<Eigen::VectorXd>(mu), GetEigenVector<Eigen::VectorXd>(diag)) {}

shared_ptr<GaussianRV> GaussianRV::PyCreate(py::list const& mu, py::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode)
{
  if ((mode == GaussianSpecification::DiagonalPrecision) || (mode == GaussianSpecification::DiagonalCovariance)) {
    return make_shared<GaussianRV>(GetEigenVector<Eigen::VectorXd>(mu), GetEigenVector<Eigen::VectorXd>(cov_or_prec), mode);
  }

  if ((mode == GaussianSpecification::PrecisionMatrix) || (mode == GaussianSpecification::CovarianceMatrix)) {
    return make_shared<GaussianRV>(GetEigenVector<Eigen::VectorXd>(mu), GetEigenMatrix(cov_or_prec), mode);
  }

  assert((mode == GaussianSpecification::DiagonalPrecision) ||
         (mode == GaussianSpecification::DiagonalCovariance) ||
         (mode == GaussianSpecification::CovarianceMatrix) ||
         (mode == GaussianSpecification::PrecisionMatrix));

  return nullptr;
}

shared_ptr<GaussianRV> GaussianRV::PyCreateConditional(unsigned int const dim, py::list const& mu, py::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode)
{
  if ((mode == GaussianSpecification::PrecisionMatrix) || (mode == GaussianSpecification::CovarianceMatrix)) {
    return make_shared<GaussianRV>(dim, GetEigenVector<Eigen::VectorXd>(mu), GetEigenMatrix(cov_or_prec), mode);
  }

  assert((mode == GaussianSpecification::CovarianceMatrix) ||
         (mode == GaussianSpecification::PrecisionMatrix));

  return nullptr;
}

py::list GaussianRV::PySampleOneSamp()
{
  return GetPythonVector<Eigen::VectorXd>(Sample());
}

py::list GaussianRV::PySampleOneSampInputs(py::list const& inputs)
{
  return GetPythonVector<Eigen::VectorXd>(Sample(PythonListToVector(inputs)));
}

py::list GaussianRV::PySample(unsigned int const N)
{
  return GetPythonMatrix(Sample(N));
}

py::list GaussianRV::PySampleInputs(py::list const& inputs, unsigned int const N)
{
  return GetPythonMatrix(Sample(PythonListToVector(inputs), N));
}

void muq::Modelling::ExportGaussianRV()
{
  py::class_<GaussianRV, shared_ptr<GaussianRV>, py::bases<RandVar, ModPiece>, boost::noncopyable> exportGaussianRV("GaussianRV", py::init<unsigned int const>());

  exportGaussianRV.def(py::init<unsigned int const, double const>());
  exportGaussianRV.def(py::init<py::list const&, double const>());
  exportGaussianRV.def(py::init<py::list const&, py::list const&>());
  exportGaussianRV.def("__init__", py::make_constructor(&GaussianRV::PyCreate));
  exportGaussianRV.def("__init__", py::make_constructor(&GaussianRV::PyCreateConditional));

  exportGaussianRV.def("Sample", &GaussianRV::PySampleOneSamp);
  exportGaussianRV.def("Sample", &GaussianRV::PySampleOneSampInputs);
  exportGaussianRV.def("Sample", &GaussianRV::PySample);
  exportGaussianRV.def("Sample", &GaussianRV::PySampleInputs);

  // convert to ModPiece and RandVar
  py::implicitly_convertible<shared_ptr<GaussianRV>, shared_ptr<ModPiece> >();
  py::implicitly_convertible<shared_ptr<GaussianRV>, shared_ptr<RandVar> >();
}

