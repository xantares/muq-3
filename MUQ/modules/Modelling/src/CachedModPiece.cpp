#include "MUQ/Modelling/CachedModPiece.h"
#include "MUQ/Utilities/LogConfig.h"
#include <boost/graph/graph_concepts.hpp>
#include "MUQ/Utilities/EigenUtils.h"
#include <cmath>

using namespace Eigen;
using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace std;

CachedModPiece::CachedModPiece(std::shared_ptr<ModPiece> sourceModPiece) : ModPiece(sourceModPiece->inputSizes,
                                                                                    sourceModPiece->outputSize,
                                                                                    sourceModPiece->hasDirectGradient,
                                                                                    sourceModPiece->hasDirectJacobian,
                                                                                    sourceModPiece->hasDirectJacobianAction,
                                                                                    sourceModPiece->hasDirectHessian,
                                                                                    sourceModPiece->isRandom),
                                                                           sourceModPiece(sourceModPiece),
                                                                           totalInputSize(inputSizes.array().sum())
{
  auto nnDataset = flann::Matrix<double>(nullptr, 0, totalInputSize);
  nnIndex =  std::make_shared<flann::Index<flann::L2<double> > >(nnDataset, flann::KDTreeSingleIndexParams());
}

CachedModPiece::CachedModPiece(Eigen::VectorXi const& inputSizes, int const outputSize) : 
  ModPiece(inputSizes, outputSize, true, true, true, true, false), // thre are no derivatives but the function should be overwritten
  sourceModPiece(nullptr),
  totalInputSize(inputSizes.array().sum())
{
  auto nnDataset = flann::Matrix<double>(nullptr, 0, totalInputSize);
  nnIndex =  std::make_shared<flann::Index<flann::L2<double> > >(nnDataset, flann::KDTreeSingleIndexParams());
}

CachedModPiece::~CachedModPiece()
{
  for (auto aPoint : nnData) {
    delete[] aPoint->ptr();
  }
}

void            CachedModPiece::LoadCache(std::string filename)       {

  Eigen::MatrixXd allPoints;
Eigen::MatrixXd allOutputs;

{
string pointName = filename + "points.dat";
  std::ifstream ifs(pointName.c_str());

  // save data to archive

    boost::archive::text_iarchive ia(ifs);

    // write class instance to archive
   ia >> allPoints;

    // archive and stream closed when destructors are called
  }

{
string pointName = filename + "outputs.dat";
  std::ifstream ifs(pointName.c_str());

  // save data to archive

    boost::archive::text_iarchive ia(ifs);

    // write class instance to archive
    ia >> allOutputs;

    // archive and stream closed when destructors are called
  }

for(int i=0; i<allOutputs.cols(); ++i)
{
auto cachedResult = FetchCachedResult(allPoints.col(i));
cachedResult->SetCachedEvaluate(allOutputs.col(i));
}



RefreshCache();
}

void            CachedModPiece::SaveCache(std::string filename)  {


RefreshCache();

  Eigen::MatrixXd allPoints(totalInputSize, GetNumOfEvals());
Eigen::MatrixXd allOutputs(outputSize, GetNumOfEvals());

  EvalCacheIter iter;
  int i = 0;

  for (iter = fnEvalCache.begin(); iter != fnEvalCache.end(); ++iter) {
    allPoints.col(i) = iter->first.point;
allOutputs.col(i) = *(iter->second->GetCachedEvaluate());
    ++i;
  }

{
string pointName = filename + "points.dat";
  std::ofstream ofs(pointName.c_str());

  // save data to archive

    boost::archive::text_oarchive oa(ofs);

    // write class instance to archive
    oa << allPoints;

    // archive and stream closed when destructors are called
  }

{
string pointName = filename + "outputs.dat";
  std::ofstream ofs(pointName.c_str());

  // save data to archive

    boost::archive::text_oarchive oa(ofs);

    // write class instance to archive
    oa << allOutputs;

    // archive and stream closed when destructors are called
  }





}

Eigen::MatrixXd CachedModPiece::EvaluateMultiImpl(std::vector<Eigen::MatrixXd> const& input)
{
  const int numCopies = input.at(0).cols();
  vector<Eigen::VectorXd> tempInput(inputSizes.size());
  
  vector<shared_ptr<ModPieceMultiStepCache>> cachedResults(numCopies);
  
  vector<int> existingCopies;
  vector<int> nonExistingCopies;
  
  // loop through all the copies and figure out which ones we have cached and which ones we haven't
  for(int copy=0; copy<numCopies; ++copy)
  {
    for(int i=0; i<inputSizes.size(); ++i)
      tempInput[i] = input[i].col(copy);
    
    cachedResults[copy] = FetchCachedResult(tempInput);
    if(cachedResults[copy]->GetCachedEvaluate()){
      existingCopies.push_back(copy);
    }else{
      nonExistingCopies.push_back(copy);
    }
  }
  
  // copy the cached values into the output
  Eigen::MatrixXd output(outputSize,numCopies);
  for(int copyNum : existingCopies)
    output.col(copyNum) = *(cachedResults[copyNum]->GetCachedEvaluate());
  
  
  // create a vector of copies that have not been evaluated
  vector<Eigen::MatrixXd> newPts(inputSizes.size());
  for(int i=0; i<inputSizes.size(); ++i)
  {
    newPts[i].resize(inputSizes[i],nonExistingCopies.size());
    int colInd = 0;
    for(int copyNum : nonExistingCopies)
    {
      newPts[i].col(colInd) = input[i].col(copyNum);
      colInd++;
    }
  }
  
  
  Eigen::MatrixXd newEvals = sourceModPiece->EvaluateMulti(newPts);
  for(int colInd=0; colInd<newEvals.cols(); ++colInd)
  {
    if (!std::isfinite(newEvals.col(colInd).sum())) { //throw away the whole result if the result is infinite
      LOG(INFO) << "Result is infinite, destroying cache";
      
      for(int i=0; i<inputSizes.size(); ++i)
        tempInput[i] = input[i].col(nonExistingCopies[colInd]);
      RemoveFromCache(tempInput);
      
    } else {
      cachedResults[nonExistingCopies[colInd]]->SetCachedEvaluate(newEvals.col(colInd));
    }
    output.col(nonExistingCopies[colInd]) = newEvals.col(colInd);

  }
  return output;
}

Eigen::VectorXd CachedModPiece::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  auto cachedResult = FetchCachedResult(input);

  if (cachedResult->GetCachedEvaluate()) {
    return *(cachedResult->GetCachedEvaluate());
  } else {
    VectorXd result = sourceModPiece->Evaluate(input);

    if (!std::isfinite(result.sum())) { //throw away the whole result if the result is infinite
      LOG(INFO) << "Result is infinite, destroying cache";
      RemoveFromCache(input);
    } else {
      cachedResult->SetCachedEvaluate(result);
    }
    return result;
  }
}

Eigen::VectorXd CachedModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & sensitivity,
                                             int const                           inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedGradient = cachedResult->GetCachedGradient(sensitivity, inputDimWrt);

  if (cachedGradient) {
    return *cachedGradient;
  } else {
    VectorXd result = sourceModPiece->Gradient(input, sensitivity, inputDimWrt);
    if (!std::isfinite(result.sum())) { //throw away the whole result if the derivative is infinite
      LOG(INFO) << "Gradient is infinite, destroying cache";
      RemoveFromCache(input);
    } else {
      cachedResult->AddCachedGradient(sensitivity, inputDimWrt, result);
    }
    return result;
  }
}

Eigen::MatrixXd CachedModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedJacobian = cachedResult->GetCachedJacobian(inputDimWrt);

  if (cachedJacobian) {
    return *cachedJacobian;
  } else {
    MatrixXd result = sourceModPiece->Jacobian(input, inputDimWrt);
    if (!std::isfinite(result.sum())) { //throw away the whole result if the derivative is infinite
      LOG(INFO) << "Jacobian is infinite, destroying cache";
      RemoveFromCache(input);
    } else {
      cachedResult->SetCachedJacobian(inputDimWrt, result);
    }
    return result;
  }
}

Eigen::VectorXd CachedModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                   Eigen::VectorXd const             & target,
                                                   int const                           inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedJacobianAction = cachedResult->GetCachedJacobianAction(target, inputDimWrt);

  if (cachedJacobianAction) {
    return *cachedJacobianAction;
  } else {
    VectorXd result = sourceModPiece->JacobianAction(input, target, inputDimWrt);
    if (!std::isfinite(result.sum())) { //throw away the whole result if the derivative is infinite
      LOG(INFO) << "JacobianAction is infinite, destroying cache";
      RemoveFromCache(input);
    } else {
      cachedResult->AddCachedJacobianAction(target, inputDimWrt, result);
    }
    return result;
  }
}

Eigen::MatrixXd CachedModPiece::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                            Eigen::VectorXd const             & sensitivity,
                                            int const                           inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedHessian = cachedResult->GetCachedHessian(sensitivity, inputDimWrt);

  if (cachedHessian) {
    return *cachedHessian;
  } else {
    Eigen::MatrixXd result = sourceModPiece->Hessian(input, sensitivity, inputDimWrt);
    if (!std::isfinite(result.sum())) { // throw away the whole result if the hessian is infinite
      LOG(INFO) << "Hessian is infinite, destroying cache";
      RemoveFromCache(input);
    } else {
      cachedResult->AddCachedHessian(sensitivity, inputDimWrt, result);
    }
    return result;
  }
}

std::shared_ptr<ModPieceMultiStepCache> CachedModPiece::FetchCachedResult(Eigen::VectorXd const& input)
{
  RefreshCache();
  assert(input.rows() == totalInputSize);
  PointWrapper pointWrapper(input);

  //check the cache for the point
  EvalCacheIter it = this->fnEvalCache.find(input);

  //if it's not there, make a new one
  if (it == this->fnEvalCache.end()) {
    auto idNN          = AddToNN(input); //get the id from adding it to the cache
    auto newCacheEntry = std::make_shared<ModPieceMultiStepCache>(SeparateInputs(input), idNN);
    fnEvalCache[pointWrapper] = newCacheEntry;

    return newCacheEntry;
  }

  //return it
  return it->second;
}

std::shared_ptr<ModPieceMultiStepCache> CachedModPiece::FetchCachedResult(std::vector<Eigen::VectorXd> const& input)
{
  RefreshCache();

  VectorXd combinedInput = CombineInputs(input);

  return FetchCachedResult(combinedInput);
}

void CachedModPiece::RemoveFromCache(std::vector<Eigen::VectorXd> const& input)
{
  RefreshCache();

  VectorXd combinedInput = CombineInputs(input);

  assert(combinedInput.rows() == totalInputSize);
  PointWrapper pointWrapper(combinedInput);

  //check the cache for the point
  EvalCacheIter iterToRemove = this->fnEvalCache.find(pointWrapper);

  //must be here, else why/how are we deleting it?
  if(iterToRemove != this->fnEvalCache.end())
{
  RemoveFromNN(iterToRemove->second->idNN); //remove the id from the NN index

  fnEvalCache.erase(iterToRemove);

  ++removedCachePoints;
}
}

bool CachedModPiece::IsEntryInCache(std::vector<Eigen::VectorXd> const& input)
{
  RefreshCache();

  VectorXd pointToTest = CombineInputs(input);
  PointWrapper point(pointToTest);

  //check the cache for the point
  EvalCacheIter it = this->fnEvalCache.find(point);

  return it != this->fnEvalCache.end();
}

bool CachedModPiece::IsEntryInCache(Eigen::VectorXd const& pointToTest)
{
  RefreshCache();

  assert(totalInputSize == pointToTest.rows());
  PointWrapper point(pointToTest);

  //check the cache for the point
  EvalCacheIter it = this->fnEvalCache.find(point);

  return it != this->fnEvalCache.end();
}

int CachedModPiece::CostOfEvaluations(Eigen::MatrixXd const& x)
{
  RefreshCache();

  assert(totalInputSize == x.rows());
  int result = 0;
  for (int i = 0; i < x.cols(); ++i) {
    if (!IsEntryInCache(x.col(i))) {
      ++result;
    }
  }
  return result;
}

Eigen::MatrixXd CachedModPiece::FindKNearestNeighbors(VectorXd const& input, unsigned const countToFind)
{
  RefreshCache();

      assert(totalInputSize == input.rows());
      assert(countToFind > 0);                //trying to find 0 will crash FLANN

      assert(countToFind <= GetNumOfEvals()); //check there's enough to find
  unsigned int dim = input.rows();


  flann::Matrix<double> query(new double[dim], 1, dim);
  for (unsigned int i = 0; i < dim; ++i) {
    query[0][i] = input(i);
  }


  std::vector<std::vector<int> > indices;
  std::vector<std::vector<double> > dists;

  // do a knn search, using 128 checks
  nnIndex->knnSearch(query, indices, dists, countToFind, flann::SearchParams(flann::FLANN_CHECKS_UNLIMITED));

  //find out how many neighbors were found
  MatrixXd neighbors = MatrixXd::Zero(dim, indices.at(0).size());
  for (unsigned int i = 0; i < neighbors.cols(); ++i) {
    double *pointRef = nnIndex->getPoint(indices.at(0).at(i));

    for (unsigned int j = 0; j < dim; ++j) {
      assert(!std::isnan(pointRef[j]));

      //copy over the i-th index from the dataset one dimension (j) at a time
      neighbors(j, i) = pointRef[j];
    }
  }
  assert(neighbors.cols() == countToFind); //check that it found as many as desired

  delete[] query.ptr();


  return neighbors;
}

Eigen::MatrixXd CachedModPiece::FindNeighborsWithinRadius(VectorXd const& input, double const radius)
{
  RefreshCache();

      assert(totalInputSize == input.rows());

  unsigned int dim = input.rows();

  //create a query
  flann::Matrix<double> query(new double[dim], 1, dim);
  for (unsigned int i = 0; i < dim; ++i) {
    query[0][i] = input(i);
  }


  std::vector<std::vector<int> > indices;
  std::vector<std::vector<double> > dists;

  // do a knn search, using unlimited checks
  //NOTE that radius search for uses squared distance with the built-in L2 functor.
  int numFound =
    nnIndex->radiusSearch(query, indices, dists, radius * radius, flann::SearchParams(flann::FLANN_CHECKS_UNLIMITED));

  //find out how many neighbors were found
  MatrixXd neighbors = MatrixXd::Zero(dim, numFound);

  for (unsigned int i = 0; i < neighbors.cols(); ++i) {
    double *pointRef = nnIndex->getPoint(indices.at(0).at(i));

    for (unsigned int j = 0; j < dim; ++j) {
      assert(!std::isnan(pointRef[j]));

      //copy over the i-th index from the dataset one dimension (j) at a time
      neighbors(j, i) = pointRef[j];
    }
  }

  delete[] query.ptr();

  return neighbors.leftCols(neighbors.cols());
}

std::list < std::shared_ptr < ModPieceMultiStepCache >> CachedModPiece::FindKNearestNeighborsCacheEntries(
  Eigen::VectorXd const & input,
  unsigned const countToFind)
{
  RefreshCache();

  std::list < std::shared_ptr < ModPieceMultiStepCache >> result;

  MatrixXd points = FindKNearestNeighbors(input, countToFind);

  for (int i = 0; i < points.cols(); ++i) {
    result.push_back(FetchCachedResult(points.col(i)));
  }

  return result;
}

std::list < std::shared_ptr < ModPieceMultiStepCache >> CachedModPiece::FindNeighborsWithinRadiusCacheEntries(
  Eigen::VectorXd const & input,
  double const radius)
{
  RefreshCache();

  std::list < std::shared_ptr < ModPieceMultiStepCache >> result;

  MatrixXd points = FindNeighborsWithinRadius(input, radius);

  for (int i = 0; i < points.cols(); ++i) {
    result.push_back(FetchCachedResult(points.col(i)));
  }

  return result;
}

Eigen::MatrixXd CachedModPiece::FindKNearestNeighbors(std::vector<Eigen::VectorXd> const& input,
                                                      unsigned const                      countToFind)
{
  RefreshCache();

  return FindKNearestNeighbors(CombineInputs(input), countToFind);
}

Eigen::MatrixXd CachedModPiece::FindNeighborsWithinRadius(std::vector<Eigen::VectorXd> const& input,
                                                          double const                        radius)
{
  RefreshCache();

  return FindNeighborsWithinRadius(CombineInputs(input), radius);
}

std::list < std::shared_ptr < ModPieceMultiStepCache >>
CachedModPiece::FindKNearestNeighborsCacheEntries(std::vector<Eigen::VectorXd> const & input,
                                                  unsigned const countToFind)
{
  RefreshCache();

  return FindKNearestNeighborsCacheEntries(CombineInputs(input), countToFind);
}

std::list < std::shared_ptr < ModPieceMultiStepCache >> CachedModPiece::FindNeighborsWithinRadiusCacheEntries(
  std::vector<Eigen::VectorXd> const & input,
  double const radius)
{
  RefreshCache();

  return FindNeighborsWithinRadiusCacheEntries(CombineInputs(input), radius);
}

bool HasNoEvaluate(std::shared_ptr<ModPieceMultiStepCache> input)
{
  return !input->GetCachedEvaluate();
}

///Check for 0th input jacobian.
bool HasNoJacobian(std::shared_ptr<ModPieceMultiStepCache> input)
{
  return !input->GetCachedJacobian(0);
}

std::list < std::shared_ptr < ModPieceMultiStepCache >> CachedModPiece::FindKNearestNeighborsWithEvaluations(
  Eigen::VectorXd const & input,
  unsigned const countToFind)
{
  RefreshCache();

  MatrixXd result(outputSize, countToFind);
  unsigned found             = 0;
  unsigned actualCountToFind = countToFind;
  std::list < std::shared_ptr < ModPieceMultiStepCache >> cachedEntries;

  while (found < countToFind) {
    //find neighbors
    cachedEntries = FindKNearestNeighborsCacheEntries(input, actualCountToFind);
    cachedEntries.remove_if(HasNoEvaluate);
    found              = cachedEntries.size();
    actualCountToFind += countToFind - found;
  }

  return cachedEntries;
}

std::list < std::shared_ptr < ModPieceMultiStepCache >> CachedModPiece::FindKNearestNeighborsWithEvalJac(
  Eigen::VectorXd const & input,
  unsigned const countToFind)
{
  RefreshCache();

  MatrixXd result(outputSize, countToFind);
  unsigned found             = 0;
  unsigned actualCountToFind = countToFind;
  std::list < std::shared_ptr < ModPieceMultiStepCache >> cachedEntries;

  while (found < countToFind) {
    //find neighbors
    cachedEntries = FindKNearestNeighborsCacheEntries(input, actualCountToFind);
    cachedEntries.remove_if(HasNoEvaluate);
    cachedEntries.remove_if(HasNoJacobian);
    found              = cachedEntries.size();
    actualCountToFind += countToFind - found;
  }

  return cachedEntries;
}


Eigen::VectorXd CachedModPiece::CombineInputs(std::vector<Eigen::VectorXd> const& input) const
{
  CheckInputs(input);
  VectorXd combined(totalInputSize);

  int currentSpot = 0;
  for (unsigned i = 0; i < input.size(); ++i) {
    combined.segment(currentSpot, input.at(i).rows()) = input.at(i);
    currentSpot                                      += input.at(i).rows();
  }

  return combined;
}

std::vector<Eigen::VectorXd> CachedModPiece::SeparateInputs(Eigen::VectorXd  const& input) const
{
  std::vector<Eigen::VectorXd> separatedInputs(inputSizes.size());

  int currentSpot = 0;

  for (int i = 0; i < inputSizes.size(); ++i) {
    separatedInputs.at(i) = input.segment(currentSpot, inputSizes(i));
    currentSpot          += inputSizes(i);
  }

  return separatedInputs;
}

unsigned int CachedModPiece::GetNumOfEvals()
{
  RefreshCache();

  return fnEvalCache.size();
}

Eigen::MatrixXd CachedModPiece::GetCachePoints()
{
  RefreshCache();

  MatrixXd allPoints(totalInputSize, GetNumOfEvals());

  EvalCacheIter iter;
  int i = 0;

  for (iter = fnEvalCache.begin(); iter != fnEvalCache.end(); ++iter) {
    allPoints.col(i) = iter->first.point;
    ++i;
  }

  return allPoints;
}

int CachedModPiece::AddToNN(Eigen::VectorXd const& newPoints)
{
  //and add to the nearest neighbor structure
  unsigned int numSamples = 1;
  unsigned int dim        = newPoints.rows();

  auto pointsToAdd = std::make_shared < flann::Matrix < double >> (new double[dim], numSamples, dim);


  //copy the cache out into a flann matrix
  for (unsigned int i = 0; i < dim; ++i) {
    (*pointsToAdd)[0][i] = newPoints(i);
  }
  nnIndex->addPoints(*pointsToAdd);

  nnData.push_front(pointsToAdd);

  return lastIdNN++;
}

void CachedModPiece::RemoveFromNN(int const idNN)
{
  nnIndex->removePoint(idNN);
}

// *********************************************************************
// Multi step cache routines
// *********************************************************************


ModPieceMultiStepCache::ModPieceMultiStepCache(std::vector<Eigen::VectorXd> const& input, int const idNN) : cachedInput(
                                                                                                              input),
                                                                                                            idNN(idNN)
{}

boost::optional<Eigen::VectorXd> ModPieceMultiStepCache::GetCachedEvaluate() const
{
  return cachedEvaluate;
}

void ModPieceMultiStepCache::SetCachedEvaluate(Eigen::VectorXd const& result)
{
  cachedEvaluate = boost::optional<Eigen::VectorXd>(result);
}

boost::optional<Eigen::VectorXd> ModPieceMultiStepCache::GetCachedGradient(Eigen::VectorXd const& sensitivity,
                                                                           int                    inputDimWrt) const
{
  //check if there's any stored for the inputDimWrt
  auto outerIter = gradientMap.find(inputDimWrt);

  if (outerIter == gradientMap.end()) {
    return boost::optional<Eigen::VectorXd>();
  }

  //it is, so check for this sensitivity
  auto innerIter = outerIter->second.find(sensitivity);

  //return it if it's there
  if (innerIter !=  outerIter->second.end()) {
    return innerIter->second;
  }

  //else signal that it's not
  return boost::optional<Eigen::VectorXd>();
}

void ModPieceMultiStepCache::AddCachedGradient(Eigen::VectorXd const& sensitivity,
                                               int                    inputDimWrt,
                                               Eigen::VectorXd const& gradient)
{
  gradientMap[inputDimWrt][PointWrapper(sensitivity)] = gradient;
}

boost::optional<Eigen::MatrixXd> ModPieceMultiStepCache::GetCachedJacobian(int inputDimWrt) const
{
  if (jacobianMap.find(inputDimWrt) != jacobianMap.end()) {
    return boost::optional<MatrixXd>(jacobianMap.at(inputDimWrt));
  }

  return boost::optional<MatrixXd>();
}

void ModPieceMultiStepCache::SetCachedJacobian(int inputDimWrt, Eigen::MatrixXd const& jacobian)
{
  assert(jacobianMap.find(inputDimWrt) == jacobianMap.end()); //should not be overwriting
  jacobianMap[inputDimWrt] = jacobian;
}

boost::optional<Eigen::VectorXd> ModPieceMultiStepCache::GetCachedJacobianAction(Eigen::VectorXd const& target,
                                                                                 int                    inputDimWrt)
const
{
  //check if there's any stored for the inputDimWrt
  auto outerIter = jacobianActionMap.find(inputDimWrt);

  if (outerIter == jacobianActionMap.end()) {
    return boost::optional<Eigen::VectorXd>();
  }

  //it is, so check for this sensitivity
  auto innerIter = outerIter->second.find(target);

  //return it if it's there
  if (innerIter !=  outerIter->second.end()) {
    return innerIter->second;
  }

  //else signal that it's not
  return boost::optional<Eigen::VectorXd>();
}

void ModPieceMultiStepCache::AddCachedJacobianAction(Eigen::VectorXd const& target,
                                                     int                    inputDimWrt,
                                                     Eigen::VectorXd const& action)
{
  jacobianActionMap[inputDimWrt][PointWrapper(target)] = action;
}

boost::optional<Eigen::MatrixXd> ModPieceMultiStepCache::GetCachedHessian(Eigen::VectorXd const& sensitivity,
                                                                          int const              inputDimWrt) const
{
  //check if there's any stored for the inputDimWrt
  auto outerIter = hessianMap.find(inputDimWrt);

  if (outerIter == hessianMap.end()) {
    return boost::optional<Eigen::MatrixXd>();
  }

  //it is, so check for this sensitivity
  auto innerIter = outerIter->second.find(sensitivity);

  //return it if it's there
  if (innerIter !=  outerIter->second.end()) {
    return innerIter->second;
  }

  //else signal that it's not
  return boost::optional<Eigen::MatrixXd>();
}

void ModPieceMultiStepCache::AddCachedHessian(Eigen::VectorXd const& sensitivity,
                                              int const              inputDimWrt,
                                              Eigen::MatrixXd const& hessian)
{
  hessianMap[inputDimWrt][PointWrapper(sensitivity)] = hessian;
}

