
#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"

using namespace muq::Modelling;


double muq::Modelling::sign(double val)
{
  return (val > 0.0) - (val < 0.0);
}

double muq::Modelling::pow2(double val)
{
  return pow(2, val);
}

ComponentwiseModel::ComponentwiseModel(int dim) : OneInputFullModPiece(dim, dim) {}


Eigen::VectorXd ComponentwiseModel::EvaluateImpl(Eigen::VectorXd const& input)
{
  Eigen::VectorXd Output(outputSize);

  for (int i = 0; i < outputSize; ++i) {
    Output[i] = BaseFunc(input[i]);
  }
  return Output;
}

Eigen::VectorXd ComponentwiseModel::GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
{
  Eigen::VectorXd GradVec(outputSize);

  for (int i = 0; i < outputSize; ++i) {
    GradVec[i] = sensitivity[i] * BaseDeriv(input[i]);
  }

  return GradVec;
}

Eigen::MatrixXd ComponentwiseModel::JacobianImpl(Eigen::VectorXd const& input)
{
  Eigen::MatrixXd jac = Eigen::MatrixXd::Zero(outputSize, outputSize);

  for (int i = 0; i < outputSize; ++i) {
    jac(i, i) = BaseDeriv(input[i]);
  }

  return jac;
}

Eigen::VectorXd ComponentwiseModel::JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target)
{
  Eigen::VectorXd Output(outputSize);

  for (int i = 0; i < outputSize; ++i) {
    Output[i] = target[i] * BaseDeriv(input[i]);
  }

  return Output;
}

Eigen::MatrixXd ComponentwiseModel::HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
{
  Eigen::MatrixXd Output = Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);

  for (int i = 0; i < inputSizes[0]; ++i) {
    Output(i, i) = sensitivity[i] * BaseSecondDeriv(input[i]);
  }

  return Output;
}

