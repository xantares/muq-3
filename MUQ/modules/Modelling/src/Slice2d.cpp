
#include "MUQ/Modelling/Slice2d.h"

using namespace muq::Modelling;
using namespace std;


/** Construct from a start index, end index, and skip interval.  The start index and end index are inclusive.
 */
Slice2dModel::Slice2dModel(int inputSize, int StartIndInX, int EndIndInX, int StartIndInY, int EndIndInY,
                           int LDAIN) : OneInputFullModPiece(inputSize,
                                                             floor(
                                                               (EndIndInX - StartIndInX) * (EndIndInY - StartIndInY))),
StartIndX(
                                          StartIndInX), StartIndY(StartIndInY), EndIndX(EndIndInX), EndIndY(EndIndInY),
                                        LDA(LDAIN)
{}

/** Fill in the update function -- this function just copies values from the input to this expressions state. */
Eigen::VectorXd Slice2dModel::EvaluateImpl(const Eigen::VectorXd& InputVec)
{
  Eigen::VectorXd out(outputSize);

  // update the state
  int it = 0;

  for (int j = StartIndY; j < EndIndY; ++j) {
    for (int i = StartIndX; i < EndIndX; ++i) {
      out[it] = InputVec[i + j * LDA];
      ++it;
    }
  }
  return out;
}

/** This funtion just stamps the SensIn vector into a longer vector for the input size, and returns the results in the
 *  GradVec vector.
 */
Eigen::VectorXd Slice2dModel::GradientImpl(const Eigen::VectorXd& Loc, const Eigen::VectorXd& SensIn)
{
  Eigen::VectorXd GradVec(inputSizes[0]);

  // fill in the Gradient vector
  int it = 0;

  for (int j = StartIndY; j < EndIndY; ++j) {
    for (int i = StartIndX; i < EndIndX; ++i) {
      GradVec[i + j * LDA] = SensIn[it];
      ++it;
    }
  }
  return GradVec;
}

Eigen::MatrixXd Slice2dModel::JacobianImpl(const Eigen::VectorXd& Loc)
{
  Eigen::MatrixXd Jac(outputSize, inputSizes[0]);

  // fill in the Gradient vector
  int it = 0;

  for (int j = StartIndY; j < EndIndY; ++j) {
    for (int i = StartIndX; i < EndIndX; ++i) {
      Jac(it, i + j * LDA) = 1.0;
    }
  }
  return Jac;
}

Eigen::VectorXd Slice2dModel::JacobianActionImpl(const Eigen::VectorXd& Loc, const Eigen::VectorXd& target)
{
  return Evaluate(target);
}

Eigen::MatrixXd Slice2dModel::HessianImpl(const Eigen::VectorXd& Loc, const Eigen::VectorXd& SensIn)
{
  return Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);
}
