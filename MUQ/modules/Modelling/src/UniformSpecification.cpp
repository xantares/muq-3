
#include "MUQ/Modelling/UniformSpecification.h"
#include <boost/concept_check.hpp>

#include <assert.h>

#include "MUQ/Utilities/RandomGenerator.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;


UniformBox::UniformBox(Eigen::VectorXd const& lb,
                       Eigen::VectorXd const& ub) : UniformSpecification(lb.rows()), lb(lb), ub(ub), scale(ub - lb)
{
  assert(lb.rows() == ub.rows());

  assert((lb.array() <= ub.array()).all());
}

Eigen::VectorXd UniformBox::Sample()
{
  auto randomness = RandomGenerator::GetUniformRandomVector(dim);

  return lb + (scale.array() * randomness.array()).matrix();
}

double UniformBox::PredicateFn(Eigen::VectorXd const& input)
{
  return static_cast<double>((((input - lb).minCoeff() >= 0) && ((ub - input).minCoeff() >= 0)));
}

