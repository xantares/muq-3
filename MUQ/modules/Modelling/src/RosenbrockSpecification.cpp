#include "MUQ/Modelling/RosenbrockSpecification.h"

#include "MUQ/Modelling/ModParameter.h"

using namespace std;
using namespace muq::Modelling;

RosenbrockSpecification::RosenbrockSpecification(double const a, double const b) : 
  Specification(Eigen::VectorXi(), ConstantParametersGraph(a, b), MakeParameterNames("a", "b")) {}

shared_ptr<ModGraph> RosenbrockSpecification::ConstantParametersGraph(double const a, double const b)
{
  auto graph = make_shared<ModGraph>();

  // add parameter for "a"
  graph->AddNode(make_shared<ModParameter>(a * Eigen::VectorXd::Ones(1)), "a");

  // add parameter for "b"
  graph->AddNode(make_shared<ModParameter>(b * Eigen::VectorXd::Ones(1)), "b");

  return graph;
}

vector<string> RosenbrockSpecification::MakeParameterNames(string const a, string const b)
{
  vector<string> names(2);
  names[0] = a;
  names[1] = b;

  return names;
}

double RosenbrockSpecification::a(std::vector<Eigen::VectorXd> const& inputs)
{
  vector<unsigned int> indices = paraMods[names[0]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for( unsigned int i=0; i<indices.size(); ++i ) {
    ins[i] = inputs[indices[i]];
  }

  const Eigen::VectorXd a = paraMods[names[0]].first->Evaluate(ins);
  assert(a.size() == 1);
  assert(a(0) > 0.0);

  return a(0);
}

double RosenbrockSpecification::b(std::vector<Eigen::VectorXd> const& inputs)
{
  vector<unsigned int> indices = paraMods[names[1]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for( unsigned int i=0; i<indices.size(); ++i ) {
    ins[i] = inputs[indices[i]];
  }

  const Eigen::VectorXd b = paraMods[names[1]].first->Evaluate(ins);
  assert(b.size() == 1);

  return b(0);
}
