
#include "MUQ/config.h"

// self include
#include "MUQ/Modelling/ModParameter.h"

// other muq-related includes
#include "MUQ/Utilities/VectorTranslater.h"


// namespace stuff
using namespace muq::Modelling;
using namespace muq::utilities;


/** Create an empty parameter -- empty state. */
ModParameter::ModParameter() {}

/** Set the parameter to a constant vector with specified dimension. */
ModParameter::ModParameter(int dim, double val)
{
  // set the input/output dimensions
  DimIn[0] = dim;
  DimOut   = dim;

  // set the state
  State = val * Eigen::VectorXd::Ones(dim);
}

ModParameter::ModParameter(const double *& InputVec, int dim)
{
  // set the state
  Translater<double *, Eigen::VectorXd> Trans((double *)InputVec, dim);
  State = *Trans.GetPtr();

  // set the input/output dimensions
  DimIn[0] = dim;
  DimOut   = dim;
}

/** Update the state.  The ModParameter class works like an identity when this function is called.  So as long as the
 *  dimensions agree, whatever vector is in InputVec0 becomes the state of this parameter.  Note that is also possible
 * to
 *  set the parameter from other vector types, but these functions are implemented in the ModPiece class.*/
void ModParameter::UpdateBase(const Eigen::VectorXd& InputVec0)
{
  assert(DimIn[0] == InputVec0);
  State = InputVec0;
}

void ModParameter::GradBase(const Eigen::VectorXd& Loc0,
                            const Eigen::VectorXd& SensIn,
                            Eigen::VectorXd      & GradVec,
                            int                    dim)
{
  assert(DimIn[0] == GradVec.size());
  assert(DimIn[0] == SensIn.size());
  assert(DimIn[0] == Loc0.size());

  GradVec = SensIn;
}

