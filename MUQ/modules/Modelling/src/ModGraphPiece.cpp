
#include "MUQ/Modelling/ModGraphPiece.h"

// std library includes
#include <sstream>
#include <deque>
#include <iomanip>

// boost includes
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/topological_sort.hpp>

// other muq includes
#include "MUQ/Modelling/ModParameter.h"

using namespace muq::Modelling;
using namespace std;

bool isFirst(const std::string& n1, const std::string& n2, int i1, int i2, const std::vector<std::string>& order)
{
  // first, find which string is first in the vector
  auto ind1 = std::find(order.begin(), order.end(), n1);
  auto ind2 = std::find(order.begin(), order.end(), n2);

  if ((ind2 - ind1) > 0) {
    return true;
  } else if (ind2 == ind1) {
    return i1 < i2;
  } else {
    return false;
  }
}

std::shared_ptr<ModGraphPiece> ModGraphPiece::Create(std::shared_ptr<ModGraph>       inGraph,
                                                     const std::string             & outNode,
                                                     const std::vector<std::string>& inputOrder)
{
	string outNode_toUse;
	if(outNode.compare("") == 0){
		outNode_toUse = inGraph->GetOutputNodeName();
	}
	else{
		outNode_toUse = outNode;
	}
	
  // first, trim the constant and extraneous branches from inGraph into a new graph
  std::shared_ptr<ModGraph> newGraph = inGraph->DependentCut(outNode_toUse);

  // now, newGraph contains some hanging input edges.  We will fill those with ModParameter instances, and store
  // seperate pointers
  std::vector<std::pair<boost::graph_traits<Graph>::vertex_descriptor, int> > inputPts = newGraph->GraphInputs();
  std::vector<std::shared_ptr<ModParameter> > inputNodes(inputPts.size());

  // build the input node names
  std::vector<std::string> inNodeNames(inputPts.size());

  for (unsigned int i = 0; i < inputPts.size(); ++i) {
    stringstream temp;
    temp << newGraph->ModelGraph[inputPts[i].first]->name << "_";
    temp << std::setfill('0') << std::setw(3);
    temp << i;
    inNodeNames[i] = temp.str();
  }

  // sort the inputs according to the names vector according to alphanumeric order
  std::vector<int> idx(inNodeNames.size());
  for (unsigned int i = 0; i < inNodeNames.size(); ++i) {
    idx[i] = i;
  }

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(), [&newGraph, &inputPts, &inputOrder](int i1, int i2)         {
              return isFirst(newGraph->ModelGraph[inputPts[i1].first]->name, newGraph->ModelGraph[
                               inputPts[i2].first]->name, i1, i2, inputOrder);
            });

  // store the size of each input
  Eigen::VectorXi inSizes(inputPts.size());

  // loop over input pts, adding nodes to graph for each input
  for (unsigned int i = 0; i < inputPts.size(); ++i) {
    inSizes[i] = newGraph->ModelGraph[inputPts[idx[i]].first]->piece->inputSizes[inputPts[idx[i]].second];

    inputNodes[i] = make_shared<ModParameter>(inSizes[i], 0.0);
    newGraph->AddNode(inputNodes[i], inNodeNames[idx[i]]);
    newGraph->AddEdge(inNodeNames[idx[i]], newGraph->ModelGraph[inputPts[idx[i]].first]->name, inputPts[idx[i]].second);
  }

  //for some reason I can't used make_shared here, I get a compiler internal error
  return shared_ptr<ModGraphPiece>(new ModGraphPiece(newGraph, *newGraph->GetNodeIterator(outNode_toUse), inputNodes,
                                                     inSizes));
}

std::shared_ptr<ModGraphPiece> ModGraphPiece::Create(std::shared_ptr<ModGraph> inGraph, const std::string& outNode)
{

	string outNode_toUse;
	if(outNode.compare("") == 0){
		outNode_toUse = inGraph->GetOutputNodeName();
	}
	else{
		outNode_toUse = outNode;
	}
	
	
  // first, trim the constant and extraneous branches from inGraph into a new graph
  std::shared_ptr<ModGraph> newGraph = inGraph->DependentCut(outNode_toUse);

  // now, newGraph contains some hanging input edges.  We will fill those with ModParameter instances, and store
  // seperate pointers
  std::vector<std::pair<boost::graph_traits<Graph>::vertex_descriptor, int> > inputPts = newGraph->GraphInputs();
  std::vector<std::shared_ptr<ModParameter> > inputNodes(inputPts.size());

  // build the input node names
  std::vector<std::string> inNodeNames(inputPts.size());

  for (unsigned int i = 0; i < inputPts.size(); ++i) {
    stringstream temp;
    temp << newGraph->ModelGraph[inputPts[i].first]->name << "_";
    temp << std::setfill('0') << std::setw(3);
    temp << inputPts[i].second;
    inNodeNames[i] = temp.str();
  }

  // sort the inputs according to the names vector according to alphanumeric order
  std::vector<int> idx(inNodeNames.size());
  for (unsigned int i = 0; i < inNodeNames.size(); ++i) {
    idx[i] = i;
  }

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(), [&inNodeNames](int i1, int i2) { return inNodeNames[i1] < inNodeNames[i2]; });

  // store the size of each input
  Eigen::VectorXi inSizes(inputPts.size());

  // loop over input pts, adding nodes to graph for each input
  for (unsigned int i = 0; i < inputPts.size(); ++i) {
    inSizes[i] = newGraph->ModelGraph[inputPts[idx[i]].first]->piece->inputSizes[inputPts[idx[i]].second];

    inputNodes[i] = make_shared<ModParameter>(inSizes[i], 0.0);
    newGraph->AddNode(inputNodes[i], inNodeNames[idx[i]]);
    newGraph->AddEdge(inNodeNames[idx[i]], newGraph->ModelGraph[inputPts[idx[i]].first]->name, inputPts[idx[i]].second);
  }

  //for some reason I can't used make_shared here, I get a compiler internal error
  return shared_ptr<ModGraphPiece>(new ModGraphPiece(newGraph, *(newGraph->GetNodeIterator(outNode_toUse)), inputNodes,
                                                     inSizes));
}

std::shared_ptr<ModGraphDensity> ModGraphDensity::Create(std::shared_ptr<ModGraph> inGraph, const std::string& outNode)
{
  //call the raw method, which does all the heavy lifting
  std::shared_ptr<ModGraphPiece> rawPiece = ModGraphPiece::Create(inGraph, outNode);

  assert(!rawPiece->isRandom);
  assert(rawPiece->outputSize == 1);

  //for some reason I can't used make_shared here, I get a compiler internal error
  return shared_ptr<ModGraphDensity>(new ModGraphDensity(rawPiece));
}

std::shared_ptr<ModGraphDensity> ModGraphDensity::Create(std::shared_ptr<ModGraph>       inGraph,
                                                         const std::string             & outNode,
                                                         std::vector<std::string> const& order)
{
  //call the raw method, which does all the heavy lifting
  std::shared_ptr<ModGraphPiece> rawPiece = ModGraphPiece::Create(inGraph, outNode, order);

  assert(!rawPiece->isRandom);
  assert(rawPiece->outputSize == 1);

  //for some reason I can't used make_shared here, I get a compiler internal error
  return shared_ptr<ModGraphDensity>(new ModGraphDensity(rawPiece));
}

ModGraphDensity::ModGraphDensity(std::shared_ptr<ModGraphPiece> genericPiece) : Density(genericPiece->inputSizes,
                                                                                        genericPiece->hasDirectGradient,
                                                                                        genericPiece->hasDirectJacobian,
                                                                                        genericPiece->hasDirectJacobianAction,
                                                                                        genericPiece->hasDirectHessian),
                                                                                genericPiece(genericPiece)
{}

std::shared_ptr<ModGraphRandVar> ModGraphRandVar::Create(std::shared_ptr<ModGraph> inGraph, const std::string& outNode)
{
  //call the raw method, which does all the heavy lifting
  auto rawPiece = ModGraphPiece::Create(inGraph, outNode);

  assert(rawPiece->isRandom);

  //for some reason I can't used make_shared here, I get a compiler internal error
  return shared_ptr<ModGraphRandVar>(new ModGraphRandVar(rawPiece)); //make_shared<ModGraphRandVar>(rawPiece);
}

ModGraphRandVar::ModGraphRandVar(std::shared_ptr<ModGraphPiece> genericPiece) : RandVar(genericPiece->inputSizes,
                                                                                        genericPiece->outputSize,
                                                                                        genericPiece->hasDirectGradient,
                                                                                        genericPiece->hasDirectJacobian,
                                                                                        genericPiece->hasDirectJacobianAction,
                                                                                        genericPiece->hasDirectHessian),
                                                                                genericPiece(genericPiece)
{}

class DependentPredicate {
public:

  DependentPredicate() {}

  DependentPredicate(const boost::graph_traits<Graph>::vertex_descriptor& baseNode, const Graph& g)
  {
    // build the map
      findDownstreamNodes(baseNode, g);
  }

  bool operator()(const boost::graph_traits<Graph>::vertex_descriptor& node) const
  {
    return doesDependMap.count(node) > 0;
  }

private:

  std::map<boost::graph_traits<Graph>::vertex_descriptor, bool> doesDependMap;


  void findDownstreamNodes(const boost::graph_traits<Graph>::vertex_descriptor& baseNode, const Graph& g)
  {
    doesDependMap[baseNode] = true;

    boost::graph_traits<Graph>::out_edge_iterator e, e_end;
    for (tie(e, e_end) = boost::out_edges(baseNode, g); e != e_end; e++) {
      findDownstreamNodes(boost::target(*e, g), g);
    }
  }
};

class DependentEdgePredicate {
public:

  DependentEdgePredicate() {}

  DependentEdgePredicate(std::shared_ptr<DependentPredicate> nodePredIn, Graph *gptrIn)
  {
    nodePred = nodePredIn;
    gptr     = gptrIn;
  }

  bool operator()(const boost::graph_traits<Graph>::edge_descriptor& edge) const
  {
    return (*nodePred)(source(edge, *gptr));
  }

private:

  std::shared_ptr<DependentPredicate> nodePred;
  Graph *gptr;
};


ModGraphPiece::ModGraphPiece(std::shared_ptr<ModGraph>                            inGraph,
                             const boost::graph_traits<Graph>::vertex_descriptor& outNode,
                             const std::vector<std::shared_ptr<ModParameter> >  & inPieces,
                             const Eigen::VectorXi                              & inSizes) : ModPiece(inSizes,
                                                                                                      inGraph->outputSize(
                                                                                                        outNode),
                                                                                                      inGraph->allDirectGradient(),
                                                                                                      inGraph->allDirectJacobian(),
                                                                                                      inGraph->allDirectJacobianAction(),
                                                                                                      inGraph->allDirectHessian(),
                                                                                                      inGraph->anyRandom()),
                                                                                             internalGraph(inGraph),
                                                                                             inputPieces(inPieces),
outputNode(outNode)
{
  //    std::cout << "Told modpiece: \n";
  //    std::cout << "\t directGradient:       " << allDirectGradient() << std::endl;
  //    std::cout << "\t directJacobian:       " << inGraph.allDirectJacobian() << std::endl;
  //    std::cout << "\t directJacobianAction: " << inGraph.allDirectJacobianAction() << std::endl;
  //    std::cout << "\t isRandom:             " << inGraph.anyRandom() << std::endl;
  //
  // build the run order
  boost::topological_sort(internalGraph->ModelGraph, std::front_inserter(runOrder));

  // build specialized sensitivity run orders for each input dimension
  sensRunOrders.resize(inputSizes.size());
  for (unsigned int i = 0; i < inputSizes.size(); ++i) {
    auto nFilt = std::make_shared<DependentPredicate>(*internalGraph->GetNodeIterator(
                                                        inputPieces[i]), internalGraph->ModelGraph);
    auto eFilt = std::make_shared<DependentEdgePredicate>(nFilt, &internalGraph->ModelGraph);

    boost::filtered_graph<Graph, DependentEdgePredicate, DependentPredicate> fg(internalGraph->ModelGraph,
                                                                                *eFilt,
                                                                                *nFilt);
    boost::topological_sort(fg, std::back_inserter(sensRunOrders[i]));
  }
}

void ModGraphPiece::BuildValMap()
{
  // first, clear out whatever we had before
  valMap.clear();

  // first, clear out whatever we had before
  inputMap.clear();

  // loop over the run order, computing output values as we go
  for (auto i = runOrder.begin(); i != runOrder.end(); i++) {
    // how many inputs does this piece have?
    int numIns = internalGraph->ModelGraph[*i]->piece->inputSizes.size();

    if (numIns > 0) {
      std::vector<Eigen::VectorXd> inputs(numIns);

      // loop over the input edges -- collecting input values
      boost::graph_traits<Graph>::in_edge_iterator e, e_end;
      for (tie(e, e_end) = boost::in_edges(*i, internalGraph->ModelGraph); e != e_end; e++) {
        inputs[internalGraph->ModelGraph[*e]->GetDim()] = valMap[boost::source(*e, internalGraph->ModelGraph)];
      }

      // evaluate the current map and store the value
      valMap[*i] = internalGraph->ModelGraph[*i]->piece->Evaluate(inputs);

      // store the inputs
      inputMap[*i] = inputs;
    } else {
      valMap[*i] = internalGraph->ModelGraph[*i]->piece->Evaluate();
    }
  }
}


Eigen::VectorXd ModGraphPiece::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  // make sure the input vector is the correct size
  assert(input.size() == inputPieces.size());

  // set the values of each input node
  for (unsigned int i = 0; i < inputPieces.size(); ++i) {
    inputPieces[i]->SetVal(input[i]);
  }

  // build the value map
  BuildValMap();

  // evaluate the graph and return the result
  return valMap[outputNode];
}

Eigen::VectorXd ModGraphPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                            Eigen::VectorXd const             & sensitivity,
                                            int const                           inputDimWrt)
{
  // evaluate the model
  Evaluate(input);

  // define the sensitivity map
  std::map<boost::graph_traits<Graph>::vertex_descriptor, Eigen::VectorXd> sensMap;

  // loop over the run order, computing output values as we go
  for (auto i = sensRunOrders[inputDimWrt].begin(); i != sensRunOrders[inputDimWrt].end(); i++) {
    // initialize the sensitivity to zero
    Eigen::VectorXd Sens = Eigen::VectorXd::Zero(internalGraph->ModelGraph[*i]->piece->outputSize);


    if (boost::out_degree(*i, internalGraph->ModelGraph) == 0) {
      Sens = sensitivity;
    } else {
      // loop over the output edges from this node
      boost::graph_traits<Graph>::out_edge_iterator e, e_end;
      for (tie(e, e_end) = boost::out_edges(*i, internalGraph->ModelGraph); e != e_end; e++) {
        auto outNode = boost::target(*e, internalGraph->ModelGraph);
        int  dim     = internalGraph->ModelGraph[*e]->GetDim();
        Sens += internalGraph->ModelGraph[outNode]->piece->Gradient(inputMap[outNode], sensMap[outNode], dim);
      }
    }
    sensMap[*i] = Sens;
  }


  return sensMap[*internalGraph->GetNodeIterator(inputPieces[inputDimWrt])];
}

Eigen::MatrixXd ModGraphPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  // evaluate the model
  Evaluate(input);

  std::map<boost::graph_traits<Graph>::vertex_descriptor, Eigen::MatrixXd> jacMap;

  // loop over the sensitivity run order for this dimension in reverse
  auto i = sensRunOrders[inputDimWrt].rbegin(); // this should be the input node

  int inDim = internalGraph->ModelGraph[*i]->piece->outputSize;
  jacMap[*i] = Eigen::MatrixXd::Identity(inDim, inDim);
  i++;

  for (; i != sensRunOrders[inputDimWrt].rend(); i++) {
    // initialize the linearize output to zero
    Eigen::MatrixXd tempJac = Eigen::MatrixXd::Zero(internalGraph->ModelGraph[*i]->piece->outputSize, inDim);

    // loop over the inputs to this node
    boost::graph_traits<Graph>::in_edge_iterator e, e_end;
    for (tie(e, e_end) = boost::in_edges(*i, internalGraph->ModelGraph); e != e_end; e++) {
      // if this node's input depends on the overall ModGraphPiece input (i.e. the source node is in the map), compute
      // the jacobian action
      auto inNode = boost::source(*e, internalGraph->ModelGraph);
      int  dim    = internalGraph->ModelGraph[*e]->GetDim();

      if (jacMap.count(inNode)) {
        tempJac += (internalGraph->ModelGraph[*i]->piece->Jacobian(inputMap[*i], dim)) * jacMap[inNode];
      }
    }
    jacMap[*i] = tempJac;
  }

  return jacMap[outputNode];
}

// compute the action of the jacobian on a vector, i.e. the action of a linearized model on the input vector
Eigen::VectorXd ModGraphPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                  Eigen::VectorXd const             & target,
                                                  int const                           inputDimWrt)
{
  // evaluate the model
  Evaluate(input);

  std::map<boost::graph_traits<Graph>::vertex_descriptor, Eigen::VectorXd> linOutMap;

  // loop over the sensitivity run order for this dimension in reverse
  auto i = sensRunOrders[inputDimWrt].rbegin(); // this should be the input node
  linOutMap[*i] = target;
  i++;
  for (; i != sensRunOrders[inputDimWrt].rend(); i++) {
    // initialize the linearize output to zero
    Eigen::VectorXd linOut = Eigen::VectorXd::Zero(internalGraph->ModelGraph[*i]->piece->outputSize);

    // loop over the inputs to this node
    boost::graph_traits<Graph>::in_edge_iterator e, e_end;
    for (tie(e, e_end) = boost::in_edges(*i, internalGraph->ModelGraph); e != e_end; e++) {
      // if this node's input depends on the overall ModGraphPiece input (i.e. the source node is in the map), compute
      // the jacobian action
      auto inNode = boost::source(*e, internalGraph->ModelGraph);
      int  dim    = internalGraph->ModelGraph[*e]->GetDim();
      if (linOutMap.count(inNode)) {
        linOut += internalGraph->ModelGraph[*i]->piece->JacobianAction(inputMap[*i], linOutMap[inNode], dim);
      }
    }
    linOutMap[*i] = linOut;
  }

  return linOutMap[outputNode];
}

Eigen::MatrixXd ModGraphPiece::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                           Eigen::VectorXd const             & sensitivity,
                                           int const                           inputDimWrt)
{
  // evaluate the model
  Evaluate(input);

  // each node's jacobian wrt to each of its inputs
  std::map<boost::graph_traits<Graph>::vertex_descriptor,
           std::map<boost::graph_traits<Graph>::vertex_descriptor, Eigen::MatrixXd> > jacMap;

  // loop input->output and compute jacobians wrt to local inputs
  auto i             = sensRunOrders[inputDimWrt].rbegin();
  const int inputDim = internalGraph->ModelGraph[*i]->piece->outputSize;

  for (; i != sensRunOrders[inputDimWrt].rend(); i++) {
    if (boost::in_degree(*i, internalGraph->ModelGraph) != 0) { // don't need to worry about input nodes, their
                                                                // jacobians
                                                                // are the identity
      // loop over the inputs
      boost::graph_traits<Graph>::in_edge_iterator e, e_end;
      for (tie(e, e_end) = boost::in_edges(*i, internalGraph->ModelGraph); e != e_end; e++) {
        auto inNode = boost::source(*e, internalGraph->ModelGraph);
        int  dim    = internalGraph->ModelGraph[*e]->GetDim();

        jacMap[*i][inNode] = internalGraph->ModelGraph[*i]->piece->Jacobian(inputMap[*i], dim);
      }
    }
  }

  // store sensitivities for each node
  std::map<boost::graph_traits<Graph>::vertex_descriptor, Eigen::VectorXd> sensMap;

  // initialize the Hessian
  Eigen::MatrixXd Hessian = Eigen::MatrixXd::Zero(inputDim, inputDim);

  // loop output->input computing the hessian
  for (auto j = sensRunOrders[inputDimWrt].begin(); j != sensRunOrders[inputDimWrt].end(); j++) {
    Eigen::VectorXd Sens = Eigen::VectorXd::Zero(internalGraph->ModelGraph[*j]->piece->outputSize);

    if (boost::out_degree(*j, internalGraph->ModelGraph) == 0) {
      Sens = sensitivity;
    } else {
      // loop over the output edges from this node computing the contribution to the Hessian from each
      boost::graph_traits<Graph>::out_edge_iterator e, e_end;
      for (tie(e, e_end) = boost::out_edges(*j, internalGraph->ModelGraph); e != e_end; e++) {
        auto outNode  = boost::target(*e, internalGraph->ModelGraph);
        const int dim = internalGraph->ModelGraph[*e]->GetDim();

        // Hessian wrt local inputs
        Eigen::MatrixXd tempHess = internalGraph->ModelGraph[outNode]->piece->Hessian(inputMap[outNode],
                                                                                      sensMap[outNode],
                                                                                      dim);

        // loop back up the graph using Jacobians to map from output space to input space for that node
        for (auto h = j; h != sensRunOrders[inputDimWrt].end(); h++) {
          if ((boost::in_degree(*h, internalGraph->ModelGraph) != 0) && jacMap[*h].count(*(h + 1))) { // top nodes are
                                                                                                      // just the
                                                                                                      // identity and we
                                                                                                      // dont want to
                                                                                                      // include nodes
                                                                                                      // that aren't
                                                                                                      // inputs to h
            Eigen::MatrixXd dum0 = Eigen::MatrixXd::Zero(internalGraph->ModelGraph[*h]->piece->outputSize,
                                                         internalGraph->ModelGraph[*(h + 1)]->piece->outputSize);
            boost::graph_traits<Graph>::in_edge_iterator in, in_end;
            for (tie(in, in_end) = boost::in_edges(*h, internalGraph->ModelGraph); in != in_end; in++) { // accumulate
                                                                                                         //
                                                                                                         // contributions
                                                                                                         // from
                                                                                                         // multiple
                                                                                                         // inputs
              auto inNode = boost::source(*in, internalGraph->ModelGraph);
              dum0 += jacMap[*h][inNode];
            }

            // we now have the hessian wrt to the inputs of one level higher
            Eigen::MatrixXd dum1 = dum0.transpose() * tempHess * dum0;
            tempHess.resize(dum1.rows(), dum1.cols());
            tempHess = dum1;
          }
        }

        // add contribution to the global hessian
        Hessian += tempHess;

        // update the sensitivities with jacobian info.
        boost::graph_traits<Graph>::in_edge_iterator in, in_end;
        for (tie(in, in_end) = boost::in_edges(*j, internalGraph->ModelGraph); in != in_end; in++) {
          auto inNode = boost::target(*in, internalGraph->ModelGraph);
          Sens += jacMap[outNode][inNode] * sensMap[outNode];
        }
      }
    }
    sensMap[*j] = Sens;
  }

  return Hessian;
}

#if MUQ_PYTHON == 1
void ModGraphPiece::PyWriteGraphViz0(const string& filename) const
{
  writeGraphViz(filename);
}

void ModGraphPiece::PyWriteGraphViz1(const string& filename, ModGraph::colorOptionsType colorOption) const
{
  writeGraphViz(filename, colorOption);
}

void ModGraphPiece::PyWriteGraphViz2(const string& filename, ModGraph::colorOptionsType colorOption,
                                     bool addDerivLabel) const
{
  writeGraphViz(filename, colorOption, addDerivLabel);
}

#endif // if MUQ_PYTHON == 1
