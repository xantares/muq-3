
#include "MUQ/Modelling/RandVar.h"
#include <iostream>

using namespace muq::Modelling;


RandVar::RandVar(Eigen::VectorXi const& inputSizes,
                 int const              outputSize,
                 bool const             hasDirectGradient,
                 bool const             hasDirectJacobian,
                 bool const             hasDirectJacobianAction,
                 bool const             hasDirectHessian) : ModPiece(inputSizes,
                                                                                      outputSize,
                                                                                      hasDirectGradient,
                                                                                      hasDirectJacobian,
                                                                                      hasDirectJacobianAction,
                                                                                      hasDirectHessian,
                                                                                      true)
{}

Eigen::MatrixXd RandVar::Sample(std::vector<Eigen::VectorXd> const& input, int NumSamps)
{
  Eigen::MatrixXd samps(outputSize, NumSamps);

  for (int i = 0; i < NumSamps; ++i) {
    samps.col(i) = Evaluate(input);
  }

  return samps;
}

