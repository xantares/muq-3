
#include <assert.h>
#include <iostream>

#include <Eigen/Cholesky>

#include "MUQ/Modelling/ScaledMvNormDens.h"
#include "MUQ/Modelling/ScaledMvNormRV.h"

using namespace muq::Modelling;
using namespace std;

double ScaledMvNormDens::LogDensityImpl(std::vector<Eigen::VectorXd> const& input)
{
  /// generate a vector for the difference between xc and the mean
  Eigen::VectorXd delta = input[0] - specification->Mean;

  const double sigma = input[1](0);

  return -0.5 * delta.dot(specification->ApplyInverseCovariance(delta).col(0)) / sigma;
}

Eigen::VectorXd ScaledMvNormDens::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                               Eigen::VectorXd const             & sensitivity,
                                               int const                           inputDimWrt)
{
  /// generate a vector for the difference between xc and the mean
  Eigen::VectorXd delta = input.at(0) - specification->Mean;

  const double sigma = input[1](0);

  if (inputDimWrt == 0) {
    return sensitivity[0] * (-1.0 * specification->ApplyInverseCovariance(delta)) / sigma;
  } else {
    const double result = 0.5 * delta.dot(specification->ApplyInverseCovariance(delta).col(0)) / pow(sigma, 2.0);
    return sensitivity[0] *result *Eigen::VectorXd:: Ones(1);
  }
}

Eigen::MatrixXd ScaledMvNormDens::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  /// generate a vector for the difference between xc and the mean
  Eigen::VectorXd delta = input.at(0) - specification->Mean;

  const double sigma = input[1](0);

  if (inputDimWrt == 0) {
    return -1.0 * specification->ApplyInverseCovariance(delta).transpose() / sigma;
  } else {
    const double result = 0.5 * delta.dot(specification->ApplyInverseCovariance(delta).col(0)) / pow(sigma, 2.0);
    return result * Eigen::VectorXd::Ones(1);
  }
}

Eigen::VectorXd ScaledMvNormDens::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & target,
                                                     int const                           inputDimWrt)
{
  /// generate a vector for the difference between xc and the mean
  Eigen::VectorXd delta = input.at(0) - specification->Mean;

  const double sigma = input[1](0);

  if (inputDimWrt == 0) {
    return (-1.0 * specification->ApplyInverseCovariance(delta).col(0)).dot(target) * Eigen::VectorXd::Ones(1) / sigma;
  } else {
    const double result = 0.5 * delta.dot(specification->ApplyInverseCovariance(delta).col(0)) / pow(sigma, 2.0);
    return target[0] *result *Eigen::VectorXd:: Ones(1);
  }
}

Eigen::MatrixXd ScaledMvNormDens::HessianImpl(vector<Eigen::VectorXd> const& input,
                                              Eigen::VectorXd const        & sensitivity,
                                              int const                      inputDimWrt)
{
  const double sigma = input[1](0);

  if (inputDimWrt == 0) {
    return -1.0 * sensitivity[0] * specification->GetInverseCovarianceMatrix() / sigma;
  } else {
    Eigen::VectorXd delta = input.at(0) - specification->Mean;

    const double result = -1.0 * delta.dot(specification->ApplyInverseCovariance(delta).col(0)) / pow(sigma, 3.0);
    return sensitivity[0] *result *Eigen::VectorXd:: Ones(1);
  }
}

