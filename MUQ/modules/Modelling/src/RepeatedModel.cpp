
#include "MUQ/Modelling/RepeatedModel.h"

using namespace muq::Modelling;
using namespace std;

RepeatedModel::RepeatedModel(std::shared_ptr<ModPiece> modelIn, int NumRepeatsIn) : OneInputFullModPiece(
                                                                                      modelIn->inputSizes[0] * NumRepeatsIn,
                                                                                      modelIn->outputSize *
                                                                                      NumRepeatsIn), model(modelIn),
                                                                                    NumRepeats(NumRepeatsIn)
{}

Eigen::VectorXd RepeatedModel::EvaluateImpl(Eigen::VectorXd const& input)
{
  Eigen::VectorXd output(outputSize);

  for (int i = 0; i < NumRepeats; ++i) {
    output.segment(model->outputSize * i,
                   model->outputSize) = model->Evaluate(input.segment(model->inputSizes[0] * i, model->inputSizes[0]));
  }

  return output;
}

Eigen::VectorXd RepeatedModel::GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
{
  Eigen::VectorXd gradient(inputSizes[0]);

  for (int i = 0; i < NumRepeats; ++i) {
    gradient.segment(model->inputSizes[0] * i,
                     model->inputSizes[0]) = model->Gradient(input.segment(model->inputSizes[0] * i,
                                                                           model->inputSizes[0]),
                                                             sensitivity.segment(model->outputSize * i,
                                                                                 model->outputSize));
  }

  return gradient;
}

Eigen::MatrixXd RepeatedModel::JacobianImpl(Eigen::VectorXd const& input)
{
  Eigen::MatrixXd jac = Eigen::MatrixXd::Zero(outputSize, inputSizes[0]);

  int bW = model->inputSizes[0];
  int bH = model->outputSize;

  for (int i = 0; i < NumRepeats; ++i) {
    jac.block(bH * i, bW * i, bH, bW) = model->Jacobian(input.segment(bW * i, bW));
  }

  return jac;
}

Eigen::VectorXd RepeatedModel::JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target)
{
  Eigen::VectorXd output = Eigen::VectorXd::Zero(outputSize);

  int bW = model->inputSizes[0];
  int bH = model->outputSize;

  for (int i = 0; i < NumRepeats; ++i) {
    output.segment(bH * i, bH) = model->JacobianAction(input.segment(bW * i, bW), target.segment(bW * i, bW));
  }

  return output;
}

Eigen::MatrixXd RepeatedModel::HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
{
  Eigen::MatrixXd hess = Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);

  int bW = model->inputSizes[0];
  int bH = model->outputSize;

  for (int i = 0; i < NumRepeats; ++i) {
    hess.block(bW * i, bW * i, bW, bW) = model->Hessian(input.segment(bW * i, bW), sensitivity.segment(i * bH, bH));
  }

  return hess;
}
