
#include "MUQ/Modelling/LinearModel.h"

#include <Eigen/Dense>

// std library includes
#include <iostream>
#include <sstream>
#include <memory>

// other muq includes
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/ModGraphPiece.h"

using namespace muq::Modelling;
using namespace std;


/** Construct the LinearModel from and Eigen::MatrixXd */
LinearModel::LinearModel(const Eigen::MatrixXd& Ain, bool useIdentityMatrix) :
  OneInputJacobianModPiece(useIdentityMatrix ? Ain.rows() : Ain.cols(), Ain.rows()), isAffine(!useIdentityMatrix),
  isId(useIdentityMatrix), useAInverse(false)
{
  if (useIdentityMatrix) {
    assert(Ain.cols() == 1);
    b = Ain;
  } else {
    A = Ain;
  }
}

/** Construct the LinearModel from and Eigen::MatrixXd */
LinearModel::LinearModel(const Eigen::VectorXd& bin, const Eigen::MatrixXd& Ain, bool useAInverse) :
  OneInputJacobianModPiece(Ain.cols(),
                           Ain.rows()),  A(Ain), b(bin), isAffine(false), isId(false), useAInverse(useAInverse)
{
    assert(Ain.rows() == bin.rows());

  if (useAInverse) {
    Eigen::LLT<Eigen::MatrixXd> cholSolver;
    cholSolver.compute(A);
    if(cholSolver.info()!=Eigen::Success){
      std::cerr << "\nERROR: In LinearModel::LinearModel, the Cholesky decomposition of the input matrix failed.  This is likely caused by the matrix not being positive definite.  At the moment, the LinearModel class can only apply the inverse when the matrix positive definite.\n\n";
      assert(cholSolver.info()==Eigen::Success);
    }
    L = cholSolver.matrixL();
  }
}

Eigen::VectorXd LinearModel::EvaluateImpl(Eigen::VectorXd const& input)
{
  if (isAffine) {
    return A * input;
  } else if (isId) {
    return input + b;
  } else if (useAInverse) {
    Eigen::MatrixXd solved = L.triangularView<Eigen::Lower>().solve(input);
    L.triangularView<Eigen::Lower>().transpose().solveInPlace(solved);
    return solved + b;
  } else {
    return A * input + b;
  }
}

Eigen::MatrixXd LinearModel::JacobianImpl(Eigen::VectorXd const& input)
{
  // do not try to invert A, use finite difference
  if (useAInverse) {
    Eigen::MatrixXd jacobian = Eigen::MatrixXd::Zero(outputSize, inputSizes(0));

    for (int i = 0; i < outputSize; ++i) {
      Eigen::VectorXd sens = Eigen::VectorXd::Zero(outputSize);
      sens(i) = 1.0;

      const Eigen::VectorXd grad = GradientByFD(vector<Eigen::VectorXd>(1, input), sens, 0);
      jacobian.row(i) = grad.transpose();
    }

    return jacobian;
  }

  if (isId) {
    return Eigen::MatrixXd::Identity(b.rows(), b.rows());
  } else {
    return A;
  }
}


void LinearModel::NullSpace(Eigen::MatrixXd& nullSpace, Eigen::MatrixXd& nullComplement) const
{
  auto eigSolver = make_shared<Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> >(A.transpose() * A);

  assert(eigSolver->info() == Eigen::Success);

  Eigen::VectorXd eigVals = eigSolver->eigenvalues();
  Eigen::MatrixXd eigVecs = eigSolver->eigenvectors();

  unsigned int rank = 0;
  for (unsigned int i = 0; i < eigVals.size(); ++i) {
    if (abs(eigVals(i)) > 1.0e-14) {
      ++rank;
    }
  }

  nullSpace      = eigVecs.leftCols(A.cols() - rank);
  nullComplement = eigVecs.rightCols(rank);
}

Eigen::MatrixXd LinearModel::GetCholesky() const
{
  assert(useAInverse);
  return L;
}


