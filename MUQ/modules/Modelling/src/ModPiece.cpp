#include "MUQ/Modelling/ModPiece.h"

#include <cxxabi.h>
#include <chrono>
#include <assert.h>

#include "MUQ/Utilities/EigenUtils.h"

using namespace std;
using namespace Eigen;
using namespace muq::Modelling;
using namespace muq::Utilities;


ModPiece::ModPiece(Eigen::VectorXi const& inputSizes,
                   int const              outputSize,
                   bool const             hasDirectGradient,
                   bool const             hasDirectJacobian,
                   bool const             hasDirectJacobianAction,
                   bool const             hasDirectHessian,
                   bool const             isRandom,
                   std::string const    & name) : inputSizes(inputSizes), outputSize(outputSize), hasDirectGradient(
                                                    hasDirectGradient),
                                                  hasDirectJacobian(hasDirectJacobian),
                                                  hasDirectJacobianAction(hasDirectJacobianAction), hasDirectHessian(
                                                    hasDirectHessian), isRandom(isRandom), myName(
                                                    name)
{
  for (int i = 0; i < inputSizes.rows(); ++i) {
    assert(inputSizes(i) > 0); //all the inputs must take nonzero inputs
  }

    assert(outputSize > 0);

  oneStepCache = make_shared<ModPieceOneStepCache>();

  static int modPieceId = 0;

  id = ++modPieceId;
}

Eigen::MatrixXd ModPiece::EvaluateMulti(std::vector<Eigen::MatrixXd> const& input)
{
  CheckMultiInputs(input);
  
  int numCopies = input.at(0).cols();
  numEvalCalls += numCopies;
  
  auto start_time = chrono::high_resolution_clock::now();
  
  Eigen::MatrixXd result = EvaluateMultiImpl(input);
  
  auto end_time = chrono::high_resolution_clock::now();
  evalTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
  
  return result;
}

Eigen::MatrixXd ModPiece::EvaluateMultiImpl(std::vector<Eigen::MatrixXd> const& input)
{
  int numCopies = input.at(0).cols();
  Eigen::MatrixXd output(outputSize,numCopies);
  
  std::vector<Eigen::VectorXd> tempInput(inputSizes.size());
  
  for(int copy=0; copy<numCopies; ++copy){
    for(int i=0; i<inputSizes.size(); ++i)
      tempInput[i] = input[i].col(copy);
    output.col(copy) = EvaluateImpl(tempInput);
  }
  
  return output;
}


Eigen::VectorXd ModPiece::Evaluate(std::vector<Eigen::VectorXd> const& input)
{
  //test the inputs
  CheckInputs(input);

  VectorXd result;
  
  if ((!isRandom)&&(useCache)) {      //if not random and the cache is turned on, try the cache
    auto cachedResult = oneStepCache->GetCachedEvaluate(input);

    if (cachedResult) { //used cached if possible
      if (cachedResult->size() != 0) {
        result = *cachedResult;
      } else {
        numEvalCalls++;
        auto start_time = chrono::high_resolution_clock::now();

        result = EvaluateImpl(input);

        auto end_time = chrono::high_resolution_clock::now();
        evalTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

        // update the cache
        oneStepCache->SetCachedEvaluate(input, result);
      }
    } else {
      //else run the real model and store the result
      numEvalCalls++;
      auto start_time = chrono::high_resolution_clock::now();

      result = EvaluateImpl(input);

      auto end_time = chrono::high_resolution_clock::now();
      evalTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

      // update the cache
      oneStepCache->SetCachedEvaluate(input, result);
    }
  } else { //otherwise it's random or the cache is off and must be re-run
    numEvalCalls++;
    auto start_time = chrono::high_resolution_clock::now();

    result = EvaluateImpl(input);

    auto end_time = chrono::high_resolution_clock::now();
    evalTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
  }  


  //check the output size, since we don't trust user methods
  if(result.rows()!=outputSize){
    std::cerr << "ERROR: The user defined EvaluateImpl function returned a vector of length " << result.rows() << " but the output should have " << outputSize << " components.\n" << std::endl;
    assert(result.rows() == outputSize);
  }
  return result;
}

Eigen::VectorXd ModPiece::Gradient(std::vector<Eigen::VectorXd> const& input,
                                   Eigen::VectorXd const             & sensitivity,
                                   int const                           inputDimWrt)
{
  assert(inputSizes.size() != 0); //derivatives don't exist if the model takes no inputs
  //test the inputs
  CheckInputs(input);
  assert(inputDimWrt < inputSizes.rows());
  assert(outputSize == sensitivity.rows());

  VectorXd result;
  if ((!isRandom)&&(useCache)) {      //if not random and the cache is turned on, try the cache
    auto cachedResult = oneStepCache->GetCachedGradient(input, sensitivity, inputDimWrt);

    if (cachedResult) { //used cached if possible
      result = *cachedResult;
    } else {
      //else run the real model and store the result
      numGradCalls++;
      auto start_time = chrono::high_resolution_clock::now();

      result = GradientImpl(input, sensitivity, inputDimWrt);

      auto end_time = chrono::high_resolution_clock::now();
      gradTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

      // update the cache
      oneStepCache->SetCachedGradient(input, sensitivity, inputDimWrt, result);
    }
  } else { //otherwise it's random or the cache is off and must be re-run
    numGradCalls++;
    auto start_time = chrono::high_resolution_clock::now();

    result = GradientImpl(input, sensitivity, inputDimWrt);

    auto end_time = chrono::high_resolution_clock::now();
    gradTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
  }

  //check the output size
  if(result.rows()!=inputSizes(inputDimWrt)){
	std::cerr << "ERROR: The user defined GradientImpl function returned a gradient of length " << result.rows() << " but the input of dimension " << inputDimWrt << " has " << inputSizes(inputDimWrt) << " components.\n" << std::endl;
    assert(result.rows() == inputSizes(inputDimWrt));
  }
  
  return result;
}

Eigen::MatrixXd ModPiece::Jacobian(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  assert(inputSizes.size() != 0); //derivatives don't exist if the model takes no inputs
  //test the inputs
  CheckInputs(input);
    assert(inputDimWrt < inputSizes.rows());

  MatrixXd result;

  if ((!isRandom)&&(useCache)) {      //if not random and the cache is turned on, try the cache
    auto cachedResult = oneStepCache->GetCachedJacobian(input, inputDimWrt);

    if (cachedResult) { //used cached if possible
      result = *cachedResult;
    } else {
      //else run the real model and store the result
      numJacCalls++;
      auto start_time = chrono::high_resolution_clock::now();

      result = JacobianImpl(input, inputDimWrt);

      auto end_time = chrono::high_resolution_clock::now();
      jacTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

      // update the cache
      oneStepCache->SetCachedJacobian(input, inputDimWrt, result);
    }
  } else { //otherwise it's random or the cache is off and must be re-run
    numJacCalls++;
    auto start_time = chrono::high_resolution_clock::now();

    result = JacobianImpl(input, inputDimWrt);

    auto end_time = chrono::high_resolution_clock::now();
    jacTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
  }

  //check the output size
  if ((result.rows() != outputSize) || (result.cols() != inputSizes(inputDimWrt))) {
    std::cerr << "In " << GetName() << ", JacobianImpl returned matrix with wrong size\n";
    assert(result.rows() == outputSize && result.cols() == inputSizes(inputDimWrt));
  }

  return result;
}

Eigen::VectorXd ModPiece::JacobianAction(std::vector<Eigen::VectorXd> const& input,
                                         Eigen::VectorXd const             & target,
                                         int const                           inputDimWrt)
{
    assert(inputSizes.size() != 0); //derivatives don't exist if the model takes no inputs
  //test the inputs
  CheckInputs(input);
  assert(inputDimWrt < inputSizes.rows());
  assert(inputSizes(inputDimWrt) == target.rows());

  VectorXd result;

  if ((!isRandom)&&(useCache)) {      //if not random and the cache is turned on, try the cache
    auto cachedResult = oneStepCache->GetCachedJacobianAction(input, target, inputDimWrt);

    if (cachedResult) { //used cached if possible
      result = *cachedResult;
    } else {
      //else run the real model and store the result
      numJacActCalls++;
      auto start_time = chrono::high_resolution_clock::now();

      result = JacobianActionImpl(input, target, inputDimWrt);

      auto end_time = chrono::high_resolution_clock::now();
      jacActTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

      // update the cache
      oneStepCache->SetCachedJacobianAction(input, target, inputDimWrt, result);
    }
  } else { //otherwise it's random or the cache is off and must be re-run
    numJacActCalls++;
    auto start_time = chrono::high_resolution_clock::now();

    result = JacobianActionImpl(input, target, inputDimWrt);

    auto end_time = chrono::high_resolution_clock::now();
    jacActTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
  }


  //check the output size
  assert(result.rows() == outputSize);
  return result;
}

Eigen::MatrixXd ModPiece::Hessian(std::vector<Eigen::VectorXd> const& input,
                                  Eigen::VectorXd const             & sensitivity,
                                  int const                           inputDimWrt)
{
  assert(inputSizes.size() != 0); //derivatives don't exist if the model takes no inputs
  //test the inputs
  CheckInputs(input);
    assert(inputDimWrt < inputSizes.rows());

  Eigen::MatrixXd result;

  if ((!isRandom)&&(useCache)) {      //if not random and the cache is turned on, try the cache
    auto cachedResult = oneStepCache->GetCachedHessian(input, sensitivity, inputDimWrt);

    if (cachedResult) { //use cached if possible
      result = *cachedResult;
    } else {
      // run the model and store
      numHessCalls++;
      auto start_time = chrono::high_resolution_clock::now();

      result = HessianImpl(input, sensitivity, inputDimWrt);

      auto end_time = chrono::high_resolution_clock::now();
      hessTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

      // update the cache
      oneStepCache->SetCachedHessian(input, sensitivity, inputDimWrt, result);
    }
  } else {
    numHessCalls++;
    auto start_time = chrono::high_resolution_clock::now();

    result = HessianImpl(input, sensitivity, inputDimWrt);

    auto end_time = chrono::high_resolution_clock::now();
    hessTime += chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
  }

  return result;
}

#if MUQ_PYTHON == 1

boost::python::list ModPiece::PyEvaluate(boost::python::list const& inputs)
{
  std::vector<Eigen::VectorXd> eigenInputs = PythonListToVector(inputs);

  return muq::Utilities::GetPythonVector<Eigen::VectorXd>(Evaluate(eigenInputs));
}

boost::python::list ModPiece::PyGradient(boost::python::list const& pyInputs,
                                         boost::python::list const& pySensitivity,
                                         int const                  inputDimWrt)
{
  Eigen::VectorXd sensitivity = GetEigenVector<Eigen::VectorXd>(pySensitivity);

  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  if ((int)inputs.size() != inputSizes.size()) {
    std::cerr << "In " << GetName() << ", PyGradient has been called with the wrong number of inputs" << endl;
    assert((int)inputs.size() == inputSizes.size());
  }

  return GetPythonVector<Eigen::VectorXd>(Gradient(inputs, sensitivity, inputDimWrt));
}

boost::python::list ModPiece::PyJacobian(boost::python::list const& pyInputs, int const inputDimWrt)
{
  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  if ((int)inputs.size() != inputSizes.size()) {
    std::cerr << "In " << GetName() << ", PyJacobian has been called with the wrong number of inputs" << endl;
    assert((int)inputs.size() == inputSizes.size());
  }

  return GetPythonMatrix(Jacobian(inputs, inputDimWrt));
}

boost::python::list ModPiece::PyJacobianAction(boost::python::list const& pyInputs,
                                               boost::python::list const& pyTarget,
                                               int const                  inputDimWrt)
{
  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  if ((int)inputs.size() != inputSizes.size()) {
    std::cerr << "In " << GetName() << ", PyJacobianAction has been called with the wrong number of inputs" << endl;
    assert((int)inputs.size() == inputSizes.size());
  }

  Eigen::VectorXd target = GetEigenVector<Eigen::VectorXd>(pyTarget);

  return GetPythonVector<Eigen::VectorXd>(JacobianAction(inputs, target, inputDimWrt));
}

boost::python::list ModPiece::PyHessian(boost::python::list const& pyInputs,
                                        boost::python::list const& pySensitivity,
                                        int const                  inputDimWrt)
{
  Eigen::VectorXd sensitivity = GetEigenVector<Eigen::VectorXd>(pySensitivity);

  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  if ((int)inputs.size() != inputSizes.size()) {
    std::cerr << "In " << GetName() << ", PyHessian called with the wrong number of inputs" << endl;
    assert((int)inputs.size() == inputSizes.size());
  }

  return GetPythonMatrix(Hessian(inputs, sensitivity, inputDimWrt));
}

#endif // if MUQ_PYTHON == 1

void ModPiece::CheckInputs(std::vector<Eigen::VectorXd> const& input) const
{
  if( input.size() != inputSizes.rows()){
    cerr << "In " << GetName() << ", the number of inputs does not match the number expected.  " << input.size() << " inputs were given, but " << inputSizes.rows() << " were expected."  << endl;
    assert(static_cast<int>(input.size()) == inputSizes.rows());
  }
  for (int i = 0; i < inputSizes.rows(); ++i) {
    if(input.at(i).rows() != inputSizes(i)){
      cerr << "In " << GetName() << ", the size of input[" << i << "] is " << input.at(i).size() << " but the expected size is " << inputSizes(i) << "." << endl;
      assert(input.at(i).rows() == inputSizes(i)); 
    }
  }
}

void ModPiece::CheckMultiInputs(std::vector<Eigen::MatrixXd> const& input) const
{
  if( input.size() != inputSizes.rows()){
    cerr << "In " << GetName() << ", the number of inputs does not match the number expected.  " << input.size() << " inputs were given, but " << inputSizes.rows() << " were expected."  << endl;
    assert(static_cast<int>(input.size()) == inputSizes.rows());
  }
  unsigned int numCopies = input.at(0).cols();
  for (int i = 0; i < inputSizes.rows(); ++i) {
    if(input.at(i).rows() != inputSizes(i)){
      cerr << "In " << GetName() << ", the size of input " << i << " is " << input.at(i).rows() << " but the expected size is " << inputSizes(i) << "." << endl;
      assert(input.at(i).rows() == inputSizes(i));
    }
    if(input.at(i).cols() != numCopies){
      cerr << "In " << GetName() << ", the number of copies of input " << i << " is " << input.at(i).cols() << " but the number of copies of input 0 is " << numCopies << "." << endl;
      assert(input.at(i).cols()==numCopies);
    }
  }
}

Eigen::VectorXd ModPiece::GradientByFD(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt)
{
  return GradientByFD(input,sensitivity,inputDimWrt,FdRelTolDefault,FdMinTolDefault); 
}

Eigen::VectorXd ModPiece::GradientByFD(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt,
                                       double                              FdRelTol,
                                       double                              FdMinTol)
{
  if (isRandom) {
    std::cerr << "In " << GetName() << ", cannot perform finite difference gradient on random ModPiece.\n";
    assert(!isRandom); //Can't use the default methods on random ModPieces
  }
  
  assert(FdRelTol > 0.0 && FdMinTol > 0);

  //test the inputs
  CheckInputs(input);
  assert(inputDimWrt < inputSizes.rows());
  assert(outputSize == sensitivity.rows());

  std::vector<Eigen::VectorXd> TempLocs = input;

  auto centerResult = Evaluate(input);

  //start the gradient
  VectorXd gradient = VectorXd::Zero(inputSizes(inputDimWrt));

  // loop through each entry of input dim, slightly perturbing the position
  for (int row = 0; row < inputSizes(inputDimWrt); ++row) {
    // compute the step length
    double dx = fmax(FdMinTol, fabs(FdRelTol * TempLocs.at (inputDimWrt) (row)));

    // update the locations with a forward difference step
    TempLocs.at (inputDimWrt) (row) += dx;

    //compute it there
    auto perturbedResult = Evaluate(TempLocs);

    //fill in the difference result
    gradient(row) += sensitivity.dot(perturbedResult - centerResult) / dx;

    // set the position back to what it was
    TempLocs.at (inputDimWrt) (row) = input.at (inputDimWrt) (row);
  }

  return gradient;
}



Eigen::MatrixXd ModPiece::JacobianByFD(std::vector<Eigen::VectorXd> const& input,
                                       int const                           inputDimWrt)
{
  return JacobianByFD(input,inputDimWrt,FdRelTolDefault,FdMinTolDefault); 
}

Eigen::MatrixXd ModPiece::JacobianByFD(std::vector<Eigen::VectorXd> const& input,
                                       int const                           inputDimWrt,
                                       double                              FdRelTol,
                                       double                              FdMinTol)
{
  if (isRandom) {
    std::cerr << "In " << GetName() << ", cannot perform finite difference Jacobian on random ModPiece.\n";
    assert(!isRandom); //Can't use the default methods on random ModPieces
  }
  
  assert(FdRelTol > 0.0 && FdMinTol > 0);

  //test the inputs
  CheckInputs(input);
  assert(inputDimWrt < inputSizes.rows());

  std::vector<Eigen::VectorXd> TempLocs = input;

  auto centerResult = Evaluate(input);

  //start the jacobian
  MatrixXd jacobian = MatrixXd::Zero(outputSize,inputSizes(inputDimWrt));

  // loop through each entry of input dim, slightly perturbing the position
  for (int col = 0; col < inputSizes(inputDimWrt); ++col) {
    
    // compute the step length
    double dx = fmax(FdMinTol, fabs(FdRelTol * TempLocs.at (inputDimWrt) (col)));

    // update the locations with a forward difference step
    TempLocs.at (inputDimWrt) (col) += dx;

    // evaluate the model at the perturbed point
    auto perturbedResult = Evaluate(TempLocs);

    //fill in the difference result
    jacobian.col(col) += (perturbedResult - centerResult) / dx;

    // set the position back to what it was
    TempLocs.at (inputDimWrt) (col) = input.at (inputDimWrt) (col);
  }

  return jacobian;
}




Eigen::VectorXd ModPiece::JacobianActionByFD(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt)
{
  return JacobianActionByFD(input,target,inputDimWrt,FdRelTolDefault,FdMinTolDefault); 
}

Eigen::VectorXd ModPiece::JacobianActionByFD(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & target,
                                      int const                           inputDimWrt,
                                      double                              FdRelTol,
                                      double                              FdMinTol)
{
  if (isRandom) {
    std::cerr << "In " << GetName() << ", cannot perform finite difference JacobianAction on random ModPiece.\n";
    assert(!isRandom); //Can't use the default methods on random ModPieces
  }
  
  assert(FdRelTol > 0.0 && FdMinTol > 0);

  //test the inputs
  CheckInputs(input);
  assert(inputDimWrt < inputSizes.rows());
  assert(target.size() == inputSizes(inputDimWrt));

  std::vector<Eigen::VectorXd> TempLocs = input;

  auto centerResult = Evaluate(input);

  //start the jacobian
  VectorXd result = VectorXd::Zero(outputSize);
    
  // compute the step length
  double dx = fmax(FdMinTol, fabs(FdRelTol * TempLocs.at (inputDimWrt).maxCoeff()));

  // update the locations with a forward difference step
  TempLocs.at (inputDimWrt) += dx*target;

  // evaluate the model at the perturbed point
  auto perturbedResult = Evaluate(TempLocs);

  return (perturbedResult-result)/dx;
}






Eigen::MatrixXd ModPiece::HessianByFD(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt)
{
  return HessianByFD(input,sensitivity,inputDimWrt,FdRelTolDefault,FdMinTolDefault);
}
Eigen::MatrixXd ModPiece::HessianByFD(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt,
                                      double                              FdRelTol,
                                      double                              FdMinTol)
{
  if (isRandom) {
    std::cerr << "In " << GetName() << ", cannot perform finite difference Hessian on random ModPiece.\n";
    assert(!isRandom); //Can't use the default methods on random ModPieces
  }
  int inDim = inputSizes(inputDimWrt);

  // either use the input or fall back to the class member defaults
  assert(FdRelTol > 0 && FdMinTol > 0);

  //test the inputs
  CheckInputs(input);
  assert(outputSize == sensitivity.rows());
  assert(inputDimWrt < inputSizes.rows());

  std::vector<Eigen::VectorXd> TempLocs = input;

  Eigen::MatrixXd Hessian = Eigen::MatrixXd::Zero(inDim, inDim);

  for (int i = 0; i < outputSize; ++i) {
    if (std::abs<double>(sensitivity(i)) > std::numeric_limits<double>::epsilon()) {
      Eigen::VectorXd gradSens = Eigen::VectorXd::Zero(outputSize);
      gradSens(i) = 1.0;

      // first derivative of each output wrt to input i
      Eigen::VectorXd grad = Gradient(input, gradSens, inputDimWrt);

      for (int j = 0; j < inDim; ++j) {
        // compute the step length
        double dx = std::max<double>(FdMinTol, std::abs<double>(FdRelTol * TempLocs.at (inputDimWrt) (j)));

        // update the locations with a forward difference step
        TempLocs.at (inputDimWrt) (j) = input.at (inputDimWrt) (j) + dx;

        // compute the gradient at this point
        Eigen::VectorXd perturbedGrad = Gradient(TempLocs, gradSens, inputDimWrt);
        for (int kk = 0; kk < inDim; ++kk) {
          if (std::abs<double>(perturbedGrad(kk) - grad(kk)) > std::numeric_limits<double>::epsilon()) {
            Hessian(j, kk) += sensitivity(i) * (perturbedGrad(kk) - grad(kk)) / dx;
          }
        }
        //Hessian.row(j) = sensitivity(i) * (perturbedGrad - grad).transpose() / dx;

        TempLocs.at (inputDimWrt) (j) = input.at (inputDimWrt) (j);
      }
    }
  }

  return Hessian;
}

#if MUQ_PYTHON == 1

boost::python::list ModPiece::PyGradientByFD(boost::python::list const& inputs,
                                             boost::python::list const& sensIn,
                                             int const                  inputDimWrt)
{
  vector<Eigen::VectorXd> eigenInputs = PythonListToVector(inputs);
    assert((int)eigenInputs.size() == inputSizes.size());

  Eigen::VectorXd sens = GetEigenVector<Eigen::VectorXd>(sensIn);

  return GetPythonVector<Eigen::VectorXd>(GradientByFD(eigenInputs, sens, inputDimWrt));
}

boost::python::list ModPiece::PyJacobianByFD(boost::python::list const& inputs,
                                             int const                  inputDimWrt)
{
  vector<Eigen::VectorXd> eigenInputs = PythonListToVector(inputs);
  assert((int)eigenInputs.size() == inputSizes.size());
  
  return GetPythonMatrix(JacobianByFD(eigenInputs, inputDimWrt));
}


boost::python::list ModPiece::PyHessianByFD(boost::python::list const& inputs,
                                            boost::python::list const& sensIn,
                                            int const                  inputDimWrt)
{
  vector<Eigen::VectorXd> eigenInputs = PythonListToVector(inputs);
    assert((int)eigenInputs.size() == inputSizes.size());
  Eigen::VectorXd sens = GetEigenVector<Eigen::VectorXd>(sensIn);

  return GetPythonMatrix(HessianByFD(eigenInputs, sens, inputDimWrt));
}

#endif // if MUQ_PYTHON == 1

Eigen::MatrixXd ModPiece::AssembleJacobian(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  if (isRandom) {
    std::cerr << "In " << GetName() << ", cannot assemble Jacobian from gradient on random ModPiece.\n";
    assert(!isRandom); //Can't use the default methods on random ModPieces
  }

  int inDim  = inputSizes(inputDimWrt);
  int outDim = outputSize;

  Eigen::MatrixXd jacobian = Eigen::MatrixXd::Zero(outDim, inDim);

  for (int i = 0; i < outDim; ++i) {
    Eigen::VectorXd sens = Eigen::VectorXd::Zero(outDim);
    sens(i) = 1.0;

    Eigen::VectorXd grad = Gradient(input, sens, inputDimWrt);
    jacobian.row(i) = grad.transpose();
  }

  return jacobian;
}

Eigen::VectorXd ModPiece::GradientByJacobian(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & sensitivity,
                                             int const                           inputDimWrt)
{
  return Jacobian(input, inputDimWrt).transpose() * sensitivity;
}

Eigen::VectorXd ModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt)
{
  if (hasDirectGradient) {
    std::cerr << "In " << GetName() <<
      ", hasDirectGradient is true, but the default GradientImpl function is being called.\n\tNOTE: Even if you want to compute the Gradient from the jacobian, the hasDirectGradient flag should be false.\n";
    assert(!hasDirectGradient); //shouldn't here if there's a direct gradient provided
  }

  if (hasDirectJacobian) {
    return GradientByJacobian(input, sensitivity, inputDimWrt);
  } else {
    return GradientByFD(input, sensitivity, inputDimWrt);
  }
}

Eigen::MatrixXd ModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  if (hasDirectJacobian) {
    std::cerr << "In " << GetName() <<
      ", hasDirectJacobian is true, but the default JacobianImpl function is being called.\n";
    assert(!hasDirectJacobian); //shouldn't here if there's a direct jacobian provided
  }

  if(hasDirectGradient){
    return AssembleJacobian(input, inputDimWrt);
  }else{
    return JacobianByFD(input,inputDimWrt); 
  } 
}

Eigen::VectorXd ModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt)
{
  if (hasDirectJacobianAction) {
    std::cerr << "In " << GetName() <<
      ", hasDirectJacobianAction is true, but the default JacobianActionImpl function is being called.\n";
    assert(!hasDirectJacobianAction); //shouldn't here if there's a direct jacobian provided
  }

  if(hasDirectJacobian){
    return Jacobian(input, inputDimWrt) * target;
  }else{
    return JacobianActionByFD(input,target,inputDimWrt);  
  }
  
}

Eigen::MatrixXd ModPiece::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt)
{
  if (hasDirectHessian) {
    std::cerr << "In " << GetName() <<
      ", hasDirectHessian is true, but the default HessianImpl function is being called.\n";
    assert(!hasDirectHessian); //shouldn't here if there's a direct jacobian provided
  }

  return HessianByFD(input, sensitivity, inputDimWrt);
}

void ModPiece::InvalidateCache()
{
  //just make a new one, which is a valid state for it
  oneStepCache->ClearCache();
}

void ModPiece::SetName(std::string const& name)
{
  myName = name;
}

double ModPiece::GetRunTime(const std::string& method) const
{
  const double toMilli = 1.0e-6;

  if (method.compare("Evaluate") == 0) {
    return (numEvalCalls == 0) ? -1.0 : toMilli *static_cast<double>(evalTime) / static_cast<double>(numEvalCalls);
  } else if (method.compare("Gradient") == 0) {
    return (numGradCalls == 0) ? -1.0 : toMilli *static_cast<double>(gradTime) / static_cast<double>(numGradCalls);
  } else if (method.compare("Jacobian") == 0) {
    return (numJacCalls == 0) ? -1.0 : toMilli *static_cast<double>(jacTime) / static_cast<double>(numJacCalls);
  } else if (method.compare("JacobianAction") == 0) {
    return (numJacActCalls ==
            0) ? -1.0 : toMilli *static_cast<double>(jacActTime) / static_cast<double>(numJacActCalls);
  } else if (method.compare("Hessian") == 0) {
    return (numHessCalls == 0) ? -1.0 : toMilli *static_cast<double>(hessTime) / static_cast<double>(numHessCalls);
  } else {
    assert(method.compare("Evaluate") == 0 || method.compare("Gradient") == 0 || method.compare(
             "Jacobian") == 0 || method.compare("JacobianAction") == 0 || method.compare("Hessian") == 0);
    return -999.0;
  }
}

unsigned long int ModPiece::GetNumCalls(const std::string& method) const
{
  if (method.compare("Evaluate") == 0) {
    return numEvalCalls;
  } else if (method.compare("Gradient") == 0) {
    return numGradCalls;
  } else if (method.compare("Jacobian") == 0) {
    return numJacCalls;
  } else if (method.compare("JacobianAction") == 0) {
    return numJacActCalls;
  } else if (method.compare("Hessian") == 0) {
    return numHessCalls;
  } else {
    assert(method.compare("Evaluate") == 0 || method.compare("Gradient") == 0 || method.compare(
             "Jacobian") == 0 || method.compare("JacobianAction") == 0 || method.compare("Hessian") == 0);
    return -999;
  }
}

void ModPiece::ResetCallTime()
{
  numEvalCalls   = 0;
  numGradCalls   = 0;
  numJacCalls    = 0;
  numJacActCalls = 0;
  numHessCalls   = 0;

  evalTime   = 0;
  gradTime   = 0;
  jacTime    = 0;
  jacActTime = 0;
  hessTime   = 0;
}

// *********************************************************************
// One step cache routines
// *********************************************************************


boost::optional<Eigen::VectorXd> ModPieceOneStepCache::GetCachedEvaluate(std::vector<Eigen::VectorXd> const& input)
const
{
  if (InputMatchesCache(input) && cachedOutput) {
    return boost::optional<Eigen::VectorXd>(*cachedOutput);
  } else {
    return boost::optional<VectorXd>();
  }
}

void ModPieceOneStepCache::SetCachedEvaluate(std::vector<Eigen::VectorXd> const& input, Eigen::VectorXd const& result)
{
  if (InputMatchesCache(input)) { //if input matches, just set the result
    cachedOutput = boost::optional<Eigen::VectorXd>(result);
  } else {                        //else we have to clear the cache and put these in
    ClearCache();
    cachedInput  = input;
    cachedOutput = boost::optional<Eigen::VectorXd>(result);
  }
  initialized = true;
}

boost::optional<Eigen::VectorXd> ModPieceOneStepCache::GetCachedGradient(std::vector<Eigen::VectorXd> const& input,
                                                                         Eigen::VectorXd const             & sensitivity,
                                                                         int                                 inputDimWrt)
const
{
  //if the input matches and there's something stored keep looking
  if (InputMatchesCache(input) && (gradientMap.find(inputDimWrt) != gradientMap.end())) {
    auto sensGradPair = gradientMap.at(inputDimWrt);

    //cached result also valid if the sensitivity exactly matches
    if (MatrixEqual(sensGradPair.first, sensitivity)) {
      return boost::optional<Eigen::VectorXd>(sensGradPair.second);
    }
  }

  return boost::optional<VectorXd>();
}

void ModPieceOneStepCache::SetCachedGradient(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & sensitivity,
                                             int                                 inputDimWrt,
                                             Eigen::VectorXd const             & gradient)
{
  if (InputMatchesCache(input)) { //if input matches, just set the result
    gradientMap[inputDimWrt] = make_pair(sensitivity, gradient);
  } else {                        //else we have to clear the cache and put these in
    ClearCache();
    cachedInput              = input;
    gradientMap[inputDimWrt] = make_pair(sensitivity, gradient);
  }
  initialized = true;
}

boost::optional<Eigen::MatrixXd> ModPieceOneStepCache::GetCachedJacobian(std::vector<Eigen::VectorXd> const& input,
                                                                         int                                 inputDimWrt)
const
{
  //if the input matches and there's something stored keep looking
  if (InputMatchesCache(input) && (jacobianMap.find(inputDimWrt) != jacobianMap.end())) {
    return boost::optional<MatrixXd>(jacobianMap.at(inputDimWrt));
  }

  return boost::optional<MatrixXd>();
}

void ModPieceOneStepCache::SetCachedJacobian(std::vector<Eigen::VectorXd> const& input,
                                             int                                 inputDimWrt,
                                             Eigen::MatrixXd const             & jacobian)
{
  if (InputMatchesCache(input)) { //if input matches, just set the result
    jacobianMap[inputDimWrt] = jacobian;
  } else {                        //else we have to clear the cache and put these in
    ClearCache();
    cachedInput              = input;
    jacobianMap[inputDimWrt] = jacobian;
  }
  initialized = true;
}

boost::optional<Eigen::VectorXd> ModPieceOneStepCache::GetCachedJacobianAction(
  std::vector<Eigen::VectorXd> const& input,
  Eigen::VectorXd const             & target,
  int                                 inputDimWrt) const
{
  //if the input matches and there's something stored keep looking
  if (InputMatchesCache(input) && (jacobianActionMap.find(inputDimWrt) != jacobianActionMap.end())) {
    auto actionGradPair = jacobianActionMap.at(inputDimWrt);

    //cached result also valid if the sensitivity exactly matches
    if (MatrixEqual(actionGradPair.first, target)) {
      return boost::optional<Eigen::VectorXd>(actionGradPair.second);
    }
  }

  return boost::optional<VectorXd>();
}

void ModPieceOneStepCache::SetCachedJacobianAction(std::vector<Eigen::VectorXd> const& input,
                                                   Eigen::VectorXd const             & target,
                                                   int                                 inputDimWrt,
                                                   Eigen::VectorXd const             & action)
{
  if (InputMatchesCache(input)) { //if input matches, just set the result
    jacobianActionMap[inputDimWrt] = make_pair(target, action);
  } else {                        //else we have to clear the cache and put these in
    ClearCache();
    cachedInput                    = input;
    jacobianActionMap[inputDimWrt] = make_pair(target, action);
  }
  initialized = true;
}

boost::optional<Eigen::MatrixXd> ModPieceOneStepCache::GetCachedHessian(std::vector<Eigen::VectorXd> const& input,
                                                                        Eigen::VectorXd const             & sensitivity,
                                                                        int                                 inputDimWrt)
const
{
  if (InputMatchesCache(input) && (hessianMap.find(inputDimWrt) != hessianMap.end())) {
    auto sensHessianPair = hessianMap.at(inputDimWrt);

    //cached result also valid if the sensitivity exactly matches
    if (MatrixEqual(sensHessianPair.first, sensitivity)) {
      return boost::optional<Eigen::MatrixXd>(sensHessianPair.second);
    }
  }

  return boost::optional<Eigen::MatrixXd>();
}

void ModPieceOneStepCache::SetCachedHessian(std::vector<Eigen::VectorXd> const& input,
                                            Eigen::VectorXd const             & sensitivity,
                                            int                                 inputDimWrt,
                                            Eigen::MatrixXd const             & hessian)
{
  if (InputMatchesCache(input)) { //if input matches, just set the result
    hessianMap[inputDimWrt] = make_pair(sensitivity, hessian);
  } else {                        //else we have to clear the cache and put these in
    ClearCache();
    cachedInput             = input;
    hessianMap[inputDimWrt] = make_pair(sensitivity, hessian);
  }
  initialized = true;
}

bool ModPieceOneStepCache::InputMatchesCache(std::vector<Eigen::VectorXd> const& input) const
{
  bool isValid = initialized && (input.size() == cachedInput.size()); //true until proven guilty

  for (int i = 0; isValid && i < static_cast<int>(input.size()); ++i) {
    isValid = isValid && MatrixEqual(input.at(i), cachedInput.at(i));
  }

  return isValid;
}

void ModPieceOneStepCache::ClearCache()
{
  initialized = false;
  cachedInput.clear();
  cachedOutput = VectorXd();
  gradientMap.clear();
  jacobianMap.clear();
  jacobianActionMap.clear();
}

#if MUQ_PYTHON == 1

boost::python::list ModPiece::GetInputSizes() const
{
  return GetPythonVector<Eigen::VectorXi>(inputSizes);
}

#endif // if MUQ_PYTHON == 1

string ModPiece::GetName() const
{
  if (myName.compare("") == 0) {
    int status;
    stringstream ss;
    ss << abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status) << "_" << id;
    return ss.str();
  }
  
  return myName;
}

