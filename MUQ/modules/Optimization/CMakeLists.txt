add_subdirectory(test)
add_subdirectory(src)

#pass the test sources to the higher level
set(all_gtest_sources ${all_gtest_sources} ${Test_Optimization_Sources} PARENT_SCOPE)
