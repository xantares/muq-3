
#include "MUQ/Optimization/Algorithms/BarrierPenalty.h"
#include "MUQ/Optimization/Problems/BarrierPenaltyProb.h"

using namespace muq::Optimization;
using namespace std;

REGISTER_OPT_DEF_TYPE(BarrierPenalty) BarrierPenalty::BarrierPenalty(std::shared_ptr<OptProbBase> ProbPtr,
                                                                     boost::property_tree::ptree& properties) :
  OptAlgBase(ProbPtr,
             properties,
             false,
             true,
             false,
             true,
             true,
             true,
             true,
             false),
  uncProperties(properties)
{
  // get penalty specific parameters
  barrierCoeff = properties.get("Opt.Barrier.StartCoeff", 1);
  barrierScale = properties.get("Opt.Barrier.CoeffScale", 0.5);

  penaltyCoeff = properties.get("Opt.Penalty.StartCoeff", 0.5);
  penaltyScale = properties.get("Opt.Penalty.CoeffScale", 2.0);

  maxOuterIts = properties.get("Opt.BarrierPenalty.OuterIts", 100);

  assert(barrierCoeff > 0.0);
  assert(barrierScale < 1.0);
  assert(penaltyCoeff > 0);
  assert(penaltyScale > 1.0);
}

/** Solve the optimization problem using x0 as a starting point for the iteration. */
Eigen::VectorXd BarrierPenalty::solve(const Eigen::VectorXd& x0)
{
  // current position
  Eigen::VectorXd xc = x0;

  bool   optimalFlag  = false;
  bool   feasibleFlag = false;
  double dx           = 10;
  int    it           = 0;

  while (((!feasibleFlag) || (!optimalFlag)) && (it < maxOuterIts)) {
    if (verbose > 0) {
      std::cout << "Solving Barrier subproblem...\n";
      std::cout << "\tOuter iteration " << it << std::endl;
    }

    // created the penalized optimization problem
    shared_ptr<OptProbBase> BarProb = make_shared<BarrierPenaltyProb>(OptProbPtr, barrierCoeff, penaltyCoeff);

    // create a unconstrained solver for this problem
    shared_ptr<OptAlgBase> uncSolver = OptAlgBase::Create(BarProb, uncProperties);


    // solve the unconstrainted problem
    Eigen::VectorXd newX = uncSolver->solve(xc);
    status = uncSolver->GetStatus();
    if (verbose > 0) {
      std::cout << "\tInner iteration terminated with status " << status << std::endl;
    }

    // if we had a successful solve, update the current position to the min of the unconstrained problem
    if (status >= 0) {
      // check to see how much we've changed
      dx = (newX - xc).norm();

      // update the current iterate with the results of the subproblem optimization
      xc = newX;
    } else {
      // we failed, so just return what we have and leave the failed status flag
      return newX;
    }

    feasibleFlag = true;
    Eigen::VectorXd cVals;
    if (OptProbPtr->NumEqualities() > 0) {
      cVals = OptProbPtr->equalityConsts.eval(xc);

      if (verbose > 0) {
        std::cout << "\tCurrent equality residual: " << cVals.norm() << std::endl << std::endl;
      }

      // check for equality feasibility
      if (cVals.norm() > ctol) {
        feasibleFlag = false;
      }
    }

    // check for convergence in inequality constraints and update mu if we need to continue
    if ((OptProbPtr->NumInequalities() > 0) && (verbose > 0)) {
      // check for convergence in equality constraints
      Eigen::VectorXd inVals;

      inVals = OptProbPtr->inequalityConsts.eval(xc);

      if (verbose > 0) {
        std::cout << "\tCurrent inequality residual: " << inVals.maxCoeff() << std::endl << std::endl;
      }
    }

    // if we are not feasible, but haven't changed position much, let's say we are "optimal"
    if (dx < xtol) {
      optimalFlag = true;
    }

    // update the barrier parameter
    barrierCoeff *= barrierScale;

    // update the penalty parameter
    penaltyCoeff *= penaltyScale;

    ++it;
  }

  return xc;
}
