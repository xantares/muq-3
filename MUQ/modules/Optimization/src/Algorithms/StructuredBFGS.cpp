
#include "MUQ/Optimization/Algorithms/StructuredBFGS.h"

#include <Eigen/Dense>

#include "MUQ/Utilities/LinearOperators/Operators/SymmetricOpBase.h"
#include "MUQ/Utilities/LinearOperators/Solvers/CG.h"

using namespace muq::Optimization;
using namespace muq::Utilities;
using namespace std;

REGISTER_OPT_DEF_TYPE(StructuredBFGS)
/** parameter list constructor */
StructuredBFGS::StructuredBFGS(shared_ptr<OptProbBase>      ProbPtr,
                               boost::property_tree::ptree& properties) : GradLineSearch(ProbPtr,
                                                                                         properties), stepCalls(0),
                                                                          ApproxHess(1e-4 * Eigen::MatrixXd::Identity(
                                                                                       ProbPtr
                                                                                       ->
                                                                                       GetDim(),
                                                                                       ProbPtr
                                                                                       ->
                                                                                       GetDim()))
{}


/** step function, return the BFGS direction*/
Eigen::VectorXd StructuredBFGS::step()
{
  Eigen::VectorXd output;

  // find the objective function and gradient
  fold = OptProbPtr->grad(xc, gc);

  // check to see if this is the first call to the step function, if this is not the first call to step, we can update
  // the Hessian
  if (stepCalls > 0) {
    // compute the change in state
    Eigen::VectorXd sk = xc - OldState;

    // compute the change in gradient
    Eigen::VectorXd yk = gc - OldGrad - OptProbPtr->applyHess(xc, sk);

    // check to see if we need a restart
    if (yk.dot(sk) < 1e-10) {
      ApproxHess = Eigen::MatrixXd::Identity(OptProbPtr->GetDim(), OptProbPtr->GetDim());
    } else {
      // use the Sherman-Morrison formula to update the approximate inverse
      Eigen::VectorXd Bs = ApproxHess.selfadjointView<Eigen::Lower>() * sk;
      ApproxHess.selfadjointView<Eigen::Lower>().rankUpdate(yk, 1.0 / yk.dot(sk));
      ApproxHess.selfadjointView<Eigen::Lower>().rankUpdate(Bs, -1.0 / sk.dot(Bs));
    }
  }

  ++stepCalls;

  // if it is, we cannot update the pproximate hessian inverse and we just use what we have:
  output = -1.0 * ApplyFullApproxInverse(xc, gc);

  // store the current position and gradient
  OldState = xc;
  OldGrad  = gc;

  return output;
}

class ApproxHessOperator : public SymmetricOpBase {
public:

  ApproxHessOperator(const Eigen::VectorXd       *xcIn,
                     const Eigen::MatrixXd       *ApproxHessIn,
                     std::shared_ptr<OptProbBase> OptProbPtrIn) : SymmetricOpBase(xcIn->size()), xc(xcIn), ApproxHess(
                                                                    ApproxHessIn),
                                                                  OptProbPtr(OptProbPtrIn)
  {}

  virtual Eigen::VectorXd apply(const Eigen::VectorXd& x) override
  {
    return ApproxHess->selfadjointView<Eigen::Lower>() * x + OptProbPtr->applyHess(*xc, x);
  }

private:

  const Eigen::VectorXd *xc;
  const Eigen::MatrixXd *ApproxHess;
  std::shared_ptr<OptProbBase> OptProbPtr;
};

Eigen::VectorXd StructuredBFGS::ApplyFullApproxInverse(const Eigen::VectorXd& xc, const Eigen::VectorXd& y)
{
  return CG(make_shared<ApproxHessOperator>(&xc, &ApproxHess, OptProbPtr)).solve(y);
}
