
#include "MUQ/Optimization/Constraints/BoundConstraint.h"


using namespace muq::Optimization;
using namespace std;


/** Construct the bound constraint */
BoundConstraint::BoundConstraint(int dimIn, int i, double valIn, bool uplo) : ConstraintBase(dimIn, 1), dimSpec(i), val(
                                                                                valIn),
                                                                              isLow(uplo)
{}

/** Evaluate the linear constraint. */
Eigen::VectorXd BoundConstraint::eval(const Eigen::VectorXd& xc)
{
  Eigen::VectorXd output(1);

  output << (isLow ? -1 : 1) * (xc[dimSpec] - val);
  return output;
}

/** Evaluate the action of the Jacobian of this constraint on a vector.  */
Eigen::VectorXd BoundConstraint::ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
{
  Eigen::VectorXd output = Eigen::VectorXd::Zero(DimIn);

  output[dimSpec] = (isLow ? -1 : 1) * vecIn[0];
  return output;
}

Eigen::MatrixXd BoundConstraint::getHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& sensitivity)
{
  return Eigen::MatrixXd::Zero(DimIn, DimIn);
}

/** Return the dimension this bound operates on. */
int BoundConstraint::WhichDim() const
{
  return dimSpec;
}

/** Get the value of this bound. */
double BoundConstraint::GetVal() const
{
  return val;
}

/** Is this a lower bound? */
bool BoundConstraint::isLower() const
{
  return isLow;
}
