
#include "MUQ/Optimization/Problems/AugLagProb.h"
#include <iostream>

using namespace muq::Optimization;
using namespace std;

/**Construct the augmented lagrangian subproblem using the original objective and constraints defined in prob, as well
 *  as the penalty and approximate lagrange multipliers given in muIn and lambdaIn.*/
AugLagProb::AugLagProb(std::shared_ptr<OptProbBase> prob,
                       const Eigen::VectorXd      & muIn,
                       const Eigen::VectorXd      & lambdaIn,
                       double                       rhoIn) : OptProbBase(prob->GetDim()), mu(muIn), lambda(lambdaIn),
                                                             baseProb(prob),
                                                             rho(rhoIn)
{}

/** Evaluate the augmented-lagrangian. */
double AugLagProb::eval(const Eigen::VectorXd& xc)
{
  // first, get the objective value
  double fval = baseProb->eval(xc);

  // now get the equality constraint values and add them to the objective
  if (baseProb->NumEqualities() > 0) {
    Eigen::VectorXd cVals = baseProb->equalityConsts.eval(xc);
    fval += (rho / 2) * ((cVals + lambda / rho).squaredNorm());
  }

  // now get the inequality constraint values and add them to the objective
  if (baseProb->NumInequalities() > 0) {
    Eigen::VectorXd inVals = baseProb->inequalityConsts.eval(xc);
    fval +=
      (rho / 2) * ((inVals + mu / rho).cwiseMax(Eigen::VectorXd::Zero(baseProb->NumInequalities()))).squaredNorm();
  }

  // return the augmented function value
  return fval;
}

/** Evaluate the gradient. */
double AugLagProb::grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient)
{
  // first, get the objective value and gradient of the unconstrained problem
  double fval = baseProb->grad(xc, gradient);

  // now, add the gradient of the equality constraint portion of the Augmented Lagrangian
  if (baseProb->NumEqualities() > 0) {
    // get value of constraints and add to objective
    Eigen::VectorXd cVals = baseProb->equalityConsts.eval(xc);
    fval += (rho / 2) * ((cVals + lambda / rho).squaredNorm());

    // get sensitivity of augmented objective to output of constraint
    Eigen::VectorXd Sens = rho * cVals + lambda;

    // get sensitivity of augmented objective to input of constraint
    gradient += baseProb->equalityConsts.ApplyJacTrans(xc, Sens);
  }

  // now, add the gradient of the inequality constraint portion of the Augmented Lagrangian
  if (baseProb->NumInequalities() > 0) {
    // get the value of the constraints and add to objective
    Eigen::VectorXd inVals = baseProb->inequalityConsts.eval(xc);
    fval +=
      (rho / 2) * ((inVals + mu / rho).cwiseMax(Eigen::VectorXd::Zero(baseProb->NumInequalities()))).squaredNorm();

    // get sensitivity of augmented objective to output of these inequality constraints
    Eigen::VectorXd Sens = rho * (inVals + mu / rho).cwiseMax(Eigen::VectorXd::Zero(baseProb->NumInequalities()));

    // propagate this sensitivity backward to get sensitivity to constraint input
    gradient += baseProb->inequalityConsts.ApplyJacTrans(xc, Sens);
  }

  // return the augmented function value
  return fval;
}

/** Apply the Hessian of the Augmented-Lagrangian subproblem to an input vector. */
Eigen::MatrixXd AugLagProb::getHess(const Eigen::VectorXd& xc)
{
  // apply the hessian of the unconstrained problem
  Eigen::MatrixXd output = baseProb->getHess(xc);

  // now, add the gradient of the equality constraint portion of the Augmented Lagrangian
  if (baseProb->NumEqualities() > 0) {
    // get value of constraints and add to objective
    Eigen::VectorXd cVals = baseProb->equalityConsts.eval(xc);

    // get sensitivity of augmented objective to output of constraint
    Eigen::VectorXd Sens = rho * cVals + lambda;

    // get sensitivity of augmented objective to input of constraint
    output += baseProb->equalityConsts.getHess(xc, Sens);
  }

  // now, add the gradient of the inequality constraint portion of the Augmented Lagrangian
  if (baseProb->NumInequalities() > 0) {
    // get the value of the constraints and add to objective
    Eigen::VectorXd inVals = baseProb->inequalityConsts.eval(xc);

    // get sensitivity of augmented objective to output of these inequality constraints
    Eigen::VectorXd Sens = rho * (inVals + mu / rho).cwiseMax(Eigen::VectorXd::Zero(baseProb->NumInequalities()));

    // propagate this sensitivity backward to get sensitivity to constraint input
    output += baseProb->inequalityConsts.getHess(xc, Sens);
  }

  // return the augmented function value
  return output;
}

