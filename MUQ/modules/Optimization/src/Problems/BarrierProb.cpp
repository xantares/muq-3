
#include "MUQ/Optimization/Problems/BarrierProb.h"
#include <cmath>

using namespace muq::Optimization;
using namespace std;


BarrierProb::BarrierProb(std::shared_ptr<OptProbBase> prob, double                       barrierCoeffIn) : OptProbBase(
                                                                                                             prob->GetDim()),
                                                                                                           barrierCoeff(
                                                                                                             barrierCoeffIn),
                                                                                                           baseProb(prob)
{}


double BarrierProb::eval(const Eigen::VectorXd& xc)
{
  // first, get the objective value
  double fval = baseProb->eval(xc);

  // now get the inequality constraint values and add them to the objective
  if (baseProb->NumInequalities() > 0) {
    Eigen::VectorXd inVals = baseProb->inequalityConsts.eval(xc);
    fval += -1.0 * barrierCoeff * (-1.0 * inVals).array().log().sum();
  }

  // return the augmented function value
  return fval;
}

/** Evaluate the gradient. */
double BarrierProb::grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient)
{
  // first, get the objective value and gradient of the unconstrained problem
  double fval = 0; //baseProb->grad(xc,gradient);

  // now, add the gradient of the inequality constraint portion
  if (baseProb->NumInequalities() > 0) {
    // get the value of the constraints and add to objective
    Eigen::VectorXd inVals = baseProb->inequalityConsts.eval(xc);
    fval += -1.0 * barrierCoeff * (-1.0 * inVals).array().log().sum();

    // get sensitivity of augmented objective to output of these inequality constraints
    Eigen::VectorXd Sens = -1.0 *barrierCoeff *inVals.array().inverse().matrix();

    // propagate this sensitivity backward to get sensitivity to constraint input
    gradient = baseProb->inequalityConsts.ApplyJacTrans(xc, Sens);
  }

  // if we are at a feasible point, go ahead and evaluate the original objective
  Eigen::VectorXd grad2;
  if (!std::isinf(fval)) {
    fval     += baseProb->grad(xc, grad2);
    gradient += grad2;
  }

  // return the augmented function value
  return fval;
}
