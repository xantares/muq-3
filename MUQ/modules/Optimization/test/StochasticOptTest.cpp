
#include "gtest/gtest.h"

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"
#include "MUQ/Optimization/Problems/OptProbBase.h"
#include "MUQ/Utilities/RandomGenerator.h"

using namespace muq::Optimization;
using namespace muq::Utilities;
using namespace std;


/** Define a stochastic isotropic quadratic problem as a simple test of the stochastic optimization routines problem. */
class StochasticQuadFunc : public muq::Optimization::OptProbBase {
public:

  StochasticQuadFunc(int dim, int NumMCIN = 100) : muq::Optimization::OptProbBase(dim), NumMC(NumMCIN) {}

  /** Evaluate the objective. */
  virtual double eval(const Eigen::VectorXd& xc)
  {
    double fout           = 0;
    Eigen::MatrixXd noise = 0.1 * RandomGenerator::GetNormalRandomMatrix(xc.size(), NumMC);

    for (int i = 0; i < NumMC; ++i) {
      fout += 0.5 * (xc - noise.col(i)).squaredNorm();
    }

    return fout;
  }

  /** Evaluate the gradient and return the objective value*/
  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient)
  {
    double fout           = 0;
    Eigen::MatrixXd noise = 0.1 * RandomGenerator::GetNormalRandomMatrix(xc.size(), NumMC);

    gradient = Eigen::VectorXd::Zero(xc.size());

    for (int i = 0; i < NumMC; ++i) {
      Eigen::VectorXd temp = xc - noise.col(i);
      gradient += temp;
      fout     += 0.5 * temp.squaredNorm();
    }
    return fout;
  }

private:

  int NumMC;
};

// set up a gtest test fixture for unconstrained stochastic solvers
class Optimization_StochasticOpTest : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd x0;

  // a base ptree, swap the Opt.Method entry for testing different algorithms
  boost::property_tree::ptree testParams;

  // an instance of the quadratic optimization problem
  std::shared_ptr<muq::Optimization::OptProbBase> prob;

  // constructor
  Optimization_StochasticOpTest()
  {
    // set the initial condition
    x0 = Eigen::VectorXd::Ones(2);
    x0 << -1, 3;

    // std::cout << "Starting point: " << x0.transpose() << std::endl;

    // set some of the optimization parameters
    testParams.put("Opt.MaxIts", 10000);
    testParams.put("Opt.ftol", 1e-11);
    testParams.put("Opt.xtol", 1e-13);
    testParams.put("Opt.gtol", 1e-10);
    testParams.put("Opt.LineSearch.LineIts", 100);
    testParams.put("Opt.StepLength", 1);
    testParams.put("Opt.verbosity", 0);

    testParams.put("Opt.SAA.NumRuns", 100);

    testParams.put("Opt.NLOPT.MaxEvals", 100000);
    testParams.put("Opt.NLOPT.xtol_rel", 1e-12);
    testParams.put("Opt.NLOPT.xtol_abs", 1e-12);
    testParams.put("Opt.NLOPT.ftol_rel", 1e-12);
    testParams.put("Opt.NLOPT.ftol_abs", 1e-12);

    // create an instance of the optimization problem
    prob = std::make_shared<StochasticQuadFunc>(2);
  }

  // the tear-down function is called just before the constructor, and in our case is where the tests are actually
  // performed
  void TearDown()
  {
    // set up the solver
    auto Solver = muq::Optimization::OptAlgBase::Create(prob, testParams);

    // solve the optimization problem
    Eigen::VectorXd xOpt = Solver->solve(x0);

    int optStat = Solver->GetStatus();

    // we expect a successful optimization
    EXPECT_GT(optStat, 0);

    // make sure the returned optimum is what we expect
    EXPECT_NEAR(xOpt[0], 0.0, 3e-2);
    EXPECT_NEAR(xOpt[1], 0.0, 3e-2);
  }
};


TEST_F(Optimization_StochasticOpTest, SAA_SD)
{
  testParams.put("Opt.Method", "SAA");

  // set the deterministic sub-method needed by SAA
  testParams.put("Opt.SAA.Method", "SD_Line");
}

TEST_F(Optimization_StochasticOpTest, SAA_BFGS)
{
  testParams.put("Opt.Method", "SAA");

  // set the deterministic sub-method needed by SAA
  testParams.put("Opt.SAA.Method", "BFGS_Line");
}

TEST_F(Optimization_StochasticOpTest, RobbinsMonro)
{
  testParams.put("Opt.Method", "RobbinsMonro");
}
