
#ifndef _UnconstrainedTestClasses_h
#define _UnconstrainedTestClasses_h


#include "gtest/gtest.h"

#include <Eigen/Dense>

#include "MUQ/Optimization/Problems/OptProbBase.h"
#include "MUQ/Optimization/Algorithms/OptAlgBase.h"


/** Define the rosenbrock function as an example optimization problem. */
class RoseFunc : public muq::Optimization::OptProbBase {
public:

  RoseFunc() : muq::Optimization::OptProbBase(2) {}

  /** Evaluate the objective. */
  virtual double eval(const Eigen::VectorXd& xc) override
  {
    // this is a two dimensional problem
    assert(xc.size() == 2);

    return pow(1 - xc[0], 2) + 100 * pow(xc[1] - xc[0] * xc[0], 2);
  }

  /** Evaluate the gradient and return the objective value*/
  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override
  {
    gradient    = Eigen::VectorXd::Zero(2);
    gradient[0] = -400 * (xc[1] - xc[0] * xc[0]) * xc[0] - 2 * (1 - xc[0]);

    gradient[1] = 200 * (xc[1] - xc[0] * xc[0]);

    return eval(xc);
  }

  virtual Eigen::VectorXd applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
  {
    Eigen::Matrix<double, 2, 2> Hess = Eigen::Matrix<double, 2, 2>::Zero(2, 2);

    Hess(0, 0) = 1200 * pow(xc[0], 2.0) - 400 * xc[1] + 2;
    Hess(0, 1) = -400 * xc[0];
    Hess(1, 0) = -400 * xc[0];
    Hess(1, 1) = 200;

    return Hess.lu().solve(vecIn);
  }
};


/** Define an isotropic quadratic problem as a simple test optimization problem. */
class IsoQuadFunc : public muq::Optimization::OptProbBase {
public:

  IsoQuadFunc(int dim) : muq::Optimization::OptProbBase(dim) {}

  /** Evaluate the objective. */
  virtual double eval(const Eigen::VectorXd& xc)
  {
    return 0.5 * xc.squaredNorm();
  }

  /** Evaluate the gradient and return the objective value*/
  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient)
  {
    gradient = xc;
    return 0.5 * xc.squaredNorm();
  }

  virtual Eigen::VectorXd applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
  {
    return vecIn;
  }
};

// set up a gtest test fixture for unconstrained deterministic solvers
class Optimization_RoseLineSearchTest : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd x0;

  // a base ptree, swap the Opt.Method entry for testing different algorithms
  boost::property_tree::ptree testParams;

  // an instance of the quadratic optimization problem
  std::shared_ptr<muq::Optimization::OptProbBase> prob;

  // constructor
  Optimization_RoseLineSearchTest()
  {
    // set the initial condition
    x0 = Eigen::VectorXd::Ones(2);
    x0 << -1, 3;

    // std::cout << "Starting point: " << x0.transpose() << std::endl;

    // set some of the optimization parameters
    testParams.put("Opt.MaxIts", 10000);
    testParams.put("Opt.ftol", 1e-11);
    testParams.put("Opt.xtol", 1e-13);
    testParams.put("Opt.gtol", 1e-10);
    testParams.put("Opt.LineSearch.LineIts", 100);
    testParams.put("Opt.StepLength", 1);
    testParams.put("Opt.verbosity", 0);

    testParams.put("Opt.NLOPT.MaxEvals", 100000);
    testParams.put("Opt.NLOPT.xtol_rel", 1e-12);
    testParams.put("Opt.NLOPT.xtol_abs", 1e-12);
    testParams.put("Opt.NLOPT.ftol_rel", 1e-12);
    testParams.put("Opt.NLOPT.ftol_abs", 1e-12);

    // create an instance of the optimization problem
    prob = std::make_shared<RoseFunc>();
  }

  // the tear-down function is called just before the constructor, and in our case is where the tests are actually
  // performed
  void TearDown()
  {
    // set up the solver
    auto Solver = muq::Optimization::OptAlgBase::Create(prob, testParams);

    // solve the optimization problem
    Eigen::VectorXd xOpt = Solver->solve(x0);

    int optStat = Solver->GetStatus();

    // we expect a successful optimization
    EXPECT_GT(optStat, 0);

    // make sure the returned optimum is what we expect
    EXPECT_NEAR(xOpt[0], 1, 1e-2);
    EXPECT_NEAR(xOpt[1], 1, 1e-2);
  }
};

// set up a gtest test fixture for unconstrained deterministic solvers
class Optimization_QuadLineSearchTest : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd x0;

  // hold the optimization status
  int optStat;

  // a base ptree, swap the Opt.Method entry for testing different algorithms
  boost::property_tree::ptree testParams;

  // an instance of the quadratic optimization problem
  std::shared_ptr<muq::Optimization::OptProbBase> prob;

  int dim;

  // constructor
  Optimization_QuadLineSearchTest(int dimIn = 20) : dim(dimIn)
  {
    // set the initial condition
    x0 = Eigen::VectorXd::Random(dim);

    // set some of the optimization parameters
    testParams.put("Opt.MaxIts", 10000);
    testParams.put("Opt.ftol", 1e-8);
    testParams.put("Opt.xtol", 1e-8);
    testParams.put("Opt.LineSearch.LineIts", 100);
    testParams.put("Opt.verbosity", 0);

    testParams.put("Opt.NLOPT.MaxEvals", 100000);
    testParams.put("Opt.NLOPT.xtol_rel", 1e-12);
    testParams.put("Opt.NLOPT.xtol_abs", 1e-12);
    testParams.put("Opt.NLOPT.ftol_rel", 1e-12);
    testParams.put("Opt.NLOPT.ftol_abs", 1e-12);

    // create an instance of the optimization problem
    prob = std::make_shared<IsoQuadFunc>(dim);
  }

  // the tear-down function is called just before the constructor, and in our case is where the tests are actually
  // performed
  void TearDown()
  {
    // set up the solver
    auto Solver = muq::Optimization::OptAlgBase::Create(prob, testParams);

    // solve the optimization problem
    Eigen::VectorXd xOpt = Solver->solve(x0);
    int optStat          = Solver->GetStatus();

    // we expect a successful optimization
    EXPECT_GT(optStat, 0);

    // make sure the returned optimum is what we expect
    for (int i = 0; i < dim; ++i) {
      EXPECT_NEAR(xOpt[i], 0, 1e-4);
    }
  }
};

#endif // ifndef _UnconstrainedTestClasses_h
