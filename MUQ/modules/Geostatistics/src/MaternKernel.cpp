
#include "MUQ/Geostatistics/MaternKernel.h"


#include <assert.h>

#include <boost/property_tree/ptree.hpp>

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/bessel.hpp>

using namespace muq::Geostatistics;

/** Construct the kernel from the characteristic length scale, power, and variance.
 */
MaternKernel::MaternKernel(double RhoIn, double NuIn, double VarIn) : IsotropicCovKernel(3), rho(RhoIn), nu(NuIn), var(
                                                                        VarIn)
{}

/** This funciton evaluates the Matern kernel using only the distance between two points.
 */
double MaternKernel::DistKernel(double d) const
{
  if (d == 0) {
    return var;
  } else {
    // compute the bessel function part
    double Kv = boost::math::cyl_bessel_k(nu, d / rho);

    // compute the gamma funciton part
    double Gv = boost::math::tgamma(nu);

    // compute the kernel
    return 2.0 * var / Gv * pow(d / (2.0 * rho), nu) * Kv;
  }
}

/** Set the covariance kernel parameters from a vector of parameters. */
void MaternKernel::SetParms(const Eigen::VectorXd& theta)
{
  // check the dimension
  assert(theta.size() == 3);

  // set the parameters
  rho = theta[0];
  nu  = theta[1];
  var = theta[2];
}

void MaternKernel::GetGrad(double d, Eigen::VectorXd& grad) const
{
  assert(false); //the above need work before this is viable.
  grad = Eigen::VectorXd::Zero(3);
}

