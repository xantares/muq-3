
#include "MUQ/Inference/python/InferenceProblemPython.h"
#include "MUQ/Inference/python/MarginalSamplingProblemPython.h"
#include "MUQ/Inference/python/MetricPython.h"

#include "MUQ/Inference/python/MCMCBasePython.h"
// #include "MUQ/Inference/MCMC/MMALA.h"
// #include "MUQ/Inference/MCMC/PreMALA.h"
// #include "MUQ/Inference/MCMC/ParallelChainMCMC.h"
// #include "MUQ/Inference/MCMC/TemperedMCMC.h"
// #include "MUQ/Inference/MCMC/DRAM.h"
// #include "MUQ/Inference/MCMC/StanHMC.h"
// #include "MUQ/Inference/MCMC/StanNUTS.h"

//#include "MUQ/Inference/python/BasisFactoryPython.h"
//#include "MUQ/Inference/python/TransportMapBasisPython.h"
//#include "MUQ/Inference/TransportMaps/TransportMap.h"
//#include "MUQ/Inference/python/LinearBasisPython.h"
//#include "MUQ/Inference/python/StaticPolynomialsPython.h"
//#include "MUQ/Inference/python/PolynomialBasisPython.h"
//#include "MUQ/Inference/python/MapUpdateManagerPython.h"

#include "MUQ/Inference/python/MAPbasePython.h"

#ifdef MUQ_USE_NLOPT
#include "MUQ/Optimization/Algorithms/NLOPTwrapper.h"
#endif

#include "MUQ/Optimization/Algorithms/BFGS.h"
#include "MUQ/Optimization/Algorithms/StructuredBFGS.h"
#include "MUQ/Optimization/Algorithms/Newton.h"

#include "MUQ/Inference/python/ImportanceSamplerPython.h"

using namespace muq::Inference;

  BOOST_PYTHON_MODULE(libmuqInference)
{
  ExportProblemClasses();
  
#ifdef MUQ_USE_NLOPT
  ExportMarginalSamplingProblem();
#endif //#ifdef MUQ_USE_NLOPT
  
  ExportMetric();

  ExportMCMCBase();

  ExportMAPBase();

//  ExportBasisSet();
//  ExportPyBasisFactory();
//  ExportTransportMapBasis();
//  ExportConstantBasis();
//  ExportLinearBasis();
//  ExportStaticHermitePoly();
//  ExportStaticLegendrePoly();
//  ExportStaticMonomial();
//  ExportPolyBasis();
//  ExportGenPolyBasis<StaticHermitePoly>( "HermitePolynomialBasis");
//  ExportGenPolyBasis<StaticLegendrePoly>("LegendrePolynomialBasis");
//  ExportTransportMap();
//  ExportMapUpdateManager();

  ExportImportanceSampler();
}
