#include "MUQ/Inference/python/PolynomialBasisPython.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Inference;

//PolynomialBasisBasePython::PolynomialBasisBasePython(int dim) : PolynomialBasisBase(dim) {}

double PolynomialBasisBase::PyMonomialEvaluate(boost::python::list const& P, double x)
{
  return MonomialEvaluate(GetEigenVector<Eigen::VectorXd>(P), x);
}

boost::python::list PolynomialBasisBase::PyMonomialDivision(boost::python::list const& pyA,
                                                                  boost::python::list const& pyB)
{
  Eigen::VectorXd A = GetEigenVector<Eigen::VectorXd>(pyA);
  Eigen::VectorXd B = GetEigenVector<Eigen::VectorXd>(pyB);

  Eigen::VectorXd Q, R;

  MonomialDivision(A, B, Q, R);

  boost::python::list pyQ = GetPythonVector<Eigen::VectorXd>(Q);
  boost::python::list pyR = GetPythonVector<Eigen::VectorXd>(R);

  boost::python::list result;
  result.append(pyQ);
  result.append(pyR);

  return result;
}

boost::python::list PolynomialBasisBase::PyMonomialRoots(boost::python::list const& P, double x)
{
  return GetPythonVector<Eigen::VectorXd>(MonomialRoots(GetEigenVector<Eigen::VectorXd>(P), x));
}

boost::python::list PolynomialBasisBase::PyGetFixedMonomialCoeffs(boost::python::list const& fixedInput,
                                                                        int                        variableDim) const
{
  return GetPythonVector<Eigen::VectorXd>(GetFixedMonomialCoeffs(GetEigenVector<Eigen::VectorXd>(fixedInput),
                                                                 variableDim));
}

void muq::Inference::ExportPolyBasis()
{
  boost::python::class_<PolynomialBasisBase, shared_ptr<PolynomialBasisBase>, boost::noncopyable,
                        boost::python::bases<TransportMapBasis> > exportPoly("PolynomialBasisBase",
                                                                             boost::python::no_init);

  exportPoly.def("MonomialEvaluate", &PolynomialBasisBase::PyMonomialEvaluate).staticmethod("MonomialEvaluate");

  exportPoly.def("MonomialDivision", &PolynomialBasisBase::PyMonomialDivision).staticmethod("MonomialDivision");
  exportPoly.def("MonomialRoots", &PolynomialBasisBase::PyMonomialRoots).staticmethod("MonomialRoots");

  exportPoly.def("GetFixedMonomialCoeffs", &PolynomialBasisBase::PyGetFixedMonomialCoeffs);
}

