#include "MUQ/Inference/python/ImportanceSamplerPython.h"

#include "MUQ/Modelling/GaussianPair.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

ImportanceSampler::ImportanceSampler(shared_ptr<ModGraph> const& graph, py::dict const& para) :
  ImportanceSampler(graph, PythonDictToPtree(para)) {}

void muq::Inference::ExportImportanceSampler()
{
  py::class_<ImportanceSampler, shared_ptr<ImportanceSampler>, py::bases<ModPiece>, boost::noncopyable> exportIS("ImportanceSampler", py::init<shared_ptr<ModGraph> const&, py::dict const&>());

  exportIS.def(py::init<shared_ptr<Density>, shared_ptr<GaussianPair>, unsigned int const, unsigned int const>());

  py::implicitly_convertible<shared_ptr<ImportanceSampler>, shared_ptr<ModPiece> >();
}
