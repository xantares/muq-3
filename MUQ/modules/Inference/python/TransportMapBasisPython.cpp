#include "MUQ/Inference/python/TransportMapBasisPython.h"

using namespace std;
using namespace muq::Inference;

void muq::Inference::ExportTransportMapBasis()
{
  boost::python::class_<TransportMapBasis, shared_ptr<TransportMapBasis>, boost::noncopyable> exportTransportMapBasis(
    "TransportMapBasis",
    boost::python::no_init);

  exportTransportMapBasis.def("evaluate", &TransportMapBasis::PyEvaluate);

  exportTransportMapBasis.def_readonly("inputDim", &TransportMapBasis::inputDim);
}

