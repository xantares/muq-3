#include "MUQ/Inference/python/MetricPython.h"

using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

EuclideanMetricPython::EuclideanMetricPython() : EuclideanMetric() {}

EuclideanMetricPython::EuclideanMetricPython(boost::python::list const& met) : EuclideanMetric(GetEigenMatrix(met)) {}

boost::python::list EuclideanMetricPython::PyComputeMetric(boost::python::list const& point)
{
  return GetPythonMatrix(ComputeMetric(GetEigenVector<Eigen::VectorXd>(point)));
}

GaussianFisherInformationMetricPython::GaussianFisherInformationMetricPython(shared_ptr<GaussianDensity>      density,
                                                                             shared_ptr<ModPiece> const& model) :
  GaussianFisherInformationMetric(density,
                                  model)
{}

boost::python::list GaussianFisherInformationMetricPython::PyComputeMetric(boost::python::list const& point)
{
  return GetPythonMatrix(ComputeMetric(GetEigenVector<Eigen::VectorXd>(point)));
}

void GaussianFisherInformationMetricPython::PySetMask(boost::python::list const& mask)
{
  SetMask(GetEigenMatrix(mask));
}

void muq::Inference::ExportMetric()
{
  // Euclidean metric
  boost::python::class_<EuclideanMetricPython, std::shared_ptr<EuclideanMetricPython>,
                        boost::noncopyable> exportEuclideanMetric("EuclideanMetric", boost::python::init<>());

  exportEuclideanMetric.def(boost::python::init<boost::python::list const&>());

  exportEuclideanMetric.def("ComputeMetric", &EuclideanMetricPython::PyComputeMetric);

  // Gaussian Fisher Information metric
  boost::python::class_<GaussianFisherInformationMetricPython, std::shared_ptr<GaussianFisherInformationMetricPython>,
                        boost::noncopyable> exportGFIM("GaussianFisherInformationMetric",
                                                       boost::python::init<std::shared_ptr<muq::Modelling::GaussianDensity>,
                                                                           std::shared_ptr<muq::Modelling::ModPiece> const&>());

  exportGFIM.def("ComputeMetric", &GaussianFisherInformationMetricPython::PyComputeMetric);
  exportGFIM.def("SetMask", &GaussianFisherInformationMetricPython::PySetMask);

  // register abstact metric
  boost::python::register_ptr_to_python<std::shared_ptr<AbstractMetric> >();

  // allow conversions to abstract metric
  boost::python::implicitly_convertible<std::shared_ptr<EuclideanMetricPython>, std::shared_ptr<AbstractMetric> >();
  boost::python::implicitly_convertible<std::shared_ptr<GaussianFisherInformationMetricPython>,
                                        std::shared_ptr<AbstractMetric> >();
}
