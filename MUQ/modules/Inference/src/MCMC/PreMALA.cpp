
#include "MUQ/Inference/MCMC/PreMALA.h"

// standard library includes
#include <string>

#include <boost/property_tree/ptree.hpp>

// other muq related includes
#include "MUQ/Modelling/RandVarBase.h"
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"
#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;


REGISTER_MCMCPROPOSAL(PreMALA);

/** This constructor defins the Langevin diffusion MCMC by reading necessary parameters in
 *  paramList
 */
PreMALA::PreMALA(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
                 boost::property_tree::ptree                            & properties) : MCMCProposal(inferenceProblem,
                                                                                                     properties)
{
  LOG(INFO) << "Constructing Preconditioned MALA MCMC";

  stepLength = ReadAndLogParameter(properties, "MCMC.PreMALA.StepLength", 1.0);
  stepLengthExp = log(stepLength);
  
  maxDrift = ReadAndLogParameter(properties, "MCMC.PreMALA.MaxDrift", std::numeric_limits<double>::infinity());

  acceptanceTarget = ReadAndLogParameter(properties, "MCMC.PreMALA.AcceptanceTarget", 0.6);

  stepLengthScaling = ReadAndLogParameter(properties, "MCMC.PreMALA.StepLengthScaling", 1.0);

  adaptDelay = ReadAndLogParameter(properties, "MCMC.PreMALA.AdaptDelay", 1000);

  // tell the problem what information we are going to need from it at each step -> Just the gradient
  samplingProblem->SetStateComputations(true, true, false);

  // get the metric, we are assuming it is constant (i.e. a Euclidean metric).
  auto metric = dynamic_pointer_cast<EuclideanMetric>(inferenceProblem->metricObject);
  assert(metric); // make sure the metric was a Euclidean metric and the dynamic pointer cast worked

  // evaluate the metric
  Eigen::VectorXd zeroPt = Eigen::VectorXd::Zero(inferenceProblem->samplingDim);
  preCondMat =  metric->ComputeMetric(zeroPt);

  auto propSpec = make_shared<GaussianSpecification>(zeroPt,preCondMat,GaussianSpecification::PrecisionMatrix);
  
  propRV   = make_shared<GaussianRV>(propSpec);   // a random variable for the proposal centered at zero
  propDens = make_shared<GaussianDensity>(propSpec); // a density for the proposal centered at zero

  // compute an ldlt solver using the preconditioned matrix
  preCondSolver.compute(preCondMat);
}

std::shared_ptr<muq::Inference::MCMCState> PreMALA::DrawProposal(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
  int const currIteration)
{
	
  Eigen::VectorXd drift = 0.5 *stepLength *stepLength *preCondSolver.solve(currentState->GetDensityGrad());
  double driftNorm = drift.norm();
  if(driftNorm>maxDrift)
    drift *= maxDrift/driftNorm;
  
  Eigen::VectorXd propMu = currentState->state + drift;
 

  Eigen::VectorXd proposedVector = propMu + stepLength * propRV->Sample();

  return samplingProblem->ConstructState(proposedVector, currIteration, logEntry);
}

double PreMALA::ProposalDensity(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                std::shared_ptr<muq::Inference::MCMCState> proposedState)
{
    Eigen::VectorXd drift = 0.5 *stepLength *stepLength *preCondSolver.solve(currentState->GetDensityGrad());
    double driftNorm = drift.norm();
    if(driftNorm>maxDrift)
      drift *= maxDrift/driftNorm;
	  
  Eigen::VectorXd propMu = currentState->state + drift;
  
  double forwardDens = propDens->LogDensity((propMu - proposedState->state) / stepLength);

  return forwardDens;
}

void PreMALA::PostProposalProcessing(std::shared_ptr<muq::Inference::MCMCState> const newState,
                                     MCMCProposal::ProposalStatus const               proposalAccepted,
                                     int const                                        iteration,
                                     std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry)
{
  if (proposalAccepted == MCMCProposal::ProposalStatus::notRun) {
    return; //not run, we didn't learn anything
  } else if (proposalAccepted == MCMCProposal::ProposalStatus::accepted) {
    ++numProposalsAccepted;
  }

  ++numProposals;
  double acceptance = static_cast<double>(numProposalsAccepted) / static_cast<double>(numProposals);

  if (iteration > adaptDelay) {
    stepLengthExp += stepLengthScaling * (acceptance - acceptanceTarget) / static_cast<double>(numProposals);


    double newStepLength = exp(stepLengthExp);

    //double precUpdate = (1.0 / newStepLength / newStepLength) / (1.0 / stepLength / stepLength);

    stepLength = newStepLength;
  }
  logEntry->loggedData["stepLength"] = Eigen::VectorXd::Constant(1, stepLength);
}

void PreMALA::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Proposal", "MALA");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MALA - Step size", stepLength);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MALA - Max drift", maxDrift);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MALA - Acceptance Target", acceptanceTarget);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MALA - Length Scaling", stepLengthScaling);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MALA - Adapt Start", adaptDelay);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MALA - Step Length Exp", stepLengthExp);
}

