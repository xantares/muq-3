
#include "MUQ/Inference/MCMC/DRKernel.h"

#include <algorithm>

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/HDF5Wrapper.h"

#include <boost/optional.hpp>

// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

REGISTER_MCMCKERNEL(DR);

/** Using the known number of DR stages and the DR stage ratio, this function computes the total scaling
 *  at each DR stage and stores that in a vector. */
std::vector<double> setStageScales(int drStages, string scaleType, double drRatio)
{
  // resize scales to number of stages
  std::vector<double> stageScales;

  stageScales.resize(drStages);

  // fill in scales
  double currScale = 1;

  stageScales.at(0) = currScale;

  if (scaleType == "Power") {
    // power law scaling, each scale is just the previous scale divided by drRatio
    for (int i = 1; i < drStages; ++i) {
      currScale     *= 1.0 / drRatio;
      stageScales[i] = currScale;
    }
  } else if (scaleType == "Exponential") {
    // exponential scaling, each scale is exp(-stage*drRatio) times the original
    for (int i = 1; i < drStages; ++i) {
      currScale      = exp(-1.0 * static_cast<double>(i) * drRatio);
      stageScales[i] = currScale;
    }
  }
  return stageScales;
}

/** Default constructor just builds an empty DR solver */
DR::DR(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
       boost::property_tree::ptree                            & properties) : MCMCKernel(inferenceProblem, properties)
{
  LOG(INFO) << "Constructing Delayed Rejection MCMC";

  drEnd = ReadAndLogParameter(properties, "MCMC.DR.NumSteps", 1e6);

  if (ReadAndLogParameter<string>(properties, "MCMC.DR.ProposalSpecification", "DRAM").compare("DRAM") == 0) {
    // also load DR parameters
    drStages = ReadAndLogParameter(properties, "MCMC.DR.stages", 1);

    numProposalCalls   = Eigen::VectorXi::Zero(drStages);
    numProposalAccepts = Eigen::VectorXi::Zero(drStages);


    double drRatio = ReadAndLogParameter(properties, "MCMC.DR.scale", 2.0);

    string scaleType = ReadAndLogParameter<string>(properties, "MCMC.DR.ScaleType", "Power");

    // compute stage scales
    auto stageScales = setStageScales(drStages, scaleType, drRatio);

    proposals.resize(drStages);

    double baseAMScale = ReadAndLogParameter(properties, "MCMC.AM.AdaptScale", 1.0);
    double baseMHStep  = ReadAndLogParameter(properties, "MCMC.MHProposal.PropSize", 1.0);
    for (int i = 0; i < drStages; ++i) {
      properties.put("MCMC.MHProposal.PropSize", baseMHStep * stageScales.at(i));

      properties.put("MCMC.Proposal", "AM");
      properties.put("MCMC.AM.AdaptScale", baseAMScale * stageScales.at(i));
      proposals.at(i) = MCMCProposal::Create(inferenceProblem, properties);
    }
    //} else if (properties.get("MCMC.DR.ProposalSpecification", "DRAM").compare("List") == 0){
  } else {
    // the specfication should be a list separated by commas.
    drStages = 0;
    std::vector<std::string> propNames;
    std::string specList = ReadAndLogParameter<string>(properties, "MCMC.DR.ProposalSpecification", "");
        assert(specList.compare("") != 0); //check there is a spec
    int pos = 0;
    std::string listPart;
    while ((pos = specList.find(",")) != std::string::npos) {
      listPart = specList.substr(0, pos);

      specList.erase(0, pos + 1);

      propNames.push_back(listPart);
      drStages++;
    }
    propNames.push_back(specList);
    drStages++;

    // loop through the proposal names and make sure the nodes exist
    for (auto iter = propNames.begin(); iter != propNames.end(); ++iter) {
      // make sure the listPart name refers to a node in the property_tree
      boost::optional<boost::property_tree::ptree&> foundProposalDefinition = properties.get_child_optional(
        "MCMC.DR." + *iter);
      if (!foundProposalDefinition) {
        std::cerr << "ERROR: Could not find MCMC.DR." + *iter +
        " section in property list even though it was listed in MCMC.DR.ProposalSpecification.\n";
        assert(foundProposalDefinition);
      }
    }

    // at this point, the names are in alpha-numeric order and will be called by DR in this order
    LOG(INFO) << "MCMC.DR.stages = " << drStages;
    numProposalCalls   = Eigen::VectorXi::Zero(drStages);
    numProposalAccepts = Eigen::VectorXi::Zero(drStages);
    proposals.resize(drStages);

    // create each of the proposals
    int stage = 0;
    for (auto iter = propNames.begin(); iter != propNames.end(); ++iter) {
      boost::property_tree::ptree subProperties;

      std::string extractPath = "MCMC.DR." + *iter;
      subProperties.put_child("MCMC", properties.get_child(extractPath));

      proposals.at(stage) = MCMCProposal::Create(inferenceProblem, subProperties);
      assert(proposals.at(stage));
      ++stage;
    }
  }
}

DR::~DR() {}


void DR::PrintStatus() const
{
  std::cout << "  DR: Number of calls = " << numProposalCalls.transpose() << "\n";
  std::cout << "  DR: Cumulative Accept Rate = ";
  double numCalls = static_cast<double>(numProposalCalls(0));
  double rate     = 100.0 * static_cast<double>(numProposalAccepts(0));
  std::cout << std::setw(4) << std::fixed << std::setprecision(1) << rate / numCalls << "%";

  for (int i = 1; i < drStages; ++i) {
    //numCalls += static_cast<double>(numProposalCalls(i));
    rate += 100.0 * static_cast<double>(numProposalAccepts(i));
    std::cout << ", " << std::setw(4) << std::fixed << std::setprecision(1) << rate / numCalls << "%";
  }
  std::cout << std::endl;
}

/** Performs a DR step, including all delayed rejection stages.  Proposal covariance is also updated at beginning of
 *  function if appropriate
 */
void DR::UpdateProposals(std::shared_ptr<muq::Inference::MCMCState> const newState,
                         vector<MCMCProposal::ProposalStatus> const     & proposalAccepted,
                         int const                                        iteration,
                         std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry)
{
  for (int stage = 0; stage < drStages; ++stage) {
    auto stageLogEntry = make_shared<HDF5LogEntry>();
    proposals.at(stage)->PostProposalProcessing(newState, proposalAccepted.at(stage), iteration, stageLogEntry);

    stringstream ss;
    ss << "dr_proposal_" << stage;
    logEntry->MergeWithPrefix(ss.str(), stageLogEntry);
  }
}

std::shared_ptr<muq::Inference::MCMCState> DR::ConstructNextState(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  int const                                     iteration,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry)
{
  // keep track of proposals and points of all DR stages
  vector < std::shared_ptr < MCMCState >> proposed_points; // list of previously proposed points
  vector<double> likelies;                                 // list of previous likelihoods
  vector<MCMCProposal::ProposalStatus> proposalStatus(drStages, MCMCProposal::ProposalStatus::notRun);

  // store current parameters
  //likelies are the full posterior densities
  likelies.push_back(currentState->GetDensity());
  proposed_points.push_back(currentState);

  int stateSize = samplingProblem->samplingDim;

  std::shared_ptr<MCMCState> proposedState;

  Eigen::VectorXd alphas = Eigen::VectorXd::Zero(drStages);
  // loop over delayed rejection stages, will break after acceptance
  for (int stage = 0; stage < drStages; ++stage) {
    // build and store proposal at next step:

    numProposalCalls(stage)++;

    auto stageLogEntry = make_shared<HDF5LogEntry>();
    proposedState = proposals.at(stage)->DrawProposal(currentState, logEntry, iteration);


    //if the proposed state does not have numeric likelihood or prior, reject it immediately
    if (!proposedState) {
      logEntry->loggedData["drStages"] = Eigen::VectorXd::Constant(1, -1);
      proposalStatus.at(stage)         = MCMCProposal::ProposalStatus::rejected;
      UpdateProposals(currentState, proposalStatus, iteration, logEntry);

      //update the log before you bail out
      stringstream ss;
      ss << "dr_proposal_" << stage;
      logEntry->MergeWithPrefix(ss.str(), stageLogEntry);


      return currentState;
    }

    // add proposed point to lists
    likelies.push_back(proposedState->GetDensity());
    proposed_points.push_back(proposedState);


    // call recursive routine to evaluate acceptance probability
    alphas(stage) = alphafun(likelies, proposed_points);

    // //////////////
    // accept or reject based on MH rule for DR
    // cap acceptance probability at 1

    // generate u\sim U(0,1) to decide acceptance
    double u = RandomGenerator::GetUniform();

    logEntry->loggedData["drStages"] = Eigen::VectorXd::Constant(1, stage);

    logEntry->loggedData["drProbabilities"] = alphas;

    stringstream ss;
    ss << "dr_proposal_" << stage;
    logEntry->MergeWithPrefix(ss.str(), stageLogEntry);

    if (u < alphas(stage)) { // the proposed sample has been accepted, we're done here
      proposalStatus.at(stage) = MCMCProposal::ProposalStatus::accepted;
      UpdateProposals(proposedState, proposalStatus, iteration, logEntry);

      numProposalAccepts(stage)++;
      return proposedState;
    } else {
      proposalStatus.at(stage) = MCMCProposal::ProposalStatus::rejected;
    }
  } // end of DR for loop

  UpdateProposals(currentState, proposalStatus, iteration, logEntry);
  return currentState; //could only be here if we've rejected
}

template<typename T>
vector<T> SelectSubVector(vector<T> const& vec, int start, int end)
{
  assert(start != end);
  if (start < end) {
    return vector<T>(vec.begin() + start, vec.begin() + end + 1);
  } else {
    //grab values then flip
    auto temp = vector<T>(vec.begin() + end, vec.begin() + start + 1);
    std::reverse(temp.begin(), temp.end());
    return temp;
  }
}

/** Function recursively computes the acceptance probability alpha in the delayed rejection framework.  it is
 *  assumed that the proposal at each stage is simply a scaled version of the original proposal. To be able to compute
 *  forward and backward probabilities, three integers are used: start, end, incr which correspond to the starting
 *  index of the other input containers, the ending index, an the increment used to get from start to end.
 *
 *  This code is adapted from a MATLAB implementation by Marko Laine in his Matlab MCMC toolbox
 *
 */
double DR::alphafun(vector<double> likelies, vector < std::shared_ptr < MCMCState >> proposed_points) const
{
  // create subcontainers of input variables to use in recursion

  int stage = likelies.size() - 1;

  double a1 =  1.0;
  double a2 = 1.0;

  for (int k = 1; k < stage; ++k) {
    //
    //  likelies.sub(stage-k, stage,-1).print();
    a2 *=
      (1.0 - alphafun(SelectSubVector(likelies, stage, stage - k), SelectSubVector(proposed_points, stage, stage - k)));
    a1 *= (1.0 - alphafun(SelectSubVector(likelies, 0, k), SelectSubVector(proposed_points, 0, k))); // forward
    // probabilities

    //
    // if a2==0, there is no chance of getting accepted, so just give up and try again
    if (a2 == 0) {
      return 0.0;
    }
  }

  double q = 0.0;

  //now put in qs

  for (int k = 1; k <= stage; ++k) {
    q +=  qfun(SelectSubVector(proposed_points, stage, stage - k));
    q -= qfun(SelectSubVector(proposed_points, 0, k)); // forward probabilities
  }

  return min(1.0, exp(*(likelies.end() - 1) - *(likelies.begin()) + q) * a2 / a1);
}

/** A utility function to evaluate the n'th stage log proposal ratio for use in computing the
 *  DR acceptance probability.  i.e. evaluates log( q_i(y_n,...,y_{n-j})/q_i(x,y_1,...,y_j) )
 *
 *  This function is also adapted from the Matlab MCMC toolbox of Marko Laine
 *
 */
double DR::qfun(vector < std::shared_ptr < MCMCState >> proposed_points) const
{
  int stage = proposed_points.size() - 1;

  //use the proposal that generated this distance move
  double q = proposals.at(stage - 1)->ProposalDensity(proposed_points.at(0), proposed_points.at(stage));

  return q;
}

void DR::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Kernel", "Delayed Rejection");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "DR - Number of stages", drStages);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "DR - Stop iteration", drEnd);

  // now, loop through the proposals and get their attributes
  for (int i = 0; i < proposals.size(); ++i) {
    std::stringstream newPrefix;
    newPrefix << prefix << "DR Proposal " << i << " - ";
    proposals.at(i)->WriteAttributes(groupName, newPrefix.str());
  }
}

