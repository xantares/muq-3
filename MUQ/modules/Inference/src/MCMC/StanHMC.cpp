
#include "MUQ/Inference/MCMC/StanHMC.h"

#include <boost/property_tree/ptree.hpp>
#include <Eigen/Dense>

#include <boost/property_tree/ptree.hpp>

#include "stan/mcmc/nuts.hpp"
#include "stan/mcmc/adaptive_hmc.hpp"

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/VectorTranslater.h"
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Inference/ProblemClasses/StanModelInterface.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

REGISTER_MCMCKERNEL(StanHMC);

StanHMC::StanHMC(std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem,
                 boost::property_tree::ptree                            & properties) : MCMCKernel(samplingProblem,
                                                                                                   properties)
{
  LOG(INFO) << "Constructing Stan Hamiltonian MCMC";

  Nleaps = ReadAndLogParameter(properties, "MCMC.StanHMC.Leaps", 10);

  //Obviously we need the gradients
  samplingProblem->SetStateComputations(true, true, false);
}

void StanHMC::SetupStartState(Eigen::VectorXd const& xStart)
{
  modelInterface = std::make_shared<StanModelInterface>(samplingProblem);


  vector<double> startVec;
  vector<int>    startInt;
  VectorTranslate(xStart, startVec, xStart.rows());

  //create one of their mcmc objects and initialize it
  hmcSampler = std::make_shared < stan::mcmc::adaptive_hmc < boost::mt19937 >>
               (*modelInterface, startVec, startInt, Nleaps);
}

std::shared_ptr<muq::Inference::MCMCState> StanHMC::ConstructNextState(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  int const                                     iteration,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry)
{
  //draw the new sample
  auto newSample = hmcSampler->next();

  totalSteps++;

  //Convert from std::vector to Eigen::VectorXd
  vector<double> newSampleVec;
  newSample.params_r(newSampleVec);
  Eigen::VectorXd newSampleEig;
  VectorTranslate(newSampleVec, newSampleEig, newSampleVec.size());

  //It just returns the new state, much like the MCMCKernel class, so now we need to figure out whether it
  //is a new state or an old one
  bool accepted = !MatrixApproxEqual(newSampleEig, currentState->state, 1e-14);

  if (accepted) {
    numAccepts++;
  }

  auto proposedState = samplingProblem->ConstructState(newSampleEig, iteration, logEntry);
  return proposedState;
}

void StanHMC::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Kernel", "Stan HMC");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "HMC - Leapfrog steps", Nleaps);
}

double StanHMC::GetAccept() const
{
  return static_cast<double>(numAccepts) / static_cast<double>(totalSteps);
}

void StanHMC::PrintStatus() const
{
  std::cout << "  HMC: Acceptance rate = " << std::setw(5) << std::fixed << std::setprecision(1) << 100.0 *
    GetAccept() << "%\n";
}
