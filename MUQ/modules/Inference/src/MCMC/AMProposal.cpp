#include "MUQ/Inference/MCMC/AMProposal.h"

#include <algorithm>

#include <boost/property_tree/ptree.hpp>


#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

REGISTER_MCMCPROPOSAL(AM);


AM::AM(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
       boost::property_tree::ptree                            & properties) : MHProposal(inferenceProblem, properties)
{
  LOG(INFO) << "Constructing Adaptive Metropolis MCMC";

  // also load AM parameters
  adaptSteps = ReadAndLogParameter(properties, "MCMC.AM.AdaptSteps", 1);
  adaptStart = ReadAndLogParameter(properties, "MCMC.AM.AdaptStart", 1);

  int dim = samplingProblem->samplingDim;

  // adaptive metropolis update scaling -- taken from Haario DRAM paper
  adaptScale = ReadAndLogParameter(properties, "MCMC.AM.AdaptScale", 2.4 * 2.4 / dim);


  // now, set the chain mean and covariance dimension
  sampleCov  = Eigen::MatrixXd::Zero(dim, dim);
  sampleMean = Eigen::VectorXd::Zero(dim);
}

AM::~AM() {}

/** Updates the MCMC chain mean that is used in the AM covariance update */
void AM::updateSampleMeanAndCov(int const currIteration, std::shared_ptr<muq::Inference::MCMCState> const currentState)
{
  double t = currIteration - 1.0;                                                    //this is the number of existing

  if (currIteration == 1) {                                                          //first estimate of mean is just
                                                                                     // the state
    sampleMean = currentState->state;
  } else if (currIteration == 2) {                                                   //first cov estimate
    auto oldMean = sampleMean;

    sampleMean = t / (1.0 + t) * sampleMean + 1.0 / (1.0 + t) * currentState->state; //update mean normally

    //compute covariance from scratch, from the definition
    sampleCov.selfadjointView<Eigen::Lower>().rankUpdate(oldMean - sampleMean, 1.0);
    sampleCov.selfadjointView<Eigen::Lower>().rankUpdate(currentState->state - sampleMean, 1.0);
  } else {                                                                           //now recursively update both
    sampleCov *= (t - 1.0) / t;
    auto oldmean = sampleMean;
    sampleMean = t / (1.0 + t) * sampleMean + 1.0 / (1.0 + t) * currentState->state; //update mean

    //note that the asymmetric form fixes the fact that the old mean was wrong
    sampleCov += 1.0 / t * (currentState->state - oldmean) * (currentState->state - sampleMean).transpose();

    //taken from en.wikipedia.org/wiki/Algorithms_for_calculating_variance but their N is off from ours
  }
}

/** Updates the proposal covariance based on the chain's current covariance */
void AM::updateProp()
{
  Eigen::MatrixXd adjustedCov = adaptScale * sampleCov + 1e-10 * Eigen::MatrixXd::Identity(
    sampleCov.rows(), sampleCov.cols());

  // update the proposal with this scaled covariance
  proposal = make_shared<GaussianPair>(proposal->specification->Mean(), adjustedCov);
}

void AM::PostProposalProcessing(std::shared_ptr<muq::Inference::MCMCState> const currentState,
                                MCMCProposal::ProposalStatus const               proposalAccepted,
                                int const                                        currIteration,
                                std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry)
{
  updateSampleMeanAndCov(currIteration, currentState); // update the running proposal covariance, which we always do


  //but only actually use the new one at certain intervals
  if (((currIteration % adaptSteps) == 0) && (currIteration > adaptStart)) {
    updateProp(); //actually update the proposal
  }

  logEntry->loggedData["AMProposalDiag"] = proposal->specification->GetCovarianceMatrix().diagonal();
}

void AM::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Proposal", "Adaptive Metropolis");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AM - Adapt Gap", adaptSteps);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AM - Adapt Start", adaptStart);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AM - Adapt Scale", adaptScale);
}
