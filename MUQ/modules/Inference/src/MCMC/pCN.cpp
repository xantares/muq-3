
#include "MUQ/Inference/MCMC/pCN.h"


// other muq related includes
#include "MUQ/Modelling/RandVarBase.h"
#include "MUQ/Utilities/ParameterList.h"

// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::utilities;
using namespace muq::Modelling;


/** Default constructor -- does nothing */
pCN::pCN() {}

/** Default destructor */
pCN::~pCN() {}

/** This constructor defins the Langevin diffusion MCMC by reading necessary parameters in
 *  paramList
 */
pCN::pCN(ParameterList& paramList, const Eigen::MatrixXd& Sigma)
{
  // save the prior covariance
  PriorCov = Sigma;

  // set the other parameters
  setParms(paramList);
}

/** Function reads in necessary parameters from ParameterList input and resizes gradient vectors. */
void pCN::setParms(ParameterList& paramList)
{
  // get Metropolis-Hastings Parameters
  MH::setParms(paramList); // proposal size will define Langevin step size

  double delta = paramList.Get("MCMC.pCN.delta", 0.95);

  beta = sqrt(8 * delta / pow(2 + delta, 2));
}

/** Sets the initial proposal mean based on the gradient at xStart and also sets the initial log density value.
 */
void pCN::setup(Density& PostDens, const Eigen::VectorXd& xStart)
{
  // call the mcmc setup, this will set the problem dimension amongst other things
  MCMC::setup(PostDens, xStart);

  Eigen::VectorXd newMu = sqrt(1 - beta * beta) * xStart;

  proposal = make_shared<GaussianPair>(newMu, (beta * beta) * PriorCov);

  // set backwards proposal equal to forward proposal for now, will change means later in algorithm
  prop2 = make_shared<GaussianPair>(newMu, (beta * beta) * PriorCov);

  prop = xStart;
}

/** Function sets proposal to the Langevin proposal at xc and returns the posterior log density at xc.  The density
 *  evaluation is lumped in here so that work done by the evaluator to compute the log density can also be utilized
 *  during computation of higher order information such as gradients, Hessians, etc...
 */
double pCN::setProp(Density& PostDens, const Eigen::VectorXd& xc, std::shared_ptr<GaussianPair> proposal)
{
  double output;

  // evaluate log proposal density
  Eigen::VectorXd *TempMean = proposal->GetMeanPtr();

  *TempMean = sqrt(1 - beta * beta) * xc;

  // set mean to scaled gradient
  output = PostDens.LogEval(xc);
  return output;
}

