/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *  USA.
 *
 *  MIT UQ Library
 *  Copyright (C) 2013 MIT
 */

#include "MUQ/Inference/MCMC/IMH.h"

// standard library includes
#include <string>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other MUQ includes
#include "MUQ/Modelling/RandVarBase.h"
#include "MUQ/Modelling/GaussianPair.h"

// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

REGISTER_MCMC_DEF_TYPE(IMH) IMH::IMH(std::shared_ptr<AbstractSamplingProblem> inferenceProblem,
                                  boost::property_tree::ptree            & properties) : IMH(inferenceProblem, properties, make_shared<GaussianPair>(Eigen::VectorXd::Zero(inferenceProblem->samplingDim),properties.get("MCMC.IMH.ProposalVar",1.0))){};



muq::Inference::SingleChainMCMC::ProposalResult IMH::MakeProposal()
{
  // draw a proposal
  Eigen::VectorXd prop = proposalRV->Sample();

  proposedState = samplingProblem->ConstructState(prop);

  //if the proposed state does not have numeric likelihood or prior, reject it immediately
  if (!proposedState) {
    return SingleChainMCMC::ProposalResult::reject;
  }
  
  double forwardDens  = proposalDensity->LogDensity(proposedState->state);
  double backwardDens = proposalDensity->LogDensity(currentState->state);

  // compute the Metropolis-Hastings acceptance ratio from the log-density evaluations
  double gamma = exp((proposedState->GetDensity()) - (currentState->GetDensity()) + (backwardDens - forwardDens));

  // accept or reject the proposed sample
  double u = RandomGenerator::GetUniform();
  if (u < gamma) {
    // propMu = backMu
    return SingleChainMCMC::ProposalResult::accept;
  } else {
    return SingleChainMCMC::ProposalResult::reject;
  }
}

