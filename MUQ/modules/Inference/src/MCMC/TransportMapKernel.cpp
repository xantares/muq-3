
#include "MUQ/Inference/MCMC/TransportMapKernel.h"

// standard library includes
#include <string>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other muq related includes
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/HDF5Wrapper.h"

#include "MUQ/Inference/TransportMaps/MapFactory.h"
#include "MUQ/Inference/TransportMaps/MapUpdateManager.h"
#include "MUQ/Inference/MCMC/MixtureProposal.h"
#include "MUQ/Inference/MCMC/MHKernel.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Modelling/Density.h"

// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

double TransferDensity(std::shared_ptr<TransportMap> map, const Eigen::VectorXd& targetPos, double targetLogDens)
{
  double output = targetLogDens - map->Jacobian(targetPos).diagonal().array().log().sum();

  return output;
}

double TransferDensityInverse(std::shared_ptr<TransportMap> map, const Eigen::VectorXd& targetPos, double refLogDens)
{
  double output = refLogDens + map->Jacobian(targetPos).diagonal().array().log().sum();

  return output;
}

Eigen::VectorXd TransferGradient(std::shared_ptr<TransportMap> map,
                                 const Eigen::VectorXd       & targetPos,
                                 const Eigen::VectorXd       & targetLogGrad)
{
  const int dim = targetPos.size();

  Eigen::MatrixXd jac   = map->Jacobian(targetPos);
  Eigen::VectorXd part1 = jac.triangularView<Eigen::Lower>().transpose().solve(targetLogGrad);

  // now we also need the components related to the jacobian
  Eigen::VectorXd part2 = Eigen::VectorXd::Zero(dim);

  // loop over the output dimensions (i.e. working along diagonal of jacobian)
  for (int i = 0; i < dim; ++i) {
    Eigen::VectorXd gx = Eigen::VectorXd::Zero(dim);
    for (int j = 0; j <= i; ++j) {
      gx(j) = map->GetSecondDerivative(targetPos, i, j, i); // second derivative of output i wrt inputs j and i
    }
    gx    /= jac(i, i);
    part2 += jac.triangularView<Eigen::Lower>().transpose().solve(gx);
  }
  return part1 - part2;
}

Eigen::VectorXd TransferGradientInverse(std::shared_ptr<TransportMap> map,
                                        const Eigen::VectorXd       & targetPos,
                                        const Eigen::VectorXd       & referenceLogGrad)
{
  const int dim = targetPos.size();

  Eigen::MatrixXd jac   = map->Jacobian(targetPos);
  Eigen::VectorXd part1 = jac.triangularView<Eigen::Lower>().transpose() * referenceLogGrad;

  // now we also need the components related to the jacobian
  Eigen::VectorXd part2 = Eigen::VectorXd::Zero(dim);

  // loop over the output dimensions (i.e. working along diagonal of jacobian)
  for (int i = 0; i < dim; ++i) {
    Eigen::VectorXd gx = Eigen::VectorXd::Zero(dim);
    for (int j = 0; j <= i; ++j) {
      gx(j) = map->GetSecondDerivative(targetPos, i, j, i); // second derivative of output i wrt inputs j and i
    }
    gx    *= jac(i, i);
    part2 += jac.triangularView<Eigen::Lower>() * gx;
  }
  return part1 + part2;
}

class InducedDensity : public Density {
public:

  InducedDensity(std::shared_ptr<AbstractSamplingProblem> samplingProbIn,
                 std::shared_ptr<TransportMap>            mapIn,
                 std::shared_ptr<Eigen::VectorXd>         invStartIn) : Density(mapIn->inputSizes,
                                                                                true,
                                                                                false,
                                                                                false,
                                                                                false), invStart(
                                                                          invStartIn), samplingProb(
                                                                          samplingProbIn),
                                                                        map(mapIn)
  {}

  virtual ~InducedDensity() = default;

private:

  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    Eigen::VectorXd tempVec = map->EvaluateInverse(input.at(0), *invStart);
    //std::cout << "tempVec = " << tempVec.transpose() << std::endl;
    auto   state = samplingProb->ConstructState(tempVec, 0, nullptr);
    double output;

    if (state) {
      auto dens = state->GetDensity();
      if (dens) {
        return TransferDensity(map, tempVec, dens); //output = state->GetDensity() -
                                                    // map->Jacobian(tempVec).diagonal().array().log().sum();
      } else {
        return -std::numeric_limits<double>::infinity();
      }
    } else {
      return -std::numeric_limits<double>::infinity();
    }
    return output;
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    Eigen::VectorXd tempVec = map->EvaluateInverse(input.at(0), *invStart);

    auto targetState = samplingProb->ConstructState(tempVec, 0, nullptr);

    return TransferGradient(map, tempVec, targetState->GetDensityGrad());
  }

  std::shared_ptr<Eigen::VectorXd> invStart;
  std::shared_ptr<AbstractSamplingProblem> samplingProb;
  std::shared_ptr<TransportMap> map; // the map from the target space to the reference
};


TransportMapKernel::TransportMapKernel(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                                       boost::property_tree::ptree            & properties,
                                       std::shared_ptr<TransportMap>            initialMap) : MCMCKernel(
                                                                                                samplingProblem,
                                                                                                properties),
                                                                                              transportMap(initialMap)
{
      assert(initialMap->outputSize == samplingProblem->samplingDim);

  adaptStart = properties.get("MCMC.TransportMap.AdaptStart", 2000);
      assert(adaptStart >= 0);

  adaptGap = properties.get("MCMC.TransportMap.AdaptGap", 100);
      assert(adaptGap > 0);

  adaptStop = std::min<int>(properties.get("MCMC.TransportMap.AdaptStop", 100000), properties.get<int>("MCMC.Steps"));
      assert(adaptStop >= adaptStart);

  adaptScale = properties.get("MCMC.TransportMap.AdaptScale", 1.0);
      assert(adaptScale >= 0.0);

  varScale = properties.get("MCMC.TransportMap.IndependenceScale", 1.0);
      assert(varScale >= 0);

  maxIndProb = properties.get("MCMC.TransportMap.MaxIndependencProb", 1.0);
      assert(maxIndProb <= 1);
      assert(maxIndProb >= 0);

  monitorMapConvergence = properties.get("MCMC.TransportMap.MonitorConvergence", false);

  mapManager = make_shared<MapUpdateManager>(transportMap, adaptStop - adaptStart, 1.0 / adaptScale);
  newSamps.resize(samplingProblem->samplingDim, adaptGap);

  // initialize the newtonStart to the correct dimension, this is replaced with the MCMC state later on
  inverseGuess = make_shared<Eigen::VectorXd>(Eigen::VectorXd::Zero(transportMap->outputSize));

  // make a new sampling problem
  auto newDens = make_shared<InducedDensity>(samplingProblem, initialMap, inverseGuess);
  mapInducedProblem = make_shared<SamplingProblem>(newDens);


  // construct the reference space kernel
  referenceKernel = MCMCKernel::Create(mapInducedProblem, properties.get_child("MCMC.TransportMap.SubMethod"));
      assert(referenceKernel);

  // figure out if the transition kernel is a Metrpolis-Hastings Kernel based on a Mixture proposal.  We assume the
  // first proposal in the mixture is the more "global" proposal
  auto mhPtr = std::dynamic_pointer_cast<MHKernel>(referenceKernel);
  propIsMixture = false;
  if (mhPtr) {
    mixPtr = std::dynamic_pointer_cast<MixtureProposal>(mhPtr->proposal);
    if (mixPtr) {
      propIsMixture = true;

      // At this point, I don't have a way of handling anything that is not a mixture of 2 proposals
      assert(mixPtr->GetNumProps() == 2);

      // get the parameters of the variance to weight function
      varScale   = properties.get("MCMC.TransportMap.VarScale", 1.0);
      maxIndProb = properties.get("MCMC.TransportMap.MaxGlobalWeight", 1.0);
      assert(maxIndProb <= 1.0);
      assert(maxIndProb >= 0.0);
    }
  }

  if (propIsMixture) {
    monitorMapConvergence = true;
  }

  if (monitorMapConvergence) {
    targetDensities.resize(adaptStop - adaptStart);
  }

  // tell the problem what information we are going to need from it at each step -> Nothing but the density value
  samplingProblem->CopyStateComputations(mapInducedProblem);

  newSampInd = 0;
}

std::shared_ptr<TransportMap> TransportMapKernel::GetMap() const
{
  return transportMap;
}

void TransportMapKernel::SetupStartState(Eigen::VectorXd const& xStart)
{
  MCMCKernel::SetupStartState(xStart);

  referenceKernel->SetupStartState(transportMap->Evaluate(xStart));
}

std::shared_ptr<MCMCState> TransportMapKernel::ConstructReferenceState(std::shared_ptr<MCMCState> targetState)
{
  Eigen::VectorXd refPosition = transportMap->Evaluate(targetState->state);

  double logLikelihood = TransferDensity(transportMap, targetState->state, targetState->GetDensity());

  boost::optional<Eigen::VectorXd> likelihoodGrad;
  boost::optional<double> logPrior; //only used for inference problems
  boost::optional<Eigen::MatrixXd> metric;
  boost::optional<Eigen::VectorXd> priorGrad;

  //if it's not valid, return null
  if (!std::isfinite(logLikelihood)) {
    return nullptr;
  }

  //compute gradient as desired
  if (samplingProblem->ComputesGrad()) {
    likelihoodGrad =
      boost::optional<Eigen::VectorXd>(TransferGradient(transportMap, targetState->state,
                                                        targetState->GetDensityGrad()));
  } else {
    likelihoodGrad = boost::optional<Eigen::VectorXd>();
  }

  //compute metric as desired
      assert(!samplingProblem->ComputesMetric());

  boost::optional<Eigen::VectorXd> extraInfo = boost::optional<Eigen::VectorXd>();

  auto refState = std::make_shared<MCMCState>(refPosition,
                                              logLikelihood,
                                              likelihoodGrad,
                                              logPrior,
                                              priorGrad,
                                              metric,
                                              extraInfo);
  return refState;
}

std::shared_ptr<MCMCState> TransportMapKernel::ConstructTargetState(std::shared_ptr<MCMCState> refState)
{
  Eigen::VectorXd targetPosition = transportMap->EvaluateInverse(refState->state, *inverseGuess);

  double logLikelihood = TransferDensityInverse(transportMap, targetPosition, refState->GetDensity());

  boost::optional<Eigen::VectorXd> likelihoodGrad;
  boost::optional<double> logPrior; //only used for inference problems
  boost::optional<Eigen::MatrixXd> metric;
  boost::optional<Eigen::VectorXd> priorGrad;

  //if it's not valid, return null
  if (!std::isfinite(logLikelihood)) {
    return nullptr;
  }

  //compute gradient as desired
  if (samplingProblem->ComputesGrad()) {
    likelihoodGrad =
      boost::optional<Eigen::VectorXd>(TransferGradientInverse(transportMap, targetPosition,
                                                               refState->GetDensityGrad()));
  } else {
    likelihoodGrad = boost::optional<Eigen::VectorXd>();
  }

  //compute metric as desired
      assert(!samplingProblem->ComputesMetric());

  boost::optional<Eigen::VectorXd> extraInfo = boost::optional<Eigen::VectorXd>();

  auto targetState = std::make_shared<MCMCState>(targetPosition,
                                                 logLikelihood,
                                                 likelihoodGrad,
                                                 logPrior,
                                                 priorGrad,
                                                 metric,
                                                 extraInfo);
  return targetState;
}

bool AreDifferentStates(const Eigen::VectorXd& state1, const Eigen::VectorXd& state2)
{
  if (state1.size() != state2.size()) {
    return true;
  }

  bool hasMoved = false;
  for (int i = 0; i < state1.size(); ++i) {
    if (state1(i) != state2(i)) {
      hasMoved = true;
      break;
    }
  }

  return hasMoved;
}

std::shared_ptr<muq::Inference::MCMCState> TransportMapKernel::ConstructNextState(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  int const                                     iteration,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry)
{
  // if this is a new state, or we just adapted, we will need to update the reference space
  if (AreDifferentStates(currentState->state, oldTargetState) || (newSampInd == 0)) {
    *inverseGuess         = currentState->state;
    currentReferenceState = ConstructReferenceState(currentState);
  }

  auto nextReferenceState = referenceKernel->ConstructNextState(currentReferenceState, iteration, logEntry);

  std::shared_ptr<muq::Inference::MCMCState> nextTargetState;
  if (nextReferenceState != nullptr) {
    if (AreDifferentStates(nextReferenceState->state, currentReferenceState->state)) {
      // evaluate the sampling density at the new state
      nextTargetState = ConstructTargetState(nextReferenceState);

      currentReferenceState = nextReferenceState;
      oldTargetState        = nextTargetState->state;
    } else {
      nextTargetState = currentState;
    }
  } else {
    nextTargetState = currentState;
  }
  PostStepProcessing(nextTargetState, iteration);

  if (monitorMapConvergence) {
    logEntry->loggedData["varT"]  = Eigen::VectorXd::Constant(1, varT);
    logEntry->loggedData["meanT"] = Eigen::VectorXd::Constant(1, meanT);
  }

  return nextTargetState;
}

void TransportMapKernel::PostStepProcessing(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                            int const                                  currIteration)
{
  if ((currIteration > adaptStart) && (currIteration < adaptStop)) {
    newSamps.col(newSampInd) = currentState->state;
    newSampInd++;

    if (monitorMapConvergence) {
      targetDensities(currIteration - adaptStart - 1) = currentState->GetDensity();
    }
  }

  // update the coefficients
  if (((currIteration % adaptGap) == 0) && (currIteration > adaptStart) && (currIteration < adaptStop)) {
    const int newInd = currIteration - adaptStart;
    const int oldInd = std::max<int>(currIteration - adaptStart - adaptGap, 0);

    Eigen::Matrix<double, 3, 1> tols(3, 1);
    tols << 5e-4, 1e-4, 1e-4;

    if (adaptScale > 0.0) {
      mapManager->UpdateMap(newSamps);
    }
    newSampInd = 0;

    // We can use the variance of our KL divergence to update the mixture weights
    if (monitorMapConvergence) {
      const int maxTestSamps = 5e4;

      const int firstMapSamp = std::max<int>(0, currIteration - adaptStart - maxTestSamps);

      const int lastMapSamp = currIteration - adaptStart;
      const int numMapSamps = lastMapSamp - firstMapSamp;

      Eigen::VectorXd logDensEvals = mapManager->GetInducedDensities(firstMapSamp, numMapSamps);
      auto T                       = logDensEvals - targetDensities.segment(firstMapSamp, numMapSamps);

      meanT = T.sum() / T.size();
      varT  = (T.array() - meanT).pow(2).sum() / (T.size() - 1);
    }

    if (propIsMixture) {
      mixPtr->SetWeights(GetMixtureWeights(varT));
    }
  }
}

Eigen::VectorXd TransportMapKernel::GetMixtureWeights(double var) const
{
  Eigen::VectorXd output(2);

  output(0) = maxIndProb / (1 + varScale * var);
  output(1) = 1.0 - output(0);
  return output;
}

void TransportMapKernel::PrintStatus() const
{
  // print the reference kernel status
  referenceKernel->PrintStatus();

  if (monitorMapConvergence && (varT >= 0)) {
    std::cout << "  TransportMap: Mean of T     = " << meanT << std::endl;
    std::cout << "  TransportMap: Variance of T = " << varT << std::endl;
  }
}

void TransportMapKernel::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Kernel", "Transport Map");

  referenceKernel->WriteAttributes(groupName, prefix + "TM Reference - ");
}
