#include "MUQ/Inference/TransportMaps/IsoTransportMap.h"

#include "MUQ/Utilities/Polynomials/Monomial.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Inference;

template<class PolyType>
std::shared_ptr<TransportMap> IsoTransportMap<PolyType>::segment(int startDim, int length) const
{
  assert(startDim + length <= outputSize);
 
  vector<Eigen::VectorXd> segCoeffs(length);
  vector<shared_ptr<MultiIndexSet>> segSets(length);

  shared_ptr<MultiIndexPool> newPool = make_shared<MultiIndexPool>();
  
  for (int d = startDim; d < startDim + length; ++d) {
    
    segCoeffs.at(d-startDim) = coeffs.at(d);
    
    segSets.at(d-startDim) = make_shared<MultiIndexSet>(startDim+length,allMultis.at(d)->GetLimiter(),newPool);
    
    for(int i=0; i<allMultis.at(d)->size(); ++i){
      auto newMulti = MultiIndex::Clone(allMultis.at(d)->at(i));
      newMulti->SetDimension(startDim + length);
      segSets.at(d-startDim)->AddActive(newMulti);
    }
  }
  
  return make_shared<IsoTransportMap<PolyType>>(startDim+length, segSets, segCoeffs);
}


template<class PolyType>
Eigen::MatrixXd IsoTransportMap<PolyType>::EvaluateMultiImpl(Eigen::MatrixXd const& xs)
{
  // evaluate all the basis functions
  Eigen::MatrixXd output = Eigen::MatrixXd::Zero(outputSize,xs.cols());
  
  // evaluate the polynomials for all the orders
  
  // loop over all of the points
#pragma omp parallel for
  for(int ptInd = 0; ptInd<xs.cols(); ++ptInd){

    std::vector<Eigen::VectorXd> basisVals(inputSizes(0));
    // compute the 1D polynomials for this input point
    #pragma omp parallel for    
    for(int d=0; d<inputSizes(0); ++d)
      basisVals.at(d) = PolyType::evaluateAll_static(maxOrders(d),xs(d,ptInd));
    
    // now, loop over all terms in the multiindex pool and add the contribution to the output
    auto pool = allMultis.at(0)->GetPool();
    #pragma omp parallel for
    for(int term=0; term<pool->size(); ++term){
      
      // if the term is active, compute its value
      if(pool->IsActive(term)){
        
        double termVal = 1.0;
        for(auto miInd = pool->at(term)->GetNzBegin(); miInd!=pool->at(term)->GetNzEnd(); ++miInd)
          termVal *= basisVals.at(miInd->first)(miInd->second);
        
        for(auto pair : pool->GetActiveIndices(term)){
          #pragma omp atomic update
          output(pair.first,ptInd) += coeffs[pair.first][pair.second]*termVal;
        }
      }// if term is active
    }// loop over terms
    
  }// loop over points
  
  return output;
}

template<class PolyType>
Eigen::MatrixXd IsoTransportMap<PolyType>::JacobianImpl(Eigen::VectorXd const& xs)
{
  // create space for the output
  Eigen::MatrixXd output = Eigen::MatrixXd::Zero(outputSize,inputSizes(0));
  
  // evaluate the polynomials for all the orders
  std::vector<Eigen::VectorXd> basisVals(inputSizes(0));
  
  // compute the 1D polynomials for this input point
  for(int d=0; d<xs.rows(); ++d)
    basisVals.at(d) = PolyType::evaluateAll_static(maxOrders(d),xs(d));
  
  #pragma omp parallel for
  for(int dimWrt=0; dimWrt<inputSizes(0); ++dimWrt)
  {
    // now, loop over all terms in the multiindex pool and add the contribution to the output
    auto pool = allMultis.at(0)->GetPool();
    for(int term=0; term<pool->size(); ++term){
      
      // if the term is active, compute its value
      if(pool->IsActive(term)){
        
        double termVal = 1.0;
        bool wrtFound = false;
        for(auto miInd = pool->at(term)->GetNzBegin(); miInd!=pool->at(term)->GetNzEnd(); ++miInd){
          if(miInd->first!=dimWrt){
            termVal *= basisVals.at(miInd->first)(miInd->second);
          }else{
            termVal *= PolyType::gradient_static(miInd->second,xs(dimWrt));
            wrtFound = true;
          }
        }
        if(wrtFound){
          for(auto pair : pool->GetActiveIndices(term))
            output(pair.first,dimWrt) += coeffs[pair.first][pair.second]*termVal;
        }
        
      }// if term is active
    }// loop over terms
    
  }// loop over input dimensions
  
  return output;
}

template<class PolyType>
Eigen::VectorXd IsoTransportMap<PolyType>::EvaluateInverse(Eigen::VectorXd const& input, Eigen::VectorXd const& x0) const
{
  // I don't know how to invert a rectangular map or a square map that isn't lower triangular
  assert(inputSizes(0) == outputSize);
  assert(input.size() == outputSize);
  assert(x0.size() == inputSizes(0));
  assert(isLowerTriangular);
  
  // is the transport map represented using only polynomial bases?  If so, we can use a Sturm sequence on an equivalent
  // monomial to find the root
  Eigen::VectorXd x = x0;
  
  for (int d = 0; d < outputSize; ++d) {
    
    // I can't invert a constant
    assert(maxOrders(d)>0);
    
    // Loop over each basis function, compute the monomial coefficients, and add it to the monomial coefficients of the expansion
    Eigen::VectorXd polyCoeffs = Eigen::VectorXd::Zero(maxOrders(d)+1);
    for (int term = 0; term < coeffs.at(d).size(); ++term) {
      
      int dPow = 0;
      double scale = coeffs.at(d)(term);
      
      for(auto pairs = allMultis.at(d)->at(term)->GetNzBegin(); pairs!=allMultis.at(d)->at(term)->GetNzEnd(); ++pairs){
        if(pairs->first!=d){
          scale *= PolyType::evaluate_static(pairs->second,x(pairs->first));
        }else{
          dPow = pairs->second;
        }
      }
      polyCoeffs(dPow) += scale;
    }
    
    // adjust the constant term in the monomial to reflect the target position
    polyCoeffs(0) -= input(d)/PolyType::evaluate_static(0,0.0);
    
    // now, find the roots of the monomial
    Eigen::VectorXd possibleRoots = PolyType::Roots(polyCoeffs, 1e-8);
    if (possibleRoots.size() > 1) {
      Eigen::VectorXd diff = (possibleRoots - Eigen::VectorXd::Constant(possibleRoots.size(), x0(d))).array().abs();
      int keepInd;
      diff.minCoeff(&keepInd);
      x(d) = possibleRoots(keepInd);
    } else {
      x(d) = possibleRoots(0);
    }
  }
  return x;
}


template<class PolyType>
std::shared_ptr<TransportMap> IsoTransportMap<PolyType>::CreateFixedMap(int startInd, Eigen::VectorXd const& fixedInput) const
{
  // check to make sure the fixed size is feasible
  assert(startInd>=0);
  assert(startInd + fixedInput.size() <= inputSizes(0));
  assert(fixedInput.size()>0);
  
  const int newInputSize = inputSizes(0)-fixedInput.size();
  
  // create a vector holding the new multiindices
  shared_ptr<MultiIndexPool> newPool = make_shared<MultiIndexPool>();
  vector<shared_ptr<MultiIndexSet>> newMultis(outputSize);
  vector<Eigen::VectorXd> newCoeffs(outputSize);
  
  // loop over each output dimension
  for (int d = 0; d < outputSize; ++d) {
    
    newMultis.at(d) = make_shared<MultiIndexSet>(newInputSize,allMultis.at(d)->GetLimiter(),newPool);
    newCoeffs.at(d) = Eigen::VectorXd::Zero(coeffs.at(d).size());
    
    // fix each term in the multiindex
    for (int i = 0; i < coeffs.at(d).size(); ++i) {
      auto newMulti = make_shared<MultiIndex>(newInputSize);
      
      double tempCoeff = coeffs.at(d)(i);
      for(auto iter=allMultis.at(d)->at(i)->GetNzBegin(); iter!=allMultis.at(d)->at(i)->GetNzEnd(); ++iter){
        // will this dimension be fixed?
        if((iter->first>=startInd)&&(iter->first<startInd+fixedInput.size())){
          tempCoeff *= PolyType::evaluate_static(iter->second,fixedInput(iter->first-startInd));
        }else{
          if(iter->first>startInd){
            newMulti->SetValue(iter->first-fixedInput.size(),iter->second);
          }else{
            newMulti->SetValue(iter->first,iter->second);
          }
        }
      }
      
      // add the multiindex to the set
      int newInd = newMultis.at(d)->AddActive(newMulti);
      newCoeffs.at(d)(newInd) += tempCoeff;
    }
    
    // resize the coefficient vector
    newCoeffs.at(d).conservativeResize(newMultis.at(d)->size());
  }
  
  return make_shared<IsoTransportMap>(newInputSize, newMultis, newCoeffs);
}

template<class PolyType>
double IsoTransportMap<PolyType>::GetSecondDerivative(Eigen::VectorXd const& input, int outputDim, int wrt1, int wrt2) const
{
  if ((wrt1 > outputDim) || (wrt2 > outputDim)) {
    return 0.0;
  } else {
    const int numBases    = coeffs.at(outputDim).size();
    const int currentSize = inputSizes(0) - outputSize + outputDim + 1;
    
    Eigen::VectorXd basisVals(numBases);
    
    if(wrt1==wrt2){
      
      for(int j=0; j<numBases; ++j){
        
        basisVals(j)=1.0;
        bool dimEncountered = false;
        auto tempMulti = allMultis.at(outputDim)->at(j);
        
        for(auto iter=tempMulti->GetNzBegin(); iter!=tempMulti->GetNzEnd(); ++iter){
          if(iter->first==wrt1){
            basisVals(j) *= PolyType::secondDerivative_static(iter->second,input(wrt1));
            dimEncountered = true;
          }else{
            basisVals(j) *= PolyType::evaluate_static(iter->second,input(iter->first));
          }
        }
        if(!dimEncountered)
          basisVals(j) = 0.0;
      }
      
        
    }else{
      
      for(int j=0; j<numBases; ++j){
        basisVals(j)=1.0;
        bool dimEncountered[2];
        dimEncountered[0] = false;
        dimEncountered[1] = false;
        
        auto tempMulti = allMultis.at(outputDim)->at(j);
        for(auto iter=tempMulti->GetNzBegin(); iter!=tempMulti->GetNzEnd(); ++iter){
        
          if(iter->first==wrt1){
            basisVals(j) *= PolyType::gradient_static(iter->second,input(wrt1));
            dimEncountered[0] = true;
          }else if(iter->first==wrt2){
            basisVals(j) *= PolyType::gradient_static(iter->second,input(wrt1));
            dimEncountered[1] = true;
          }else{
            basisVals(j) *= PolyType::evaluate_static(iter->second,input(iter->first));
          }
          
        }
        if((!dimEncountered[0])||(!dimEncountered[1]))
          basisVals(j) = 0.0;
      }
    }
    
    // compute the dot product of coeffs and basis values
    return basisVals.dot(coeffs.at(outputDim));
  }
}


template<class PolyType>
Eigen::MatrixXd IsoTransportMap<PolyType>::GetBasisValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const{
  
  Eigen::MatrixXd output(samps.cols(),allMultis.at(dim)->size());
  
  // evaluate the polynomials for all the orders
  std::vector<Eigen::VectorXd> basisVals(inputSizes(0));
  
  // loop over all of the points
  #pragma omp parallel for
  for(int ptInd = 0; ptInd<samps.cols(); ++ptInd){

    for(int term=0; term<allMultis.at(dim)->size(); ++term){
      
      output(ptInd,term) = 1.0;
      for(auto miInd = allMultis.at(dim)->at(term)->GetNzBegin(); miInd!=allMultis.at(dim)->at(term)->GetNzEnd(); ++miInd)
        output(ptInd,term) *= PolyType::evaluate_static(miInd->second,samps(miInd->first,ptInd));
      
    }// loop over terms
  }// loop over points
  
  return output;
}

template<class PolyType>
Eigen::MatrixXd IsoTransportMap<PolyType>::GetDerivValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const{
  
  Eigen::MatrixXd output(samps.cols(),allMultis.at(dim)->size());
  
  // evaluate the polynomials for all the orders
  std::vector<Eigen::VectorXd> basisVals(inputSizes(0));
  
  // loop over all of the points
  #pragma omp parallel for
  for(int ptInd = 0; ptInd<samps.cols(); ++ptInd){

    for(int term=0; term<allMultis.at(dim)->size(); ++term){
      
      output(ptInd,term) = 1.0;
      bool derivFound = false;
      for(auto miInd = allMultis.at(dim)->at(term)->GetNzBegin(); miInd!=allMultis.at(dim)->at(term)->GetNzEnd(); ++miInd){
        if(miInd->first!=dim){
          output(ptInd,term) *= PolyType::evaluate_static(miInd->second,samps(miInd->first,ptInd));
        }else{
          output(ptInd,term) *= PolyType::gradient_static(miInd->second,samps(miInd->first,ptInd));
          derivFound = true;
        }
      }
      if(!derivFound)
        output(ptInd,term) = 0.0;
      
    }// loop over terms
  }// loop over points
  
  return output;

}

template<class PolyType>
void IsoTransportMap<PolyType>::FillForConstruction(const Eigen::Ref<const Eigen::MatrixXd>& newSamps,
                         std::vector < std::shared_ptr < Eigen::MatrixXd >> &fs,
                         std::vector < std::shared_ptr < Eigen::MatrixXd >> &gs,
                         int oldNumSamps,
                         int newNumSamps) const
{
  
  // create space for the output
  for(int d=0; d<fs.size(); ++d){
    fs.at(d)->block(oldNumSamps,0,newNumSamps-oldNumSamps,fs.at(d)->cols()).setZero();
    gs.at(d)->block(oldNumSamps,0,newNumSamps-oldNumSamps,fs.at(d)->cols()).setZero();
  }
  
  
  // loop over all of the points
#pragma omp parallel for
  for(int ptInd = 0; ptInd<newSamps.cols(); ++ptInd){

    // evaluate the polynomials for all the orders
    std::vector<Eigen::VectorXd> basisVals(inputSizes(0));

    
    // compute the 1D polynomials for this input point
    #pragma omp parallel for
    for(int d=0; d<inputSizes(0); ++d)
      basisVals.at(d) = PolyType::evaluateAll_static(maxOrders(d),newSamps(d,ptInd));
    
    // now, loop over all terms in the multiindex pool and add the contribution to the output
    auto pool = allMultis.at(0)->GetPool();
    #pragma omp parallel for
    for(int term=0; term<pool->size(); ++term){
      
      // if the term is active, compute its value
      if(pool->IsActive(term)){
        
        double fVal = 1.0;
        Eigen::VectorXd gVals = Eigen::VectorXd::Ones(inputSizes(0));
        vector<bool> wrtFound(inputSizes(0),false);
        
        for(auto miInd = pool->at(term)->GetNzBegin(); miInd!=pool->at(term)->GetNzEnd(); ++miInd){
          fVal *= basisVals.at(miInd->first)(miInd->second);
          wrtFound.at(miInd->first) = true;
        }
        
        for(auto pair : pool->GetActiveIndices(term)){
          (*fs.at(pair.first))(ptInd+oldNumSamps,pair.second) = fVal;
          
          if(wrtFound.at(pair.first)){
            double gVal = 1.0;
            for(auto miInd = pool->at(term)->GetNzBegin(); miInd!=pool->at(term)->GetNzEnd(); ++miInd){
              if(miInd->first==pair.first){
                gVal *= PolyType::gradient_static(miInd->second,newSamps(pair.first,ptInd));
              }else{
                gVal *= basisVals.at(miInd->first)(miInd->second);
              }
            }
            
           (*gs.at(pair.first))(ptInd+oldNumSamps,pair.second) = gVal;
            
          }
        }
        
      }// if term is active
    }// loop over terms
    
  }// loop over points
}


template<class PolyType>
const std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> IsoTransportMap<PolyType>::GetPolys() const{
  std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> output(inputSizes(0));
  for(int i=0; i<inputSizes(0); ++i)
    output.at(i) = make_shared<PolyType>();
  return output;
}


namespace muq {
  namespace Inference{
    template class IsoTransportMap<Monomial>;
    template class IsoTransportMap<LegendrePolynomials1DRecursive>;
    template class IsoTransportMap<HermitePolynomials1DRecursive>;
  }
}



