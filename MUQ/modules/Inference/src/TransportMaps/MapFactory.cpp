
// self include
#include "MUQ/Inference/TransportMaps/MapFactory.h"

// Eigen includes
#include <Eigen/Dense>

// other muq-includes
//#include "MUQ/Optimization/Problems/OptProbBase.h"

//#include "MUQ/Utilities/LinearOperators/Operators/SymmetricOpBase.h"
//#include "MUQ/Utilities/LinearOperators/Solvers/MinRes.h"
//#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Inference/TransportMaps/TransportMap.h"
#include "MUQ/Inference/TransportMaps/IsoTransportMap.h"
#include "MUQ/Inference/TransportMaps/RbfMap.h"
#include "MUQ/Inference/TransportMaps/BasisSet.h"

#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/Monomial.h"

#include "MUQ/Inference/TransportMaps/LinearBasis.h"
#include "MUQ/Inference/TransportMaps/MapUpdateManager.h"

//#include <boost/math/special_functions/erf.hpp>

using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;
using namespace std;


Eigen::VectorXd MapFactory::GetIdentityCoeff(std::shared_ptr<MultiIndexSet> const& multis, int dim, double linearScale){
  
  Eigen::VectorXd coeffs = Eigen::VectorXd::Zero(multis->size());
  
  // find a linear basis in this dimension
  bool linearTermFound = false;
  
  for (int j = 0; j < multis->size(); ++j) {
    
    // check if it's linear
    if(multis->at(j)->GetOrder()==1){
      
      // check if the correct dimension is linear
      if(multis->at(j)->GetValue(dim)==1){
        // set the coefficient
        linearTermFound = true;
        coeffs(j) = linearScale;
        break;
      }
    }
  }
  assert(linearTermFound);
  return coeffs;
}


// implementation of BuildSampsToStdSerial
std::shared_ptr<TransportMap> MapFactory::BuildToNormal(Eigen::MatrixXd const&                             samps,
                                                        std::vector<std::shared_ptr<MultiIndexSet>> const& multis,
                                                        std::vector<Eigen::VectorXi>                       optLevelsIn,
                                                        std::vector<Eigen::VectorXd>                       coeffGuess,
                                                        boost::property_tree::ptree                        options)
{
  const int dim = multis.size();
  assert(dim==samps.rows());
  for(int d=0; d<multis.size(); ++d)
    assert(multis.at(d)->GetMultiIndexLength()==dim);
  
  shared_ptr<TransportMap> map;
  if(coeffGuess.size()>0){
    map = CreateZeroMap(multis,options);
    map->SetCoeffs(coeffGuess);
  }else{
    map = CreateIdentity(multis,options);
  }
  
  MapUpdateManager manager(map, samps.cols(), options.get("TransportMapOpt.Regularization",1e-3));

  manager.UpdateMap(samps, optLevelsIn, options);

  return map;
}

// implementation of BuildSampsToStdSerial
std::shared_ptr<TransportMap> MapFactory::BuildToNormal(Eigen::MatrixXd const&       samps,
                                                        std::vector<BasisSet> const& bases,
                                                        std::vector<Eigen::VectorXi> optLevelsIn,
                                                        std::vector<Eigen::VectorXd> coeffGuess,
                                                        boost::property_tree::ptree  options)
{
  const int dim = bases.size();
  assert(bases.size()==samps.rows());
  
  shared_ptr<TransportMap> map;
  if(coeffGuess.size()>0){
    map = CreateZeroMap(bases);
    map->SetCoeffs(coeffGuess);
  }else{
    map = CreateIdentity(bases);
  }
  
  MapUpdateManager manager(map, samps.cols(), options.get("TransportMapOpt.Regularization",1e-3));
  
  manager.UpdateMap(samps, optLevelsIn, options);
  
  return map;
}


std::shared_ptr<TransportMap> MapFactory::RegressSampsToSamps(Eigen::MatrixXd const&                             sampsIn,
                                                              Eigen::MatrixXd const&                             sampsOut,
                                                              std::vector<std::shared_ptr<MultiIndexSet>> const& multis,
                                                              boost::property_tree::ptree                        options)
{
  // check a few dimensions
  assert(multis.size() == sampsOut.rows());
  assert(sampsOut.rows() == sampsIn.rows());

  int dimInOut = sampsIn.rows();
  int Nsamps   = sampsIn.cols();

  std::vector<Eigen::VectorXd> coeffs(dimInOut);
  shared_ptr<TransportMap> map = CreateZeroMap(multis,options);
  
  // compute the coefficients for each output
  #pragma omp parallel for schedule(static,1)
  for (int i = 0; i < dimInOut; ++i)
    map->coeffs.at(i) = map->GetBasisValues(sampsIn,i).colPivHouseholderQr().solve(sampsOut.row(i).transpose());
  
  return map;
}


std::shared_ptr<TransportMap> MapFactory::CreateIdentity(std::vector<std::shared_ptr<MultiIndexSet>> const& multis,
                                                         boost::property_tree::ptree                        options)
{
  string polyType = options.get("TransportMap.PolyType","Hermite");
  shared_ptr<TransportMap> map;
  vector<Eigen::VectorXd> x0(multis.size());
  
  if(!polyType.compare("Hermite")){
    
    for (int d = 0; d < multis.size(); ++d)
      x0.at(d) = GetIdentityCoeff(multis.at(d), d, 0.5);
    
    map = make_shared<IsoTransportMap<HermitePolynomials1DRecursive>>(multis.size(),multis,x0);
    
  }else if(!polyType.compare("Legendre")){
    
    for (int d = 0; d < multis.size(); ++d)
      x0.at(d) = GetIdentityCoeff(multis.at(d), d, 1.0);
    
    map = make_shared<IsoTransportMap<LegendrePolynomials1DRecursive>>(multis.size(),multis,x0);
    
  }else if(!polyType.compare("Monomial")){
    
    for (int d = 0; d < multis.size(); ++d)
      x0.at(d) = GetIdentityCoeff(multis.at(d), d, 1.0);
    
    map = make_shared<IsoTransportMap<Monomial>>(multis.size(),multis,x0);
    
  }else if(!polyType.compare("Heterogeneous")){
    assert(false);
  }else{
    std::cerr << "ERROR: In MapFactory::BuildToNormal, invalid polynomial type.  Valid options for  TransportMap.PolyType are Hermite, Legendre, Monomial, or Heterogeneous.  I received " << polyType << std::endl << std::endl;
    assert(false);
  }

  return map;
}

std::shared_ptr<TransportMap> MapFactory::CreateIdentity(std::vector<BasisSet> const& bases)
{
  vector<Eigen::VectorXd> coeffs(bases.size());
  
  for(int d=0; d<bases.size(); ++d){
    coeffs.at(d) = Eigen::VectorXd::Zero(bases.at(d).size());
    bool linearTermFound = false;
    
    for(int term=0; term<bases.at(d).size(); ++term){
      if((dynamic_pointer_cast<LinearBasis>(bases.at(d).at(term))!=nullptr)&&(bases.at(d).at(term)->GetDim()==d)){
        coeffs.at(d)(term) = 1.0;
        linearTermFound = true;
        break;
      }
    }
    assert(linearTermFound);
  }
  
  return make_shared<RbfMap>(bases.size(),bases,coeffs);
}

std::shared_ptr<TransportMap> MapFactory::CreateZeroMap(std::vector<BasisSet> const& bases)
{
  vector<Eigen::VectorXd> coeffs(bases.size());
  for(int d=0; d<bases.size(); ++d)
    coeffs.at(d) = Eigen::VectorXd::Zero(bases.at(d).size());
  
  return make_shared<RbfMap>(bases.size(),bases,coeffs);
}

std::shared_ptr<TransportMap> MapFactory::CreateZeroMap(std::vector<std::shared_ptr<MultiIndexSet>> const& multis,
                                                        boost::property_tree::ptree                        options)
{
  string polyType = options.get("TransportMap.PolyType","Hermite");
  shared_ptr<TransportMap> map;
  
  vector<Eigen::VectorXd> coeffs(multis.size());
  for(int d=0; d<multis.size(); ++d)
    coeffs.at(d) = Eigen::VectorXd::Zero(multis.at(d)->size());
  
  if(!polyType.compare("Hermite")){
    
    map = make_shared<IsoTransportMap<HermitePolynomials1DRecursive>>(multis.size(),multis,coeffs);
    
  }else if(!polyType.compare("Legendre")){
    
    map = make_shared<IsoTransportMap<LegendrePolynomials1DRecursive>>(multis.size(),multis,coeffs);
    
  }else if(!polyType.compare("Monomial")){
    
    map = make_shared<IsoTransportMap<Monomial>>(multis.size(),multis,coeffs);
    
  }else if(!polyType.compare("Heterogeneous")){
    assert(false);
  }else{
    std::cerr << "ERROR: In MapFactory::BuildToNormal, invalid polynomial type.  Valid options for  TransportMap.PolyType are Hermite, Legendre, Monomial, or Heterogeneous.  I received " << polyType << std::endl << std::endl;
    assert(false);
  }
  
  return map;
}


///namespace MapFactory{

	class PairwiseTriangularOrderComp{
	public:
	  PairwiseTriangularOrderComp(const Eigen::MatrixXd &samps) : numSamps(samps.cols()), stdSamps(samps.rows(),samps.cols()){
		  Eigen::VectorXd mu = samps.rowwise().sum()/samps.cols();
		  Eigen::MatrixXd diff = samps.colwise()-mu;
		  Eigen::VectorXd std = (diff.array().pow(2).rowwise().sum()/(samps.cols()-1)).array().sqrt();
		  stdSamps = std.array().inverse().matrix().asDiagonal()*diff;
		  stdSamps2 = stdSamps.array().pow(2).matrix();
		  s3sum = stdSamps.array().pow(3).matrix().rowwise().sum()/numSamps;
	  };
	  
	  bool operator()(int ind1, int ind2){
		  
		  double s12 = ((stdSamps2.row(ind1).array()*stdSamps.row(ind2).array()).sum()/numSamps) / s3sum(ind1);
		  double s21 = ((stdSamps2.row(ind2).array()*stdSamps.row(ind1).array()).sum()/numSamps) / s3sum(ind2);
		  
		  return std::abs(s12) > std::abs(s21);
	  };
	  	
	private:
	  const int numSamps;
	  Eigen::MatrixXd stdSamps,stdSamps2;
	  Eigen::VectorXd s3sum;
	};
	
	//}

std::vector<int> MapFactory::GetTriangularOrder(const Eigen::MatrixXd & samps){
	
	std::vector<int> order(samps.rows());
	for(int i=0; i<samps.rows(); ++i)
	  order[i] = i;
	
	std::sort(order.begin(),order.end(),PairwiseTriangularOrderComp(samps));
	 
	return order;
}
