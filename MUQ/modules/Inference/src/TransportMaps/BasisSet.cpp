#include "MUQ/Inference/TransportMaps/BasisSet.h"

#include <typeinfo>

using namespace std;
using namespace muq::Inference;


BasisSet& BasisSet::operator+=(const BasisSet& rhs){
  for(int i=0; i<rhs.size(); ++i)
    Add(rhs.at(i));
  return *this;
}


void BasisSet::Add(std::shared_ptr<UnivariateBasis> const& rhs){
  // check to see if the basis already exists
  for(int i=0; i<size(); ++i){
    if((typeid(*rhs) != typeid(*bases.at(i)))||(rhs->GetDim()!=bases.at(i)->GetDim())){
      bases.push_back(rhs);
      break;
    }
  }
}

BasisSet& BasisSet::operator+=(std::shared_ptr<UnivariateBasis> const& rhs){
  Add(rhs);
  return *this;
}

std::shared_ptr<BasisSet> muq::Inference::operator+=(std::shared_ptr<BasisSet> const& lhs, std::shared_ptr<UnivariateBasis> const& rhs){
  
  (*lhs)+=rhs;
  return lhs;
}

std::shared_ptr<BasisSet> muq::Inference::operator+=(std::shared_ptr<BasisSet> const& lhs, std::shared_ptr<BasisSet> const& rhs){
  
  (*lhs)+=(*rhs);
  return lhs;
  
}