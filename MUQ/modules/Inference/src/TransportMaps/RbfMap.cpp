#include "MUQ/Inference/TransportMaps/RbfMap.h"

#include "MUQ/Inference/TransportMaps/UnivariateBasis.h"
#include "MUQ/Inference/TransportMaps/LinearBasis.h"


using namespace std;
using namespace muq::Utilities;
using namespace muq::Inference;

RbfMap::RbfMap(int                                 inputDim,
               const std::vector<BasisSet>&        basesIn,
               const std::vector<Eigen::VectorXd>& coeffsIn) : TransportMap(inputDim,coeffsIn),
                                                               bases(basesIn)
{
  // check that all dimension and sizes are consistent
  assert(outputSize==coeffsIn.size());
  assert(outputSize==basesIn.size());
  isLowerTriangular = true;
  for(int d=0; d<outputSize; ++d){
    assert(basesIn.at(d).size()==coeffsIn.at(d).size());
    for(int i=0; i<basesIn.at(d).size(); ++i){
      assert(basesIn.at(d).at(i)->GetDim()<inputSizes(0));
      if(basesIn.at(d).at(i)->GetDim()>d)
        isLowerTriangular = false;
    }
  }
}

std::shared_ptr<TransportMap> RbfMap::segment(int startDim, int length) const
{
  assert(startDim + length <= outputSize);
 
  vector<Eigen::VectorXd> segCoeffs(length);
  vector<BasisSet> segBases(length);
  
  for (int d = startDim; d < startDim + length; ++d) {
    segCoeffs.at(d-startDim) = coeffs.at(d);
    
    segBases.at(d-startDim).reserve(bases.at(d).size());
    for(int term=0; term<bases.at(d).size(); ++term)
      segBases.at(d-startDim).push_back(bases.at(d).at(term)->Clone());
  }
  
  return make_shared<RbfMap>(startDim+length, segBases, segCoeffs);
}


Eigen::MatrixXd RbfMap::EvaluateMultiImpl(Eigen::MatrixXd const& xs)
{
  // evaluate all the basis functions
  Eigen::MatrixXd output = Eigen::MatrixXd::Zero(outputSize,xs.cols());
  
  // loop over all of the points
#pragma omp parallel for schedule(static)
  for(int ptInd = 0; ptInd<xs.cols(); ++ptInd){
    
#pragma omp parallel for
    for(int d=0; d<outputSize; ++d){
      
      // fill in the basis function values
      Eigen::VectorXd basisVals(coeffs.at(d).size());
      for(int term=0; term<coeffs.at(d).size(); ++term)
        basisVals(term) = bases.at(d).at(term)->Evaluate(xs.col(ptInd));
      
      output(d,ptInd) = basisVals.dot(coeffs.at(d));
      
    } // loop over dimensions
  }// loop over points
  
  return output;
}

Eigen::MatrixXd RbfMap::JacobianImpl(Eigen::VectorXd const& xs)
{
  // create space for the output
  Eigen::MatrixXd output = Eigen::MatrixXd::Zero(outputSize,inputSizes(0));
  
  for(int col=0; col<inputSizes(0); ++col){
    for(int row=0; row<outputSize; ++row){
      
      // fill in the basis function values
      Eigen::VectorXd basisVals(coeffs.at(row).size());
      for(int term=0; term<coeffs.at(row).size(); ++term)
        basisVals(term) = bases.at(row).at(term)->Derivative(xs,col);
      
      output(row,col) = basisVals.dot(coeffs.at(row));
      
    } // loop over output dimensions
  }// loop over input dimensions
  
  return output;
}

Eigen::VectorXd RbfMap::EvaluateInverse(Eigen::VectorXd const& input, Eigen::VectorXd const& x0) const
{
  // I don't know how to invert a rectangular map or a square map that isn't lower triangular
  assert(inputSizes(0) == outputSize);
  assert(input.size() == outputSize);
  assert(x0.size() == inputSizes(0));
  assert(isLowerTriangular);
  
  
  // set some tolerance and max iteration counts
  const int maxLineIts   = 40;
  const int maxNewtonIts = 100;
  const double residTol  = 1e-8;
  
  // set the initial guess
  Eigen::VectorXd x = x0;
  double resid      = 1.0;
  double deriv      = 1.0;
  
  
  int it = 0;
  
  // loop over each dimension
  for (int d = 0; d < outputSize; ++d) {
    
    Eigen::VectorXd basisVals(bases.at(d).size());
    Eigen::VectorXd gradVals(bases.at(d).size());
    
    // fill in the initial basis values
    for (int j = 0; j < bases.at(d).size(); ++j)
      basisVals(j) = bases.at(d).at(j)->Evaluate(x);
    
    // get the initial residual
    const double currTarget = input(d);
    double currResid        = coeffs.at(d).dot(basisVals) - currTarget;
    
    deriv = 1.0;
    it    = 0;
    
    while ((abs(currResid) > residTol) && (it < maxNewtonIts)) {
      
      // fill in the new derivative values
      for (int j = 0; j < bases.at(d).size(); ++j)
        gradVals(j) = bases.at(d).at(j)->Derivative(x, d);
      
      deriv = coeffs.at(d).dot(gradVals);
      if(deriv<=0){
        std::cout << "Current Iteration = " << it << std::endl;
        std::cout << "Current dimension = " << d << std::endl;
        std::cout << "Current Location = " << x.head(d+1).transpose() << std::endl;
        std::cout << "Current derivative = " << deriv << std::endl;
      }
      assert(deriv > 0);
      
      double step = -1.0 * currResid / deriv;
      
      // perform a backtracing line search
      int lineIts       = 0;
      double propResid  = abs(currResid) + 1.0; // just something big to get us going in the line search
      const double oldx = x(d);
      
      while ((pow(propResid, 2.0) > (1 - 1e-5) * pow(currResid, 2.0)) && (lineIts < maxLineIts)) {
        lineIts++;
        
        x(d) = oldx + step;
        
        // fill in the new basis and derivative values
        for (int j = 0; j < bases.at(d).size(); ++j)
          basisVals(j) = bases.at(d).at(j)->Evaluate(x);
        
        propResid =  coeffs.at(d).dot(basisVals) - currTarget;
        step     *= 0.5;
      }
      
      assert(lineIts < maxLineIts);
      
      currResid = propResid;
      
      ++it;
    }
  }
  
  return x;
}

void RbfMap::Print() const
{
  for (int d = 0; d < outputSize; ++d) {
    std::cout << "Dimension " << d << std::endl;
    for(int term=0; term<bases.at(d).size(); ++term){
      if(dynamic_pointer_cast<ConstantBasis>(bases.at(d).at(term))!=nullptr){
        std::cout << "  Term " << term << " is constant with coefficient = " << coeffs.at(d)(term) << std::endl;
      }else if(dynamic_pointer_cast<LinearBasis>(bases.at(d).at(term))!=nullptr){
        std::cout << "  Term " << term << " is linear in " << bases.at(d).at(term)->GetDim() << " with coefficient = " << coeffs.at(d)(term) << std::endl;
      }else{
        std::cout << "  Term " << term << " is sigmoid in " << bases.at(d).at(term)->GetDim() << " with coefficient = " << coeffs.at(d)(term) << std::endl;
      }
      
    }
  }
}

std::shared_ptr<TransportMap> RbfMap::CreateFixedMap(int startInd, Eigen::VectorXd const& fixedInput) const
{
  // check to make sure the fixed size is feasible
  assert(startInd>=0);
  assert(startInd + fixedInput.size() <= inputSizes(0));
  assert(fixedInput.size()>0);
  
  const int newInputSize = inputSizes(0)-fixedInput.size();
  
  // create a vector holding the new multiindices
  vector<BasisSet> newBases(outputSize);
  vector<Eigen::VectorXd> newCoeffs(outputSize);
  
  // loop over each output dimension
  for (int d = 0; d < outputSize; ++d) {
    
    vector<double> tempCoeffs(1);
    
    // create a constant term, we assume this will always exist
    newBases.at(d).push_back(make_shared<ConstantBasis>(0));
    tempCoeffs.at(0) = 0.0;
    
    // fix each term in the multiindex
    for (int i = 0; i < bases.at(d).size(); ++i) {
      
      int basisDim = bases.at(d).at(i)->GetDim();
      if(dynamic_pointer_cast<ConstantBasis>(bases.at(d).at(i))!=nullptr){
        tempCoeffs.at(0) += coeffs.at(d)(i);
      }else if((basisDim>=startInd)&&(basisDim<startInd+fixedInput.size())){
        tempCoeffs.at(0) += coeffs.at(d)(i)*bases.at(d).at(i)->Evaluate(fixedInput(basisDim-startInd));
      }else{
        newBases.at(d).push_back(bases.at(d).at(i)->Clone());
        if(basisDim>startInd)
          newBases.at(d).at(newBases.at(d).size()-1)->SetDim(basisDim-fixedInput.size());
        
        tempCoeffs.push_back(coeffs.at(d)(i));
      }
    }
    
    // copy the coefficients into an Eigen::Vector
    newCoeffs.at(d) = Eigen::Map<Eigen::VectorXd>(&tempCoeffs[0],tempCoeffs.size());
  }
  
  return make_shared<RbfMap>(newInputSize, newBases, newCoeffs);
}

double RbfMap::GetSecondDerivative(Eigen::VectorXd const& input, int outputDim, int wrt1, int wrt2) const
{
  if ((wrt1 > outputDim) || (wrt2 > outputDim)) {
    return 0.0;
  } else {
    
    Eigen::VectorXd basisVals(bases.at(outputDim).size());
    for(int term=0; term<bases.at(outputDim).size(); ++term)
      basisVals(term) = bases.at(outputDim).at(term)->SecondDerivative(input,wrt1,wrt2);
    return coeffs.at(outputDim).dot(basisVals);
  }
}


Eigen::MatrixXd RbfMap::GetBasisValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const{
  
  Eigen::MatrixXd output(samps.cols(),bases.at(dim).size());
  
  // loop over all of the points
  for(int ptInd = 0; ptInd<samps.cols(); ++ptInd){
    for(int term=0; term<bases.at(dim).size(); ++term){
      output(ptInd,term) = bases.at(dim).at(term)->Evaluate(samps.col(ptInd));
      
    }// loop over terms
  }// loop over points
  
  return output;
}

Eigen::MatrixXd RbfMap::GetDerivValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const{
  
  Eigen::MatrixXd output(samps.cols(),bases.at(dim).size());
  
  // loop over all of the points
  for(int ptInd = 0; ptInd<samps.cols(); ++ptInd){
    for(int term=0; term<bases.at(dim).size(); ++term){
      output(ptInd,term) = bases.at(dim).at(term)->Derivative(samps.col(ptInd),dim);
      
    }// loop over terms
  }// loop over points
  
  return output;
}


void RbfMap::FillForConstruction(const Eigen::Ref<const Eigen::MatrixXd>& newSamps,
                                 std::vector < std::shared_ptr < Eigen::MatrixXd >> &fs,
                                 std::vector < std::shared_ptr < Eigen::MatrixXd >> &gs,
                                 int oldNumSamps,
                                 int newNumSamps) const
{
  // create space for the output
  for(int d=0; d<fs.size(); ++d){
    fs.at(d)->block(oldNumSamps,0,newNumSamps-oldNumSamps,fs.at(d)->cols()) = GetBasisValues(newSamps,d);
    gs.at(d)->block(oldNumSamps,0,newNumSamps-oldNumSamps,fs.at(d)->cols()) = GetDerivValues(newSamps,d);
  }
}


