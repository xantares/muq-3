
#include "MUQ/Inference/ProblemClasses/InferenceProblemBase.h"

// std includes
#include <functional>

// other muq inclues
#include "MUQ/Utilities/FiniteDifferencer.h"

using namespace muq::utilities;
using namespace muq::Inference;
using namespace std;


/** Evaluate the gradient of the log density at xc */
double InferenceProblemBase::GradLogEval(const Eigen::VectorXd& xc, Eigen::VectorXd& grad)
{
  auto f1 = std::bind(&InferenceProblemBase::LogEval, this, std::placeholders::_1);

  return computeFdGrad(f1, xc, grad, 1e-8, 1e-6);
}

