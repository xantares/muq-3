
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"

// std library includes
#include <memory>

// other muq includes
#include "MUQ/Inference/MCMC/SingleChainMCMC.h"

using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;
using namespace std;

InferenceProblem::InferenceProblem(std::shared_ptr<ModGraph>  completeModel,
                                   vector<string> const     & inputOrder,
                                   shared_ptr<AbstractMetric> metric) : AbstractSamplingProblem(
                                                                          completeModel->inputSizes() (0),
                                                                          completeModel,
                                                                          metric)
{
  SetGraphImpl(completeModel, inputOrder);
}

InferenceProblem::InferenceProblem(std::shared_ptr<ModGraph>                                       completeModel,
                                   std::shared_ptr<muq::Inference::AbstractMetric> metric) : AbstractSamplingProblem(
                                                                                               completeModel->inputSizes() (
                                                                                                 0),
                                                                                               completeModel,
                                                                                               metric)
{
  SetGraphImpl(completeModel, std::vector<std::string>());
}

void InferenceProblem::SetGraph(std::shared_ptr<ModGraph> graph)
{
  SetGraphImpl(graph, std::vector<std::string>());
}

void InferenceProblem::SetGraph(std::shared_ptr<ModGraph> graph, std::vector<std::string> const& inOrder)
{
	SetGraphImpl(graph, inOrder);
  
}

  ///Set the graph. This is necessary so that the virtual function isn't called during construction.
  void InferenceProblem::SetGraphImpl(std::shared_ptr<ModGraph> graph,  std::vector<std::string> const& inOrder){
	  AbstractSamplingProblem::SetGraphImpl(graph);


  errorMod = std::dynamic_pointer_cast<Density>(posteriorGraph->GetNodeModel(string("likelihood")));

  // for the marginal target the order of the two inputs matters
  likelihood = ModGraphDensity::Create(posteriorGraph, string("likelihood"), inOrder);
  posterior = ModGraphDensity::Create(posteriorGraph, string("posterior"), inOrder);

  // extract the prior, if the prior is only a single node, extract just that node, otherwise, use a ModGraphDensity
  std::shared_ptr<ModGraphDensity> tempPrior = ModGraphDensity::Create(posteriorGraph, string("prior"));

  if (tempPrior->genericPiece->internalGraph->NumNodes() == 3) {
    prior = std::dynamic_pointer_cast<Density>(tempPrior->genericPiece->internalGraph->GetNodeModel("prior"));
  } else {
    prior = tempPrior;
  }


  //make sure both models have only input
    assert(posteriorGraph->inputSizes().size() <= 2);
    assert(likelihood->inputSizes.size() <= 2);
    assert(prior->inputSizes.size() == 1);
  if (posteriorGraph->inputSizes().size() == 1) {
    assert(posteriorGraph->inputSizes() (0) == likelihood->inputSizes(0));
  }

  // check to see if a forward model was explicitly defined
  if (posteriorGraph->NodeExists("forwardModel")) {
    forwardMod = ModGraphPiece::Create(posteriorGraph, string("forwardModel"), inOrder); //now pull it out
  }
  }

std::shared_ptr<MCMCState> InferenceProblem::ConstructState(Eigen::VectorXd const& state, int const currIteration, shared_ptr<HDF5LogEntry> logEntry)
{
    assert(posteriorGraph->inputSizes().rows() == 1 && posteriorGraph->inputSizes() (0)  == samplingDim);

  //compute the prior first, in case it's not finite
  auto   logPrior = boost::optional<double>(prior->LogDensity(state));
  double logLikelihood;

  boost::optional<Eigen::VectorXd> likelihoodGrad;

  boost::optional<Eigen::MatrixXd> metric;
  boost::optional<Eigen::VectorXd> priorGrad;

  //next compute the likelihood
  if (std::isfinite(*logPrior)) {
    logLikelihood = likelihood->LogDensity(state);
  } else {
    return nullptr; //invalid if prior is not finite
  }
  //also invalid if likelihood is not finite
  if (!std::isfinite(logLikelihood)) {
    return nullptr;
  }

  boost::optional<Eigen::VectorXd> forwardModel;
  if (forwardMod) {
    forwardModel =  boost::optional<Eigen::VectorXd>(forwardMod->Evaluate(state));
  } else {
    forwardModel =  boost::optional<Eigen::VectorXd>();
  }

  //compute likelihood grad as desired
  if (useLikelihoodGrad) {
    likelihoodGrad =
      boost::optional<Eigen::VectorXd>(likelihood->Gradient(state, Eigen::VectorXd::Constant(1, 1.0), 0));
  } else {
    likelihoodGrad = boost::optional<Eigen::VectorXd>();
  }

  //compute metric as desired
  if (useMetric) {
    metric = boost::optional<Eigen::MatrixXd>(metricObject->ComputeMetric(state));
  } else {
    metric = boost::optional<Eigen::MatrixXd>();
  }

  //compute prior grad as desired
  if (usePriorGrad) {
    priorGrad = boost::optional<Eigen::MatrixXd>(prior->Gradient(state, Eigen::VectorXd::Constant(1, 1.0), 0));
  } else {
    priorGrad = boost::optional<Eigen::VectorXd>();
  }


  auto mcmcState = std::make_shared<MCMCState>(state,
                                               logLikelihood,
                                               likelihoodGrad,
                                               logPrior,
                                               priorGrad,
                                               metric,
                                               forwardModel);

  return mcmcState;
}

void InferenceProblem::InvalidateCaches()
{
  if (forwardMod) {
    forwardMod->InvalidateCache();
  }
  errorMod->InvalidateCache();
  likelihood->InvalidateCache();
  prior->InvalidateCache();
}

