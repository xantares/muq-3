import unittest
import numpy as np

import libmuqUtilities
import libmuqModelling
import libmuqInference

class GooMod(libmuqModelling.ModPiece):
    def __init__(self):
        super(GooMod, self).__init__([3, 3], 3, False, False, False, False, False)

    def EvaluateImpl(self, inputs):
        return inputs[1]

class ImportanceSamplerTest(unittest.TestCase):
    def testImportanceSampler(self):
        seed = libmuqUtilities.RandomGeneratorTemporarySetSeed(56335740)

        # taking the expection of this function
        f = GooMod()

        # number of importance samples 
        N = int(1e3)

        # expectation wrt to this distribution
        piMean = [0.1, 0.01, 0.0]
        piCov  = [[0.5, 0.1, 0.0], [0.1, 0.5, 0.0], [0.0, 0.0, 0.5]]
        pi     = libmuqModelling.GaussianDensity(piMean, piCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        # importance sampling pair
        qPair = libmuqModelling.GaussianPair(3)

        # make the graph 
        graph = libmuqModelling.ModGraph()

        graph.AddNode(f, "function")
        graph.AddNode(pi, "pi")
        graph.AddNode(libmuqModelling.VectorPassthroughModel(3), "para")
        graph.AddNode(libmuqModelling.VectorPassthroughModel(3), "inputPara")
        graph.AddNode(qPair.GetDensity(), "qDens")
        graph.AddNode(qPair.GetRV(), "qRV")

        graph.AddEdge("para", "pi", 0)
        graph.AddEdge("para", "qDens", 0)
        graph.AddEdge("para", "function", 1)
        graph.AddEdge("inputPara", "function", 0)

        # options for the importance sampler 
        para = dict()

        para["ImportanceSampler.OutputNames"] = "function,pi,qDens,qRV"
        para["ImportanceSampler.MarginalizedNode"] = "para"
        para["ImportanceSampler.N"] = N

        # create importance sampler
        ipSampler = libmuqInference.ImportanceSampler(graph, para)

        input = [1.0, 2.0, 3.0]

        # compute the expecation
        result = ipSampler.Evaluate([input])

        # precomputed
        expectedResult = [0.12753135211209898, 0.06355750694756766, 0.009619665430829727]

        self.assertEqual(len(result), len(expectedResult))
        for i in range(len(result)):
            self.assertAlmostEqual(result[i], expectedResult[i], 12)
