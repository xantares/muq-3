
#include "gtest/gtest.h"


#include "MUQ/Inference/TransportMaps/TransportMap.h"
#include "MUQ/Inference/TransportMaps/IsoTransportMap.h"
#include "MUQ/Inference/TransportMaps/MapFactory.h"
#include "MUQ/Inference/TransportMaps/Sigmoids.h"
#include "MUQ/Inference/TransportMaps/RbfMap.h"
#include "MUQ/Inference/TransportMaps/LinearBasis.h"
#include "MUQ/Inference/TransportMaps/BasisFactory.h"

#include <boost/math/constants/constants.hpp>

#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/Monomial.h"
#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"

#include "MUQ/Utilities/RandomGenerator.h"

using namespace boost::math::constants;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace std;


TEST(InferenceTransportMaps, TriangularOrdering_Quadratic){

  const int numSamps = 1e3;
	Eigen::MatrixXd samps = RandomGenerator::GetNormalRandomMatrix(2,numSamps);
	samps.row(1) += samps.row(0).array().pow(2).matrix();
	
	std::vector<int> order = MapFactory::GetTriangularOrder(samps);
	EXPECT_EQ(0,order[0]);
	EXPECT_EQ(1,order[1]);
	
	samps = RandomGenerator::GetNormalRandomMatrix(2,numSamps);
	samps.row(0) += samps.row(1).array().pow(2).matrix();
	
	order = MapFactory::GetTriangularOrder(samps);
	EXPECT_EQ(1,order[0]);
	EXPECT_EQ(0,order[1]);
}

class InferenceTransportMaps_MonomialSimple : public ::testing::Test{

	protected:
	virtual void SetUp() {
    
    dim = 3;
    const int maxOrder = 3;
    
    auto pool = make_shared<MultiIndexPool>();
    vector<shared_ptr<MultiIndexSet>> multis(dim);
    vector<Eigen::VectorXd> coeffs(dim);
    for(int d=0; d<dim; ++d){
      multis.at(d) = MultiIndexFactory::CreateTotalOrder(dim,maxOrder,0,make_shared<DimensionLimiter>(0,d+1),pool);
      coeffs.at(d) = Eigen::VectorXd::Ones(multis.at(d)->size());
    }
    
    map = make_shared<IsoTransportMap<Monomial>>(dim,multis,coeffs);
	};
  
  int dim;
  shared_ptr<TransportMap> map;
};

TEST_F(InferenceTransportMaps_MonomialSimple, BasicConstruction)
{
  
  EXPECT_EQ(dim,map->outputSize);
  EXPECT_EQ(dim,map->inputSizes(0));
  EXPECT_EQ(1,map->inputSizes.size());
  
  // now evaluate the map
  Eigen::VectorXd input = 0.5*Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd output = map->Evaluate(input);
  EXPECT_DOUBLE_EQ(1+0.5+0.25+0.125,output(0));
  EXPECT_DOUBLE_EQ(1+2*(0.5+0.25+0.125)+0.25+0.125+0.125,output(1));
  
}

TEST_F(InferenceTransportMaps_MonomialSimple, Segment)
{
  
  Eigen::VectorXd input = 0.5*Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd result0 = map->Evaluate(input);
  
  EXPECT_EQ(dim,map->outputSize);
  EXPECT_EQ(dim,map->inputSizes(0));
  EXPECT_EQ(1,map->inputSizes.size());
  
  // try to segment the map
  auto headMap = map->head(1);
  EXPECT_EQ(1,headMap->outputSize);
  EXPECT_EQ(1,headMap->inputSizes(0));
  
  Eigen::VectorXd result1 = headMap->Evaluate(input.head(1));
  EXPECT_DOUBLE_EQ(result0(0),result1(0));
  
  auto tailMap = map->tail(2);
  EXPECT_EQ(2,tailMap->outputSize);
  EXPECT_EQ(dim,tailMap->inputSizes(0));
  
  Eigen::VectorXd result2 = tailMap->Evaluate(input);
  EXPECT_DOUBLE_EQ(result0(1),result2(0));
  EXPECT_DOUBLE_EQ(result0(2),result2(1));
}


TEST_F(InferenceTransportMaps_MonomialSimple, Fixed)
{
  Eigen::VectorXd input(dim);
  input << 0.25, 0.5, 0.75;
  Eigen::VectorXd result0 = map->Evaluate(input);
  
  // tail map will have three inputs but 2 outputs
  auto tailMap = map->tail(2);
  EXPECT_EQ(2,tailMap->outputSize);
  EXPECT_EQ(dim,tailMap->inputSizes(0));
  
  Eigen::VectorXd result2 = tailMap->Evaluate(input);
  EXPECT_DOUBLE_EQ(result0(1),result2(0));
  EXPECT_DOUBLE_EQ(result0(2),result2(1));
  
  // fixedHeadMap will fix the first input of tail map, resulting in two inputs and two outputs
  auto fixedHeadMap = tailMap->CreateFixedMap(0,input.head(1));
  EXPECT_EQ(2,fixedHeadMap->outputSize);
  EXPECT_EQ(2,fixedHeadMap->inputSizes(0));
  
  Eigen::VectorXd result3 = fixedHeadMap->Evaluate(input.tail(2));
  EXPECT_DOUBLE_EQ(result2(0),result3(0));
  EXPECT_DOUBLE_EQ(result2(1),result3(1));
  
  // fixedTailMap will fix the second 2 inputs of tail map, resulting in one input and two outputs
  auto fixedTailMap = tailMap->CreateFixedMap(1,input.tail(2));
  EXPECT_EQ(2,fixedTailMap->outputSize);
  EXPECT_EQ(1,fixedTailMap->inputSizes(0));
 
  Eigen::VectorXd result4 = fixedTailMap->Evaluate(input.head(1));
  EXPECT_DOUBLE_EQ(result2(0),result4(0));
  EXPECT_DOUBLE_EQ(result2(1),result4(1));
  
}


TEST_F(InferenceTransportMaps_MonomialSimple, Inverse)
{

  Eigen::VectorXd input = Eigen::VectorXd::Random(dim);
  Eigen::VectorXd output = map->Evaluate(input);
  
  Eigen::VectorXd input2 = map->EvaluateInverse(output,Eigen::VectorXd::Zero(dim));
  
  for(int d=0; d<dim; ++d)
    EXPECT_NEAR(input(d),input2(d),1e-4);
}


class InferenceTransportMaps_HermiteSimple : public ::testing::Test{
  
protected:
  virtual void SetUp() {
    
    dim = 3;
    const int maxOrder = 3;
    
    auto pool = make_shared<MultiIndexPool>();
    vector<shared_ptr<MultiIndexSet>> multis(dim);
    vector<Eigen::VectorXd> coeffs(dim);
    for(int d=0; d<dim; ++d){
      multis.at(d) = MultiIndexFactory::CreateTotalOrder(dim,maxOrder,0,make_shared<DimensionLimiter>(0,d+1),pool);
      coeffs.at(d) = Eigen::VectorXd::Ones(multis.at(d)->size());
    }
    
    map = make_shared<IsoTransportMap<HermitePolynomials1DRecursive>>(dim,multis,coeffs);
  };
  
  int dim;
  shared_ptr<TransportMap> map;
};

TEST_F(InferenceTransportMaps_HermiteSimple, BasicConstruction)
{
  
  EXPECT_EQ(dim,map->outputSize);
  EXPECT_EQ(dim,map->inputSizes(0));
  EXPECT_EQ(1,map->inputSizes.size());
  
  // now evaluate the map
  Eigen::VectorXd input = 0.5*Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd output = map->Evaluate(input);
  EXPECT_DOUBLE_EQ(1+1+-1+-5,output(0));
  EXPECT_DOUBLE_EQ(-10,output(1));
  
}

TEST_F(InferenceTransportMaps_HermiteSimple, Segment)
{
  
  Eigen::VectorXd input = 0.5*Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd result0 = map->Evaluate(input);
  
  EXPECT_EQ(dim,map->outputSize);
  EXPECT_EQ(dim,map->inputSizes(0));
  EXPECT_EQ(1,map->inputSizes.size());
  
  // try to segment the map
  auto headMap = map->head(1);
  EXPECT_EQ(1,headMap->outputSize);
  EXPECT_EQ(1,headMap->inputSizes(0));
  
  Eigen::VectorXd result1 = headMap->Evaluate(input.head(1));
  EXPECT_DOUBLE_EQ(result0(0),result1(0));
  
  auto tailMap = map->tail(2);
  EXPECT_EQ(2,tailMap->outputSize);
  EXPECT_EQ(dim,tailMap->inputSizes(0));
  
  Eigen::VectorXd result2 = tailMap->Evaluate(input);
  EXPECT_DOUBLE_EQ(result0(1),result2(0));
  EXPECT_DOUBLE_EQ(result0(2),result2(1));
}


TEST_F(InferenceTransportMaps_HermiteSimple, Fixed)
{
  Eigen::VectorXd input(dim);
  input << 0.25, 0.5, 0.75;
  Eigen::VectorXd result0 = map->Evaluate(input);
  
  // tail map will have three inputs but 2 outputs
  auto tailMap = map->tail(2);
  EXPECT_EQ(2,tailMap->outputSize);
  EXPECT_EQ(dim,tailMap->inputSizes(0));
  
  Eigen::VectorXd result2 = tailMap->Evaluate(input);
  EXPECT_DOUBLE_EQ(result0(1),result2(0));
  EXPECT_DOUBLE_EQ(result0(2),result2(1));
  
  // fixedHeadMap will fix the first input of tail map, resulting in two inputs and two outputs
  auto fixedHeadMap = tailMap->CreateFixedMap(0,input.head(1));
  EXPECT_EQ(2,fixedHeadMap->outputSize);
  EXPECT_EQ(2,fixedHeadMap->inputSizes(0));
  
  Eigen::VectorXd result3 = fixedHeadMap->Evaluate(input.tail(2));
  EXPECT_DOUBLE_EQ(result2(0),result3(0));
  EXPECT_DOUBLE_EQ(result2(1),result3(1));
  
  // fixedTailMap will fix the second 2 inputs of tail map, resulting in one input and two outputs
  auto fixedTailMap = tailMap->CreateFixedMap(1,input.tail(2));
  EXPECT_EQ(2,fixedTailMap->outputSize);
  EXPECT_EQ(1,fixedTailMap->inputSizes(0));
  
  Eigen::VectorXd result4 = fixedTailMap->Evaluate(input.head(1));
  EXPECT_DOUBLE_EQ(result2(0),result4(0));
  EXPECT_DOUBLE_EQ(result2(1),result4(1));
  
}


TEST_F(InferenceTransportMaps_HermiteSimple, Inverse)
{
  Eigen::VectorXd refVec(dim);
  refVec << 0.2, -0.6, 0.75;
  
  Eigen::VectorXd tarVec = map->EvaluateInverse(refVec,Eigen::VectorXd::Zero(dim));
  Eigen::VectorXd refVec2 = map->Evaluate(tarVec);
  
  for(int d=0; d<dim; ++d)
    EXPECT_NEAR(refVec(d),refVec2(d),1e-4);
}


class InferenceTransportMaps_RbfSimple : public ::testing::Test{
  
protected:
  virtual void SetUp() {
    
    dim = 3;
    const int maxOrder = 3;
    
    vector<BasisSet> bases = BasisFactory::CreateLinear(dim);
    
    // add sigmoid bases at a few locations
    bases.at(0) += make_shared<Sigmoid2>(0,0.5,2.0);
    bases.at(1) += make_shared<Sigmoid2>(0,0.5,2.0);
    bases.at(1) += make_shared<Sigmoid2>(1,-0.5,1.0);
    bases.at(2) += make_shared<Sigmoid2>(0,-0.5,2.0);
    bases.at(2) += make_shared<Sigmoid2>(1,0.5,4.0);
    bases.at(2) += make_shared<Sigmoid2>(2,0.25,3.0);
    
    vector<Eigen::VectorXd> coeffs(dim);
    for(int d=0; d<dim; ++d)
      coeffs.at(d) = Eigen::VectorXd::Ones(bases.at(d).size());
    
    map = make_shared<RbfMap>(dim,bases,coeffs);
  };
  
  int dim;
  shared_ptr<TransportMap> map;
};



TEST_F(InferenceTransportMaps_RbfSimple, BasicConstruction)
{
  
  EXPECT_EQ(dim,map->outputSize);
  EXPECT_EQ(dim,map->inputSizes(0));
  EXPECT_EQ(1,map->inputSizes.size());
  
  // now evaluate the map
  Eigen::VectorXd input = 0.5*Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd output = map->Evaluate(input);
  EXPECT_NEAR(1.5,output(0),1e-6);
  EXPECT_NEAR(2.5,output(1),1e-6);
  EXPECT_NEAR(2.9102564102564101,output(2),1e-6);
  
}

TEST_F(InferenceTransportMaps_RbfSimple, Segment)
{
  
  Eigen::VectorXd input = 0.5*Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd result0 = map->Evaluate(input);
  
  EXPECT_EQ(dim,map->outputSize);
  EXPECT_EQ(dim,map->inputSizes(0));
  EXPECT_EQ(1,map->inputSizes.size());
  
  // try to segment the map
  auto headMap = map->head(1);
  EXPECT_EQ(1,headMap->outputSize);
  EXPECT_EQ(1,headMap->inputSizes(0));
  
  Eigen::VectorXd result1 = headMap->Evaluate(input.head(1));
  EXPECT_DOUBLE_EQ(result0(0),result1(0));
  
  auto tailMap = map->tail(2);
  EXPECT_EQ(2,tailMap->outputSize);
  EXPECT_EQ(dim,tailMap->inputSizes(0));
  
  Eigen::VectorXd result2 = tailMap->Evaluate(input);
  EXPECT_DOUBLE_EQ(result0(1),result2(0));
  EXPECT_DOUBLE_EQ(result0(2),result2(1));
}


TEST_F(InferenceTransportMaps_RbfSimple, Fixed)
{
  Eigen::VectorXd input(dim);
  input << 0.25, 0.5, 0.75;
  Eigen::VectorXd result0 = map->Evaluate(input);
  
  // tail map will have three inputs but 2 outputs
  auto tailMap = map->tail(2);
  
  EXPECT_EQ(2,tailMap->outputSize);
  EXPECT_EQ(dim,tailMap->inputSizes(0));
  
  Eigen::VectorXd result2 = tailMap->Evaluate(input);
  EXPECT_DOUBLE_EQ(result0(1),result2(0));
  EXPECT_DOUBLE_EQ(result0(2),result2(1));
  
  // fixedHeadMap will fix the first input of tail map, resulting in two inputs and two outputs
  auto fixedHeadMap = tailMap->CreateFixedMap(0,input.head(1));
  EXPECT_EQ(2,fixedHeadMap->outputSize);
  EXPECT_EQ(2,fixedHeadMap->inputSizes(0));
  
  Eigen::VectorXd result3 = fixedHeadMap->Evaluate(input.tail(2));
  EXPECT_DOUBLE_EQ(result2(0),result3(0));
  EXPECT_DOUBLE_EQ(result2(1),result3(1));
  
  // fixedTailMap will fix the second 2 inputs of tail map, resulting in one input and two outputs
  auto fixedTailMap = tailMap->CreateFixedMap(1,input.tail(2));
  EXPECT_EQ(2,fixedTailMap->outputSize);
  EXPECT_EQ(1,fixedTailMap->inputSizes(0));
  
  Eigen::VectorXd result4 = fixedTailMap->Evaluate(input.head(1));
  EXPECT_DOUBLE_EQ(result2(0),result4(0));
  EXPECT_DOUBLE_EQ(result2(1),result4(1));
}


TEST_F(InferenceTransportMaps_RbfSimple, Inverse)
{
  
  Eigen::VectorXd input(dim);
  input << 0.25, -0.1, 0.6;
  Eigen::VectorXd output = map->Evaluate(input);
  
  Eigen::VectorXd input2 = map->EvaluateInverse(output,Eigen::VectorXd::Zero(dim));
  
  for(int d=0; d<dim; ++d)
    EXPECT_NEAR(input(d),input2(d),1e-4);
}




class InferenceTransportMaps_BananaSplit : public ::testing::Test{

	protected:
	virtual void SetUp() {
	    RandomGeneratorTemporarySetSeed(5192012);

	    // first, generate a bunch of samples from a univariate lognormal distribution
	    samps.resize(2, numSamps);

	    // create a univariate standard normal random variable
	    for (int i = 0; i < numSamps; ++i) {
        auto temp = RandomGenerator::GetNormalRandomVector(2);
	      samps(0, i) = a * temp(0);
	      samps(1, i) = temp(1) / a + b * (pow(samps(0, i), 2.0) + a * a);
	    }	
	};
	
	virtual void TearDown(){
		
	    Eigen::VectorXd xs(20), ys(20);
	    xs.setLinSpaced(10, -2, 2);
	    ys.setLinSpaced(10, -0.5, 6);
	    for (int i = 0; i < ys.size(); ++i) {
	      for (int j = 0; j < xs.size(); ++j) {
	        Eigen::VectorXd input(2);
	        input << xs(j), ys(i);
	        Eigen::VectorXd mapVal = map->Evaluate(input);
	        double mapJac          = map->Jacobian(input).diagonal().prod();
          
	        double mapDens         = exp(-0.5 * mapVal.squaredNorm()) * mapJac/two_pi<double>();

	        Eigen::VectorXd trueVal(2);
	        trueVal(0) = input(0) / a;
	        trueVal(1) = a * (input(1) - b * (input(0) * input(0) + a * a));
	        double trueJac  = 1.0;
	        double trueDens = exp(-0.5 * trueVal.squaredNorm()) * trueJac/two_pi<double>();

          EXPECT_NEAR(trueVal(0),mapVal(0),1e-1);
          EXPECT_NEAR(trueVal(1),mapVal(1),1e-1);
	        EXPECT_NEAR(trueDens, mapDens, 1e-2);
	      }
	    }

	    auto marginalMaps = map->Split(1);

	    // check the first part of the split
	    for (int j = 0; j < xs.size(); ++j) {
	      Eigen::VectorXd input(1);
	      input << xs(j);
	      Eigen::VectorXd mapVal = marginalMaps.first->Evaluate(input);
	      double mapJac          = marginalMaps.first->Jacobian(input).diagonal().prod();
	      double mapDens         = exp(-0.5 * mapVal.squaredNorm()) * mapJac/root_two_pi<double>();

	      Eigen::VectorXd trueVal(1);
	      trueVal(0) = input(0) / a;
	      double trueJac  = 1.0;
	      double trueDens = exp(-0.5 * trueVal.squaredNorm()) * trueJac/root_two_pi<double>();

	      EXPECT_NEAR(trueDens, mapDens, 1e-2);
	    }
	};


  const double a = 1.0;
  const double b = 2;
	const int numSamps = 5e4;
	
	Eigen::MatrixXd samps;
	std::shared_ptr<TransportMap> map;
};

TEST_F(InferenceTransportMaps_BananaSplit, BasicPolynomial)
{
  const int dim = 2;
  
  // add the constant and linear terms for each output
  auto multis = MultiIndexFactory::CreateTriTotalOrder(dim, 1);

  // add one quadratic term to the map
  multis.at(1) += MultiIndexFactory::CreateSingleTerm(dim, 0, 2);

  // build the map
  map = MapFactory::BuildToNormal(samps, multis);
  
}

TEST_F(InferenceTransportMaps_BananaSplit, BasicRbf)
{
  const int dim = 2;
  
  // add the constant and linear terms for each output
  auto bases = BasisFactory::CreateLinear(dim);
  
  for(double x=-5.0; x<5.0; x+=0.5)
    bases.at(1) += make_shared<Sigmoid1>(0,x,1.0);
  
  // build the map
  map = MapFactory::BuildToNormal(samps, bases);
}


TEST_F(InferenceTransportMaps_BananaSplit, MultilevelPolynomial)
{
  const int dim = 2;
  
  // add the constant and linear terms for each output
  auto multis = MultiIndexFactory::CreateTriTotalOrder(dim, 1);
  
  // add one quadratic term to the map
  multis.at(1) += MultiIndexFactory::CreateSingleTerm(dim, 0, 2);
  
  // get the optimization levels based on the total order of each polynomial basis
  vector<Eigen::VectorXi> optLevels(2);
  optLevels.at(0).resize(1);
  optLevels.at(0) << 2;
  optLevels.at(1).resize(2);
  optLevels.at(1) << 3,4;
  
  // build the map
  map = MapFactory::BuildToNormal(samps, multis, optLevels);
}

TEST(InferenceTransportMaps_DiagonalRbf, Construction)
{
  const int numSamps = 1e4;
  Eigen::MatrixXd samps = RandomGenerator::GetNormalRandomMatrix(1,numSamps) + RandomGenerator::GetNormalRandomMatrix(1,numSamps).array().pow(2.0).matrix();
  
  boost::property_tree::ptree options;
  options.put("TransportMaps.RbfType","Sigmoid1");
  options.put("TransportMaps.RbfScale",3.0);
  auto bases = BasisFactory::CreateDiagonal(samps,options);
  auto map = MapFactory::BuildToNormal(samps, bases);
  
  Eigen::VectorXd trueLogPdf(7);
  trueLogPdf << -1.696101115214261,
  -1.201220350557616,
  -1.576123232218563,
  -2.383636814592162,
  -3.237690006184883,
  -3.965298315756504,
  -4.564264067677385;
  
  Eigen::VectorXd testPts(7);
  testPts << -0.666666666666667,
  0.444444444444445,
  1.555555555555555,
  2.666666666666667,
  3.777777777777778,
  4.888888888888889,
  6.000000000000000;
  
  for(int i=0; i<testPts.size(); ++i){
    Eigen::VectorXd pt = testPts.row(i).transpose();
    EXPECT_NEAR(trueLogPdf(i),-0.5*pow(map->Evaluate(pt)(0),2.0)+log(map->Jacobian(pt)(0,0)) - log(root_two_pi<double>()),0.1*abs(trueLogPdf(i)));
  }
}

TEST(InferenceSigmoids, S1)
{
  Eigen::VectorXd xs(20);
  xs.setLinSpaced(20,-3,3);
  const double a = 2.0;
  const double x0 = 0.5;
  
  Eigen::VectorXd trueVals(20);
  trueVals << -1.208672797000000e+01, -9.892317120000000e+00, -7.877895497000000e+00, -6.052793238000000e+00, -4.434462732000000e+00, -3.047050703000000e+00, -1.916323816000000e+00, -1.061035264000000e+00, -4.824314439000000e-01, -1.550437462000000e-01, -2.237098723000000e-02, -1.028132796000000e-05,  1.359027191000000e-02, 1.213836866000000e-01, 4.115591928000000e-01, 9.457047513000000e-01, 1.754340832000000e+00, 2.840123066000000e+00, 4.186477285000000e+00, 5.768125802000000e+00;
  
  Eigen::VectorXd trueDs(20);
  trueDs << 7.229948547957360e+00,     6.666858325919374e+00,     6.086295148909463e+00,     5.463408287401454e+00,     4.773006385934902e+00, 3.999868777451926e+00,     3.150925145933047e+00,     2.264729283246691e+00,     1.412954312828350e+00,     6.904844937867527e-01, 1.949107351607939e-01,     1.172026294454223e-03,     1.401926205750896e-01,     5.896924510713026e-01,     1.280828140817767e+00, 2.118014138624374e+00,     3.004264730364429e+00,     3.863026610901843e+00,     4.650046575706066e+00,     5.353629149820222e+00;
  
  Eigen::VectorXd trueD2s(20);
  trueD2s << -1.777086326043118e+00,    -1.798967452511076e+00,    -1.891367680336526e+00,    -2.067440045225970e+00,    -2.313817704804267e+00, -2.579606416227695e+00,    -2.776796873958698e+00,    -2.797421539074615e+00,    -2.545947563158490e+00,    -1.976777983192759e+00, -1.121110161141547e+00,    -8.906543124821664e-02,     9.573979601716980e-01,     1.851991327910473e+00,     2.473342383194153e+00, 2.776444743792320e+00,     2.795434346990870e+00,     2.620046144164544e+00,     2.358731865036594e+00,     2.104495235019173e+00;
  
  Sigmoid1 s1(0,x0,a);
  for(int i=0; i<trueVals.size(); ++i){
    EXPECT_NEAR(trueVals(i),s1.Evaluate(xs(i)),1e-7);
    EXPECT_NEAR(trueDs(i),s1.Derivative(xs(i)),1e-7);
    EXPECT_NEAR(trueD2s(i),s1.SecondDerivative(xs(i)),1e-7);
  }
}

TEST(InferenceSigmoids, S2)
{
  Eigen::VectorXd xs(20);
  xs.setLinSpaced(20,-3,3);
  const double a = 2.0;
  const double x0 = 0.5;
  
  Eigen::VectorXd trueVals(20);
  trueVals << -6.363636363000000e-01,    -6.142131979000000e-01,    -5.891891892000000e-01,    -5.606936417000000e-01,    -5.279503105000000e-01, -4.899328859000000e-01,    -4.452554745000000e-01,    -3.919999999000000e-01,    -3.274336284000000e-01,    -2.475247524000000e-01, -1.460674158000000e-01,    -1.298701300000000e-02,     1.264367816000000e-01,     2.323232323000000e-01,     3.153153154000000e-01, 3.821138211000000e-01,     4.370370371000000e-01,     4.829931973000000e-01,     5.220125786000001e-01,     5.555555555000000e-01;
  
  Eigen::VectorXd trueDs(20);
  trueDs << 6.611570250000000e-02,     7.441572830000000e-02,     8.438276109999999e-02,     9.649503830000000e-02,     1.114154546000000e-01, 1.300842304000000e-01,     1.538707443000000e-01,     1.848320000000000e-01,     2.261727622000000e-01,     2.831094990000000e-01, 3.646004293000000e-01,     4.870973182000000e-01,     3.815563482000000e-01,     2.946638098000000e-01,     2.343965587000000e-01, 1.908916651000000e-01,     1.584636489000000e-01,     1.336480170000000e-01,     1.142359875000000e-01,     9.876543209999999e-02;
  
  Eigen::VectorXd trueD2s(20);
  trueD2s << 2.404207363000000e-02,     2.870860585000000e-02,     3.466535051000000e-02,     4.239088388000000e-02,     5.259363083000000e-02, 6.635168796000000e-02,     8.535895303999999e-02,     1.123778560000000e-01,     1.521161940000000e-01,     2.130328903000000e-01, 3.113441867000000e-01,     4.807713788000000e-01,    -3.333135917000000e-01,    -2.262065610000000e-01,    -1.604877338000000e-01, -1.179493214000000e-01,    -8.920916523000000e-02,    -6.909693394999999e-02,    -5.460336507000000e-02,    -4.389574760000000e-02;
  
  Sigmoid2 s2(0,x0,a);
  for(int i=0; i<trueVals.size(); ++i){
    EXPECT_NEAR(trueVals(i),s2.Evaluate(xs(i)),1e-7);
    EXPECT_NEAR(trueDs(i),s2.Derivative(xs(i)),1e-7);
    EXPECT_NEAR(trueD2s(i),s2.SecondDerivative(xs(i)),1e-7);
  }
}


TEST(InferenceSigmoids, S3)
{
  Eigen::VectorXd xs(20);
  xs.setLinSpaced(20,-3,3);
  const double a = 2.0;
  const double x0 = 0.5;
  
  Eigen::VectorXd trueVals(20);
  trueVals << 0, 0, 0, 0, 0, -1.496664951000000e-03,    -3.126594619000000e-02,    -8.137346917999999e-02,    -1.282006306000000e-01,    -1.481288271000000e-01, -1.175394555000000e-01,    -1.281391239000000e-02,     1.058713916000000e-01,     1.471766110000000e-01,     1.340278283000000e-01, 9.004364709000000e-02,     3.884267021000000e-02,     4.043501217000000e-03,     0,0;
  
  Eigen::VectorXd trueDs(20);
  trueDs << 0,                         0,     0, 0, 0, -3.713642641000000e-02,    -1.389369806000000e-01,    -1.659452909000000e-01,    -1.181613573000000e-01,     4.414820000000000e-03, 2.017832410000000e-01,     4.739439057000000e-01,     2.419494460000000e-01,     3.211565090000000e-02,    -1.029259005000000e-01, -1.631752077000000e-01,    -1.486322715000000e-01,    -5.929709127000000e-02,     0, 0;
  
  Eigen::VectorXd trueD2s(20);
  trueD2s << 0,                         0,     0, 0, 0, -4.407894740000000e-01,    -2.039473685000000e-01,     3.289473700000000e-02,     2.697368422000000e-01,     5.065789474000000e-01, 7.434210526000000e-01,     9.802631578000001e-01,    -7.828947368000000e-01,    -5.460526315000001e-01,    -3.092105260000000e-01, -7.236842130000000e-02,     1.644736843000000e-01,     4.013157897000000e-01,                         0,                         0;
  
  Sigmoid3 s3(0,x0,a);
  for(int i=0; i<trueVals.size(); ++i){
    EXPECT_NEAR(trueVals(i),s3.Evaluate(xs(i)),1e-7);
    EXPECT_NEAR(trueDs(i),s3.Derivative(xs(i)),1e-7);
    EXPECT_NEAR(trueD2s(i),s3.SecondDerivative(xs(i)),1e-7);
  }
}

//
//
//TEST_F(InferenceTransportMaps_BananaSplit, UpdateManager)
//{
//  // add the constant and linear terms for each output
//  auto bases = BasisFactory::TotalOrderHermite(2, 1);
//
//  // add some hermite polynomials to the map
//  bases.at(1) += BasisFactory::SingleTermHermite(2, 0, 2);
//
//  // build the map
//  map = MapFactory::CreateIdentity(bases);
//
//  // create a map update manager
//  MapUpdateManager manager(map, numSamps, 0.0);
//
//  // use the first half of the samples to construct the map
//  manager.UpdateMap(samps.leftCols(numSamps / 2));
//  
//  // use all of the samples to construct the map
//  manager.UpdateMap(samps.rightCols(numSamps / 2));
//
//}
//
//
//TEST(InferenceTransportMaps, MonomialDivision)
//{
//  // create the first polynomial (a cubic 2x^4+x^3+0.1x^2-x+1)
//  Eigen::VectorXd poly1(5);
//
//  poly1 << 1, -1, 0.1, 1, 2;
//
//  // create the second polynomial (a quadratic x^2+0.2x-1)
//  Eigen::VectorXd poly2(3);
//  poly2 << -1, 0.2, 1;
//
//  // perform polynomial long division
//  Eigen::VectorXd Q, R;
//  PolynomialBasisBase::MonomialDivision(poly1, poly2, Q, R);
//
//  Eigen::Matrix<double, 3, 1> Qtrue { 1.98, 0.6, 2 };
//  Eigen::Matrix<double, 2, 1> Rtrue { 2.98, -0.796 };
//
//  for (int i = 0; i < 3; ++i) {
//    EXPECT_DOUBLE_EQ(Qtrue(i), Q(i));
//  }
//  for (int i = 0; i < 2; ++i) {
//    EXPECT_DOUBLE_EQ(Rtrue(i), R(i));
//  }
//
//  poly1.resize(4);
//  poly1 << 0, -1.0, 0.0, 1.0;
//  poly2.resize(3);
//  poly2 << -1.0, 0.0, 3.0;
//  PolynomialBasisBase::MonomialDivision(poly1, poly2, Q, R);
//
//    EXPECT_DOUBLE_EQ(0.0,        Q(0));
//    EXPECT_DOUBLE_EQ(1.0 / 3.0,  Q(1));
//    EXPECT_DOUBLE_EQ(0.0,        R(0));
//    EXPECT_DOUBLE_EQ(-2.0 / 3.0, R(1));
//}
//
//TEST(InferenceTransportMaps, MonomialRealRoots)
//{
//  // create the first polynomial (a cubic x^3-x)
//  Eigen::VectorXd poly(4);
//
//  poly << 0, -1.0, 0.0, 1.0;
//
//  const double tol      = 1e-8;
//  Eigen::VectorXd roots = PolynomialBasisBase::MonomialRoots(poly, tol);
//
//  EXPECT_NEAR(-1.0, roots(0), tol);
//  EXPECT_NEAR(0.0,  roots(1), tol);
//  EXPECT_NEAR(1.0,  roots(2), tol);
//}
//
//
//TEST(InferenceTransportMaps, MonomialRealRootsHard)
//{
//  // create the first polynomial
//  Eigen::VectorXd poly(4);
//
//  poly << -1.21831,   0.91591,  0.161934,  0.0625551;
//
//  const double tol      = 1e-8;
//  Eigen::VectorXd roots = PolynomialBasisBase::MonomialRoots(poly, tol);
//
//  EXPECT_NEAR(1.053863328, roots(0), tol);
//}
//
//TEST(InferenceTransportMaps, Hermite2FixedMonomial)
//{
//  Eigen::RowVectorXu multiIndex(3);
//
//  multiIndex << 2, 1, 4;
//  auto hermiteTerm = make_shared<HermitePolynomialBasis>(multiIndex);
//
//  // now, try to construct a monomial in the third direction at a particular point
//  Eigen::VectorXd testPt(3);
//  testPt << 1, 2, 3;
//
//  auto monoCoeffs = hermiteTerm->GetFixedMonomialCoeffs(testPt, 2);
//
//  Eigen::Matrix<double, 8, 1> trueCoeffs(8);
//  trueCoeffs << 96.0, 0.0, -384.0, 0.0, 128.0, 0.0, 0.0, 0.0;
//  for (int i = 0; i < 8; ++i) {
//    EXPECT_DOUBLE_EQ(trueCoeffs(i),                 monoCoeffs(i));
//  }
//
//    EXPECT_DOUBLE_EQ(hermiteTerm->evaluate(testPt), PolynomialBasisBase::MonomialEvaluate(monoCoeffs, testPt(2)));
//}
//
//TEST(InferenceTransportMaps, PolynomialInverse)
//{
//  RandomGenerator::SetSeed(5192012);
//
//  // first, generate a bunch of samples from a univariate lognormal distribution
//  const int numSamps = 5e3;
//  Eigen::MatrixXd samps(2, numSamps);
//
//  // create a univariate standard normal random variable
//  auto gaussRv = make_shared<GaussianRV>(2);
//
//  const double a = 1.0;
//  const double b = 2;
//
//  for (int i = 0; i < numSamps; ++i) {
//    auto temp = gaussRv->Sample();
//    samps(0, i) = a * temp(0);
//    samps(1, i) = temp(1) / a + b * (pow(samps(0, i), 2.0) + a * a);
//  }
//
//
//  auto bases = BasisFactory::TotalOrderHermite(2, 1);
//
//  // add some hermite polynomials to the map
//  bases.at(1) += BasisFactory::SingleTermHermite(2, 0, 2);
//
//
//  std::vector<Eigen::VectorXi> optLevels(2);
//  optLevels.at(0) = Eigen::VectorXi(2);
//  optLevels.at(0) << 2, bases.at(0).size();
//  optLevels.at(1) = Eigen::VectorXi(2);
//  optLevels.at(1) << 3, bases.at(1).size();
//
//  // build the map
//  auto map = MapFactory::BuildSampsToStdSerial(samps, bases, optLevels);
//
//  // evaluate the inverse map
//  Eigen::VectorXd trueMean = samps.rowwise().sum() / numSamps;
//  Eigen::VectorXd testMean = Eigen::VectorXd::Zero(2);
//  for (int k = 0; k < 1e4; ++k) {
//    testMean += map->EvaluateInverse(gaussRv->Sample(), 1.0 * Eigen::VectorXd::Ones(2));
//  }
//  testMean /= 1e4;
//
//  EXPECT_NEAR(trueMean(0), testMean(0), 5e-2);
//  EXPECT_NEAR(trueMean(1), testMean(1), 5e-2);
//}
//
//
//TEST(InferenceTransportMaps, NewtonInverse)
//{
//  RandomGenerator::SetSeed(5192012);
//
//  // first, generate a bunch of samples from a univariate lognormal distribution
//  const int numSamps = 5e3;
//  Eigen::MatrixXd samps(2, numSamps);
//
//  // create a univariate standard normal random variable
//  auto gaussRv = make_shared<GaussianRV>(2);
//
//  const double a = 1.0;
//  const double b = 2;
//
//  for (int i = 0; i < numSamps; ++i) {
//    auto temp = gaussRv->Sample();
//    samps(0, i) = a * temp(0);
//    samps(1, i) = temp(1) / a + b * (pow(samps(0, i), 2.0) + a * a);
//  }
//
//  // because we are using the full linear function, the result is not treated as a polynomial and a Newton solver is
//  // used.
//  auto bases = BasisFactory::FullLinear(2);
//
//  // add some hermite polynomials to the map
//  bases.at(1) += BasisFactory::SingleTermHermite(2, 0, 2);
//
//
//  std::vector<Eigen::VectorXi> optLevels(2);
//  optLevels.at(0) = Eigen::VectorXi(2);
//  optLevels.at(0) << 2, bases.at(0).size();
//  optLevels.at(1) = Eigen::VectorXi(2);
//  optLevels.at(1) << 3, bases.at(1).size();
//
//  // build the map
//  auto map = MapFactory::BuildSampsToStdSerial(samps, bases, optLevels);
//
//  // evaluate the inverse map
//  Eigen::VectorXd trueMean = samps.rowwise().sum() / numSamps;
//  Eigen::VectorXd testMean = Eigen::VectorXd::Zero(2);
//  for (int k = 0; k < 1e4; ++k) {
//    testMean += map->EvaluateInverse(gaussRv->Sample(), 1.0 * Eigen::VectorXd::Ones(2));
//  }
//  testMean /= 1e4;
//
//  EXPECT_NEAR(trueMean(0), testMean(0), 5e-2);
//  EXPECT_NEAR(trueMean(1), testMean(1), 5e-2);
//}
//
//
//TEST(InferenceTransportMaps, GaussianSplitFix)
//{
//  RandomGenerator::SetSeed(5192012);
//
//  // first, generate a bunch of samples from a univariate lognormal distribution
//  const int numSamps = 5e2;
//
//  // create a univariate standard normal random variable
//  Eigen::MatrixXd cov(2, 2);
//  Eigen::VectorXd mu = Eigen::VectorXd::Zero(2);
//
//  cov << 1.0, 0.8, 0.8, 1.2;
//  auto gaussRv = make_shared<GaussianRV>(mu, cov);
//
//  auto bases = BasisFactory::FullLinear(2);
//
//  // build the map
//  Eigen::MatrixXd targetSamps = gaussRv->Sample(numSamps);
//  auto invMap                 = MapFactory::BuildSampsToStdSerial(targetSamps, bases);
//
//  // compute the corresponding reference samples
//  Eigen::MatrixXd refSamps(2, numSamps);
//  for (int i = 0; i < numSamps; ++i) {
//    refSamps.col(i) = invMap->Evaluate(targetSamps.col(i));
//  }
//
//  // build the reference to target map
//  auto map = MapFactory::RegressSampsToSampsSerial(refSamps, targetSamps, bases);
//
//  // split the map in two
//  auto f1Inv = invMap->head(1);
//  auto f2    = map->tail(1);
//
//  // fix the first input of second map
//  Eigen::VectorXd temp = f1Inv->Evaluate(0.5 * Eigen::VectorXd::Ones(1));
//  auto condMap         = f2->CreateFixedMap(0, temp);
//
//
//  // compute some conditional moments to ensure the maps was constructed properly
//  double condMean, condVar;
//  gaussRv  = make_shared<GaussianRV>(1);
//  condMean = 0.0;
//  condVar  = 0.0;
//  for (int i = 0; i < 5e3; ++i) {
//    auto samp = condMap->Evaluate(gaussRv->Sample());
//    condMean += samp(0);
//  }
//  condMean /= 5e3;
//  for (int i = 0; i < 5e3; ++i) {
//    auto samp = condMap->Evaluate(gaussRv->Sample());
//    condVar += pow(samp(0) - condMean, 2.0);
//  }
//  condVar /= (5e3 - 1);
//
//  EXPECT_NEAR(0.8 * 0.5,       condMean, 5e-2);
//  EXPECT_NEAR(1.2 - 0.8 * 0.8, condVar,  5e-2);
//}
//
//
//// TEST(InferenceTransportMaps, RadialBasisLognormal){
////
////   RandomGenerator::SetSeed(5192012);
////
////   // first, generate a bunch of samples from a univariate lognormal distribution
////   const int numSamps = 5e3;
////   Eigen::MatrixXd samps(1,numSamps);
////
////   // create a univariate standard normal random variable
////   auto gaussRv = make_shared<GaussianRV>(1);
////
////   for(int i=0; i<numSamps; ++i)
////     samps(i) = exp(gaussRv->Sample()(0));
////
////   // scale everything into a box
////   double minSamp = 0.0;//samps.minCoeff()-1e-3;
////   double maxSamp = samps.maxCoeff()+1e-3;
////   double sampScale = 1.0;//maxSamp-minSamp;
////   double sampMu = samps.sum()/numSamps;
////   double sampVar = (samps-sampMu*Eigen::MatrixXd::Ones(1,numSamps)).array().square().sum()/(numSamps-1);
////   //samps = (samps - minSamp*Eigen::MatrixXd::Ones(1,numSamps))/sampScale;
////
////   // define a set of bases for the optimization problem
////   const int numKernels = 1;
////   const double dx = 0.6/numKernels;
////   const double scale = 4;
////
////   vector<vector<shared_ptr<TransportMapBasis>>> bases(1);
////
////
////   // build a nnIndex using flann
////   flann::Matrix<double> dataset(samps.data(),samps.cols(),samps.rows());
////   flann::Index<flann::L2<double>> nnIndex(dataset,flann::KDTreeIndexParams(8));
////   nnIndex.buildIndex();
////
////
////   //bases.at(0).push_back(make_shared<ConstantBasis>(1));
////   //bases.at(0).push_back(make_shared<LinearBasis>(1,0));
////   for(int i=0; i<2; ++i)
////     bases.at(0).push_back(make_shared<HermitePolynomialBasis>(i*Eigen::VectorXi::Ones(1)));
////   //bases.at(0).push_back(make_shared<LogarithmicRadialBasis<>>(Eigen::VectorXd::Zero(1),6));
////
////   // build the map
////   std::vector<Eigen::VectorXi> optLevels(1);
////   optLevels.at(0) = Eigen::VectorXi(2);
////   optLevels.at(0) << 2, bases.at(0).size();
////   std::shared_ptr<TransportMap> map;
////   for(int i=0; i<8; ++i){
////
////     optLevels.at(0) << 2, bases.at(0).size();
////
////     // compute the best map given the current bases
////     map = MapFactory::BuildSampsToStdSerial(samps,bases,optLevels);
////
////
////     std::cout << std::endl << std::endl;
////     for(int kk=0; kk<100; ++kk){
////       double x = 0.1*double(kk)+1e-2;
////       double scaledX = (x-minSamp)/sampScale;
////       double mapVal = map->Evaluate(scaledX*Eigen::VectorXd::Ones(1))(0);
////       double mapJac = map->Jacobian(scaledX*Eigen::VectorXd::Ones(1))(0,0)/sampScale;
////       double mapDens = 1/(sqrt(2*3.14159))*exp(-0.5*mapVal*mapVal)*mapJac;
////
////       double trueDens = 1.0/(x*sqrt(2*3.14159))*exp(-0.5*(log(x)*log(x)));
////
////       std::cout << mapDens << ", ";
////     //EXPECT_NEAR(trueDens,mapDens,8e-2);
////     }
////
////      std::cout << std::endl << std::endl;
////
////     Eigen::VectorXd mapJacs(numSamps);
////     Eigen::VectorXd mapVals(numSamps);
////     for(int s=0; s<numSamps; ++s){
////       mapVals(s) = map->Evaluate(samps.col(s)).squaredNorm();
////       mapJacs(s) = map->Jacobian(samps.col(s)).diagonal().prod();
////     }
////
////     // enrich the basis
////     Eigen::VectorXd weightedObj = Eigen::VectorXd::Zero(numSamps);
////     Eigen::VectorXd optWidth(numSamps);
////     std::vector<std::vector<int>> nearestInds;
////     std::vector<std::vector<double>> nearestDists;
////     const double searchRadius = 0.2*exp(-0.6*i);
////     //#pragma omp parallel for
////     for(int center=0; center<numSamps; ++center){
////
////       flann::Matrix<double> flannPt(&samps(0,center),1,samps.rows());
////       nnIndex.radiusSearch(flannPt,nearestInds,nearestDists,searchRadius,flann::SearchParams(2e5));
////       for(auto pt = nearestInds.at(0).begin(); pt != nearestInds.at(0).end(); ++pt)
////         weightedObj(center) += exp(-0.5*mapVals(*pt))*(0.5*mapVals(*pt)-log(mapJacs(*pt)));
////
////
////     }
////     int ind;
////     weightedObj.maxCoeff(&ind);
////
////  //   flann::Matrix<double> flannQuery(&samps(0,ind),1,samps.rows());
////  //   nnIndex.radiusSearch(flannQuery,nearestInds,nearestDists,searchRadius,flann::SearchParams(2e5));
//// //
////     //std::cout << "Adding a new RBF at x = " << samps.col(ind).transpose() << std::endl;
////     Eigen::Matrix<double,3,1> state(3,1);
////      const double tvWeight = 0.6*exp(-0.6*i);
////
////     state << 0.001, log(0.1), samps(0,ind); // w, eps, pos
////     for(int gradStep = 0; gradStep<450; ++gradStep){
////       double f = 0;
////       Eigen::Matrix<double,3,1> grad = Eigen::Matrix<double,3,1>::Zero(3,1);
////
////
////       // compute the objective and gradient
////     for(int s=0; s<numSamps; ++s){
////      double x = samps(0,s);
////      double eps = exp(state(1));
////      double B = exp(-1.0*pow(eps*(x-state(2)),2.0));
////      double dBde = -2*eps*pow(x-state(2),2.0)*B*eps;
////      double dBdx = -2*pow(eps,2.0)*(x-state(2))*B;
////      double dBdxc = -1.0*dBdx;
////
////      double d2Bdxde = (-4*eps*(x-state(2))*B + -2*pow(eps,2.0)*(x-state(2))*dBde)*eps;
////      double d2Bdxdxc = 2*eps*eps*B - 2*eps*eps*(x-state(2))*dBdxc;
////
////      double Dfdx = map->Jacobian(samps.col(s)).diagonal().prod();
////      grad(0) += 2*B+2*state(0)*B*B - pow(Dfdx+state(0)*dBdx,-1.0)*dBdx;
////      grad(1) += 2*state(0)*dBde + 2*state(0)*state(0)*B*dBde - state(0)*pow(Dfdx+state(0)*dBdx,-1.0)*d2Bdxde;
////         grad(2) += 2*state(0)*dBdxc + 2*state(0)*state(0)*B*dBdxc - state(0)*pow(Dfdx+state(0)*dBdx,-1.0)*d2Bdxdxc;
////
////      // total variation objective
////      double d2dx2 = -2.0*eps*eps*B*(1.0-2.0*eps*eps*pow(x-state(2),2));
////      double signMult = (state(2)*d2dx2>0) ? 1.0 : -1.0;
////
////      grad(0) += tvWeight*abs(d2dx2);
////      grad(1) +=
//// tvWeight*signMult*(state(2)*B*(-4.0*eps+20*pow(eps,3)*pow(x-state(2),2.0)-8.0*pow(eps,5)*pow(x-state(2),4)))*eps;
////      grad(2) += tvWeight*signMult*(state(2)*B*(-4.0*(pow(eps,4)+2*pow(eps,2))*(x-state(2)) +
//// 8*pow(eps,4)*pow(x-state(2),3)));
////       }
////       grad /= numSamps;
////       //grad(2) -= 1e-2/(state(2)+0.2);
////
////       // add a penalty for smoothness
////       //grad(1) += 1e-3;
////
////       // update the state
////       state -= 0.1*pow(gradStep+1,-0.001)*grad;
////       if(grad.norm()<1e-6)
////      break;
////     }
////     std::cout << "Final state = " << state.transpose() << std::endl;
////    /*
////      // compute the hessian of the objective at this point by fitting a quadratic
////      const int numNeighbors = 2;
////      Eigen::VectorXd queryPt(samps.rows());
////      std::vector<std::vector<int>> nearestInds;
////       std::vector<std::vector<double>> nearestDists;
////
////      flann::Matrix<double> flannQuery(queryPt.data(),1,samps.rows());
////
////      nnIndex.knnSearch(flannQuery, nearestInds, nearestDists, numNeighbors, flann::SearchParams(20));
////      */
//// //      const int numNeighbors = nearestInds.at(0).size();
//// //      std::cout << "numNeighbors = " << numNeighbors << std::endl;
//// //      Eigen::MatrixXd vand(numNeighbors+1,3);
//// //      vand.col(0) = Eigen::VectorXd::Ones(numNeighbors+1);
//// //      Eigen::VectorXd rhs(numNeighbors+1);
//// //      vand(0,1) = samps(0,ind); vand(0,2) = samps(0,ind)*samps(0,ind);
//// //      rhs(0) = 0.5*mapVals(ind)-log(mapJacs(ind));
//// //
//// //      for(int s=0; s<numNeighbors; ++s){
//// //        vand(s+1,1) = samps(0,nearestInds.at(0)[s]);
//// //        vand(s+1,2) = 0.5*samps(0,nearestInds.at(0)[s])*samps(0,nearestInds.at(0)[s]);
//// //        rhs(s+1) = 0.5*mapVals(nearestInds.at(0)[s])-log(mapJacs(nearestInds.at(0)[s]));
//// //      }
//// //      Eigen::VectorXd coeffs = vand.colPivHouseholderQr().solve(rhs);
//// //      std::cout << "Hessian at x=" << samps.col(ind).transpose() << " is " << coeffs(2) << std::endl;
//// //
////     bases.at(0).push_back(make_shared<GaussianRadialBasis<>>(state(2)*Eigen::VectorXd::Ones(1),exp(state(1))));
////   }
////
////
////     std::cout << std::endl << "Map Jacobian = \n";
////     for(int kk=0; kk<100; ++kk){
////       double x = 0.1*double(kk)+1e-2;
////       double scaledX = (x-minSamp)/sampScale;
////       double mapVal = map->Evaluate(scaledX*Eigen::VectorXd::Ones(1))(0);
////       double mapJac = map->Jacobian(scaledX*Eigen::VectorXd::Ones(1))(0,0)/sampScale;
////
////       std::cout << mapJac << ", ";
////     //EXPECT_NEAR(trueDens,mapDens,8e-2);
////     }
////     std::cout << std::endl << std::endl;
////
////     std::cout << std::endl << "Map Vals = \n";
////     for(int kk=0; kk<100; ++kk){
////       double x = 0.1*double(kk)+1e-2;
////       double scaledX = (x-minSamp)/sampScale;
////       double mapVal = map->Evaluate(scaledX*Eigen::VectorXd::Ones(1))(0);
////
////       std::cout << mapVal << ", ";
////     //EXPECT_NEAR(trueDens,mapDens,8e-2);
////     }
////     std::cout << std::endl << std::endl;
////
////     std::cout << std::endl << "True Density = \n";
////     for(int kk=0; kk<100; ++kk){
////       double x = 0.1*double(kk)+1e-2;
////
////       double trueDens = 1.0/(x*sqrt(2*3.14159))*exp(-0.5*(log(x)*log(x)));
////        std::cout << trueDens << ", ";
////     }
////     std::cout << std::endl << std::endl;
////
////     std::cout << std::endl << "x = \n";
////     for(int kk=0; kk<100; ++kk){
////       double x = 0.1*double(kk)+1e-2;
////        std::cout << x << ", ";
////     }
////     std::cout << std::endl << std::endl;
////
////
//// }
////
////
//// TEST(InferenceTransportMaps, RadialBasisLognormalAuto){
////
////   RandomGenerator::SetSeed(5192012);
////
////   // first, generate a bunch of samples from a univariate lognormal distribution
////   const int numSamps = 5e4;
////   Eigen::MatrixXd samps(1,numSamps);
////
////   // create a univariate standard normal random variable
////   auto gaussRv = make_shared<GaussianRV>(1);
////
////   for(int i=0; i<numSamps; ++i)
////     samps(i) = exp(gaussRv->Sample()(0));
////
////   const int numKernels = 20;
////   auto bases = BasisFactory::ConstructRadialBasis(samps,numKernels);
////
////   // build the map
////   std::vector<Eigen::VectorXi> optLevels(1);
////   optLevels.at(0) = Eigen::VectorXi(3);
////   optLevels.at(0) << 2, 10, bases.at(0).size();
////
////   // compute the best map given the current bases
////   auto map = MapFactory::BuildSampsToStdSerial(samps,bases,optLevels);
////
////     for(int i=0; i<100; ++i){
////     double x = 0.1*double(i)+1e-2;
////     std::cout << x << ", ";
////     //EXPECT_NEAR(trueDens,mapDens,8e-2);
////   }
////   std::cout << std::endl << std::endl;
////
////   for(int i=0; i<100; ++i){
////     double x = 0.1*double(i)+1e-2;
////     double trueDens = 1.0/(x*sqrt(2*3.14159))*exp(-0.5*(log(x)*log(x)));
////     std::cout << trueDens << ", ";
////     //EXPECT_NEAR(trueDens,mapDens,8e-2);
////   }
////   std::cout << std::endl << std::endl;
////
////   // compare the map-induced density with the true density
////   for(int i=0; i<100; ++i){
////     double x = 0.1*double(i)+1e-2;
////
////     double mapVal = map->Evaluate(x*Eigen::VectorXd::Ones(1))(0);
////     double mapJac = map->Jacobian(x*Eigen::VectorXd::Ones(1))(0,0);
////     double mapDens = 1/(sqrt(2*3.14159))*exp(-0.5*mapVal*mapVal)*mapJac;
////
////     double trueDens = 1.0/(x*sqrt(2*3.14159))*exp(-0.5*(log(x)*log(x)));
////     std::cout << mapDens << ", ";
////     //EXPECT_NEAR(trueDens,mapDens,8e-2);
////   }
////   std::cout << std::endl << std::endl;
//// }
