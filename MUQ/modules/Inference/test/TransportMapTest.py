import unittest

import libmuqUtilities
import libmuqModelling
import libmuqInference

import numpy as np
from numpy import linalg as la

class TransportMapTest(unittest.TestCase):
    def testBasicSet(self):
        basis1 = libmuqInference.BasisSet()

        basis1 += libmuqInference.ConstantBasis(2)

        self.assertEqual(1, basis1.size())

        basis1 += libmuqInference.LinearBasis(2, 1)

        self.assertEqual(2, basis1.size())

        basis2 = libmuqInference.BasisSet()

        basis2 += libmuqInference.LinearBasis(2, 0)

        self.assertEqual(1, basis2.size())

        basis3 = libmuqInference.BasisSet()
        basis3 += basis1
        basis3 += basis2

        self.assertEqual(3, basis3.size())

        evalPt = [1.1, 2.2]

        self.assertEqual(1.0, basis3.at(0).evaluate(evalPt))
        self.assertEqual(2.2, basis3.at(1).evaluate(evalPt))
        self.assertEqual(1.1, basis3.at(2).evaluate(evalPt))

    def testPolynomialBasis(self):
        x = 0.6

        H5  = libmuqInference.StaticHermitePoly.evaluate(5, x)
        dH5 = libmuqInference.StaticHermitePoly.derivative(5, x)

        self.assertAlmostEqual(32.0 * pow(x, 5.0) - 160.0 * pow(x, 3.0) + 120.0 * x, H5, 12)
        self.assertAlmostEqual(32.0 * 5.0 * pow(x, 4.0) - 160.0 * 3.0 * pow(x, 2.0) + 120.0, dH5, 12);

        L5  = libmuqInference.StaticLegendrePoly.evaluate(5, x)
        dL5 = libmuqInference.StaticLegendrePoly.derivative(5, x)

        self.assertAlmostEqual((63.0 * pow(x, 5.0) - 70.0 * pow(x, 3.0) + 15.0 * x) / 8.0, L5, 12)
        self.assertAlmostEqual((63.0 * 5.0 * pow(x, 4.0) - 70.0 * 3.0 * pow(x, 2.0) + 15.0) / 8.0, dL5, 12);

        multiIndex = [3, 7, 6]
        
        testPt = [x]*3

        mvHermite = libmuqInference.HermitePolynomialBasis(multiIndex)
        result    = mvHermite.evaluate(testPt)

        self.assertAlmostEqual(libmuqInference.StaticHermitePoly.evaluate(3, x) * libmuqInference.StaticHermitePoly.evaluate(7, x) * libmuqInference.StaticHermitePoly.evaluate(6, x), result, 12)

        mvLegendre = libmuqInference.LegendrePolynomialBasis(multiIndex)
        result     = mvLegendre.evaluate(testPt)

        self.assertAlmostEqual(libmuqInference.StaticLegendrePoly.evaluate(3, x) * libmuqInference.StaticLegendrePoly.evaluate(7, x) * libmuqInference.StaticLegendrePoly.evaluate(6, x), result, 12)

    def testHermite2Monomial(self):
        maxOrder = 5

        monoCoeffs = libmuqInference.StaticHermitePoly.GetMonomialCoeffs(0, maxOrder)
        trueCoeffs = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticHermitePoly.GetMonomialCoeffs(1, maxOrder)
        trueCoeffs = [0.0, 2.0, 0.0, 0.0, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticHermitePoly.GetMonomialCoeffs(2, maxOrder)
        trueCoeffs = [-2.0, 0.0, 4.0, 0.0, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticHermitePoly.GetMonomialCoeffs(3, maxOrder)
        trueCoeffs = [0.0, -12.0, 0.0, 8.0, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticHermitePoly.GetMonomialCoeffs(4, maxOrder)
        trueCoeffs = [12.0, 0.0, -48.0, 0.0, 16.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticHermitePoly.GetMonomialCoeffs(5, maxOrder)
        trueCoeffs = [0.0, 120.0, 0.0, -160.0, 0.0, 32.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        pt = 1.23
        self.assertAlmostEqual(libmuqInference.StaticHermitePoly.evaluate(5, pt), libmuqInference.PolynomialBasisBase.MonomialEvaluate(monoCoeffs, pt), 12)

    def testLegendre2Monomial(self):
        maxOrder = 5

        monoCoeffs = libmuqInference.StaticLegendrePoly.GetMonomialCoeffs(0, maxOrder)
        trueCoeffs = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticLegendrePoly.GetMonomialCoeffs(1, maxOrder)
        trueCoeffs = [0.0, 1.0, 0.0, 0.0, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticLegendrePoly.GetMonomialCoeffs(2, maxOrder)
        trueCoeffs = [-0.5, 0.0, 1.5, 0.0, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticLegendrePoly.GetMonomialCoeffs(3, maxOrder)
        trueCoeffs = [0.0, -1.5, 0.0, 2.5, 0.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticLegendrePoly.GetMonomialCoeffs(4, maxOrder)
        trueCoeffs = [3.0 / 8.0, 0.0, -30.0 / 8.0, 0.0, 35.0 / 8.0, 0.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        monoCoeffs = libmuqInference.StaticLegendrePoly.GetMonomialCoeffs(5, maxOrder)
        trueCoeffs = [0.0, 15.0 / 8.0, 0.0, -70.0 / 8.0, 0.0, 63.0 / 8.0]
        for i in range (maxOrder + 1):
            self.assertEqual(trueCoeffs[i], monoCoeffs[i])

        pt = 0.8
        self.assertAlmostEqual(libmuqInference.StaticLegendrePoly.evaluate(5, pt), libmuqInference.PolynomialBasisBase.MonomialEvaluate(monoCoeffs, pt), 12)

    def testTotalOrderCreate(self):
        basis = libmuqInference.BasisFactory.FullLinear(2)
        dum   = libmuqInference.BasisFactory.TotalOrderHermite(2, 5)

        basis = dum + basis

        self.assertEqual(basis[0].size(), 6)

    def testPolynomialBananaSplit(self):
        libmuqUtilities.RandomGenerator.SetSeed(5192012)
        numSamps = int(5e4)
        
        # univariate standard normal RV
        gaussRv = libmuqModelling.GaussianRV(2)

        a = 1.0
        b = 2.0

        samps = [None]*numSamps
        for i in range(numSamps):
            samps[i] = [0.0]*2

            temp = gaussRv.Sample()
            samps[i][0] = a * temp[0]
            samps[i][1] = temp[1] / a + b * (pow(samps[i][0], 2.0) + a * a)

        samps = np.array(samps).transpose().tolist()

        # add the constant linear terms for each output
        bases = libmuqInference.BasisFactory.TotalOrderHermite(2, 1)

        # add some hermite polynomials to the map 
        bases[1] += libmuqInference.BasisFactory.SingleTermHermite(2, 0, 2)

        optLevels = [[2, bases[0].size()], [3, bases[1].size()]]

        map0 = libmuqInference.TransportMap(samps, bases, optLevels)

        xs = np.linspace(-2.0, 2.0, 10)
        ys = np.linspace(-0.5, 6.0, 10)
        for i in range(len(ys)):
            for j in range(len(xs)):
                inpt = [xs[j], ys[i]]
                mapVal  = map0.Evaluate(inpt)
                mapJac  = np.prod(np.array(map0.Jacobian(inpt)).diagonal())
                mapDens = pow(2 * 3.14159, -1.0) * np.exp(-0.5 * pow(la.norm(np.array(mapVal), 2.0), 2.0)) * mapJac;
                trueVal = [inpt[0] / a, a*inpt[1] - b * (inpt[0] * inpt[0] + a * a)]
                trueJac = 1.0
                trueDens = pow(2 * 3.14159, -1.0) * np.exp(-0.5 * pow(la.norm(np.array(trueVal), 2.0), 2.0)) * trueJac

                self.assertAlmostEqual(trueDens, mapDens, 2)

        marginalMaps = map0.Split(1)

        for j in range(len(xs)):
            inpt = [xs[j]]

            mapVal  = marginalMaps[0].Evaluate(inpt)
            mapJac  = np.prod(np.array(marginalMaps[0].Jacobian(inpt)).diagonal())
            mapDens = pow(2 * 3.14159, -0.5) * np.exp(-0.5 * pow(la.norm(np.array(mapVal), 2.0), 2.0)) * mapJac;

            trueVal = [inpt[0] / a]
            trueJac = 1.0
            trueDens = pow(2 * 3.14159, -0.5) * np.exp(-0.5 * pow(la.norm(np.array(trueVal), 2.0), 2.0)) * trueJac

            self.assertAlmostEqual(trueDens, mapDens, 2)
            
    def testPolynomialUpdateManager(self):
        libmuqUtilities.RandomGenerator.SetSeed(5192012)

        numSamps = int(5e4)

        gaussRv = libmuqModelling.GaussianRV(2)

        a = 1.0
        b = 2.0

        samps = [None]*numSamps
        for i in range(numSamps):
            samps[i] = [0.0]*2

            temp = gaussRv.Sample()
            samps[i][0] = a * temp[0]
            samps[i][1] = temp[1] / a + b * (pow(samps[i][0], 2.0) + a * a)

        samps = np.array(samps).transpose().tolist()

        bases = libmuqInference.BasisFactory.TotalOrderHermite(2, 1)

        bases[1] += libmuqInference.BasisFactory.SingleTermHermite(2, 0, 2)

        map0 = libmuqInference.TransportMap(bases)

        manager = libmuqInference.MapUpdateManager(map0, numSamps, 0.0)
        
        manager.UpdateMap((np.array(samps)[:,0:numSamps/2]).tolist())

        xs = np.linspace(-2.0, 2.0, 10)
        ys = np.linspace(-0.5, 6.0, 10)
        for i in range(len(ys)):
            for j in range(len(xs)):
                inpt = [xs[j], ys[i]]

                mapVal  = map0.Evaluate(inpt)
                mapJac  = np.prod(np.array(map0.Jacobian(inpt)).diagonal())
                mapDens = pow(2 * 3.14159, -1.0) * np.exp(-0.5 * pow(la.norm(np.array(mapVal), 2.0), 2.0)) * mapJac;

                trueVal = [inpt[0] / a, a*inpt[1] - b * (inpt[0] * inpt[0] + a * a)]
                trueJac = 1.0
                trueDens = pow(2 * 3.14159, -1.0) * np.exp(-0.5 * pow(la.norm(np.array(trueVal), 2.0), 2.0)) * trueJac
                self.assertAlmostEqual(trueDens, mapDens, 2)

        manager.UpdateMap((np.array(samps)[:,numSamps/2:numSamps-1]).tolist())

        xs = np.linspace(-2.0, 2.0, 10)
        ys = np.linspace(-0.5, 6.0, 10)
        for i in range(len(ys)):
            for j in range(len(xs)):
                inpt = [xs[j], ys[i]]

                mapVal  = map0.Evaluate(inpt)
                mapJac  = np.prod(np.array(map0.Jacobian(inpt)).diagonal())
                mapDens = pow(2 * 3.14159, -1.0) * np.exp(-0.5 * pow(la.norm(np.array(mapVal), 2.0), 2.0)) * mapJac;

                trueVal = [inpt[0] / a, a*inpt[1] - b * (inpt[0] * inpt[0] + a * a)]
                trueJac = 1.0
                trueDens = pow(2 * 3.14159, -1.0) * np.exp(-0.5 * pow(la.norm(np.array(trueVal), 2.0), 2.0)) * trueJac
                self.assertAlmostEqual(trueDens, mapDens, 2)

        densEvals = manager.GetInducedDensities(0, min(numSamps, 1000))

        for i in range(min(numSamps, 1000)):
            inpt = [samps[0][i], samps[1][i]]

            mapVal    = map0.Evaluate(inpt)
            logMapJac = np.sum(np.log((np.array(map0.Jacobian(inpt)).diagonal())))
            mapDens   = -1.0 * (np.log(2.0) + np.log(3.14159)) - 0.5 * pow(la.norm(np.array(mapVal), 2.0), 2.0) + logMapJac;
            
            self.assertAlmostEqual(mapDens, densEvals[i], 5)

    def testMonomialDivision(self):
        poly1 = [1.0, -1.0, 0.1, 1.0, 2.0]
        poly2 = [-1.0, 0.2, 1.0]

        [Q, R] = libmuqInference.PolynomialBasisBase.MonomialDivision(poly1, poly2)

        Qtrue = [1.98, 0.6, 2.0]
        Rtrue = [2.98, -0.796]

        for i in range(3):
            self.assertAlmostEqual(Q[i], Qtrue[i], 12)

        for i in range(2):
            self.assertAlmostEqual(R[i], Rtrue[i], 12)

        poly1 = [0.0, -1.0, 0.0, 1.0]
        poly2 = [-1.0, 0.0, 3.0]

        [Q, R] = libmuqInference.PolynomialBasisBase.MonomialDivision(poly1, poly2)

        self.assertEqual(0.0, Q[0])
        self.assertEqual(1.0 / 3.0, Q[1])
        self.assertEqual(0.0, R[0])
        self.assertAlmostEqual(-2.0 / 3.0, R[1], 12)

    def testMonomialRealRoots(self):
        poly = [0.0, -1.0, 0.0, 1.0]
        
        tol   = 1.0e-8
        roots = libmuqInference.PolynomialBasisBase.MonomialRoots(poly, tol)

        self.assertAlmostEqual(-1.0, roots[0], 8)
        self.assertAlmostEqual(0.0, roots[1], 8)
        self.assertAlmostEqual(1.0, roots[2], 8)

    def testMonomialRealRootsHard(self):
        poly = [-1.21831, 0.91591, 0.161934, 0.062551]
        
        tol   = 1.0e-8
        roots = libmuqInference.PolynomialBasisBase.MonomialRoots(poly, tol)

        self.assertAlmostEqual(1.053863328, roots[0], 5)
        
    def testHermite2FixedMonomial(self):
        multiIndex = [2, 1, 4]

        hermiteTerm = libmuqInference.HermitePolynomialBasis(multiIndex)

        testPt = [1.0, 2.0, 3.0]
        
        monoCoeffs = hermiteTerm.GetFixedMonomialCoeffs(testPt, 2)

        trueCoeffs = [96.0, 0.0, -384.0, 0.0, 128.0, 0.0, 0.0, 0.0]

        for i in range(8):
            self.assertAlmostEqual(trueCoeffs[i], monoCoeffs[i], 12)

        self.assertAlmostEqual(hermiteTerm.evaluate(testPt), libmuqInference.PolynomialBasisBase.MonomialEvaluate(monoCoeffs, testPt[2]))

    def testPolynomialInverse(self):
        libmuqUtilities.RandomGenerator.SetSeed(5192012)

        numSamps = int(5e3)

        # univariate standard normal RV
        gaussRv = libmuqModelling.GaussianRV(2)

        a = 1.0
        b = 2.0

        samps = [None]*numSamps
        for i in range(numSamps):
            samps[i] = [0.0]*2

            temp = gaussRv.Sample()
            samps[i][0] = a * temp[0]
            samps[i][1] = temp[1] / a + b * (pow(samps[i][0], 2.0) + a * a)

        samps = np.array(samps).transpose().tolist()

        bases = libmuqInference.BasisFactory.TotalOrderHermite(2, 1)

        bases[1] += libmuqInference.BasisFactory.SingleTermHermite(2, 0, 2)

        optLevels = [[2, bases[0].size()], [3, bases[1].size()]]

        map0 = libmuqInference.TransportMap(samps, bases, optLevels)

        trueMean = np.sum(np.array(samps), axis=1) / numSamps
        testMean = np.array([0.0, 0.0])
        for k in range(int(1e4)):
            testMean += np.array(map0.EvaluateInverse(gaussRv.Sample(), [1.0, 1.0]))
        testMean = testMean / 1e4

        self.assertAlmostEqual(trueMean[0], testMean[0], 2)
        self.assertAlmostEqual(trueMean[1], testMean[1], 2)

        self.assertTrue(False)


        
