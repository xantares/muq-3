import unittest
import numpy as np

import libmuqModelling
import libmuqInference

class BasicMAPTest(unittest.TestCase):
    def testBFGS_Line(self):
        densMean = [1.0]*2
        densCov  = [[1.0, 0.8], [0.8, 0.9]]

        gaussTest = libmuqModelling.GaussianDensity(densMean, densCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        xc = [0.0]*2

        params = dict()
        params["Opt.Method"]    = "BFGS_Line"
        params["Opt.xtol"]      = 0
        params["Opt.gtol"]      = 1e-12
        params["Opt.ftol"]      = 0
        params["Opt.verbosity"] = 0
        
        prob   = libmuqInference.SamplingProblem(gaussTest)
        solver = libmuqInference.MAPBase(prob, params)

        xMAP = solver.Solve(xc)
        
        for i in range(2):
            self.assertAlmostEqual(xMAP[i], 1.0, 6)

    def testNewton(self):
        densMean = [1.0]*2
        densCov  = [[1.0, 0.8], [0.8, 0.9]]

        gaussTest = libmuqModelling.GaussianDensity(densMean, densCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        xc = [0.0]*2

        params = dict()
        params["Opt.Method"]    = "Newton"
        params["Opt.xtol"]      = 0
        params["Opt.gtol"]      = 1e-12
        params["Opt.ftol"]      = 0
        params["Opt.verbosity"] = 0
        
        prob   = libmuqInference.SamplingProblem(gaussTest)
        solver = libmuqInference.MAPBase(prob, params)

        xMAP = solver.Solve(xc)
        
        for i in range(2):
            self.assertAlmostEqual(xMAP[i], 1.0, 6)

    def testStructuredBFGS(self):
        densMean = [1.0]*2
        densCov  = [[1.0, 0.8], [0.8, 0.9]]

        gaussTest = libmuqModelling.GaussianDensity(densMean, densCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        xc = [0.0]*2

        params = dict()
        params["Opt.Method"]    = "StructuredBFGS"
        params["Opt.xtol"]      = 0
        params["Opt.gtol"]      = 1e-12
        params["Opt.ftol"]      = 0
        params["Opt.verbosity"] = 0
        
        prob   = libmuqInference.SamplingProblem(gaussTest)
        solver = libmuqInference.MAPBase(prob, params)

        xMAP = solver.Solve(xc)
        
        for i in range(2):
            self.assertAlmostEqual(xMAP[i], 1.0, 6)

class AllDefMAPTest(unittest.TestCase):
    def setUp(self):
        priorMean = [0.0]*2
        priorCov  = [[1.0, 0.7], [0.7, 2.0]]

        dataMean = [7.0]*2
        dataCov  = [[2.0, 0.0], [0.0, 2.0]]

        # models
        inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        likelihood      = libmuqModelling.GaussianDensity(dataMean, dataCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)
        prior           = libmuqModelling.GaussianDensity(priorMean, priorCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)
        posterior       = libmuqModelling.DensityProduct(2)
        forwardModel    = libmuqModelling.ExpModel(2)

        # make the graph
        graph = libmuqModelling.ModGraph()

        graph.AddNode(inferenceTarget, "inferenceTarget")
        graph.AddNode(likelihood, "likelihood")
        graph.AddNode(prior, "prior")
        graph.AddNode(posterior, "posterior")
        graph.AddNode(forwardModel, "forwardModel")

        graph.AddEdge("inferenceTarget", "forwardModel", 0)
        graph.AddEdge("forwardModel", "likelihood", 0)
        graph.AddEdge("inferenceTarget", "prior", 0)
        graph.AddEdge("likelihood", "posterior", 0)
        graph.AddEdge("prior", "posterior", 1)

        # MAP parameters
        self.params = dict()
        self.params["Opt.xtol"]      = 1.0e-12
        self.params["Opt.gtol"]      = 1.0e-12
        self.params["Opt.ftol"]      = 1.0e-8
        self.params["Opt.verbosity"] = 0

        # make the inference problem
        self.prob = libmuqInference.InferenceProblem(graph)

    def testAllDefTestBFGS(self):
        self.params["Opt.Method"] = "BFGS_Line"

    def testAllDefTestNewton(self):
        self.params["Opt.Method"] = "Newton"

    def testAllDefTestStructuredBFGS(self):
        self.params["Opt.Method"] = "StructuredBFGS"

    def tearDown(self):
        # create the solver
        solver = libmuqInference.MAPBase(self.prob, self.params)

        # solve
        x0 = [0.0]*2
        xMAP = solver.Solve(x0)

        self.assertAlmostEqual(1.8737144117906237, xMAP[0], 4)
        self.assertAlmostEqual(1.9287933176757821, xMAP[1], 4)

