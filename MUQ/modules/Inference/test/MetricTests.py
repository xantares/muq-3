import unittest 
import numpy as np 

import libmuqInference 

class MetricTest (unittest.TestCase): 
    def testEuclideanMetric(self):
        metric = libmuqInference.EuclideanMetric()
        
        mat = metric.ComputeMetric([0.0]*5)

        for i in range(5):
            for j in range(5):
                if i==j:
                    self.assertEqual(mat[i][j], 1.0)
                else:
                    self.assertEqual(mat[i][j], 0.0)

