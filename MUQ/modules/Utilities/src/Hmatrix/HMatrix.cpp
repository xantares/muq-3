
#include "MUQ/Utilities/Hmatrix/Hmatrix.h"
#include "MUQ/Utilities/Hmatrix/ElementPairTree.h"
#include "MUQ/Utilities/Hmatrix/ElementPairNode.h"
#include "MUQ/Utilities/mesh/Mesh.h"

#include "MUQ/Geostatistics/IsotropicCovKernel.h"

#include <Eigen/Eigenvalues>


using namespace muq::Utilities;

template<unsigned int dim>
HMatrix<dim>::HMatrix(muq::Geostatistics::CovKernelPtr& KernPtr, std::shared_ptr < Mesh < dim >>& MeshPtr)
{
  // first, build the tree
  Tree = std::shared_ptr < ElementPairTree < dim >> (new ElementPairTree<dim>(0.8, MeshPtr));

  // now go through the tree and build the matrices
  Tree->root->RecurseMatConstruct(Tree->ClusterTree, KernPtr);

  // the hierarchical covariance matrix is now ready to rumble
}

template<unsigned int dim>
int HMatrix<dim>::rows() const
{
  return Tree->ClusterTree->Elements.size();
}

template<unsigned int dim>
int HMatrix<dim>::cols() const
{
  return Tree->ClusterTree->Elements.size();
}

/** Apply this hierarchical matrix to a vector and return the result. */
template<unsigned int dim>
Eigen::VectorXd HMatrix<dim>::MatVec(const Eigen::VectorXd& x) const
{
  // first, initialize the output to a zero vector
  Eigen::VectorXd Result = Eigen::VectorXd::Zero(x.rows());

  // recurse through the tree, adding all the relevant components
  Tree->root->RecurseMatMult(Tree->ClusterTree, x, Result);

  // return the result
  return Result;
}

template class muq::Utilities::HMatrix<1>;
template class muq::Utilities::HMatrix<2>;
template class muq::Utilities::HMatrix<3>;
