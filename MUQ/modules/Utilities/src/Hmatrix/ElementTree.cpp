
#include <type_traits>

#include "MUQ/Utilities/Hmatrix/ElementTree.h"
#include "MUQ/Utilities/Hmatrix/ElementNode.h"
#include "MUQ/Utilities/Hmatrix/ElementPairNode.h"
#include "MUQ/Utilities/mesh/Mesh.h"
#include "MUQ/Utilities/Hmatrix/ConvexHull.h"

#include "MUQ/Geostatistics/IsotropicCovKernel.h"

#include <Eigen/Eigenvalues>


using namespace muq::Utilities;

template<unsigned int dim>
ElementTree<dim>::ElementTree(std::shared_ptr < Mesh < dim >>& MeshIn)
{
  // get the number of elements in the mesh
  unsigned int Neles = MeshIn->NumEles();

  // first, start with a list of all elements
  Elements.resize(Neles);
  for (unsigned int i = 0; i < Neles; ++i) {
    Elements[i] = i;
  }

  // build a matrix of the element positions
  ElePos = Eigen::Matrix<double, dim, Eigen::Dynamic>::Zero(dim, Neles);
  for (unsigned int i = 0; i < Neles; ++i) {
    ElePos.col(i) = MeshIn->GetElePos(i);
  }

  root = std::shared_ptr < ElementNode < dim >> (new ElementNode<dim>(0, Neles));

  // now recurse down and split everything up
    DecompEleList(root);

  //recurse and build all the convex hulls
  root->SetConvexHull(ElePos, Elements);
}

/** Decompose a list of elements into two other lists based on spatial locaiton of the elements. */
template<unsigned int dim>
void ElementTree<dim>::DecompEleList(std::shared_ptr < ElementNode < dim >>& Parent)
{
  unsigned int Npos = Parent->EndInd - Parent->StartInd;

  if (Npos > 128) {
    // first, construct the position matrix and find the largest singular vector
    Eigen::Matrix<double, dim, Eigen::Dynamic> PosIn(dim, Npos);
    for (unsigned int i = 0; i < Npos; ++i) {
      PosIn.col(i) = ElePos.col(Elements[i + Parent->StartInd]);
    }

    // find the mean position
    Parent->MeanPos = Eigen::Matrix<double, dim, 1>::Zero(dim, 1);
    for (unsigned int d = 0; d < dim; ++d) {
      Parent->MeanPos(d) = PosIn.row(d).sum() / Npos;
    }
    for (unsigned int i = 0; i < Npos; ++i) {
      PosIn.col(i) -= Parent->MeanPos;
    }

    // find the covariance of positions
    Eigen::Matrix<double, dim, dim> PosCov = PosIn * PosIn.transpose();

    // find the largest eigenvector of the covariance matrix
    Eigen::EigenSolver < Eigen::Matrix < double, dim, dim >> es(PosCov);
    auto EigVals = es.eigenvalues();
    Eigen::Matrix<double, dim,
                  1> temp =
      (EigVals.real().array() * EigVals.real().array() + EigVals.imag().array() * EigVals.imag().array()).cwiseSqrt();
    Eigen::VectorXd::Index maxInd;
    temp.maxCoeff(&maxInd);

    Parent->EigVec = es.eigenvectors().real().col(maxInd);

    // split the input elements using this eigen vector
    std::vector<unsigned int> Child1List;
    std::vector<unsigned int> Child2List;

    for (unsigned int i = 0; i < Npos; ++i) {
      //std::cout << PosIn.col(i).transpose() << std::endl;
      if (PosIn.col(i).dot(Parent->EigVec) >= 0) {
        Child2List.push_back(Elements[i + Parent->StartInd]);
      } else {
        Child1List.push_back(Elements[i + Parent->StartInd]);
      }
    }

    for (unsigned int i = 0; i < Child1List.size(); ++i) {
      Elements[i + Parent->StartInd] = Child1List[i];
    }

    unsigned int j = 0;
    for (unsigned int i = Child1List.size(); i < Npos; ++i, ++j) {
      Elements[i + Parent->StartInd] = Child2List[j];
    }

    // now add a new treenode to the tree
    Parent->child1 = std::shared_ptr < ElementNode < dim >>
                     (new ElementNode<dim>(Parent->StartInd, Parent->StartInd + Child1List.size()));
    DecompEleList(Parent->child1);

    // now add the second new treenode to the tree
    Parent->child2 = std::shared_ptr < ElementNode < dim >>
                     (new ElementNode<dim>(Parent->StartInd + Child1List.size(), Parent->EndInd));
    DecompEleList(Parent->child2);
  }
}

template<unsigned int dim>
double muq::Utilities::ElementTree<dim>::GetDiam(std::shared_ptr < ElementNode < dim >>& Node) const
{
  return Node->CHull.GetDiam();
}

template<unsigned int dim>
double ElementTree<dim>::GetDist(std::shared_ptr < ElementNode < dim >>& Node1,
                                 std::shared_ptr < ElementNode < dim >>& Node2) const
{
  return Node1->CHull.GetDist(Node2->CHull);
}

template class muq::Utilities::ElementTree<1>;
template class muq::Utilities::ElementTree<2>;
template class muq::Utilities::ElementTree<3>;
