
#include "MUQ/Utilities/Variable.h"

#include <string>

#include <boost/serialization/export.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

using namespace muq::Utilities;

Variable::Variable() {}


Variable::Variable(std::string                                  name,
         std::shared_ptr<RecursivePolynomialFamily1D> polyFamily) : Variable(name,polyFamily,nullptr){};


Variable::Variable(std::string                                  name,
         std::shared_ptr<QuadratureFamily1D>          quadFamily) : Variable(name,nullptr,quadFamily){};


Variable::Variable(std::string nameIn, RecursivePolynomialFamily1D::Ptr polyFamilyIn, QuadratureFamily1D::Ptr quadFamilyIn) : polyFamily(polyFamilyIn), quadFamily(quadFamilyIn),name(nameIn){}


template<class Archive>
void Variable::serialize(Archive& ar, const unsigned int version)
{
  ar& name;
  ar& polyFamily;
  ar& quadFamily;
}

template void Variable::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void Variable::serialize(boost::archive::text_iarchive& ar, const unsigned int version);


//BOOST_CLASS_EXPORT(Variable)
