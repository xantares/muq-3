#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"

#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>

using namespace muq::Utilities;


double LegendrePolynomials1DRecursive::alpha(double x, unsigned int k)
{
  return -(2.0 * (double)k + 1.0) * x / ((double)k + 1);
}

double LegendrePolynomials1DRecursive::beta(double x, unsigned int k)
{
  return (double)k / ((double)k + 1);
}

double LegendrePolynomials1DRecursive::phi0(double x)
{
  return 1.0;
}

double LegendrePolynomials1DRecursive::phi1(double x)
{
  return x;
}

double LegendrePolynomials1DRecursive::GetNormalization_static(unsigned int n)
{
  return 2.0 / (2.0 * (double)n + 1);

  //	return 1.0;
}

double LegendrePolynomials1DRecursive::gradient_static(unsigned int order, double x)
{
  //this definition provided by http://functions.wolfram.com/Polynomials/LegendreP/20/01/01/
  if (order == 0) {
    //order 0 is a constant
    return 0;
  } else {
    double n = static_cast<double>(order);
    return n / (x * x - 1.0) * (x * evaluate_static(order, x) - evaluate_static(order - 1, x));
  }
}

double LegendrePolynomials1DRecursive::secondDerivative_static(unsigned int order, double x)
{
  //this definition provided by http://functions.wolfram.com/Polynomials/LegendreP/20/01/01/
  if (order < 2) {
    return 0;
  } else {
    double n = static_cast<double>(order);
    double den = x*x-1.0;
    return n / den * (1.0-2.0*x*x) * evaluate_static(order, x)+ n*x/den*(2*evaluate_static(order-1,x) + gradient_static(order,x)) - n/den*gradient_static(order-1,x);
  }
}

Eigen::VectorXd LegendrePolynomials1DRecursive::GetMonomialCoeffs(const int order){
  
  Eigen::VectorXd monoCoeffs = Eigen::VectorXd::Zero(order+1);
  
  if(order==0){
    monoCoeffs(0) = 1.0;
  }else if(order ==1){
    monoCoeffs(1) = 1.0;
  }else if(order==2){
    monoCoeffs(0) = -0.5;
    monoCoeffs(2) = 1.5;
  }else if(order==3){
    monoCoeffs(1) = -1.5;
    monoCoeffs(3) = 2.5;
  }else if(order==4){
    monoCoeffs(0) = 0.375;
    monoCoeffs(2) = -3.75;
    monoCoeffs(4) = 4.375;
  }else if(order==5){
    monoCoeffs(1) = 1.875;
    monoCoeffs(3) = -8.75;
    monoCoeffs(5) = 7.875;
  }else{
    Eigen::VectorXd oldOldCoeffs = Eigen::VectorXd::Zero(order+1);
    Eigen::VectorXd oldCoeffs = Eigen::VectorXd::Zero(order+1);
    oldOldCoeffs(0) = 1.0; // constant term -- same as monomial
    oldCoeffs(1) = 1.0;    // linear term -- same as monomial
    for(int i=2; i<=order; ++i){
      monoCoeffs = Eigen::VectorXd::Zero(order+1);
      monoCoeffs.tail(order) = ((2*(i-1)+1)/double(i))*oldCoeffs.head(order);
      monoCoeffs -= ((i-1)/double(i))*oldOldCoeffs;
      
      oldOldCoeffs = oldCoeffs;
      oldCoeffs = monoCoeffs;
    }
  }
  return monoCoeffs;
}

template<class Archive>
void LegendrePolynomials1DRecursive::serialize(Archive& ar, const unsigned int version)
{
  //nothing to serialize except for the parent and the type of the object
  ar& boost::serialization::base_object<Static1DPolynomial<LegendrePolynomials1DRecursive>>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::Static1DPolynomial<LegendrePolynomials1DRecursive>)
BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::LegendrePolynomials1DRecursive)

template void LegendrePolynomials1DRecursive::serialize(boost::archive::text_oarchive& ar, unsigned int version);
template void LegendrePolynomials1DRecursive::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

template void Static1DPolynomial<LegendrePolynomials1DRecursive>::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void Static1DPolynomial<LegendrePolynomials1DRecursive>::serialize(boost::archive::text_iarchive& ar, const unsigned int version);


