#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

using namespace Eigen;
using namespace muq::Utilities;

GaussLegendreQuadrature1D::GaussLegendreQuadrature1D() {}

GaussLegendreQuadrature1D::~GaussLegendreQuadrature1D() {}

///Implements the monic coefficients;
RowVectorXd GaussLegendreQuadrature1D::GetMonicCoeff(unsigned int const j) const
{
  RowVectorXd monicCoeff(2);
  double jd = (double)j;

  double Bnsqrt = (jd / (2 * jd + 1)) * sqrt((2 * jd + 1) / (2 * jd - 1));

  monicCoeff << 0, Bnsqrt * Bnsqrt;

  return monicCoeff;
}

double GaussLegendreQuadrature1D::IntegralOfWeight() const
{
  return 2;
}

template<class Archive>
void GaussLegendreQuadrature1D::serialize(Archive& ar, const unsigned int version)
{
  ar& boost::serialization::base_object<GaussianQuadratureFamily1D>(*this);
}

BOOST_CLASS_EXPORT(GaussLegendreQuadrature1D)

