#include "MUQ/Utilities/Quadrature/ExpGrowthQuadratureFamily1D.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

using namespace Eigen;
using namespace muq::Utilities;

ExpGrowthQuadratureFamily1D::ExpGrowthQuadratureFamily1D() {}

ExpGrowthQuadratureFamily1D::ExpGrowthQuadratureFamily1D(QuadratureFamily1D::Ptr inputRule)
{
  //all we need to do is keep the input rule around.
  baseQuadrature1DRule = inputRule;
}

ExpGrowthQuadratureFamily1D::~ExpGrowthQuadratureFamily1D()
{
  // TODO Auto-generated destructor stub
}

unsigned int ExpGrowthQuadratureFamily1D::GetPrecisePolyOrder(unsigned int const order) const
{
  assert(order > 0);

  //simply call the base with the new order
  return baseQuadrature1DRule->GetPrecisePolyOrder(pow(2, order - 1));
}

void ExpGrowthQuadratureFamily1D::ComputeNodesAndWeights(unsigned int const            order,
                                                         std::shared_ptr<RowVectorXd>& nodes,
                                                         std::shared_ptr<RowVectorXd>& weights) const
{
  assert(order > 0);

  //simply call the base with the new order
  BOOST_LOG_POLY("quad", debug) << "Get exp order: " << pow(2, order - 1);
  nodes   = baseQuadrature1DRule->GetNodes(pow(2, order - 1));
  weights = baseQuadrature1DRule->GetWeights(pow(2, order - 1));

  //	BOOST_LOG_POLY("quad",debug) << "got " << *nodes << *weights;
}

template<class Archive>
void ExpGrowthQuadratureFamily1D::serialize(Archive& ar, const unsigned int version)
{
  ar& boost::serialization::base_object<QuadratureFamily1D>(*this);
  ar& baseQuadrature1DRule;
}

BOOST_CLASS_EXPORT(muq::Utilities::ExpGrowthQuadratureFamily1D) template void ExpGrowthQuadratureFamily1D::serialize(
  boost::archive::text_oarchive& ar,
  const unsigned int             version);
template void                                                                 ExpGrowthQuadratureFamily1D::serialize(
  boost::archive::text_iarchive& ar,
  const unsigned int             version);

