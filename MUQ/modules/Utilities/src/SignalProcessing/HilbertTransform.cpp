#include "MUQ/Utilities/SignalProcessing/HilbertTransform.h"

#include <unsupported/Eigen/FFT>

using namespace muq::Utilities;

void HilbertTransform::Compute(const Eigen::VectorXd &inputSignal){
	
	assert(inputSignal.size()>1);
	
	// store the signal
    int dim = inputSignal.size();
	
	// compute the fft of the input signal
    Eigen::VectorXcd fourier;
	Eigen::FFT<double> fft;
    fft.fwd(fourier, inputSignal);
	
	if(dim%2==0){
	  // length of signal is even	
	  for(int i=1; i<dim/2; ++i)
	    fourier(i) *= 2.0;
	  for(int i=dim/2+1; i<dim; ++i)
	    fourier(i) = std::complex<double>(0.0,0.0);
		
	}else{
	  // length of signal is odd
	  for(int i=1;i<(dim+1)/2; ++i)	
	    fourier(i) *= 2.0; 	
  	  for(int i=(dim+1)/2; i<dim; ++i)
  	    fourier(i) = std::complex<double>(0.0,0.0);
	}
	
	// compute the inverse fft for the hilbert transform
	fft.inv(analyticSig,fourier);
}


Eigen::VectorXcd HilbertTransform::GetAnalytic() const{
	return analyticSig;
}

Eigen::VectorXd HilbertTransform::GetEnvelope() const{
	Eigen::VectorXd envelope(analyticSig.size());
	for(int i=0; i<analyticSig.size(); ++i)
	    envelope(i) = std::abs(analyticSig(i));
	return envelope;
}

Eigen::VectorXd HilbertTransform::GetHilbert() const{
	Eigen::VectorXd hilbert(analyticSig.size());
	for(int i=0; i<analyticSig.size(); ++i)
	    hilbert(i) = analyticSig(i).imag();
	return hilbert;
}

Eigen::VectorXd HilbertTransform::GetSignal() const{
	Eigen::VectorXd signal(analyticSig.size());
	for(int i=0; i<analyticSig.size(); ++i)
	    signal(i) = analyticSig(i).real();
	return signal;
}

