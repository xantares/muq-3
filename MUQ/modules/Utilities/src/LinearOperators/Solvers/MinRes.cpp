
#include "MUQ/Utilities/LinearOperators/Solvers/MinRes.h"

// standard library includes
#include <memory>
#include <iostream>

// other muq includes
#include "MUQ/Utilities/LinearOperators/Operators/SymmetricOpBase.h"
#include "MUQ/Utilities/RandomGenerator.h"

using namespace muq::Utilities;
using namespace std;

// NOTE: The code in this function was adapted from the internal Eigen minres solver
Eigen::VectorXd MinRes::solve(const Eigen::VectorXd& b)
{
	Eigen::VectorXd x = Eigen::VectorXd::Zero(b.size());
	
    // initialize
    const int maxIters(b.rows());  // initialize maxIters to iters
    const int N(b.rows());    // the size of the matrix
    const double rhsNorm2(b.squaredNorm());
    const double threshold2(tol*tol*rhsNorm2); // convergence threshold (compared to residualNorm2)

    // Initialize preconditioned Lanczos
//            Eigen::VectorXd v_old(N); // will be initialized inside loop
    Eigen::VectorXd v( Eigen::VectorXd::Zero(N) ); //initialize v
    Eigen::VectorXd v_new(b); //initialize v_new
    double residualNorm2(v_new.squaredNorm());
//            Eigen::VectorXd w(N); // will be initialized inside loop
    Eigen::VectorXd w_new(v_new); // initialize w_new
//            double beta; // will be initialized inside loop
    double beta_new2(v_new.dot(w_new));
    double beta_new(sqrt(beta_new2));
    const double beta_one(beta_new);
    v_new /= beta_new;
    w_new /= beta_new;
    // Initialize other variables
    double c(1.0); // the cosine of the Givens rotation
    double c_old(1.0);
    double s(0.0); // the sine of the Givens rotation
    double s_old(0.0); // the sine of the Givens rotation
//            Eigen::VectorXd p_oold(N); // will be initialized in loop
    Eigen::VectorXd p_old(Eigen::VectorXd::Zero(N)); // initialize p_old=0
    Eigen::VectorXd p(p_old); // initialize p=0
    double eta(1.0);
    
	Eigen::VectorXd v_old, w, p_oold;
    int iters = 0; // reset iters
    while ( iters < maxIters ){
    
        // Preconditioned Lanczos
        /* Note that there are 4 variants on the Lanczos algorithm. These are
         * described in Paige, C. C. (1972). Computational variants of
         * the Lanczos method for the eigenproblem. IMA Journal of Applied
         * Mathematics, 10(3), 373–381. The current implementation corresponds 
         * to the case A(2,7) in the paper. It also corresponds to 
         * algorithm 6.14 in Y. Saad, Iterative Methods ￼￼￼for Sparse Linear
         * Systems, 2003 p.173. For the preconditioned version see 
         * A. Greenbaum, Iterative Methods for Solving Linear Systems, SIAM (1987).
         */
        const double beta(beta_new);
        
		v_old = v;
        v = v_new;
        w = w_new; 
		
        v_new.noalias() = A->apply(w) - beta*v_old; // compute v_new
        
		const double alpha = v_new.dot(w);
        v_new -= alpha*v; // overwrite v_new
        w_new = v_new; // overwrite w_new
        beta_new2 = v_new.dot(w_new); // compute beta_new
        beta_new = sqrt(beta_new2); // compute beta_new
        v_new /= beta_new; // overwrite v_new for next iteration
        w_new /= beta_new; // overwrite w_new for next iteration
    
        // Givens rotation
        const double r2 =s*alpha+c*c_old*beta; // s, s_old, c and c_old are still from previous iteration
        const double r3 =s_old*beta; // s, s_old, c and c_old are still from previous iteration
        const double r1_hat=c*alpha-c_old*s*beta;
        const double r1 =sqrt( std::pow(r1_hat,2) + std::pow(beta_new,2) );
        c_old = c; // store for next iteration
        s_old = s; // store for next iteration
        c=r1_hat/r1; // new cosine
        s=beta_new/r1; // new sine
    
        // Update solution
        p_oold = p_old;
        p_old = p;
        p.noalias()=(w-r2*p_old-r3*p_oold) /r1; // IS NOALIAS REQUIRED?
        x += beta_one*c*eta*p;
        residualNorm2 *= s*s;
    
        if ( residualNorm2 < threshold2){
            break;
        }
    
        eta=-s*eta; // update eta
        iters++; // increment iteration number (for output purposes)
		
    }
  
    return x;
}


