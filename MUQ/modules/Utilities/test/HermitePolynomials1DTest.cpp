#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/ProbabilistHermite.h"

#include <boost/timer/timer.hpp>

#include "gtest/gtest.h"

using namespace muq::Utilities;

TEST(UtilitiesHermitePolyTest, spotChecks)
{
  HermitePolynomials1DRecursive HermitePolyR;

  //A sample of points, the lhs is from mathematica. Note that these
  //have very poor absolute accuracy because the numbers are huge, but the relative
  //precision is to the full double.

  EXPECT_NEAR(1.0,                    HermitePolyR.evaluate(0, .4),       1e-14);
  EXPECT_NEAR(.6,                     HermitePolyR.evaluate(1, .3),       1e-14);
  EXPECT_NEAR(-.56,                   HermitePolyR.evaluate(2, .6),       1e-14);
  EXPECT_NEAR(33.6235290625,          HermitePolyR.evaluate(5, 0.325),    1e-14);
  EXPECT_NEAR(6219.5581337600015,     HermitePolyR.evaluate(8, 1.6),      2e-12);
  EXPECT_NEAR(6.075804453410837e11,   HermitePolyR.evaluate(20, -.845),   3e-4);
  EXPECT_NEAR(-5.8505463205709636e38, HermitePolyR.evaluate(50, .1264),   1e24);
  EXPECT_NEAR(5.4520440325350614e216, HermitePolyR.evaluate(200, -.3598), 2.5e201);

  EXPECT_NEAR(3.223845682635297e39,   HermitePolyR.gradient(50, .3),      1e25);
  
  EXPECT_DOUBLE_EQ(2.0, HermitePolyR.gradient(1, 0.5));
  EXPECT_DOUBLE_EQ(2.0, HermitePolyR.gradient(1, 1.0));
  EXPECT_DOUBLE_EQ(2.0, HermitePolyR.gradient(1, -0.5));
  
  EXPECT_DOUBLE_EQ(24*0.25-12, HermitePolyR.gradient(3, 0.5));
  EXPECT_DOUBLE_EQ(24-12, HermitePolyR.gradient(3, 1.0));
  EXPECT_DOUBLE_EQ(24*0.25-12, HermitePolyR.gradient(3, -0.5));
  
  EXPECT_EQ(0.0, HermitePolyR.gradient(0, -0.5));
  EXPECT_EQ(0.0, HermitePolyR.gradient(0, 0.0));
  EXPECT_EQ(0.0, HermitePolyR.gradient(0, 0.5));
  
}


TEST(UtilitiesProbHermitePolyTest, spotChecks)
{
  ProbabilistHermite poly;
  
  //A sample of points, the lhs is from mathematica. Note that these
  //have very poor absolute accuracy because the numbers are huge, but the relative
  //precision is to the full double.
  
  EXPECT_NEAR(1.0,                    poly.evaluate(0, .4),       1e-14);
  EXPECT_NEAR(.3,                     poly.evaluate(1, .3),       1e-14);
  EXPECT_NEAR(-0.6400,                poly.evaluate(2, .6),       1e-14);
  EXPECT_NEAR(4.535344658203125e+00,  poly.evaluate(5, 0.325),    1e-14);
  EXPECT_NEAR(-2.075637503999991e+01, poly.evaluate(8, 1.6),      2e-12);
  EXPECT_NEAR(-6.102571728158410e+08, poly.evaluate(20, -.845),   3e-4);
  
  EXPECT_DOUBLE_EQ(1.0, poly.gradient(1, 0.5));
  EXPECT_DOUBLE_EQ(1.0, poly.gradient(1, 1.0));
  EXPECT_DOUBLE_EQ(1.0, poly.gradient(1, -0.5));
  
  EXPECT_DOUBLE_EQ(3*0.25-3, poly.gradient(3, 0.5));
  EXPECT_DOUBLE_EQ(3-3, poly.gradient(3, 1.0));
  EXPECT_DOUBLE_EQ(3*0.25-3, poly.gradient(3, -0.5));
  
  EXPECT_EQ(0.0, poly.gradient(0, -0.5));
  EXPECT_EQ(0.0, poly.gradient(0, 0.0));
  EXPECT_EQ(0.0, poly.gradient(0, 0.5));
  
}

TEST(UtilitiesProbHermitePolyTest, SturmSolve){
  
  // create a cubic polynomial that caused us issues at one point, this is almost linear
  Eigen::VectorXd poly(5);
  poly << 8.147236863931789e-01,     9.057919370756192e-01,     1.269868162935061e-01,     9.133758561390194e-01,     6.323592462254095e-01;
  
  // compute the roots
  Eigen::VectorXd roots = ProbabilistHermite::Roots(poly, 1e-6);
  
  // compare the roots to the truth
  EXPECT_NEAR(-2.924713468970414e+00,roots(0),1e-4);
  EXPECT_NEAR(-1.079711083256760e+00,roots(1),1e-4);
  EXPECT_NEAR(6.934823585835934e-01,roots(2),1e-4);
  EXPECT_NEAR(1.866548264732107e+00,roots(3),1e-4);
}

TEST(UtilitiesHermitePolyTest, SturmSolve){
  
  // create a cubic polynomial that caused us issues at one point, this is almost linear
  Eigen::VectorXd poly(5);
  poly << 8.147236863931789e-01,     9.057919370756192e-01,     1.269868162935061e-01,     9.133758561390194e-01,     6.323592462254095e-01;
  
  // compute the roots
  Eigen::VectorXd roots = HermitePolynomials1DRecursive::Roots(poly, 1e-6);
  
  // compare the roots to the truth
  EXPECT_NEAR(-1.904980078887119e+00,roots(0),1e-4);
  EXPECT_NEAR(-6.949715242251162e-01,roots(1),1e-4);
  EXPECT_NEAR(4.162658411042948e-01,roots(2),1e-4);
  EXPECT_NEAR(1.461488797552201e+00,roots(3),1e-4);
}

TEST(UtilitiesHermitePolyTest, spotChecks2)
{
  HermitePolynomials1DRecursive hermitePolyR;
  
  double x = 0.25;
  unsigned maxOrder = 11;
  Eigen::VectorXd trueVals(maxOrder+1);
  for(int p=0; p<=maxOrder; ++p)
    trueVals(p) = hermitePolyR.evaluate(p,x);
  
  Eigen::VectorXd testVals = hermitePolyR.evaluateAll(maxOrder,x);
  
  for(int p=0; p<=maxOrder; ++p){
    EXPECT_DOUBLE_EQ(trueVals(p),testVals(p));
  }
}