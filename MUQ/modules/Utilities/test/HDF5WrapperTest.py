import sys
import unittest
import numpy as np # math stuff in python
import random

import libmuqUtilities

class HDF5WrapperTest(unittest.TestCase):
    def testMatrixReadWrite(self):
        fileName = "results/tests/hdf5WrapperPythonTest.h5"

        hdf5file = libmuqUtilities.HDF5Wrapper()

        hdf5file.OpenFile(fileName)

        vec = [1.0, 2.0, 3.0, 4.0, 5.0]

        hdf5file.WriteVector("/vector", vec)
        self.assertTrue(hdf5file.DoesDataSetExist("/vector"))

        vecRead = hdf5file.ReadVector("/vector")

        mat = [[1.0, 2.0],
               [4.0, 5.0],
               [7.0, 8.0]]

        hdf5file.WriteMatrix("/matrix", mat)
        self.assertTrue(hdf5file.DoesDataSetExist("/matrix"))

        matRead = hdf5file.ReadMatrix("/matrix")

        hdf5file.CloseFile()

        self.assertEqual(len(mat), len(matRead))
        for i in range(len(mat)):
            self.assertEqual(len(mat[i]), len(matRead[i]))
            for j in range(len(mat[i])):
                self.assertEqual(mat[i][j], matRead[i][j])

        self.assertEqual(len(vec), len(vecRead))
        for i in range(len(vec)):
            self.assertEqual(vec[i], vecRead[i])

