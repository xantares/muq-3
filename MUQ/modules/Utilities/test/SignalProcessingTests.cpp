#include "gtest/gtest.h"

#include "MUQ/Utilities/SignalProcessing/HilbertTransform.h"

using namespace std;
using namespace muq::Utilities;

TEST(Utilties_SignalProcessing, HilbertTransform)
{
	// generate the initial signal
	Eigen::VectorXd t(1024);
	t.setLinSpaced(1024,0,2);
	
	Eigen::VectorXd trueEnv = (-8.0*((t.array()-1.0).pow(2))).exp();
	Eigen::VectorXd sinSig = (40.0*t.array()).sin();
	Eigen::VectorXd allSig = sinSig.array()*trueEnv.array();
	
	// compute the Hilbert transform and envelope
	HilbertTransform hilb;
	hilb.Compute(allSig);
	
	Eigen::VectorXd compEnv = hilb.GetEnvelope();
	
	for(int i=0; i<1024; ++i)
	  EXPECT_NEAR(trueEnv(i),compEnv(i),5e-3);
	
}