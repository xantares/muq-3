#include "MUQ/Utilities/QuadratureTestFunctions.h"

#include <iostream>

using namespace Eigen;

Eigen::VectorXd simplePoly1D(Eigen::VectorXd const& vars)
{
  VectorXd result(1);

  result << 2 * vars(0) * vars(0) + 3;
  return result;
}

Eigen::VectorXd simplePoly3D(Eigen::VectorXd const& vars)
{
  //x*x*y + yyxx -xyz
  Eigen::VectorXd result(1);

  result << vars(0) * vars(0) * vars(1) + vars(0) * vars(0) * vars(1) * vars(1) - vars(0) * vars(1) * vars(2);
  return result;
}

Eigen::VectorXd dasqTest2DIso1(Eigen::VectorXd const& vars)
{
  //shift domain to [0,1]
  VectorXd temp = (vars.array() + 1.0) / 2.0;
  double   val  = 10.0 * exp(-temp(0) * temp(0)) + 10.0 * exp(-temp(1) * temp(1));

  val /= 4; //correct for domain shift
  VectorXd result(1);
  result << val;
  return result;
}

Eigen::VectorXd dasqTest2DIso2(Eigen::VectorXd const& vars)
{
  //shift domain to [0,1]
  VectorXd temp = (vars.array() + 1.0) / 2.0;
  double   val  = exp(temp(0)) + exp(temp(1)) + exp(temp(0) * temp(1));

  val /= 4; //correct for domain shift
  VectorXd result(1);
  result << val;
  return result;
}

Eigen::VectorXd dasqTest2DIso3(Eigen::VectorXd const& vars)
{
  //shift domain to [0,1]
  VectorXd temp = (vars.array() + 1.0) / 2.0;
  double   val  = exp(-10.0 * temp(0) * temp(0) - 10.0 * temp(1) * temp(1));

  val /= 4; //correct for domain shift
  VectorXd result(1);
  result << val;
  return result;
}

Eigen::VectorXd dasqTest2DAniso1(Eigen::VectorXd const& vars)
{
  //shift domain to [0,1]
  VectorXd temp = (vars.array() + 1.0) / 2.0;
  double   val  = exp(-temp(0) * temp(0)) + 10.0 * exp(-temp(1) * temp(1));

  val /= 4; //correct for domain shift
  VectorXd result(1);
  result << val;
  return result;
}

Eigen::VectorXd dasqTest2DAniso2(Eigen::VectorXd const& vars)
{
  //shift domain to [0,1]
  VectorXd temp = (vars.array() + 1.0) / 2.0;
  double   val  = exp(temp(0)) + 10.0 * exp(temp(1)) + 10.0 * exp(temp(0) * temp(1));

  val /= 4; //correct for domain shift
  VectorXd result(1);
  result << val;
  return result;
}

Eigen::VectorXd dasqTest2DAniso3(Eigen::VectorXd const& vars)
{
  //shift domain to [0,1]
  VectorXd temp = (vars.array() + 1.0) / 2.0;
  double   val  = exp(-10.0 * temp(0) * temp(0) - 5.0 * temp(1) * temp(1));

  val /= 4; //correct for domain shift
  VectorXd result(1);
  result << val;
  return result;
}

