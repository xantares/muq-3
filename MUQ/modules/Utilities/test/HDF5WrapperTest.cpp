#include "gtest/gtest.h"

#include <boost/filesystem.hpp>

#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/HDF5Logging.h"

using namespace std;
using namespace muq::Utilities;

class HDF5WrapperTest_ReadWrite : public::testing::Test {
protected:

  virtual void SetUp()
  {
    tempDir  = boost::filesystem::temp_directory_path();
    tempName = boost::filesystem::unique_path("%%%%-%%%%-%%%%-%%%%.h5");

    tempDir /= tempName; //append with added /
    HDF5Wrapper::OpenFile(tempDir.string());
  }

  virtual void TearDown()
  {
    HDF5Logging::WipeBuffers();
    HDF5Wrapper::CloseFile();
  }

  boost::filesystem::path tempDir;
  boost::filesystem::path tempName;
};

TEST_F(HDF5WrapperTest_ReadWrite, Double)
{
  Eigen::VectorXd testVec = Eigen::VectorXd::Random(3);
  Eigen::MatrixXd testMat = Eigen::MatrixXd::Random(3, 2);

  // write and read the vector/matrix to file
  HDF5Wrapper::WriteMatrix("/vector", testVec);
  
  assert(HDF5Wrapper::DoesDataSetExist("/vector"));
  Eigen::VectorXd truthVec = HDF5Wrapper::ReadMatrix("/vector");

  HDF5Wrapper::WriteMatrix("/matrix", testMat);

  assert(HDF5Wrapper::DoesDataSetExist("/matrix"));
  Eigen::MatrixXd truthMat = HDF5Wrapper::ReadMatrix("/matrix");

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthVec, testVec, 1e-10);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthMat, testMat, 1e-10);

  Eigen::VectorXi testMatSize = HDF5Wrapper::GetDatasetSize("/matrix");
  EXPECT_EQ(3,testMatSize(0));
  EXPECT_EQ(2,testMatSize(1));

  Eigen::VectorXi testVecSize = HDF5Wrapper::GetDatasetSize("/vector");
  EXPECT_EQ(3,testVecSize(0));
  EXPECT_EQ(1,testVecSize(1));
  
}

TEST_F(HDF5WrapperTest_ReadWrite, Float)
{
  Eigen::VectorXf testVec = Eigen::VectorXf::Random(3);
  Eigen::MatrixXf testMat = Eigen::MatrixXf::Random(3, 2);

  // write and read the vector/matrix to file
  HDF5Wrapper::WriteMatrix("/vector", testVec);

  assert(HDF5Wrapper::DoesDataSetExist("/vector"));
  Eigen::VectorXf truthVec = HDF5Wrapper::ReadMatrix<float>("/vector");

  HDF5Wrapper::WriteMatrix("/matrix", testMat);
  assert(HDF5Wrapper::DoesDataSetExist("/matrix"));
  Eigen::MatrixXf truthMat = HDF5Wrapper::ReadMatrix<float>("/matrix");

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthVec, testVec, 1e-10);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthMat, testMat, 1e-10);
  
  Eigen::VectorXi testMatSize = HDF5Wrapper::GetDatasetSize("/matrix");
  EXPECT_EQ(3,testMatSize(0));
  EXPECT_EQ(2,testMatSize(1));

  Eigen::VectorXi testVecSize = HDF5Wrapper::GetDatasetSize("/vector");
  EXPECT_EQ(3,testVecSize(0));
  EXPECT_EQ(1,testVecSize(1));
}

TEST_F(HDF5WrapperTest_ReadWrite, Int)
{
  Eigen::VectorXi testVec = Eigen::VectorXi::Random(3);
  Eigen::MatrixXi testMat = Eigen::MatrixXi::Random(3, 2);

  // write and read the vector/matrix to file
  HDF5Wrapper::WriteMatrix("/vector", testVec);

  assert(HDF5Wrapper::DoesDataSetExist("/vector"));
  Eigen::VectorXi truthVec = HDF5Wrapper::ReadMatrix<int>("/vector");

  HDF5Wrapper::WriteMatrix("/matrix", testMat);
  assert(HDF5Wrapper::DoesDataSetExist("/matrix"));
  Eigen::MatrixXi truthMat = HDF5Wrapper::ReadMatrix<int>("/matrix");

  EXPECT_PRED_FORMAT2(MatrixEqual, truthVec, testVec);
  EXPECT_PRED_FORMAT2(MatrixEqual, truthMat, testMat);
  
  Eigen::VectorXi testMatSize = HDF5Wrapper::GetDatasetSize("/matrix");
  EXPECT_EQ(3,testMatSize(0));
  EXPECT_EQ(2,testMatSize(1));

  Eigen::VectorXi testVecSize = HDF5Wrapper::GetDatasetSize("/vector");
  EXPECT_EQ(3,testVecSize(0));
  EXPECT_EQ(1,testVecSize(1));
}

TEST_F(HDF5WrapperTest_ReadWrite, Unsigned)
{
  Eigen::VectorXu testVec = Eigen::VectorXu::Random(3);
  Eigen::MatrixXu testMat = Eigen::MatrixXu::Random(3, 2);

  // write and read the vector/matrix to file
  HDF5Wrapper::WriteMatrix("/vector", testVec);

  assert(HDF5Wrapper::DoesDataSetExist("/vector"));
  Eigen::VectorXu truthVec = HDF5Wrapper::ReadMatrix<unsigned>("/vector");

  HDF5Wrapper::WriteMatrix("/matrix", testMat);
  assert(HDF5Wrapper::DoesDataSetExist("/matrix"));
  Eigen::MatrixXu truthMat = HDF5Wrapper::ReadMatrix<unsigned>("/matrix");

  EXPECT_PRED_FORMAT2(MatrixEqual, truthVec, testVec);
  EXPECT_PRED_FORMAT2(MatrixEqual, truthMat, testMat);
  
  Eigen::VectorXi testMatSize = HDF5Wrapper::GetDatasetSize("/matrix");
  EXPECT_EQ(3,testMatSize(0));
  EXPECT_EQ(2,testMatSize(1));

  Eigen::VectorXi testVecSize = HDF5Wrapper::GetDatasetSize("/vector");
  EXPECT_EQ(3,testVecSize(0));
  EXPECT_EQ(1,testVecSize(1));
}


TEST(HDF5WrapperTest, MultiLevelGroup)
{
  Eigen::VectorXd testVec = Eigen::VectorXd::Random(3);
  Eigen::MatrixXd testMat = Eigen::MatrixXd::Random(3, 2);

  boost::filesystem::path tempDir  = boost::filesystem::temp_directory_path();
  boost::filesystem::path tempName = boost::filesystem::unique_path("%%%%-%%%%-%%%%-%%%%.h5");

  tempDir /= tempName; //append with added /
  HDF5Wrapper::OpenFile(tempDir.string());

  //some tests on an empty file
  EXPECT_FALSE(HDF5Wrapper::DoesDataSetExist("/here/is/a/vector"));
  EXPECT_FALSE(HDF5Wrapper::DoesGroupExist("/here/"));
  EXPECT_FALSE(HDF5Wrapper::DoesGroupExist("/here/is"));

  HDF5Wrapper::CreateGroup("/a/crazy/path");
  HDF5Wrapper::CreateGroup("/here/is/a");
  HDF5Wrapper::CreateGroup("/here/is/b/");
  // write and read the vector/matrix to file
  HDF5Wrapper::WriteMatrix("/here/is/a/vector", testVec);
  ASSERT_TRUE(HDF5Wrapper::DoesDataSetExist("/here/is/a/vector"));
  Eigen::VectorXd truthVec = HDF5Wrapper::ReadMatrix("/here/is/a/vector");

  HDF5Wrapper::WriteMatrix("/path/to/a/matrix", testMat);
  ASSERT_TRUE(HDF5Wrapper::DoesDataSetExist("/path/to/a/matrix"));
  Eigen::MatrixXd truthMat = HDF5Wrapper::ReadMatrix("/path/to/a/matrix");

  HDF5Wrapper::CloseFile();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthVec, testVec, 1e-10);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthMat, testMat, 1e-10);
}

TEST(HDF5WrapperTest, Attributes)
{
  std::string groupName = "/a/crazy/path";

  HDF5Wrapper::OpenFile("results/tests/Hdf5Attributes.h5");

  // first, create a group so we have a thing to put attributes on

  HDF5Wrapper::CreateGroup(groupName);

  EXPECT_EQ(true, HDF5Wrapper::DoesGroupExist(groupName));

  HDF5Wrapper::WriteScalarAttribute(groupName, "derivedAtt2", 9);
  HDF5Wrapper::WriteScalarAttribute<double>(groupName, "doubleAtt", 1.234);
  HDF5Wrapper::WriteScalarAttribute<float>(groupName, "floatAtt", 1.234);
  HDF5Wrapper::WriteScalarAttribute<int>(groupName, "intAtt", -11);
  HDF5Wrapper::WriteScalarAttribute<unsigned>(groupName, "unsignedAtt", 11);
  HDF5Wrapper::WriteStringAttribute(groupName, "stringAtt", "myTestString");

  HDF5Wrapper::CloseFile();
  
  HDF5Wrapper::OpenFile("results/tests/Hdf5Attributes.h5");

  EXPECT_EQ(9,HDF5Wrapper::GetScalarAttribute<double>(groupName, "derivedAtt2"));
  EXPECT_DOUBLE_EQ(1.234,HDF5Wrapper::GetScalarAttribute<double>(groupName, "doubleAtt"));
  EXPECT_FLOAT_EQ(1.234,HDF5Wrapper::GetScalarAttribute<float>(groupName, "floatAtt"));
  EXPECT_EQ(-11, HDF5Wrapper::GetScalarAttribute<int>(groupName, "intAtt"));
  EXPECT_EQ(11, HDF5Wrapper::GetScalarAttribute<unsigned>(groupName, "unsignedAtt"));
  EXPECT_EQ("myTestString",HDF5Wrapper::GetStringAttribute(groupName, "stringAtt"));

  HDF5Wrapper::CloseFile();
}

#ifndef MUQ_MPI //can't use this test with mpi because it merges files on close, which it then tries to read
TEST(HDF5Logging, Basic)
{
  Eigen::VectorXd testVec = Eigen::VectorXd::Random(3);
  Eigen::MatrixXd testMat = Eigen::MatrixXd::Random(3, 2);


  boost::filesystem::path tempDir  = boost::filesystem::temp_directory_path();
  boost::filesystem::path tempName = boost::filesystem::unique_path("%%%%-%%%%-%%%%-%%%%.h5");

  tempDir /= tempName;
  HDF5Wrapper::OpenFile(tempDir.string());

  boost::property_tree::ptree properties;
  properties.put("HDF5.LogWriteFrequency", 2);
  HDF5Logging::Configure(properties);

  auto logEntry = make_shared<HDF5LogEntry>();

  int numIters = 10;

  Eigen::MatrixXd scalarEveryTime = Eigen::MatrixXd::Random(1, 10);
  Eigen::MatrixXd vectorEveryTime = Eigen::MatrixXd::Random(3, numIters);


  for (int i = 0; i < numIters; ++i) {
    logEntry->loggedData["scalar"] = scalarEveryTime.col(i);
    logEntry->loggedData["vector"] = vectorEveryTime.col(i);

    HDF5Logging::WriteEntry("/path/to/data", logEntry, i, numIters);
  }


  auto logEntry2 = make_shared<HDF5LogEntry>();

  logEntry2->loggedData["strangePattern"] = Eigen::VectorXd::Constant(1, 2);
  HDF5Logging::WriteEntry("/path/to/data", logEntry2, 4, numIters);
  logEntry2->loggedData["strangePattern"] = Eigen::VectorXd::Constant(1, 7);
  HDF5Logging::WriteEntry("/path/to/data", logEntry2, 2, numIters);
  logEntry2->loggedData["strangePattern"] = Eigen::VectorXd::Constant(1, 9);
  HDF5Logging::WriteEntry("/path/to/data", logEntry2, 9, numIters);

  HDF5Logging::FlushBuffer(); //need to flush
  HDF5Wrapper::CloseFile();

  HDF5Wrapper::OpenFile(tempDir.string());
  Eigen::MatrixXd actualScalar         = HDF5Wrapper::ReadMatrix("/path/to/data/scalar");
  Eigen::MatrixXd actualVector         = HDF5Wrapper::ReadMatrix("/path/to/data/vector");
  Eigen::MatrixXd actualStrangePattern = HDF5Wrapper::ReadMatrix("/path/to/data/strangePattern");

  Eigen::RowVectorXd trueStrangePattern = Eigen::VectorXd::Constant(10, std::numeric_limits<double>::quiet_NaN());
  trueStrangePattern(2) = 7;
  trueStrangePattern(4) = 2;
  trueStrangePattern(9) = 9;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, scalarEveryTime, actualScalar, 1e-10);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, vectorEveryTime, actualVector, 1e-10);

  for (int i = 0; i < numIters; ++i) {
    EXPECT_EQ(std::isnan(trueStrangePattern(i)), std::isnan(actualStrangePattern(i)));
    if (!std::isnan(trueStrangePattern(i))) {
      EXPECT_DOUBLE_EQ(trueStrangePattern(i), actualStrangePattern(i));
    }
  }

  HDF5Logging::WipeBuffers();
  HDF5Wrapper::CloseFile();
}
#endif // ifndef MUQ_MPI

//use the fixture, which will open and close a temporary file
TEST_F(HDF5WrapperTest_ReadWrite, ParameterLogging_1)
{
  boost::property_tree::ptree properties;

  properties.put("Something.double", 1.1);
  properties.put("Something.int", 1);
  properties.put("Something.string", "test");

  EXPECT_EQ(1.1, ReadAndLogParameter(properties, "Something.double", 2.2, "/place/to/log"));
  EXPECT_EQ(2.2, ReadAndLogParameter(properties, "Something.double_not_set", 2.2, "/place/to/log"));
  EXPECT_EQ(1,   ReadAndLogParameter(properties, "Something.int", 2, "/place/to/log"));
  EXPECT_EQ(2,   ReadAndLogParameter(properties, "Something.int_not_set", 2, "/place/to/log"));
  EXPECT_TRUE(ReadAndLogParameter(properties, "Something.string", std::string("other value"),
                                  "/place/to/log").compare("test") == 0);
  EXPECT_TRUE(ReadAndLogParameter(properties, "Something.not_set", std::string("other value"),
                                  "/place/to/log").compare("other value") == 0);
}

//use the fixture, which will open and close a temporary file
TEST_F(HDF5WrapperTest_ReadWrite, ParameterLogging_2)
{
  boost::property_tree::ptree properties;

  properties.put("Something.double", 1.1);
  properties.put("Something.int", 1);
  properties.put("Something.string", "test");
  properties.put("HDF5OutputGroup", "/List/place/to/log");

  EXPECT_EQ(1.1, ReadAndLogParameter(properties, "Something.double", 2.2));
  EXPECT_EQ(2.2, ReadAndLogParameter(properties, "Something.double_not_set", 2.2));
  EXPECT_EQ(1,   ReadAndLogParameter(properties, "Something.int", 2));
  EXPECT_EQ(2,   ReadAndLogParameter(properties, "Something.int_not_set", 2));
  EXPECT_TRUE(ReadAndLogParameter(properties, "Something.string", std::string("other value")).compare("test") == 0);
  EXPECT_TRUE(ReadAndLogParameter(properties, "Something.not_set", std::string("other value")).compare(
                "other value") == 0);
}

//Careful with this test, hdfview doesn't show links correctly!
TEST(HDF5_ReadWrite, Links)
{
  boost::filesystem::path tempDir  = boost::filesystem::temp_directory_path();
  boost::filesystem::path tempName = boost::filesystem::unique_path("%%%%-%%%%-%%%%-%%%%.h5");

  tempDir /= tempName; //append with added /
  HDF5Wrapper::OpenFile(tempDir.string(), true);

  //try some attribute logging
  boost::property_tree::ptree properties;

  properties.put("Something.double", 1.1);
  properties.put("Something.int", 1);
  properties.put("Something.string", "test");
  properties.put("HDF5OutputGroup", "/List/place/to/log");

  EXPECT_EQ(1.1, ReadAndLogParameter(properties, "Something.double", 2.2));
  EXPECT_EQ(2.2, ReadAndLogParameter(properties, "Something.double_not_set", 2.2));
  EXPECT_EQ(1,   ReadAndLogParameter(properties, "Something.int", 2));
  EXPECT_EQ(2,   ReadAndLogParameter(properties, "Something.int_not_set", 2));
  EXPECT_TRUE(ReadAndLogParameter(properties, "Something.string", std::string("other value")).compare("test") == 0);
  EXPECT_TRUE(ReadAndLogParameter(properties, "Something.not_set", std::string("other value")).compare(
                "other value") == 0);

  //create some data
  Eigen::VectorXd testVec = Eigen::VectorXd::Random(3);
  Eigen::MatrixXd testMat = Eigen::MatrixXd::Random(3, 2);

  // write and read the vector/matrix to file
  HDF5Wrapper::WriteMatrix("/vector", testVec);
  HDF5Wrapper::WriteMatrix("/group/with/data/vector", testVec);

  assert(HDF5Wrapper::DoesDataSetExist("/vector"));
  Eigen::VectorXd truthVec = HDF5Wrapper::ReadMatrix("/vector");

  HDF5Wrapper::WriteMatrix("/matrix", testMat);
  assert(HDF5Wrapper::DoesDataSetExist("/matrix"));
  Eigen::MatrixXd truthMat = HDF5Wrapper::ReadMatrix("/matrix");
  HDF5Wrapper::WriteMatrix("/group/with/data/matrix", testMat);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthVec, testVec, 1e-10);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthMat, testMat, 1e-10);


  HDF5Logging::WipeBuffers();
  HDF5Wrapper::CloseFile();
}
