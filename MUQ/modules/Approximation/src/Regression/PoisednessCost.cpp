#include "MUQ/Approximation/Regression/PoisednessCost.h"

#include <Eigen/Dense>

using namespace std;
using namespace muq::Utilities;
using namespace muq::Optimization;
using namespace muq::Approximation;

#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"

PoisednessCost::PoisednessCost(shared_ptr<MultiIndexSet> const& terms, shared_ptr<VariableCollection> const& variables, Eigen::MatrixXd const& V, Eigen::MatrixXd const& K, unsigned int const dim) :
  OptProbBase(dim), Vand(V), terms(terms), variables(variables)
{
  // the matrix to invert
  const Eigen::MatrixXd C = Vand.transpose() * K.asDiagonal() * Vand;

  // we have the matrix, we need the factorization
  Eigen::LLT<Eigen::MatrixXd> CholSolver;
  CholSolver.compute(C);
  cholDecomp = CholSolver.matrixL();
}

double PoisednessCost::eval(Eigen::VectorXd const& in) 
{
  const Eigen::VectorXd phi = EvaluateBasis(in);

  // solve the system 
  Eigen::MatrixXd solved = cholDecomp.triangularView<Eigen::Lower>().solve(phi);
  cholDecomp.triangularView<Eigen::Lower>().transpose().solveInPlace(solved);

  // apply the vandermonde matrix
  solved = Vand * solved;

  return solved.norm();
}

double PoisednessCost::grad(Eigen::VectorXd const& in, Eigen::VectorXd& gradient) 
{
  // there is no derivative of the inifinity norm ...
  cerr << endl << "ERROR: PoisednessCost does not support gradient-based methods" << endl << endl;
  assert(false);
  
  return 0.0;
}

Eigen::VectorXd PoisednessCost::EvaluateBasis(Eigen::VectorXd const& point)
{
  // number of coefficients 
  const unsigned int numCoeff = terms->GetNumberOfIndices();

  // the Vandermonde matrix 
  Eigen::VectorXd phi(numCoeff);

  // each term is built by evaluating the polynomial basis 
  for( unsigned int i=0; i<numCoeff; ++i ) { // loop through the terms
    // the multiindex 
    const Eigen::RowVectorXu multiIndex = terms->IndexToMulti(i);
      
    // each term is a product of the 1D variables
    double result = 1.0;
    for( unsigned int v=0; v < variables->length(); ++v ) {
      result *= variables->GetVariable(v)->polyFamily->evaluate(multiIndex(v), point(v));
    }

    // insert each entry in the Vandermonde matrix 
    phi(i) = result;
  }

  return phi;
}
