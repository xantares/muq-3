#include "MUQ/Approximation/Regression/PolynomialRegressor.h"

#include <boost/unordered_map.hpp>
#include <boost/math/constants/constants.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/functional/hash.hpp>
#include <boost/serialization/export.hpp>
#include <boost/tuple/tuple.hpp>

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

#include "MUQ/Approximation/Regression/PoisednessCost.h"
#include "MUQ/Approximation/BallConstraint.h"

#include "MUQ/Utilities/HDF5Logging.h"


using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Optimization;
using namespace muq::Approximation;

// the polynomial options
map<string,
    PolynomialRegressor::BasisTypes> PolynomialRegressor::basisTypeMap =
{ { "legendre", Legendre }, { "hermite", Hermite } };

PolynomialRegressor::PolynomialRegressor(unsigned int const                 inDim,
                                         unsigned int const                 outDim,
                                         boost::property_tree::ptree const& para) : 
  Regressor(inDim, outDim), order(ReadAndLogParameter<int>(para, "PolynomialRegressor.Order", 0)),
  cubicWeights(ReadAndLogParameter<int>(para, "PolynomialRegressor.CubicWeights", false)),
  crossValidate(ReadAndLogParameter<int>(para, "PolynomialRegressor.CrossValidate", false)), 
  maxCVSamps(ReadAndLogParameter<int>(para, "PolynomialRegressor.MaxCVSamples", 30))
{
  if (cubicWeights) {
    // only supported for low order
    assert(order < 3);
  }

  // create the terms in the multi index
  terms = MultiIndexFactory::CreateTotalOrder(inDim, order);

  // determine what basis to use
  string basis = ReadAndLogParameter<string>(para, "PolynomialRegressor.Basis", "legendre");
  transform(basis.begin(), basis.end(), basis.begin(), ::tolower);

  // a single 1d polynomial
  shared_ptr<RecursivePolynomialFamily1D> poly;

  // initialize the polynomial type
  switch (basisTypeMap[basis]) {
  case Legendre:
  {
    poly = make_shared<LegendrePolynomials1DRecursive>();
    break;
  }

  case Hermite:
  {
    poly = make_shared<HermitePolynomials1DRecursive>();
    break;
  }

  default:
    assert(false);
  }

  // create the input variables by stacking up 1D variables
  variables = make_shared<VariableCollection>();
  for (int i = 0; i < inputSizes[0]; ++i) {
    // name the variable x_i
    stringstream name;
    name << "x" << i;

    // add it; we are not using the quadrature so the nullptr is okay
    variables->PushVariable(name.str(), poly, nullptr);
  }
}

unsigned PolynomialRegressor::GetNumberOfCVFits() const
{
  return cvFits.size();
}

Eigen::VectorXd PolynomialRegressor::EvaluateWithCoefficients(Eigen::VectorXd const& inputs,
                                                              Eigen::MatrixXd const& coeffToUse) const
{
  // evaluate the polynomials at the inputs
  Eigen::MatrixXd polyTerms = VandermondeMatrix((inputs - currentCenter) / currentRadius);

    assert(polyTerms.rows() == 1);

  // each column is an evaluation
  Eigen::MatrixXd result = coeffToUse * polyTerms.transpose();

  return result;
}

Eigen::MatrixXd PolynomialRegressor::AllCrossValidationEvaluate(Eigen::VectorXd const& inputs) const
{
  // each column is an evaluation
  Eigen::MatrixXd result(outputSize, cvFits.size());

  // apply the CV coefficeints
  for (unsigned int i = 0; i < cvFits.size(); ++i) {
    result.col(i) = EvaluateWithCoefficients(inputs, cvFits[i]);
  }

  return result;
}

Eigen::VectorXd PolynomialRegressor::EvaluateImpl(Eigen::VectorXd const& inputs)
{
  return EvaluateWithCoefficients(inputs, coefficients);
}

Eigen::VectorXd PolynomialRegressor::OneCrossValidationEvaluate(Eigen::VectorXd const& inputs, unsigned const i) const
{
    assert(i < GetNumberOfCVFits()); //ensure that the fit exists
  return EvaluateWithCoefficients(inputs, cvFits[i]);
}

Eigen::MatrixXd PolynomialRegressor::JacobianWithCoefficients(Eigen::VectorXd const& inputs,
                                                              Eigen::MatrixXd const& coeffToUse) const
{
  // center the inputs
  const Eigen::VectorXd centeredInputs = (inputs - currentCenter) / currentRadius;

  // number of coefficients
  const unsigned int numCoeff = terms->GetNumberOfIndices();

  // initalize the jacobian
  Eigen::MatrixXd jacobian(outputSize, inputSizes(0));

  // compute the derivative wrt each input
  for (unsigned int i = 0; i < inputSizes(0); ++i) {
    // polynomial terms
    Eigen::VectorXd polyTerms = Eigen::VectorXd::Ones(numCoeff);

    // loop over the terms
    for (unsigned int t = 0; t < numCoeff; ++t) {
      // the multiindex
      const Eigen::RowVectorXu multiIndex = terms->IndexToMulti(t);

      // loop over the variables
      for (unsigned int v = 0; v < variables->length(); ++v) {
        // the term is the product of 1D terms; the gradient is the product, replacing one with its derivative
        double onePolyEval;
        if (v == i) {
          onePolyEval = variables->GetVariable(v)->polyFamily->gradient(multiIndex(v), centeredInputs(v));
        } else {
          onePolyEval = variables->GetVariable(v)->polyFamily->evaluate(multiIndex(v), centeredInputs(v));
        }

        polyTerms(t) *= onePolyEval;
      }
    }

    // get the weighted sum of all the polynomial terms
    const Eigen::MatrixXd result = coeffToUse * polyTerms;
    assert(result.cols() == 1);

    //put this column of the jacobian in the result
    jacobian.col(i) = result.col(0);
  }

  return jacobian / currentRadius;
}

Eigen::MatrixXd PolynomialRegressor::JacobianImpl(Eigen::VectorXd const& inputs)
{
  return JacobianWithCoefficients(inputs, coefficients);
}

Eigen::MatrixXd PolynomialRegressor::OneCrossValidationJacobian(Eigen::VectorXd const& inputs, unsigned const i) const
{
    assert(i < GetNumberOfCVFits()); //ensure that the fit exists
  return JacobianWithCoefficients(inputs, cvFits[i]);
}

void PolynomialRegressor::Fit(Eigen::MatrixXd const& xs, Eigen::MatrixXd const& ys, Eigen::VectorXd const& center)
{
    assert(outputSize == ys.rows());
    assert(inputSizes(0) == xs.rows());
    assert(xs.cols() == ys.cols());

  InvalidateCache();

  // set center
  currentCenter = center;

  // center the points 
  Eigen::MatrixXd xsCentered = xs;
  CenterPoints(xsCentered);

  // set the weights equal to one
  Eigen::VectorXd weights = Eigen::VectorXd::Ones(ys.cols());
  if (cubicWeights) { // we want to use cubic weighting
    weights = CubicWeights(xsCentered);
  }

  // construct the Vandermonde matrix
  Eigen::MatrixXd vand = weights.asDiagonal() * VandermondeMatrix(xsCentered);

  // make the solver to do the regression
  auto solver = vand.colPivHouseholderQr();

  // zero out the coefficients
  coefficients = Eigen::MatrixXd::Zero(outputSize, vand.cols());

  // build the rhs
  const Eigen::MatrixXd rhs = weights.asDiagonal() * ys.transpose();

  // solve the least squares problem
  coefficients = solver.solve(rhs).transpose();

  // cross validate the result
  if (crossValidate) {
    CrossValidate(solver, rhs);
  }
}

void PolynomialRegressor::CrossValidate(Eigen::ColPivHouseholderQR<Eigen::MatrixXd> const& solver,
                                        Eigen::MatrixXd const                            & rhs)
{
  // total number of points
  const unsigned int numPoints = rhs.rows();

  // cross validate with either all the samples or the max. allowed
  const unsigned int numSamps = min(numPoints, maxCVSamps);

  // the indices that will be left out (one at a time)
  Eigen::VectorXi samples(numSamps);

  // choose the samples randomly
  int im = 0;

  for (int in = 0; in < numPoints && im < numSamps; ++in) {
    int rn = numPoints - in;
    int rm = numSamps - im;

    if (RandomGenerator::GetUniform() < static_cast<double>(rm) / static_cast<double>(rn)) {
      samples(im++) = in;
    }
  }
    assert(im == numSamps);

  // apply the Q^T matrix to the rhs to get the rhs of the system we need to solve
  const Eigen::MatrixXd QTRhs = solver.householderQ().transpose() * rhs;

  // for each sample
  cvFits.resize(numSamps);
  for (unsigned int i = 0; i < numSamps; ++i) {
    // R matrix from the QR decomposition
    Eigen::MatrixXd R = solver.matrixQR().triangularView<Eigen::Upper>();

    // the rhs we are working with is Q^T applied to the original rhs
    Eigen::MatrixXd newRhs = QTRhs;

    // sanity checks
    assert(R.rows() == rhs.rows());
    assert(samples(i) < R.rows());
    assert(R.rows() > R.cols());

    // extract the row to remove
    Eigen::VectorXd remove = solver.householderQ().transpose() * Eigen::VectorXd::Unit(numPoints, samples(i));

    // rotate so we can discard the first row
    Eigen::JacobiRotation<double> rotate;
    for (unsigned int j = remove.size() - 1; j > 0; --j) {
      rotate.makeGivens(remove(j - 1), remove(j));

      remove.applyOnTheLeft(j - 1, j, rotate.adjoint());
      newRhs.applyOnTheLeft(j - 1, j, rotate.adjoint());
      R.applyOnTheLeft(j - 1, j, rotate.adjoint());
    }

    // solve the new system without the first row
    R.block(1, 0, R.cols(), R.cols()).triangularView<Eigen::Upper>().solveInPlace(newRhs.block(1, 0, R.cols(), newRhs.cols()));
    

    // the coefficients for the cross validation
    Eigen::MatrixXd coefficientsCV = Eigen::MatrixXd::Zero(R.cols(), rhs.cols());

    // undo column pivoting offset by 1 (we're discarding the first row)
    for (unsigned int j = 0; j < solver.nonzeroPivots(); ++j) {
      coefficientsCV.row(solver.colsPermutation().indices().coeff(j)) = newRhs.row(j + 1);
    }
    for (unsigned int j = solver.nonzeroPivots(); j < solver.cols(); ++j) {
      coefficientsCV.row(solver.colsPermutation().indices().coeff(j)).setZero();
    }

    cvFits[i] = coefficientsCV.transpose();
  }
}

Eigen::VectorXd PolynomialRegressor::CubicWeights(Eigen::MatrixXd const& xs) const
{
  // only supported for low order
    assert(order < 3);

  // compute the norm of the centered points
  Eigen::VectorXd weights = xs.colwise().norm();

  // sort them
  Eigen::VectorXd sortedWeights = weights;
  sort(sortedWeights.data(), sortedWeights.data() + sortedWeights.size());

  // get the number required to interpolate
  const unsigned int num = NumPointsForInterpolation();

  // how far before we start cubically decreasing the weight?
  const double radius = weights.maxCoeff() - sortedWeights(num);
  assert(radius > 0.0);

  // set the weights
  for (unsigned int i = 0; i < weights.size(); ++i) {
    // before we start fading they are one
    if (weights(i) < sortedWeights(num)) {
      weights(i) = 1.0;
    } else { // decay cubically
      weights(i) = pow(1.0 - pow((weights(i) - sortedWeights(num)) / radius, 3.0), 3.0);
    }

    // add a bit to zero weights to prevent numerical issues
    if (weights(i) < 1.0e-14) {
      weights(i) = 1.0e-8;
    }
  }

  return weights;
}

void PolynomialRegressor::CenterPoints(Eigen::MatrixXd& xs)
{
  // if center isn't zero, rescale points
  if (!MatrixEqual(currentCenter, Eigen::VectorXd::Zero(inputSizes(0)))) {
    xs = xs.colwise() - currentCenter;
  }
  currentRadius = xs.colwise().norm().maxCoeff();
  assert(currentRadius>0);
  
  xs /= currentRadius;
}

int PolynomialRegressor::NumPointsForInterpolation() const
{
  switch (order) {
  case 0:                                                 // constant
    return 1;

  case 1:                                                 // linear
    return inputSizes(0) + 1;

  case 2:                                                 // quadratic
    return (inputSizes(0) + 1) * (inputSizes(0) + 2) / 2; // will always be an int

  default:
  {
    cerr << endl << endl << "ERROR: Formula not available to compute the number of points required for interpolation" <<
      endl << "\tPolynomialRegressor.cpp NumPointsForInterpolation()" << endl;
    assert(false);
    return -1;
  }
  }
}

Eigen::MatrixXd PolynomialRegressor::VandermondeMatrix(Eigen::MatrixXd const& xs) const
{
  // number of points
  const unsigned int N = xs.cols();

  // number of coefficients
  const unsigned int numCoeff = terms->GetNumberOfIndices();

  // the Vandermonde matrix
  Eigen::MatrixXd vand(N, numCoeff);

  // each term is built by evaluating the polynomial basis
  for (unsigned int pt = 0; pt < N; ++pt) {       // loop through the points
    for (unsigned int i = 0; i < numCoeff; ++i) { // loop through the terms
      // the multiindex
      const Eigen::RowVectorXu multiIndex = terms->IndexToMulti(i);

      // the point
      const Eigen::VectorXd point = xs.col(pt);

      // each term is a product of the 1D variables
      double result = 1.0;
      for (unsigned int v = 0; v < variables->length(); ++v) {
        result *= variables->GetVariable(v)->polyFamily->evaluate(multiIndex(v), point(v));
      }

      // insert each entry in the Vandermonde matrix
      vand(pt, i) = result;
    }
  }

  return vand;
}

void PolynomialRegressor::CubicWeightingOn()
{
  // can only do cubic weighting if the order is less than 3
  assert(order < 3);

  cubicWeights = true;
}

void PolynomialRegressor::CubicWeightingOff()
{
  cubicWeights = false;
}

void PolynomialRegressor::CrossValidationOn()
{
  crossValidate = true;
}

void PolynomialRegressor::CrossValidationOff()
{
  crossValidate = false;
}

void PolynomialRegressor::SetMaxCVSamples(unsigned int const cvSamps)
{
  maxCVSamps = cvSamps;
}

unsigned int PolynomialRegressor::RegressionOrder() const
{
  return order;
}

double PolynomialRegressor::PoisednessConstant(Eigen::MatrixXd const     & xs,
                                               Eigen::VectorXd const     & point,
                                               boost::property_tree::ptree para) const
{
  // dimension
  const unsigned int dim = point.size();

  // center the points
  Eigen::MatrixXd centered = xs.colwise() - point;

  // scaling, the farthest point
  const double scaling = centered.colwise().norm().maxCoeff();

  assert(scaling > 0); // it can't be the center itself

  // scale the centered points
  centered = centered.array() / scaling;

  // compute the vandermonde matrix at the points
  const Eigen::MatrixXd V = VandermondeMatrix(centered);

  // the weights are one
  Eigen::VectorXd K = Eigen::VectorXd::Ones(centered.cols());
  if (cubicWeights) { // unless we want cubic weights
    K = CubicWeights(centered);
  }

  // cost function for the optimization
  auto problem = make_shared<PoisednessCost>(terms, variables, V, K, dim);

  // the constraint for the optimization problem
  auto constraint = make_shared<BallConstraint>(dim);

  // add the constraint to the problem (it is inequality)
  problem->AddConstraint(constraint, false);

  // create a solver for the optimization
  auto solver = OptAlgBase::Create(problem, para);

  // solve the optimization and rescale
  Eigen::VectorXd maxPoint = solver->solve(Eigen::VectorXd::Ones(dim) / dim);
  assert(solver->GetStatus() > 0); // assert sucess

  // rescale and get the cost at the maximum
  return scaling * problem->eval(maxPoint) / currentRadius;
}

double PolynomialRegressor::NoisyError(Eigen::MatrixXd const& xs, Eigen::VectorXd const& point, double const var) const
{
  // if the variance is zero
  if (var == 0.0) {
    // return zero
    return 0.0;
  }

  // make sure the variance is positive
  assert(var > 0.0);

  // compute the vandermonde matrix at the points
  const Eigen::MatrixXd V = VandermondeMatrix(xs);

  // the weights are one
  Eigen::VectorXd K = Eigen::VectorXd::Ones(xs.cols());
  if (cubicWeights) { // unless we want cubic weights
    K = CubicWeights(xs);
  }

  // we have the matrix, we may need the cholesky factorization
  Eigen::LLT<Eigen::MatrixXd> CholSolver;
  CholSolver.compute(V.transpose() * K.asDiagonal() * V);
  Eigen::MatrixXd cholDecomp = CholSolver.matrixL();

  // solve the system
  Eigen::MatrixXd solved =
    cholDecomp.triangularView<Eigen::Lower>().solve(V.transpose() * (var * Eigen::VectorXd::Ones(V.rows())));
  cholDecomp.triangularView<Eigen::Lower>().transpose().solveInPlace(solved);

  // dot with the basis functions evaluated at point
  const Eigen::VectorXd noisyError = VandermondeMatrix(point) * solved;
  assert(noisyError.size() == 1);

  return abs(noisyError(0));
}

