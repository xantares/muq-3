#include "MUQ/Approximation/Expansions/PolynomialExpansion.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/export.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"

#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

#include "MUQ/Utilities/LogConfig.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Approximation;
using namespace muq::Modelling;

PolynomialExpansion::PolynomialExpansion(int outputDim,
                                         shared_ptr<MultiIndexSet> multiSet,
                                         shared_ptr<RecursivePolynomialFamily1D> poly) :
ExpansionBase(multiSet->GetMultiIndexLength(),outputDim,multiSet->GetNumberOfIndices()),
multis(multiSet),
polys(multiSet->GetMultiIndexLength(),poly){};


PolynomialExpansion::PolynomialExpansion(Eigen::MatrixXd const& coeffs,
                    std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                    std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D> poly) :
ExpansionBase(multiSet->GetMultiIndexLength(),coeffs),
multis(multiSet),
polys(multiSet->GetMultiIndexLength(),poly){};

PolynomialExpansion::PolynomialExpansion(Eigen::MatrixXd const& coeffs,
                                         std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                                         std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polysIn) :
ExpansionBase(multiSet->GetMultiIndexLength(),coeffs),
multis(multiSet),
polys(polysIn){assert(polysIn.size()==multiSet->GetMultiIndexLength());};


PolynomialExpansion::PolynomialExpansion(int outputDim,
                                         std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                                         std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polysIn) : 
                                         ExpansionBase(multiSet->GetMultiIndexLength(),outputDim,multiSet->GetNumberOfIndices()),
                                         multis(multiSet),
                                         polys(polysIn){};
                                         
                          
Eigen::MatrixXd PolynomialExpansion::EvaluateTerms(Eigen::MatrixXd const& xs) const{
  
  assert(xs.rows()==inputSizes(0));
  assert(xs.rows()==polys.size());
  
  Eigen::MatrixXd output = Eigen::MatrixXd::Ones(multis->GetNumberOfIndices(), xs.cols());
  
  // evaluate all polynomials up to the maximum order
  std::vector<Eigen::VectorXd> basisVals(xs.rows());
  Eigen::VectorXu maxOrders = multis->GetMaxOrders();
    
  // loop over all of the points
  for(int ptInd = 0; ptInd<xs.cols(); ++ptInd){
  
    for(int d=0; d<xs.rows(); ++d)
      basisVals.at(d) = polys.at(d)->evaluateAll(maxOrders(d),xs(d,ptInd));
  
    // compute each term in the expansion
    for(int term=0; term<multis->GetNumberOfIndices(); ++term){
  
      // compute the tensor product of 1d polynomials
      for(auto pairs : multis->IndexToMultiPtr(term)->nzInds)
        output(term,ptInd) *= basisVals.at(pairs.first)(pairs.second);
    }
  }
  return output;
  
};


Eigen::MatrixXd PolynomialExpansion::EvaluateTermDerivs(Eigen::MatrixXd const& xs, int dimWrt) const{
  
  Eigen::MatrixXd output = Eigen::MatrixXd::Ones(multis->GetNumberOfIndices(), xs.cols());
  
  // loop over all of the points
  for(int ptInd = 0; ptInd<xs.cols(); ++ptInd){
    
    // compute each term in the expansion
    for(int term=0; term<multis->GetNumberOfIndices(); ++term){
      
      output(term,ptInd) = polys[dimWrt]->gradient(multis->IndexToMultiPtr(term)->GetValue(dimWrt),xs(dimWrt,ptInd));
      
      // compute the tensor product of 1d polynomials
      if(abs(output(term,ptInd))>1e-14){
        for(auto pairs : multis->IndexToMultiPtr(term)->nzInds){
          if(pairs.first!=dimWrt)
            output(term,ptInd) *= polys[pairs.first]->evaluate(pairs.second,xs(pairs.first,ptInd));
        }
      }
    }
    
  }
  return output;
  
};


ostream& muq::Approximation::operator<<(ostream& output, const PolynomialExpansion& expansion)
{
  Eigen::MatrixXu terms = expansion.multis->GetAllMultiIndices();

  int prePrec = output.precision();

  output.precision(15);
  output.setf(ios::scientific);
  output.setf(ios::showpos);


  output << terms.rows() << endl;
  output << terms << endl;
  Eigen::MatrixXd coeffsTrans = expansion.coeffs.transpose();
  output << coeffsTrans << endl;

  // reset the stream format
  output.precision(prePrec);
  output.unsetf(ios::scientific);
  output.unsetf(ios::showpos);

  return output; // for multiple << operators.
}


template<class Archive>
void PolynomialExpansion::serialize(Archive & ar, const unsigned int version){
  ar & coeffs;
  ar & multis;
  ar & polys;
};

template void PolynomialExpansion::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void PolynomialExpansion::serialize(boost::archive::text_iarchive& ar, const unsigned int version);


// shared_ptr<PolynomialExpansion> PolynomialExpansion::LoadFromFile(std::string fileName)
// {
//   shared_ptr<PolynomialExpansion> expansion(new PolynomialExpansion(1, 1)); //feed in arguments that will get replaced
//   // create and open an archive for input
//   std::ifstream ifs(fileName.c_str());
// 
//   assert(ifs.good());
//   boost::archive::text_iarchive ia(ifs);
// 
//   // read class state from archive
//   ia >> expansion;
// 
//   return expansion;
// }

void PolynomialExpansion::SaveToFile(shared_ptr<PolynomialExpansion> expansion, std::string fileName)
{
  std::ofstream ofs(fileName.c_str());


  if (!ofs.good()) {
    LOG(ERROR) << "PolynomialExpansion::SaveToFile file name not valid. Falling back to \"PolySave.dat\"";
    std::ofstream ofs("PolySave.dat");
  }
  assert(ofs.good());


  // save data to archive
  {
    boost::archive::text_oarchive oa(ofs);

    // write class instance to archive
    oa << expansion;

    // archive and stream closed when destructors are called
  }
}

BOOST_CLASS_EXPORT(muq::Approximation::PolynomialExpansion)