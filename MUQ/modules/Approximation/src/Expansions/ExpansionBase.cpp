#include "MUQ/Approximation/Expansions/ExpansionBase.h"

using namespace std;
using namespace muq::Approximation;
using namespace muq::Modelling;

ExpansionBase::ExpansionBase(int inputDim, int outputDim, int numTerms) : OneInputJacobianModPiece(inputDim,outputDim), coeffs(Eigen::MatrixXd::Zero(outputDim,numTerms)){};

ExpansionBase::ExpansionBase(int inputDim, Eigen::MatrixXd const& coeffsIn) : OneInputJacobianModPiece(inputDim,coeffsIn.rows()), coeffs(coeffsIn){};

//Eigen::MatrixXd ExpansionBase::EvaluateMultiImpl(Eigen::MatrixXd const& xs){
//  
//  return coeffs*EvaluateTerms(xs);
//}

Eigen::VectorXd ExpansionBase::EvaluateImpl(Eigen::VectorXd const& x){
  
  return coeffs*EvaluateTerms(x);
}

Eigen::MatrixXd ExpansionBase::JacobianImpl(Eigen::VectorXd const& x){
  
  Eigen::MatrixXd jac(outputSize,inputSizes(0));
  for(int i=0; i<inputSizes(0); ++i)
    jac.col(i) = coeffs*EvaluateTermDerivs(x,i);
  
  return jac;
}