#include "MUQ/Approximation/Incremental/GPApproximator.h"

#include <boost/math/special_functions.hpp>

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

#include "MUQ/Approximation/Incremental/SpaceFillOptimization.h"
#include "MUQ/Approximation/BallConstraint.h"
#include "MUQ/Utilities/HDF5Logging.h"
#include "MUQ/Utilities/RandomGenerator.h"


using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Optimization;
using namespace muq::Approximation;

GPApproximator::GPApproximator(shared_ptr<ModPiece> const& fnIn, boost::property_tree::ptree para) : Approximator(fnIn,
                                                                                                                  para)
{
  // the approximated function must only have one input
  assert(fn->inputSizes.size() == 1);

  double dim = inputSizes(0); //convert dim to a double

  numNNSamples    = ReadAndLogParameter(para, "LocalGPApproximation.numNNSamples", dim * 2);
  numCandidates   = ReadAndLogParameter(para, "LocalGPApproximation.numCandidates", numCandidates);
  numRefineStages = ReadAndLogParameter(para, "LocalGPApproximation.numRefineStages", dim / 2);

  numCVSamples = ReadAndLogParameter(para, "LocalGPApproximation.numCVSamples", numCVSamples);

  // make the regressor
  regressor = make_shared<GaussianProcessRegressor>(fn->inputSizes(0), fn->outputSize, para);

  // number of nearest neighbors to use - default is the number for interpolation, padded by sqrt(d)
  kN = ReadAndLogParameter<int>(para, "Approximator.kN", static_cast<int>(pow(dim, 2.5)));

  // the number of nearest neighbors must at least interpolate
  if (kN < dim) {
    cerr << endl << endl << "ERROR: GP Regressor requires " << dim << " but only " << kN <<
      " nearest neighbors are used for local regression." << endl << endl;
    assert(kN >= dim);
  }
}

void GPApproximator::ComputeRegression(Eigen::VectorXd const& point)
{
  // don't recompute if the point is the current center and the cache is up-to-date
  if (usedCurrentCache && MatrixEqual(point, center)) {
    return;
  }

  // inputs and outputs of nearest neighbors
  Eigen::MatrixXd inputs;
  Eigen::MatrixXd outputs;

  // find the nearest neighbors
  NearestNeighbors(inputs, outputs, point);

  // fit the regressor
  regressor->Fit(inputs, outputs, point);

  // the regression used the most up-to-date cache
  usedCurrentCache = true;

  // set the center
  center = point;

  //need to limit the desired number of samples down to how many there are
  unsigned samplesToUse = min(static_cast<unsigned>(numNNSamples), fn->GetNumOfEvals());
  int nnSamplesUsed     = samplesToUse;
  //first, gather the samples
  std::list<std::shared_ptr<ModPieceMultiStepCache> > allSamples;


  allSamples = fn->FindKNearestNeighborsWithEvaluations(point, samplesToUse);

  MatrixXd samples(fn->inputSizes(0), samplesToUse);
  MatrixXd samplesEvaluated(fn->outputSize, samplesToUse);

  //grab the points and evaluations
  {
    int i = 0;

    for (auto aSample : allSamples) {
      samples.col(i)          = aSample->cachedInput.at(0);
      samplesEvaluated.col(i) = *(aSample->GetCachedEvaluate());
      ++i;
    }
  }

  //figure out how far the points are from the center
  VectorXd distances = (samples.colwise() - point).colwise().norm();


  std::sort(distances.data(), distances.data() + distances.size());
  double currentFitRadius = distances.maxCoeff(); //use all the points //CHANGE FILL ALSO

  //    LOG(INFO) << "initial NN " << samples;
  //compute the initial fit!
  regressor->Fit(samples, samplesEvaluated);

  //find all nearby candidates
  samplesToUse = min(static_cast<unsigned>(numCandidates), fn->GetNumOfEvals());
  allSamples   = fn->FindKNearestNeighborsWithEvaluations(point, samplesToUse);

  //a list to collect them into
  vector<VectorXd> candidates;


  for (auto aSample : allSamples) {
    VectorXd candidate = aSample->cachedInput.at(0);
    if ((candidate - point).norm() > currentFitRadius) { //collect the ones we haven't already used
      // LOG(INFO) << "candidate " << candidate.transpose();
      candidates.push_back(candidate);
    }
  }

  samplesToUse = min(static_cast<unsigned>(kN), fn->GetNumOfEvals());

  //now add more samples until we have them all
  for (int i = samples.cols(); i < samplesToUse; ++i) {
    VectorXd newSample = regressor->SelectNextPointRand(point, candidates);
    //          LOG(INFO) << "Found new sample " << newSample;
    samples.conservativeResize(samples.rows(), samples.cols() + 1);
    samples.col(samples.cols() - 1) = newSample;
    samplesEvaluated.conservativeResize(samplesEvaluated.rows(), samplesEvaluated.cols() + 1);
    assert(fn->IsEntryInCache(newSample));
    samplesEvaluated.col(samplesEvaluated.cols() - 1) = fn->Evaluate(newSample);
    if (((i - nnSamplesUsed) != 0) &&
        ((i - nnSamplesUsed) % ((samplesToUse - nnSamplesUsed) / numRefineStages) == 0)) { //don't repeat if we just did
                                                                                           // it!
      regressor->Fit(samples, samplesEvaluated);
    }
  }

  //the final fit
  regressor->Fit(samples, samplesEvaluated);

  //draw cv samples given the predicted variance
  cvSamples =
    (muq::Utilities::RandomGenerator::GetNormalRandomMatrix(outputSize,
                                                            numCVSamples).array() *
     regressor->GetPredictedVariance(point).replicate(1,
                                                      numCVSamples).array()).matrix() +
    regressor->Evaluate(point).replicate(1, numCVSamples);
}

Eigen::VectorXd GPApproximator::EvaluateImpl(Eigen::VectorXd const& input)
{
	ComputeRegression(input);

    //return the regression nomial evaluation
    return regressor->Evaluate(input);
}

Eigen::MatrixXd GPApproximator::JacobianImpl(Eigen::VectorXd const& input)
{
    // do the local regression
    ComputeRegression(input);

    //return the regression nomial jacobian
    return regressor->Jacobian(input);

}

bool GPApproximator::ResetNumNearestNeighbors(unsigned int const newKN)
{
    assert(newKN > 0);

  kN = newKN;

  return fn->GetNumOfEvals() >= kN;
}

unsigned int GPApproximator::NumNearestNeighbors() const
{
  return kN;
}

unsigned int GPApproximator::NumAvailableNearestNeighbors() const
{
  return fn->GetNumOfEvals();
}

double GPApproximator::LocalRadius(Eigen::VectorXd const& point) const
{
  // find the nearest neighbors
  Eigen::MatrixXd inputs;
  Eigen::MatrixXd outputs; // these aren't acutally necessary

  NearestNeighbors(inputs, outputs, point);

  return (inputs.colwise() - point).colwise().norm().maxCoeff();
}

int GPApproximator::GetNumberOfCrossValidationFits() const
{
  return cvSamples.cols();
}

void GPApproximatorWithModes::SetMode(Modes newMode)
{
  mode = newMode;
  InvalidateCache();
}

///Select which index is desired, and set to crossValidation mode.
void GPApproximatorWithModes::SetCrossValidationIndex(int const i)
{
  assert(i >= 0);
  assert(i < GetNumberOfCrossValidationFits());
  cvIndex = i;
  mode    = crossValidation;
  InvalidateCache();
}

Eigen::VectorXd GPApproximatorWithModes::EvaluateImpl(Eigen::VectorXd const& input)
{
  switch (mode) {
  case standard:
    // do the local regression
    ComputeRegression(input);

    //return the regression nomial evaluation
    return regressor->Evaluate(input);

    break;

  case crossValidation:
    // do the local regression
    ComputeRegression(input);
    //return the cv evaluation
    assert(cvIndex < GetNumberOfCrossValidationFits());
    return cvSamples.col(cvIndex);

    break;

  case trueDirect:
    //return the actual evaluation
    return fn->Evaluate(input);

    break;
  }

    assert(false); //shouldn't get here
  return Eigen::VectorXd();
}

Eigen::MatrixXd GPApproximatorWithModes::JacobianImpl(Eigen::VectorXd const& input)
{
  switch (mode) {
  case standard:
    // do the local regression
    ComputeRegression(input);

    //return the regression nomial jacobian
    return regressor->Jacobian(input);

    break;

  case crossValidation:
    // do the local regression

    assert(false && "no GP Jacobian cross validation implemented");

    break;

  case trueDirect:
    //return the actual Jacobian
    return fn->Jacobian(input);

    break;
  }

    assert(false); //shouldn't get here
  return Eigen::MatrixXd();
}