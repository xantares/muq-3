#include "MUQ/Approximation/pce/PolynomialChaosExpansion.h"

#include <fstream>
#include <iostream>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/functional/hash.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/tuple/tuple.hpp>

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/Utilities/VariableCollection.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

PolynomialChaosExpansion::PolynomialChaosExpansion(vector<std::shared_ptr<RecursivePolynomialFamily1D>> polysIn,
                                                   int                                             outputSize) : 
                          PolynomialExpansion(polysIn.size(),outputSize)
{
  polys = polysIn;
}
                           
PolynomialChaosExpansion::PolynomialChaosExpansion(VariableCollection::Ptr variables,
                                                   int                     outputSize) : PolynomialChaosExpansion(Variables2Polys(variables),outputSize){}

PolynomialChaosExpansion::PolynomialChaosExpansion(int const inputSize,
                                                   int const outputSize) : PolynomialExpansion(inputSize,
                                                                                               outputSize)
{}

PolynomialChaosExpansion::PolynomialChaosExpansion(Eigen::MatrixXd const& coeffs,
                                                   shared_ptr<MultiIndexSet> multiSet,
                                                   shared_ptr<RecursivePolynomialFamily1D> poly) : PolynomialExpansion(coeffs,multiSet,poly){};

PolynomialChaosExpansion::PolynomialChaosExpansion(Eigen::MatrixXd const& coeffs,
                                                   shared_ptr<MultiIndexSet> multiSet,
                                                   vector<shared_ptr<RecursivePolynomialFamily1D>> polys) : PolynomialExpansion(coeffs,multiSet,polys){};

PolynomialChaosExpansion::PolynomialChaosExpansion(int outputDim,
                                                   shared_ptr<MultiIndexSet> multiSet,
                                                   shared_ptr<RecursivePolynomialFamily1D> poly) : PolynomialExpansion(outputDim,multiSet,poly){};
      
PolynomialChaosExpansion::PolynomialChaosExpansion(int outputDim,
                                                   shared_ptr<MultiIndexSet> multiSet,
                                                   vector<shared_ptr<RecursivePolynomialFamily1D>> polys) : PolynomialExpansion(outputDim,multiSet,polys){};
                           
vector<shared_ptr<RecursivePolynomialFamily1D>> PolynomialChaosExpansion::Variables2Polys(shared_ptr<VariableCollection> variables)
{
  vector<shared_ptr<RecursivePolynomialFamily1D>> output(variables->length());
  for(int i=0; i<output.size(); ++i)
    output[i] = variables->GetVariable(i)->polyFamily;
  return output;
}
  

Eigen::VectorXd PolynomialChaosExpansion::GetNormalizationVec() const{
  
  VectorXd result = VectorXd::Zero(multis->GetNumberOfIndices());

  //compute each one
  for (unsigned int i = 0; i < multis->GetNumberOfIndices(); i++) {
    double norm = 1.0; //start the normalization at 1

    //loop over the dimensions and multiply the normalizations for each component polynomial
    Eigen::RowVectorXu fullMulti = multis->IndexToMulti(i);
    for(int ind=0; ind<fullMulti.size(); ++ind)
       norm *= polys[ind]->GetNormalization(fullMulti(ind));
    
    result(i) = sqrt(norm);
  }
  return result;
};

// ostream& muq::Approximation::operator<<(ostream& output, const PolynomialChaosExpansion& pce)
// {
//   MatrixXu pceTerms = pce.terms->GetAllMultiIndices();
// 
//   int prePrec = output.precision();
// 
//   output.precision(15);
//   output.setf(ios::scientific);
//   output.setf(ios::showpos);
// 
// 
//   output << pceTerms.rows() << endl;
//   output << pceTerms << endl;
//   MatrixXd coeffsTrans = pce.coeffs.transpose();
//   output << coeffsTrans << endl;
// 
//   // reset the stream format
//   output.precision(prePrec);
//   output.unsetf(ios::scientific);
//   output.unsetf(ios::showpos);
// 
//   return output; // for multiple << operators.
// }

ostream& muq::Approximation::operator<<(ostream& output, const PolynomialChaosExpansion::Ptr& pce)
{
  output << *dynamic_pointer_cast<PolynomialExpansion>(pce);

  return output; // for multiple << operators.
}

void PolynomialChaosExpansion::print(string basename)
{
  ofstream myfile((basename + "_results.dat").c_str());

  assert(myfile.good()); //file opening must work
  myfile << *this;
  myfile.close();
}

void PolynomialChaosExpansion::printNormalized(std::string basename)
{
  ofstream myfile((basename + "_results.dat").c_str());

  assert(myfile.is_open());

  myfile.precision(15);
  myfile.setf(ios::scientific);
  myfile.setf(ios::showpos);


  MatrixXu pceTerms = multis->GetAllMultiIndices();
  myfile << pceTerms.rows() << endl;
  myfile << pceTerms << endl;


  MatrixXd normCoeff = coeffs.cwiseProduct(GetNormalizationVec().transpose().replicate(outputSize, 1));
  myfile << normCoeff.transpose() << endl;
  myfile.close();
}

// bool PolynomialChaosExpansion::IsPolynomialInExpansion(RowVectorXu toFind)
// {
//   return terms->IsMultiInFamily(toFind);
// }

// PolynomialChaosExpansion::Ptr PolynomialChaosExpansion::SubtractExpansions(PolynomialChaosExpansion::Ptr const a,
//                                                                            PolynomialChaosExpansion::Ptr const b)
// {
//   assert(false);
// 
//   return ;
// }

VectorXd PolynomialChaosExpansion::ComputeVariance() const
{
  VectorXd normalVec = GetNormalizationVec();

  //if there's only the one constant term, the PCE has variance zero
  if (normalVec.rows() <= 1) {
    return VectorXd::Zero(coeffs.rows());
  }

  VectorXd squareNorm = normalVec.tail(normalVec.rows() - 1).array().square();

  //for each output, the variance is the dot product of the squared coeffs with the squared norms of the PCE terms.
  //Thus, grab all but the leftmost column.
  //Have to normalize by what the constant integrates to.

  VectorXd dimMeasures(inputSizes(0));
  for (unsigned int i = 0; i < inputSizes(0); ++i)
    dimMeasures(i) = polys[i]->GetNormalization(0);

  //Since the variance is an expectation, we must normalize if the polynomials aren't quite set up
  //to integrate to one.
  return coeffs.rightCols(coeffs.cols() - 1).array().square().matrix() * squareNorm / dimMeasures.prod();
}

MatrixXd PolynomialChaosExpansion::ComputeCovariance() const
{
  VectorXd normalVec = GetNormalizationVec();

  //if there's only the one constant term, the PCE has variance zero
  if (normalVec.rows() <= 1) {
    return MatrixXd::Zero(coeffs.rows(),coeffs.rows());
  }

  VectorXd squareNorm = normalVec.tail(normalVec.rows() - 1).array().square();

  //for each output, the variance is the dot product of the squared coeffs with the squared norms of the PCE terms.
  //Thus, grab all but the leftmost column.
  //Have to normalize by what the constant integrates to.

  VectorXd dimMeasures(inputSizes(0));
  for (unsigned int i = 0; i < inputSizes(0); ++i)
    dimMeasures(i) = polys[i]->GetNormalization(0);

  Eigen::VectorXd quadVec = squareNorm / dimMeasures.prod();

  // fill in upper portion of covariance matrix
  Eigen::MatrixXd cov(outputSize, outputSize);
  int npolys = coeffs.cols();
  for (int j = 0; j < outputSize; ++j) {   // column index
    for (int i = j; i < outputSize; ++i) { // row index
      cov(i, j) = coeffs.row(j).tail(npolys - 1) * quadVec.asDiagonal() * coeffs.row(i).tail(npolys - 1).transpose();
    }
  }

  // lower to upper triangular copy
  for (int j = 1; j < outputSize; ++j) { // column index
    for (int i = 0; i < j; ++i) {        // row index
      cov(i, j) = cov(j, i);
    }
  }

  return cov;
}

Eigen::VectorXd PolynomialChaosExpansion::ComputeMean() const
{
  return coeffs.col(0);
}

Eigen::MatrixXd PolynomialChaosExpansion::ComputeVarianceJacobian() const
{
  VectorXd normalVec = GetNormalizationVec();

  //if there's only the one constant term, the PCE has variance zero
  if (normalVec.rows() <= 1) {
    return MatrixXd::Zero(coeffs.rows(), coeffs.cols());
  }

  unsigned int npolys = coeffs.cols();

  VectorXd squareNorm = normalVec.tail(normalVec.rows() - 1).array().square();

  VectorXd dimMeasures(inputSizes(0));
  for (unsigned int i = 0; i < inputSizes(0); ++i)
    dimMeasures(i) = polys[i]->GetNormalization(0);

  Eigen::MatrixXd Jacobian = Eigen::MatrixXd::Zero(coeffs.rows(), npolys);

  Eigen::VectorXd scales = squareNorm / dimMeasures.prod();
  for (unsigned int i = 1; i < npolys; ++i) {
    Jacobian.col(i) = 2.0 * coeffs.col(i) * scales(i - 1);
  }

  //Since the variance is an expectation, we must normalize if the polynomials aren't quite set up
  //to integrate to one.
  return Jacobian;
}

Eigen::VectorXd PolynomialChaosExpansion::ComputeVarianceGradient(int outInd) const
{
  VectorXd normalVec = GetNormalizationVec();

  //if there's only the one constant term, the PCE has variance zero
  if (normalVec.rows() <= 1) {
    return VectorXd::Zero(1);
  }

  VectorXd squareNorm = normalVec.tail(normalVec.rows() - 1).array().square();

  VectorXd dimMeasures(inputSizes(0));
  for (unsigned int i = 0; i < inputSizes(0); ++i) {
    dimMeasures(i) = polys[i]->GetNormalization(0);
  }

  Eigen::VectorXd Gradient = Eigen::VectorXd::Zero(coeffs.cols());

  Eigen::VectorXd scales = squareNorm / dimMeasures.prod();
  for (unsigned int i = 1; i < coeffs.cols(); ++i) {
    Gradient(i) = coeffs(outInd, i) * scales(i - 1);
  }

  //Since the variance is an expectation, we must normalize if the polynomials aren't quite set up
  //to integrate to one.
  return Gradient;
}

Eigen::MatrixXd PolynomialChaosExpansion::ComputeVarianceHessian() const
{
  VectorXd normalVec = GetNormalizationVec();

  //if there's only the one constant term, the PCE has variance zero
  if (normalVec.rows() <= 1) {
    return MatrixXd::Zero(coeffs.cols(), coeffs.cols());
  }

  normalVec(0) = 0;
  VectorXd squareNorm = normalVec.array().square();

  VectorXd dimMeasures(inputSizes(0));
  for (unsigned int i = 0; i < inputSizes(0); ++i) {
    dimMeasures(i) = polys[i]->GetNormalization(0);
  }

  return ((squareNorm / dimMeasures.prod())).asDiagonal();
}

VectorXd PolynomialChaosExpansion::ComputeMagnitude() const
{
  VectorXd normalVec = GetNormalizationVec();

  //take the matrix product between the squared coeffs and squared norms, then sqrt each term
  return (coeffs.array().square().matrix() * normalVec.array().square().matrix()).array().sqrt();
}

// PolynomialChaosExpansion::Ptr PolynomialChaosExpansion::ComputeWeightedSum(
//   std::vector<PolynomialChaosExpansion::Ptr> expansions,
//   VectorXd const& weights,
//   VariableCollection::Ptr variables,
//   unsigned int const outputDim,
//   UnstructuredMultiIndexFamily::Ptr const& polynomials,
//   std::map<unsigned int, std::set<unsigned int> > const& basisMultiIndexCache)
// {
//   PolynomialChaosExpansion::Ptr sumPCE(new PolynomialChaosExpansion(variables, outputDim));
// 
//   //we already know exactly which terms in the pce we need, it was passed in
//   sumPCE->terms = polynomials;
// 
//   //allocate space for all the coeffs
//   sumPCE->coeffs = MatrixXd::Zero(outputDim, sumPCE->terms->GetNumberOfIndices());
// 
//   //find the non-zero weights
//   VectorXu nonZeroWeights = FindNonZeroInVector(weights);
// 
//   //loop over every polynomial in the result
//   for (unsigned int i = 0; i < sumPCE->terms->GetNumberOfIndices(); i++) {
//     //allocate space for the values of the individual terms
//     MatrixXd termCoeffs = MatrixXd::Zero(outputDim, weights.rows());
// 
//     //save the multi-index we're looking for
//     RowVectorXu multiToFind = sumPCE->terms->IndexToMulti(i);
// 
//     //Find the non-zero weighted polys that actually have term i
//     //Start with this sorting because it's usually very small
//     for (unsigned int j = 0; j < nonZeroWeights.rows(); ++j) {
//       //if this one has the current polynomial
//       PolynomialChaosExpansion::Ptr jthEst = expansions.at(nonZeroWeights(j));
// 
//       //do this by checking our cache whether this term is included, if the cache was provided
//       std::map<unsigned int, std::set<unsigned int> >::const_iterator it = basisMultiIndexCache.find(i);
//       if (it->second.find(nonZeroWeights(j)) != it->second.end()) {
//         //copy the coeffs into the results
//         unsigned int index = jthEst->terms->MultiToIndex(multiToFind);
//         termCoeffs.col(nonZeroWeights(j)) = jthEst->coeffs.col(index);
//       }
//     }
// 
//     //finish by summing the coeffs with the weights and store them in the result
//     sumPCE->coeffs.col(i) = (termCoeffs * weights);
//   }
// 
//   return sumPCE;
// }
// 
shared_ptr<PolynomialChaosExpansion> PolynomialChaosExpansion::ComputeWeightedSum(vector<std::shared_ptr<PolynomialChaosExpansion>>    expansions,
                                                                                  Eigen::VectorXd const&                          weights,
                                                                                  unsigned int const                              outputDim,
                                                                                  std::shared_ptr<MultiIndexSet> const&           polynomials,
                                                                                  map<unsigned int,set<unsigned int> > const&     basisMultiIndexCache)
{
  // assume all the expansions have the same type, and use that for the result
  std::shared_ptr<PolynomialChaosExpansion> sumPCE = make_shared<PolynomialChaosExpansion>(outputDim, polynomials, expansions[0]->polys);

  //find the non-zero weights
  VectorXu nonZeroWeights = FindNonZeroInVector(weights);

  //loop over every polynomial in the result
  for (unsigned int i = 0; i < sumPCE->multis->GetNumberOfIndices(); i++) {
    //allocate space for the values of the individual terms
    MatrixXd termCoeffs = MatrixXd::Zero(outputDim, weights.rows());

    //save the multi-index we're looking for
    auto multiToFind = sumPCE->multis->IndexToMultiPtr(i);
    
    //Find the non-zero weighted polys that actually have term i
    //Start with this sorting because it's usually very small
    for (unsigned int j = 0; j < nonZeroWeights.rows(); ++j) {
      //if this one has the current polynomial
      auto jthEst = expansions.at(nonZeroWeights(j));

      //do this by checking our cache whether this term is included, if the cache was provided
      std::map<unsigned int, std::set<unsigned int> >::const_iterator it = basisMultiIndexCache.find(i);

      if (it->second.find(nonZeroWeights(j)) != it->second.end()) {
        //copy the coeffs into the results
        unsigned int index = jthEst->multis->MultiToIndex(multiToFind);
        termCoeffs.col(nonZeroWeights(j)) = jthEst->coeffs.col(index);
      }
    }

    //finish by summing the coeffs with the weights and store them in the result
    sumPCE->coeffs.col(i) = (termCoeffs * weights);
  }
  
  return sumPCE;
}
                  
shared_ptr<PolynomialChaosExpansion> PolynomialChaosExpansion::ComputeWeightedSum(vector<std::shared_ptr<PolynomialChaosExpansion>> expansions,
                                                                                  Eigen::VectorXd                              const& weights)
{
  assert(weights.size()==expansions.size());

  // check the output size of each expansion
  int outputSize = expansions[0]->outputSize;
  int inputSize = expansions[0]->inputSizes(0);
  for(int i=1; i<expansions.size(); ++i){
    assert(outputSize==expansions[i]->outputSize);
    assert(inputSize==expansions[i]->inputSizes(0));
  }
  
  
  
  // the collection of all polynomials that will be in the result
  auto allPolynomials = make_shared<MultiIndexSet>(inputSize);
  
  //the cache of which expansions contain which polynomials
  std::map<unsigned int, std::set<unsigned int> > basisMultiIndexCache;
  for(int j=0; j<expansions.size(); ++j)
  {
    // if the weight is zero, skip this expansion
    if(abs(weights(j))>1e-13){
      
      //loop over all the polynomials in this expansion
      for (unsigned int i = 0; i < expansions.at(j)->multis->GetNumberOfIndices(); ++i) {
      
        //add the polynomial to the list of ones we need in the sum
        auto polyTerm = expansions.at(j)->multis->IndexToMultiPtr(i);
        allPolynomials->AddActive(polyTerm);
        
        //include this one in the cache of expansions that include the polynomial
        basisMultiIndexCache[allPolynomials->MultiToIndex(polyTerm)].insert(j);
      }
    }
  }
  return ComputeWeightedSum(expansions,weights,outputSize,allPolynomials,basisMultiIndexCache);
}

// unsigned int PolynomialChaosExpansion::NumberOfPolynomials() const
// {
//   return terms->GetNumberOfIndices();
// }

// bool evalPointEqStruct::operator()(boost::tuple<unsigned int, unsigned int> const& a,
//                                    boost::tuple<unsigned int, unsigned int> const& b) const
// {
//   return (a.get<0>() == b.get<0>()) && (a.get<1>() == b.get<1>());
// }

// namespace boost { namespace tuples {
//                   std::size_t hash_value(const boost::tuples::tuple<unsigned int, unsigned int>& b)
//                   {
//                     size_t seed = 0;
// 
//                     //combine the hashes
//                     boost::hash_combine(seed, b.get<0>());
//                     boost::hash_combine(seed, b.get<1>());
// 
//                     return seed;
//                   }
//                   } }
// 
// 
 std::shared_ptr<PolynomialChaosExpansion> PolynomialChaosExpansion::LoadFromFile(std::string fileName)
 {
   std::shared_ptr<PolynomialChaosExpansion> pce(new PolynomialChaosExpansion(1, 1)); //feed in arguments that will get replaced
   // create and open an archive for input
   std::ifstream ifs(fileName.c_str());
 
   assert(ifs.good());
   boost::archive::text_iarchive ia(ifs);
 
   // read class state from archive
   ia >> pce;
 
   return pce;
 }

 void PolynomialChaosExpansion::SaveToFile(std::shared_ptr<PolynomialChaosExpansion> pce, std::string fileName)
 {
   std::ofstream ofs(fileName.c_str());
 
   if (!ofs.good()) {
     LOG(ERROR) << "PolynomialChaosExpansion::SaveToFile file name not valid. Falling back to \"PceSave.dat\"";
     std::ofstream ofs("PceSave.dat");
   }
   assert(ofs.good());
 
 
   // save data to archive
   {
     boost::archive::text_oarchive oa(ofs);
 
     // write class instance to archive
     oa << pce;
 
     // archive and stream closed when destructors are called
   }
 }

VectorXd PolynomialChaosExpansion::ComputeSobolTotalSensitivityIndex(unsigned const targetDim) const
{
  //we're going to fill this index with either 1 or 0, depending on whether the term includes the target dimension
  ArrayXd contributesToIndex = VectorXd::Zero(GetNumTerms());

  Array<unsigned, 1, Dynamic> deltaDim = RowVectorXu::Ones(inputSizes(0));
  deltaDim(targetDim) = 0;

  //contributes as long as targetDim is involved
  for (unsigned int i = 0; i < multis->GetNumberOfIndices(); ++i)
    contributesToIndex(i) =  static_cast<double>((multis->IndexToMultiPtr(i)->GetValue(targetDim) != 0));

  //grab the total normalizations
  VectorXd normalVec = GetNormalizationVec();

  //filter the normalizations so that the ones that don't involve targetDim are zero
  VectorXd filterdVec = normalVec.array() * contributesToIndex;

  VectorXd dimMeasures(inputSizes(0));
  for (unsigned int i = 0; i < inputSizes(0); ++i)
    dimMeasures(i) = polys[i]->GetNormalization(0);

  //we want the sum of normalized involved coeffs divided by the normalized sum of all of them.
  //Don't forget the constant normalization the variance requires
  return (coeffs.array().square().matrix() *
          filterdVec.array().square().matrix()).array() / dimMeasures.prod() / ComputeVariance().array();
}

MatrixXd PolynomialChaosExpansion::ComputeAllSobolTotalSensitivityIndices() const
{
  MatrixXd result(coeffs.rows(), inputSizes(0));

  for (unsigned int i = 0; i < inputSizes(0); ++i) {
    result.col(i) = ComputeSobolTotalSensitivityIndex(i);
  }

  return result;
}

VectorXd PolynomialChaosExpansion::ComputeMainSensitivityIndex(unsigned const targetDim) const
{
  //we're going to fill this index with either 1 or 0, depending on whether the term includes the target dimension
  ArrayXd contributesToIndex = VectorXd::Zero(GetNumTerms());

  //create a row vector with a one at only the place we're interested in
  Array<unsigned, 1, Dynamic> deltaDim = RowVectorXu::Ones(inputSizes(0));
  deltaDim(targetDim) = 0;

  //each one contributes iff it is the only non-zero one
  for (unsigned int i = 0; i < multis->GetNumberOfIndices(); ++i) {
    RowVectorXu oneIndex = multis->IndexToMulti(i);
    contributesToIndex(i) =
      static_cast<double>((oneIndex(targetDim) != 0) && ((oneIndex.array() * deltaDim).matrix().sum() == 0));

    //     cout << "one " << oneIndex << " A " << (oneIndex(targetDim) != 0) << " B " << ((oneIndex.array() *
    // deltaDim).matrix().sum() == 0) << endl;
  }


  //grab the total normalizations
  VectorXd normalVec = GetNormalizationVec();

  //filter the normalizations so that the ones that don't involve targetDim are zero
  VectorXd filterdVec = normalVec.array() * contributesToIndex;

  VectorXd dimMeasures(inputSizes(0));
  for (unsigned int i = 0; i < inputSizes(0); ++i) {
    dimMeasures(i) = polys[i]->GetNormalization(0);
  }


  //we want the sum of normalized involved coeffs divided by the normalized sum of all of them.
  //Don't forget the constant normalization the variance requires
  return (coeffs.array().square().matrix() *
          filterdVec.array().square().matrix()).array() / dimMeasures.prod() / ComputeVariance().array();
}

MatrixXd PolynomialChaosExpansion::ComputeAllMainSensitivityIndices() const
{
  //   cout << "norm vec " << GetNormalizationVec() << endl;
  //   cout << "coeffs " << coeffs.transpose() << endl;
  MatrixXd result(coeffs.rows(), inputSizes(0));

  for (unsigned int i = 0; i < inputSizes(0); ++i) {
    result.col(i) = ComputeMainSensitivityIndex(i);
  }

  return result;
}

template<class Archive>
void PolynomialChaosExpansion::serialize(Archive& ar, const unsigned int version){
  ar & boost::serialization::base_object<PolynomialExpansion>(*this);
};

template void PolynomialChaosExpansion::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void PolynomialChaosExpansion::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

BOOST_CLASS_EXPORT(muq::Approximation::PolynomialChaosExpansion)


