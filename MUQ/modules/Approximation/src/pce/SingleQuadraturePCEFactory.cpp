#include "MUQ/Approximation/pce/SingleQuadraturePCEFactory.h"

#include <Eigen/Core>

#include "MUQ/Utilities/LogConfig.h"
//#include "MUQ/Utilities/multiIndex/AdaptiveMultiIndexFamily.h"
//#include "MUQ/Utilities/multiIndex/MultiIndexFamily.h"
#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"


#include "MUQ/Utilities/Quadrature/FullTensorQuadrature.h"
#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"
#include "MUQ/Utilities/Variable.h"
#include "MUQ/Utilities/VariableCollection.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

SingleQuadraturePCEFactory::SingleQuadraturePCEFactory(shared_ptr<VariableCollection> variables) : variables(variables) {}

SingleQuadraturePCEFactory::~SingleQuadraturePCEFactory() {}

PolynomialChaosExpansion::Ptr SingleQuadraturePCEFactory::ConstructPCE(shared_ptr<ModPiece> fn,
                                                                       RowVectorXu const                       & order,
                                                                       RowVectorXu const                       & offset,
                                                                       shared_ptr<MultiIndexSet>                 tensorOrdersIncluded)
{
  //allocate a nearly empty multi-index family for the pce terms
  auto pceTerms = MultiIndexFactory::CreateFullTensor(order.cols(),0);
  
  shared_ptr<PolynomialChaosExpansion> result = make_shared<PolynomialChaosExpansion>(fn->outputSize,
                                                                                      pceTerms,
                                                                                      PolynomialChaosExpansion::Variables2Polys(variables));
  
  FullTensorQuadrature fullQuad(variables, order + offset);

  //Compute the quadrature nodes and weights
  MatrixXd nodes;
  VectorXd weights;

  fullQuad.GetNodesAndWeights(nodes, weights);

  
  //if there wasn't an input group of pce terms to include, default to ones that are integrable
  if (!tensorOrdersIncluded) {
    RowVectorXu maxPCEorder;
    maxPCEorder.resizeLike(order);
    for (unsigned int i = 0; i < order.cols(); i++) {
      //note that this uses integer division to round down, we want only ones that are
      //perfectly integrated
        maxPCEorder(i) = variables->GetVariable(i)->quadFamily->GetPrecisePolyOrder(order(i) + offset(i)) / 2;
    }
    //now create a multi-index family with the terms by forcibly adding the max term,
    //all predecessors will be added automatically
    pceTerms->ForciblyActivate(maxPCEorder);
  } else {
    //activate all the ones integrable under all the quadrature rules in the input set
    for (unsigned int i = 0; i < tensorOrdersIncluded->GetNumberOfIndices(); ++i) {
      RowVectorXu oneTensorRule = tensorOrdersIncluded->IndexToMulti(i) + offset; //note offset to tensor orders
      RowVectorXu maxPCEorder;
      maxPCEorder.resizeLike<>(order);
      for (unsigned int i = 0; i < order.cols(); i++) {
        //note that this uses integer division to round down, we want only ones that are
        //perfectly integrated
        maxPCEorder(i) = variables->GetVariable(i)->quadFamily->GetPrecisePolyOrder(oneTensorRule(i)) / 2;
      }

      //now create a multi-index family with the terms by forcibly adding the max term,
      //all predecessors will be added automatically
      pceTerms->ForciblyActivate(maxPCEorder);
    }
  }

  //evaluate the function at the nodes
  MatrixXd nodeEvals = fn->EvaluateMulti(nodes);
 
  //allocate space for results
  result->coeffs.resize(fn->outputSize, pceTerms->GetNumberOfIndices());

  //compute the coeffs for every basis term
  Eigen::MatrixXd polyEvals = result->EvaluateTerms(nodes);
  Eigen::VectorXd allNorms = result->GetNormalizationVec().array().square().matrix();
  
  //Compute the weighted sum, giving <f, term(i)>
  for (unsigned int i = 0; i < pceTerms->GetNumberOfIndices(); ++i)
    result->coeffs.col(i) = nodeEvals * polyEvals.row(i).asDiagonal() * weights / allNorms(i);

  //copy the terms into the pce
  result->multis = pceTerms;

  return result;
}

