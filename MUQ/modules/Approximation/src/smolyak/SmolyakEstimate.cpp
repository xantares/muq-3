
#include <boost/date_time/posix_time/posix_time.hpp>

#include "MUQ/Approximation/smolyak/SmolyakEstimate.h"


#include <assert.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include <boost/serialization/vector.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/export.hpp>
#include <boost/lexical_cast.hpp>

#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"
#include "MUQ/Utilities/multiIndex/MultiIndexLimiter.h"
#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Approximation/pce/PolynomialChaosExpansion.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Approximation;


template<typename T>
SmolyakEstimate<T>::SmolyakEstimate(unsigned int numDimensions, std::shared_ptr<MultiIndexLimiter> limiterIn) : limiter(limiterIn)
{
  SmolyakEstimate<T>::termsIncluded = MultiIndexFactory::CreateTotalOrder(numDimensions, 0, 0, limiter);
  SmolyakEstimate<T>::termsIncluded->SetLimiter(limiter);
  
  //reset the term weights
  termWeights.resize(0);
  
  //clear the vectors
  termEstimates.clear();
  differentialMagnitudes.clear();
}


template<typename T>
SmolyakEstimate<T>::SmolyakEstimate(unsigned int numDimensions, unsigned int simplexLimit)
{
  //always initialize with exactly the (0,...0) element
  if (simplexLimit == 0) {
    limiter = make_shared<NoLimiter>();
  } else {
    limiter = make_shared<TotalOrderLimiter>(simplexLimit-1);
  }
  
  SmolyakEstimate<T>::termsIncluded = MultiIndexFactory::CreateTotalOrder(numDimensions, 0, 0, limiter);
  SmolyakEstimate<T>::termsIncluded->SetLimiter(limiter);
  
  //reset the term weights
  termWeights.resize(0);

  //clear the vectors
  termEstimates.clear();
  differentialMagnitudes.clear();
}

template<typename T>
SmolyakEstimate<T>::SmolyakEstimate(unsigned int numDimensions, shared_ptr<MultiIndexSet> limitingSet) : limiter(make_shared<GeneralLimiter>(limitingSet))
{
  SmolyakEstimate<T>::termsIncluded = MultiIndexFactory::CreateTotalOrder(numDimensions, 0, 0, limiter);
  SmolyakEstimate<T>::termsIncluded->SetLimiter(limiter);
  
  //reset the term weights
  termWeights.resize(0);

  //clear the vectors
  termEstimates.clear();
  differentialMagnitudes.clear();
}

template<typename T>
SmolyakEstimate<T>::~SmolyakEstimate() {}

template<typename T>
T SmolyakEstimate<T>::StartAdaptiveTimed(shared_ptr<MultiIndexSet> initialTerms, double secondsLimit)
{
  time_t start, end;
  std::time(&start); //record the start time

  //compute all the initial terms
  ComputeInitialTerms(initialTerms);

  //time so far
  std::time(&end);

  return AdaptForTime(secondsLimit, std::difftime(end, start));
}

template<typename T>
T SmolyakEstimate<T>::AdaptForTime(double secondsLimit, double timeElapsed)
{
  using namespace boost::posix_time;

  ptime start =  microsec_clock::local_time();

  //    time_t start,end;
  //  time (&start); //record the start time

  //time so far
  //time (&end);
  ptime now          =  microsec_clock::local_time();
  time_duration diff = now - start;
  while (diff.total_microseconds() * 1e-6 + timeElapsed < secondsLimit) {
    VectorXu newTermIndices;
    if (!Refine(newTermIndices)) {
      break;
    }
    AddBatchOfTerms(newTermIndices);
    RecordAdaptProgress();

    //time so far
    now  =  microsec_clock::local_time();
    diff = now - start;
  }

  finalEstimate = PreciseWeightedSumOfEstimates(termWeights);

  //finally, return the global result


  return finalEstimate;
}

template<typename T>
T SmolyakEstimate<T>::StartAdaptiveToTolerance(shared_ptr<MultiIndexSet> initialTerms, double errorLimit)
{
  //compute all the initial terms
  ComputeInitialTerms(initialTerms);
  return AdaptToTolerance(errorLimit);
}

template<typename T>
T SmolyakEstimate<T>::AdaptToTolerance(double errorLimit)
{
  BOOST_LOG_POLY("smolyak", debug) << "Current indicator: " << ComputeGlobalErrorIndicator() << " tol: " << errorLimit;
  while (errorLimit < ComputeGlobalErrorIndicator()) {
  
    Eigen::VectorXu newTermIndices;
    if (!Refine(newTermIndices)) {
      break;
    }
    AddBatchOfTerms(newTermIndices);
    RecordAdaptProgress();
    BOOST_LOG_POLY("smolyak",debug) << "Current indicator: " << ComputeGlobalErrorIndicator() << " tol: " << errorLimit;
  }
  
    BOOST_LOG_POLY("smolyak", debug) << "Used terms: " << termsIncluded->GetAllMultiIndices();
    BOOST_LOG_POLY("smolyak", debug) << "Used weights: " << termWeights;

  //	for(unsigned int i=0; i<termEstimates.size(); i++)
  //	{
  //		BOOST_LOG_POLY("smolyak",debug) << "Estimate " << i << ": " << termEstimates[i];
  //	}

  finalEstimate = PreciseWeightedSumOfEstimates(termWeights);

  //finally, return the global result
  return finalEstimate;
}

template<typename T>
T SmolyakEstimate<T>::StartFixedTerms(shared_ptr<MultiIndexSet> fixedTerms)
{
  BOOST_LOG_POLY("smolyak", debug) << "Computing fixed with terms: " << fixedTerms->GetAllMultiIndices();

  //compute all the initial terms
  ComputeInitialTerms(fixedTerms);

  BOOST_LOG_POLY("smolyak", debug) << "Done computing terms.";

  finalEstimate = PreciseWeightedSumOfEstimates(termWeights);
  RecordAdaptProgress();

  //finally, return the global result
  return finalEstimate;
}

template<typename T>
T SmolyakEstimate<T>::AddFixedTerms(shared_ptr<MultiIndexSet> fixedTerms)
{
  BOOST_LOG_POLY("smolyak", debug) << "Adding fixed terms.";

  AddBatchOfTerms(fixedTerms);
  finalEstimate = PreciseWeightedSumOfEstimates(termWeights);
  RecordAdaptProgress();

  //finally, return the global result
  return finalEstimate;
}

template<typename T>
void SmolyakEstimate<T>::OutputVerbose(std::string baseName)
{
  std::string termsName = "results/" + baseName + "_terms.dat";
  MatrixXu    allTerms  = GetEffectiveIncludedTerms();

  std::ofstream allTermsFile(termsName.c_str());

  assert(allTermsFile.good());
  allTermsFile << allTerms;
  allTermsFile.close();

  //save the terms in the pce and the coefficients
  std::string resultName = "results/" + baseName + "_result.dat";

  std::ofstream myfile(resultName.c_str());
  assert(myfile.good());
  myfile << finalEstimate;
  myfile.close();
}

template<typename T>
bool SmolyakEstimate<T>::Refine(VectorXu& newTermIndices)
{
  double largestError        = -1.0;
  unsigned int indexToExpand = 0;
  bool termFound             = false;

  //search for the expandable term with the largest local error indicator
  for (unsigned int i = 0; i < termsIncluded->GetNumberOfIndices(); i++) {
    if (IsTermEligibleForExpansion(i)) { //only compute the error indicator if it's eligible.
      //        double ratio = ComputeLocalErrorIndicator(i)/ComputeLocalErrorIndicator(0);
      //        double work = CostOfExpandingTerm(i);
      //        double localError = std::max(ratio,0.5/work);
      double localError = ComputeLocalErrorIndicator(i);

      //        double localError = ComputeLocalErrorIndicator(i)/ CostOfExpandingTerm(i);
      //                double localError = ComputeLocalErrorIndicator(i) - .005* CostOfExpandingTerm(i);


      if (localError > largestError) {
        largestError  = localError;
        indexToExpand = i;
        termFound     = true;
      }
    }
  }

  if (!termFound) {
    BOOST_LOG_POLY("smolyak", debug) << "No more terms to add. Halt!";
    return false;
  }

    BOOST_LOG_POLY("smolyak",
                 debug) << "Refining: " << indexToExpand << " with error: " << largestError << " which is: " <<
    termsIncluded->IndexToMulti(indexToExpand);

  //Expand the largest one, and return all the indices added to the termsIncluded set
  //in the process.
  newTermIndices = termsIncluded->Expand(indexToExpand).transpose();
    BOOST_LOG_POLY("smolyak", debug) << "Refine selected: " << newTermIndices << "which are: ";
  for (unsigned int i = 0; i < newTermIndices.rows(); ++i) {
    BOOST_LOG_POLY("smolyak", debug) << termsIncluded->IndexToMulti(newTermIndices(i));
  }
  return true;
}

template<typename T>
double SmolyakEstimate<T>::CostOfExpandingTerm(unsigned const index)
{
  double cost = 0;

  //add in the cost of all the forward neighbors
  auto neighborsToActivate = termsIncluded->GetAdmissibleForwardNeighbors(index);

  for (int i = 0; i < neighborsToActivate.rows(); ++i) {
    cost += CostOfOneTerm(neighborsToActivate.row(i));
  }

  return cost;
}

template<typename T>
void SmolyakEstimate<T>::AddBatchOfTerms(VectorXu const& newTermIndices)
{
  BOOST_LOG_POLY("smolyak", debug) << "Expanding storage for new batch." << newTermIndices;
  ExpandStorage();
  BOOST_LOG_POLY("smolyak", debug) << "Done.";

  BOOST_LOG_POLY("smolyak", debug) << "Computing terms.";
  //Compute all the estimates first, because some of them may be needed
  //for the differentials computed next
  for (unsigned int i = 0; i < newTermIndices.rows(); i++) {
    termEstimates.at(newTermIndices(i)) = ComputeOneEstimate(termsIncluded->IndexToMulti(newTermIndices(i)));
  }
  
  BOOST_LOG_POLY("smolyak", debug) << "Done.";

  BOOST_LOG_POLY("smolyak", debug) << "Updating differentials and weights.";
  
  //now compute the weights and differentials
  for (unsigned int i = 0; i < newTermIndices.rows(); i++) {
    //compute the differential coefficients
    VectorXd differentialCoeff;
    ComputeDifferentialCoeff(newTermIndices(i), differentialCoeff);

    //update the weights
    termWeights = termWeights + differentialCoeff;

    differentialMagnitudes.at(newTermIndices(i)) = ComputeMagnitude(WeightedSumOfEstimates(differentialCoeff));
  }
  BOOST_LOG_POLY("smolyak", debug) << "Done updating differentials and weights.";

  //as a final step, compute the current estimate, using the less precise weighted sum function
  finalEstimate = WeightedSumOfEstimates(termWeights);
}

template<typename T>
void SmolyakEstimate<T>::AddBatchOfTerms(shared_ptr<MultiIndexSet> const multiIndicesToAdd)
{
  RowVectorXu termsAdded;

  BOOST_LOG_POLY("smolyak", debug) << "Forcibly adding terms: " << multiIndicesToAdd->GetAllMultiIndices();

  //first, forcibly add all the terms to figure out which ones we're adding
  for (unsigned int i = 0; i < multiIndicesToAdd->GetNumberOfIndices(); i++) {
    RowVectorXu toAdd = termsIncluded->ForciblyActivate(multiIndicesToAdd->IndexToMultiPtr(i));

    //expand and copy
    termsAdded.conservativeResize(termsAdded.cols() + toAdd.cols());
    termsAdded.tail(toAdd.cols()) = toAdd;
  }

  BOOST_LOG_POLY("smolyak", debug) << "Hence, adding indices:" << termsAdded;

  //call the regular method
  AddBatchOfTerms(termsAdded.transpose());
}

template<typename T>
void SmolyakEstimate<T>::ComputeInitialTerms(shared_ptr<MultiIndexSet> const initialTerms)
{
  //first, add all the input initial terms to termsIncluded.
  for (unsigned int i = 0; i < initialTerms->GetNumberOfIndices(); i++) {
    termsIncluded->ForciblyActivate(initialTerms->IndexToMultiPtr(i));
  }

  BOOST_LOG_POLY("smolyak", debug) << "Actually going to start with terms: " << termsIncluded->GetAllMultiIndices();

  //now add all of them as a batch
  Eigen::VectorXu newTermInds(initialTerms->GetNumberOfIndices());
  for(unsigned int i = 0; i<initialTerms->GetNumberOfIndices(); ++i)
    newTermInds[i] = i;
    
  AddBatchOfTerms(newTermInds);
  RecordAdaptProgress();
}

template<typename T>
void SmolyakEstimate<T>::ExpandStorage()
{
  //check that there aren't 0 elements total
  assert(termsIncluded->GetNumberOfIndices() > 0);

  //find out how many terms are included
  unsigned int newLength = termsIncluded->GetNumberOfIndices();

  //resize the vectors
  termEstimates.resize(newLength);
  differentialMagnitudes.resize(newLength);

  //resize the colvec
  if (termWeights.rows() == 0) {
    termWeights.setZero(newLength);
  } else if (termWeights.rows() < newLength) {}
  {
    unsigned int oldSize = termWeights.rows();
    termWeights.conservativeResize(newLength);
    termWeights.tail(newLength - oldSize) = VectorXd::Zero(newLength - oldSize);
  }
}

template<typename T>
void SmolyakEstimate<T>::ComputeDifferentialCoeff(unsigned int const index, VectorXd& coeff) const
{
  //allocate a vector we can multiply against the tensorRuleValues
  coeff = VectorXd::Zero(termsIncluded->GetNumberOfIndices());

  //the base multiIndex
  RowVectorXu baseMultiIndex = termsIncluded->IndexToMulti(index);


  for (unsigned int i = 0; i < termsIncluded->GetNumberOfIndices(); ++i) {
    RowVectorXi baseInt = baseMultiIndex.cast<int>();
    RowVectorXi iInt    = termsIncluded->IndexToMulti(i).template cast<int>();

    RowVectorXi neighborDifferential = baseInt - iInt;

    //if this isn't a direct backwards neighbor, skip it
    int diffMax = neighborDifferential.maxCoeff();
    int diffMin = neighborDifferential.minCoeff();
    if ((diffMax > 1) || (diffMin < 0)) {
      continue;
    }

    //else, compute the difference
    //compute how many -1 terms the neighbor got from subtracting the neighbor difference
    //note that this could potentially overflow, converting from unsigned to signed
    int parity = neighborDifferential.array().sum();

    //take this neighbor and add (-1)^parity, computed with ternary operator
    coeff(i) = coeff(i) + (parity % 2 == 0 ? 1 : -1);
  }
}

template<typename T>
T SmolyakEstimate<T>::StartFixedTerms(unsigned int initialSimplexLevels)
{
  assert(initialSimplexLevels > 0); //0 doesn't work because of the -1 below.
  return StartFixedTerms(MultiIndexFactory::CreateTotalOrder(termsIncluded->GetMultiIndexLength(), initialSimplexLevels - 1,0,limiter));
}

template<typename T>
T SmolyakEstimate<T>::AddFixedTerms(unsigned int initialSimplexLevels)
{
  assert(initialSimplexLevels > 0); //0 doesn't work because of the -1 below.
  return AddFixedTerms(MultiIndexFactory::CreateTotalOrder(termsIncluded->GetMultiIndexLength(), initialSimplexLevels - 1,0,limiter));
}

template<typename T>
T SmolyakEstimate<T>::StartAdaptiveTimed(unsigned int initialSimplexLevels, double secondsLimit)
{
  //just call through with the simplex
  return StartAdaptiveTimed(MultiIndexFactory::CreateTotalOrder(termsIncluded->GetMultiIndexLength(), initialSimplexLevels - 1,0,limiter), secondsLimit);
}

template<typename T>
T SmolyakEstimate<T>::StartAdaptiveToTolerance(unsigned int initialSimplexLevels, double errorLimit)
{
  //just call through with the simplex
  return StartAdaptiveToTolerance(MultiIndexFactory::CreateTotalOrder(termsIncluded->GetMultiIndexLength(), initialSimplexLevels - 1,0,limiter), errorLimit);
}

template<typename T>
T SmolyakEstimate<T>::PreciseWeightedSumOfEstimates(VectorXd const& weights) const
{
  return WeightedSumOfEstimates(weights);
}

template<typename T>
double SmolyakEstimate<T>::ComputeGlobalErrorIndicator()
{
  BOOST_LOG_POLY("smolyak", debug) << "Computing global error indicator.";
  double estimate = 0.0;
  for (unsigned int i = 0; i < termsIncluded->GetNumberOfIndices(); i++) {
    if (IsTermEligibleForExpansion(i)) {
      estimate += ComputeLocalErrorIndicator(i);
    }
  }

  BOOST_LOG_POLY("smolyak", debug) << "Done computing global error indicator.";
  return estimate;
}

template<typename T>
double SmolyakEstimate<T>::GetGlobalErrorIndicator()
{
  return ComputeGlobalErrorIndicator();
}

template<typename T>
double SmolyakEstimate<T>::ComputeLocalErrorIndicator(unsigned int const termIndex)
{
  return differentialMagnitudes.at(termIndex);
}

template<typename T>
bool SmolyakEstimate<T>::IsTermEligibleForExpansion(unsigned int const termIndex)
{
  return termsIncluded->IsExpandable(termIndex);
}

template<typename T>
SmolyakEstimate<T>::SmolyakEstimate() {}

template<typename T>
MatrixXu SmolyakEstimate<T>::GetEffectiveIncludedTerms()
{
  return termsIncluded->GetAllMultiIndices();
}

template<typename T>
void SmolyakEstimate<T>::RecordAdaptProgress()
{
  finalEstimate = PreciseWeightedSumOfEstimates(termWeights);

  estimatesTrace.push_back(finalEstimate);
  workDoneTrace.push_back(ComputeWorkDone());
}

template<typename T>
std::vector<T> *SmolyakEstimate<T>::GetEstimateTrace()
{
  return &estimatesTrace;
}

template<typename T>
std::vector<unsigned int> *SmolyakEstimate<T>::GetWorkTrace()
{
  return &workDoneTrace;
}

// template<typename T> template<class Archive>
// void SmolyakEstimate<T>::serialize(Archive & ar, const unsigned int version)
// {
//     ar & termsIncluded;
//     ar & termEstimates;
//     ar & differentialMagnitudes;
//     ar & termWeights;
//     //ar & differentialNeighborCache;
//     ar & finalEstimate;
//     ar & estimatesTrace;
//     ar & workDoneTrace;
// }
//
//
// ///force it to build the templates

#if MUQ_PYTHON == 1

template<typename T>
T SmolyakEstimate<T>::PyStartFixedTerms1(std::shared_ptr<muq::Utilities::MultiIndexSet> fixedTerms){
  return StartFixedTerms(fixedTerms);
}
template<typename T>
T SmolyakEstimate<T>::PyStartFixedTerms2(unsigned int initialSimplexLevels){
  return StartFixedTerms(initialSimplexLevels);
}
template<typename T>
T SmolyakEstimate<T>::PyAddFixedTerms1(std::shared_ptr<muq::Utilities::MultiIndexSet> fixedTerms){
  return AddFixedTerms(fixedTerms);
}
template<typename T>
T SmolyakEstimate<T>::PyAddFixedTerms2(unsigned int simplexLevelsToInclude){
  return AddFixedTerms(simplexLevelsToInclude);
}
template<typename T>
T SmolyakEstimate<T>::PyStartAdaptiveTimed1(std::shared_ptr<muq::Utilities::MultiIndexSet> initialTerms, double secondsLimit){
  return StartAdaptiveTimed(initialTerms,secondsLimit);
}
template<typename T>
T SmolyakEstimate<T>::PyStartAdaptiveTimed2(unsigned int initialSimplexLevels, double secondsLimit){
  return StartAdaptiveTimed(initialSimplexLevels,secondsLimit);
}
template<typename T>
T SmolyakEstimate<T>::PyStartAdaptiveToTolerance1(std::shared_ptr<muq::Utilities::MultiIndexSet> initialTerms, double errorLimit){
  return StartAdaptiveToTolerance(initialTerms,errorLimit);
}
template<typename T>
T SmolyakEstimate<T>::PyStartAdaptiveToTolerance2(unsigned int initialSimplexLevels, double errorLimit){
  return StartAdaptiveToTolerance(initialSimplexLevels,errorLimit);
}

#endif

namespace muq {
namespace Approximation {
template class SmolyakEstimate<Eigen::VectorXd>;
template class SmolyakEstimate<PolynomialChaosExpansion::Ptr>;
}
}


//
//
// BOOST_CLASS_EXPORT(SmolyakEstimate<Eigen::VectorXd>)
//
//
// template void SmolyakEstimate<VectorXd>::serialize(boost::archive::text_oarchive & ar,
//                                                     const unsigned int version);
//
// template void SmolyakEstimate<VectorXd>::serialize(boost::archive::text_iarchive & ar,
//                                                     const unsigned int version);
// //
// BOOST_CLASS_EXPORT(SmolyakEstimate<PolynomialChaosExpansion::Ptr>)
//
// template void SmolyakEstimate<PolynomialChaosExpansion::Ptr>::serialize(boost::archive::text_oarchive & ar,
//                                                                      const unsigned int version);
// template void SmolyakEstimate<PolynomialChaosExpansion::Ptr>::serialize(boost::archive::text_iarchive & ar,
//                                                                      const unsigned int version);
