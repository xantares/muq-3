#include "MUQ/Approximation/BallConstraint.h"

using namespace std;
using namespace muq::Optimization;
using namespace muq::Approximation;

BallConstraint::BallConstraint(unsigned int const dim) : ConstraintBase(dim, 1) {}

Eigen::VectorXd BallConstraint::eval(Eigen::VectorXd const& point)
{
  return (point.squaredNorm() - 1.0) * Eigen::VectorXd::Ones(1);
}

Eigen::VectorXd BallConstraint::ApplyJacTrans(Eigen::VectorXd const& point, Eigen::VectorXd const& target) 
{
  return 2.0 * point * target;
}
