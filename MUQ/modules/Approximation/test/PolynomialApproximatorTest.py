import unittest
import numpy as np # math stuff in python
import random

import libmuqUtilities
import libmuqModelling
import libmuqApproximation

# A model to approximate
class func(libmuqModelling.OneInputJacobianModPiece):
    def __init__(self):
        super(func, self).__init__(3, 2)

    def EvaluateImpl(self, ins):
        return [ins[0]*ins[0] + ins[1]*ins[2], ins[2]*ins[0] + ins[0]*ins[1]]

    def JacobianImpl(self, ins, inputDimWrt):
        return [[2.0 * ins[0], ins[2], ins[1]],
                [ins[2] + ins[1], ins[0], ins[0]]]


class PolynomialApproximatorTest(unittest.TestCase):
    def testBasicTest(self):
        # create the model we want to approximate 
        fn = func()

        para = dict()

        # options for polynomial regressor 
        para["PolynomialRegressor.Order"] = 2

        # options for the approximator
        para["PolynomialApproximator.kN"] = 15

        # create the approximator 
        approx = libmuqApproximation.PolynomialApproximator(fn, para)

        # data points for the model 
        suppliedPts = [None]*30 
        for i in range(30):
            suppliedPts[i] = [0.0]*3
            for j in range(3):
                suppliedPts[i][j] = 4.0 * np.pi * random.random() - 2.0 * np.pi

        # add the supplied points to the cache
        approx.AddToCache(suppliedPts)

        # chose where to evaluate the approximation
        center = [0.1, 0.2, 0.3]

        # evaluate the approximation 
        result = approx.Evaluate(center)
        
        # the truth 
        truth = fn.Evaluate(center)

        # should be exact because we are approximating a quadratic with quadratic polynomials
        for i in range(2):
            self.assertAlmostEqual(truth[i], result[i], 13)

        # evaluate the approximation jacobian
        jac = approx.Jacobian(center)
        
        # the truth 
        jacTrue = fn.Jacobian(center)

        # should be exact because we are approximating a quadratic with quadratic polynomials
        for i in range(2):
            for j in range(3):
                self.assertAlmostEqual(jacTrue[i][j], jac[i][j], 13)

