#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Modelling/ModPieceTemplates.h"

#include "MUQ/Approximation/Regression/PolynomialRegressor.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

class RegressorTest : public::testing::Test {
public:
   /// A matrix holding the input points.
  Eigen::MatrixXd inputPts;

  /// A matrix holding the output points.
  Eigen::MatrixXd outputPts;

  /// The linear part of the true forward model.
  Eigen::MatrixXd L;

  /// The locaiton to evaluate the approximation for testing.
  Eigen::VectorXd testIn;

  /// Store the true output at the test input. 
  Eigen::VectorXd trueOut;

  /// Store the true jacobian at the test input
  Eigen::MatrixXd trueJac;

  /// Define the tolerance allowed in the approximation at testIn.
  const double tol;

  /// Define a tolerance for reduced order regressions.
  const double redtol;

  RegressorTest() : 
    tol(1e-4), redtol(1e-1)
  {
    unsigned int Npts = 100;

    // create the linear portion
    L = Eigen::MatrixXd::Zero(2, 2);
    L <<0.5, 0.1, 0.2, 0.8;

    // resize the input and output points
	
	inputPts.resize(2,100);
    inputPts <<0.958868   , 0.806733   , 0.333761   , -0.672064   , 0.777898   , 0.299414   , 0.258959    , 0.40124   , -0.342446   , -0.851678   , -0.552687  , 0.0213719   , -0.439916   , 0.438537  , -0.0570331   , 0.888636   , -0.327298   , -0.130973   , -0.310114   , 0.666487   , 0.350952  , -0.0361284   , 0.424175   , 0.243646   , -0.172033   , 0.347873   , -0.305768   , 0.218212   , 0.461459   , 0.480877   , 0.841829   , 0.306261  , 0.0648819   , -0.479006    , 0.37225   , -0.777449   , 0.153381   , 0.333113   , 0.551534   , -0.340716   , 0.968726   , 0.654782   , -0.623598   , 0.917274   , 0.529743   , -0.757714   , -0.232336   , 0.886103   , 0.723834   , 0.587314   , -0.405423   , 0.819286 , -0.00371215   , -0.674487   , 0.729158  , -0.0726757 , -0.00804521   , -0.639158   , 0.455101   , 0.206218   , 0.676267   , -0.643585 , -0.00294904   , -0.723523   , -0.350386   , 0.816969   , 0.673656 , -0.00785116   , -0.211346   , 0.217766    , -0.69754   , -0.784303   , -0.272803   , -0.337228   , -0.145345   , 0.167141   , 0.317493  , -0.0251463   , 0.766073  , 0.0354294   , 0.115121   , 0.659878   , -0.511346    , 0.45872   , 0.969689   , 0.795121   , -0.178424   , 0.566564   , -0.412644    , 0.73107   , -0.901675   , 0.972934   , -0.578234   , 0.730362   , -0.800881   , -0.396474   , 0.618191   , -0.896983  , -0.0845688   , 0.384153
  , 0.487622   , 0.967191 , -0.00548296   , 0.660024   , -0.846011   , -0.503912   , -0.541726   , -0.366266   , -0.537144   , 0.266144   , 0.302264   , 0.942931  , 0.0922138   , -0.773439    , 0.18508  , -0.0981649   , 0.695369   , -0.993537   , 0.196963   , -0.532217  , -0.0340994   , -0.390089   , -0.634888   , -0.918271   , 0.391968    , 0.27528   , -0.630755   , 0.254316   , -0.343251   , -0.595574   , 0.369513   , -0.485469   , -0.824713   , 0.754768    , -0.81252   , -0.276798   , 0.186423   , -0.422444   , -0.423241   , -0.620498   , -0.992843   , -0.337042   , -0.127006   , 0.837861   , 0.398151   , 0.371572   , 0.548547   , 0.832546   , -0.592904  , 0.0960841   , 0.809865   , 0.747958   , 0.152399   , -0.452178  , -0.0152024   , 0.697884   , -0.417893   , 0.368357   , -0.721884  , -0.0151566   , 0.448504   , -0.556069   , -0.757482   , -0.279115   , 0.863791   , 0.244191   , 0.636255   , -0.330057   , 0.317662   , -0.482188    , -0.85491   , 0.294415   , -0.423461   , -0.817703   , 0.868989   , -0.469077   , 0.523556   , -0.685456   , 0.251331   , -0.584313   , -0.147601   , -0.211223   , -0.347973   , 0.277308   , -0.323514   , -0.727851   , -0.989183   , 0.548772   , -0.770664   , 0.442012    , -0.10179   , 0.415818   , -0.052212   , -0.812161   , -0.234208    , 0.31424   , -0.736596   , -0.893155   , 0.561737    , -0.11488;
    outputPts = Eigen::MatrixXd::Zero(2, Npts);

    // generate the points
    for (unsigned int i = 0; i < Npts; ++i) {
      Eigen::VectorXd temp = L * inputPts.col(i);
      outputPts(0, i) = exp(temp(0));
      outputPts(1, i) = cos(temp(1));
    }

    // set the test input location
    testIn  = Eigen::VectorXd::Zero(2);
    trueOut = Eigen::VectorXd::Zero(2);
    trueJac = Eigen::MatrixXd::Zero(2, 2);

    // compute the true output
    Eigen::VectorXd temp = L * testIn;
    trueOut(0) = exp(temp(0));
    trueOut(1) = cos(temp(1));

    // compute the true jacobian
    trueJac(0, 0) = trueOut(0) * L(0, 0);
    trueJac(0, 1) = trueOut(0) * L(0, 1);
    trueJac(1, 0) = -1.0 * L(1, 0) * sin(temp(1));
    trueJac(1, 1) = -1.0 * L(1, 1) * sin(temp(1));
  }
};

TEST_F(RegressorTest, LegendrePoly) 
{
  boost::property_tree::ptree pt;

  pt.put("PolynomialRegressor.Order", 5);
  pt.put("PolynomialRegressor.Basis", "Legendre");
 
  // create the regressor 
  auto reg = make_shared<PolynomialRegressor>(2, 2, pt);

  // fit the global polynomial 
  reg->Fit(inputPts, outputPts);  

  // evaluate the regression
  const Eigen::VectorXd result = reg->Evaluate(testIn);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueOut, result, tol);

  // evaluate the jacobian
  const Eigen::MatrixXd jac = reg->Jacobian(testIn);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueJac, jac, tol);
}

TEST_F(RegressorTest, HermitePoly) 
{
  boost::property_tree::ptree pt;

  pt.put("PolynomialRegressor.Order", 5);
  pt.put("PolynomialRegressor.Basis", "Hermite");
 
  // create the regressor 
  auto reg = make_shared<PolynomialRegressor>(2, 2, pt);

  // fit the global polynomial 
  reg->Fit(inputPts, outputPts);  

  // evaluate the regression
  const Eigen::VectorXd result = reg->Evaluate(testIn);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueOut, result, tol);

  // evaluate the jacobian
  const Eigen::MatrixXd jac = reg->Jacobian(testIn);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueJac, jac, tol);
}

Eigen::VectorXd testLinearFn(Eigen::VectorXd input)
{
  Eigen::VectorXd result = Eigen::VectorXd::Zero(3);

  result(0) = 1.5;
  result(1) = 0.6 * input(0);
  result(2) = -0.2 + 2.0 * input(0) -0.3 * input(1);
  return result;
}

TEST(LowOrderRegressorTest, Linear) 
{
  auto fn = make_shared<WrapVectorFunctionModPiece>(testLinearFn, 2, 3);

  boost::property_tree::ptree pt;
  
  pt.put("PolynomialRegressor.Order", 1);
  pt.put("PolynomialRegressor.CubicWeights", false);
  pt.put("PolynomialRegressor.CrossValidate", false);
  pt.put("PolynomialRegressor.MaxCVSamples", 10);

  auto reg = make_shared<PolynomialRegressor>(2, 3, pt);

  Eigen::MatrixXd xs(2, 6);
  xs << 0.5268, 1.4282, 0.2282, -0.3005, -1.0143, 0.0701, 
        0.4655, -0.4213, 0.0696, -1.7138, 1.0023,   -0.9914;
  Eigen::MatrixXd ys(3, 6);
  
  for (int i = 0; i < xs.cols(); ++i) {
    ys.col(i) = fn->Evaluate(xs.col(i));
  }

  // the center to do the fitting 
  const Eigen::Vector2d center(0.3, 0.1);

  // fit the data with center
  reg->Fit(xs, ys, center);

  // a test point 
  const Eigen::Vector2d testPoint(0.1, 0.2);

  // evaluate the regression
  const Eigen::VectorXd result = reg->Evaluate(testPoint);
  
  // compute the truth
  const Eigen::VectorXd truth = fn->Evaluate(testPoint);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truth, result, 1e-14);

  // compute the jacobian
  const Eigen::MatrixXd jac = reg->Jacobian(testPoint);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, jac, fn->Jacobian(testPoint), 1e-12);

  // turn on cross validation (can also do this at construct time)
  reg->CrossValidationOn();

  // refit with cross validation
  reg->Fit(xs, ys, center);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truth.replicate(1, 6), reg->AllCrossValidationEvaluate(testPoint), 1e-12);
}

Eigen::VectorXd testPolynomialFn(Eigen::VectorXd input)
{
  Eigen::VectorXd result = Eigen::VectorXd::Zero(3);

  result(0) = 1.5;
  result(1) = input(0);
  result(2) = -1.0 + input(0) * input(1) - input(1) * input(1);

  return result;
}

TEST(LowOrderRegressorTest, Quadratic) 
{
  auto fn = make_shared<WrapVectorFunctionModPiece>(testPolynomialFn, 2, 3);

  boost::property_tree::ptree pt;
  
  pt.put("PolynomialRegressor.Order", 2);
  pt.put("PolynomialRegressor.CubicWeights", false);
  pt.put("PolynomialRegressor.CrossValidate", false);
  pt.put("PolynomialRegressor.MaxCVSamples", 10);

  auto reg = make_shared<PolynomialRegressor>(2, 3, pt);

  Eigen::MatrixXd xs(2, 10);
  xs << 0.5268, 1.4282,  0.2282, -0.3005, -1.0143, 0.0701, -1.0109, -0.3321, -0.4165, -0.0473,
        0.4655,  -0.4213, 0.0696, -1.7138, 1.0023, -0.9914, -0.2807, 0.1699, 0.8949,   -0.8010;

  Eigen::MatrixXd ys(3, 10);
  for (int i = 0; i < xs.cols(); ++i) {
    ys.col(i) = fn->Evaluate(xs.col(i));
  }

  // the center to do the fitting 
  const Eigen::Vector2d center(0.3, 0.1);

  // fit the data with center
  reg->Fit(xs, ys, center);

  // a test point 
  const Eigen::Vector2d testPoint(0.1, 0.2);

  // evaluate the regression
  const Eigen::VectorXd result = reg->Evaluate(testPoint);
  
  // compute the truth
  const Eigen::VectorXd truth = fn->Evaluate(testPoint);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truth, result, 1e-14);

  // compute the jacobian
  const Eigen::MatrixXd jac = reg->Jacobian(testPoint);

  // had to lower tolerances because fn->Jac is using finite difference
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, jac, fn->Jacobian(testPoint), 1e-4);

  // turn on cross validation (can also do this at construct time)
  reg->CrossValidationOn();

  // refit with cross validation
  reg->Fit(xs, ys, center);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truth.replicate(1, 10), reg->AllCrossValidationEvaluate(testPoint), 1e-12);
}
