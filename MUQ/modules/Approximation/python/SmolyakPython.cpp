#include "MUQ/Approximation/python/SmolyakPython.h"

#include "MUQ/Approximation/smolyak/SmolyakEstimate.h"
#include "MUQ/Approximation/smolyak/SmolyakPCEFactory.h"
#include "MUQ/Approximation/smolyak/SmolyakQuadrature.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

#include <boost/python/dict.hpp>

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

PolynomialChaosExpansion::Ptr SmolyakPCEFactory::PyComputeFixedPolynomials(boost::python::list const& polys){
  return ComputeFixedPolynomials(GetEigenMatrix<unsigned>(polys));
}
boost::python::list SmolyakPCEFactory::PyGetEffectiveIncludedTerms(){
  return GetPythonMatrix<unsigned>(GetEffectiveIncludedTerms());
}

shared_ptr<SmolyakPCEFactory> CreateFactory0(std::shared_ptr<muq::Utilities::VariableCollection>       inVariables,
                                             std::shared_ptr<muq::Modelling::ModPiece> inFn){
  return make_shared<SmolyakPCEFactory>(inVariables,inFn);
}

shared_ptr<SmolyakPCEFactory> CreateFactory1(std::shared_ptr<muq::Utilities::VariableCollection>       inVariables,
                                             std::shared_ptr<muq::Modelling::ModPiece> inFn,
                                             unsigned int                              simplexLimit){
  return make_shared<SmolyakPCEFactory>(inVariables,inFn,simplexLimit);
}

shared_ptr<SmolyakPCEFactory> CreateFactory2(std::shared_ptr<muq::Utilities::VariableCollection> inVariables,
                                             std::shared_ptr<muq::Modelling::ModPiece>           inFn,
                                             std::shared_ptr<muq::Utilities::MultiIndexSet>      limitingSet){
  return make_shared<SmolyakPCEFactory>(inVariables,inFn,limitingSet);
}


shared_ptr<SmolyakPCEFactory> CreateFactory3(boost::python::dict const& paramsIn,
                                             std::shared_ptr<muq::Modelling::ModPiece> inFn){
  return make_shared<SmolyakPCEFactory>(PythonDictToPtree(paramsIn),inFn);
}

shared_ptr<SmolyakQuadrature> CreateQuadrature1(VariableCollection::Ptr inVariables,
                                                shared_ptr<ModPiece>    inFnWrapper){
  return make_shared<SmolyakQuadrature>(inVariables,inFnWrapper);
}

shared_ptr<SmolyakQuadrature> CreateQuadrature2(boost::python::dict const& paramsIn,
                                                std::shared_ptr<muq::Modelling::ModPiece> inFn){
  return make_shared<SmolyakQuadrature>(PythonDictToPtree(paramsIn),inFn);
}

template<typename T>
void ExportSmolyakEstimate(){
  py::class_<SmolyakEstimate<T>, shared_ptr<SmolyakEstimate<T>>, boost::noncopyable> exportSmo("SmolyakPCEFactory", py::no_init);
  
  exportSmo.def("StartFixedTerms", &SmolyakEstimate<T>::PyStartFixedTerms1);
  exportSmo.def("StartFixedTerms", &SmolyakEstimate<T>::PyStartFixedTerms2);
  exportSmo.def("AddFixedTerms", &SmolyakEstimate<T>::PyAddFixedTerms1);
  exportSmo.def("AddFixedTerms", &SmolyakEstimate<T>::PyAddFixedTerms2);
  exportSmo.def("StartAdaptiveTimed", &SmolyakEstimate<T>::PyStartAdaptiveTimed1);
  exportSmo.def("StartAdaptiveTimed", &SmolyakEstimate<T>::PyStartAdaptiveTimed2);
  exportSmo.def("StartAdaptiveToTolerance", &SmolyakEstimate<T>::PyStartAdaptiveToTolerance1);
  exportSmo.def("StartAdaptiveToTolerance", &SmolyakEstimate<T>::PyStartAdaptiveToTolerance2);
  exportSmo.def("OutputVerbose", &SmolyakEstimate<T>::OutputVerbose);
  exportSmo.def("AdaptForTime", &SmolyakEstimate<T>::AdaptForTime);
  exportSmo.def("AdaptToTolerance", &SmolyakEstimate<T>::AdaptToTolerance);
  exportSmo.def("GetGlobalErrorIndicator", &SmolyakEstimate<T>::GetGlobalErrorIndicator);
  exportSmo.def("ComputeWorkDone", &SmolyakEstimate<T>::ComputeWorkDone);
}
void muq::Approximation::ExportSmolyakPCEFactory(){
  
  
  ExportSmolyakEstimate<shared_ptr<PolynomialChaosExpansion>>();
  
  py::class_<SmolyakPCEFactory, shared_ptr<SmolyakPCEFactory>,
  py::bases<SmolyakEstimate<shared_ptr<PolynomialChaosExpansion>>>, boost::noncopyable> exportPce("SmolyakPCEFactory", py::no_init);
  exportPce.def("__init__",py::make_constructor(CreateFactory0));
  exportPce.def("__init__",py::make_constructor(CreateFactory1));
  exportPce.def("__init__",py::make_constructor(CreateFactory2));
  exportPce.def("__init__",py::make_constructor(CreateFactory3));
  
  exportPce.def("ComputeFixedPolynomials",&SmolyakPCEFactory::PyComputeFixedPolynomials);
  exportPce.def("GetEffectiveIncludedTerms",&SmolyakPCEFactory::PyGetEffectiveIncludedTerms);
  
  py::implicitly_convertible<shared_ptr<SmolyakPCEFactory>, shared_ptr<SmolyakEstimate<shared_ptr<PolynomialChaosExpansion>>> >();
}

void muq::Approximation::ExportSmolyakQuadrature(){
  
  
  ExportSmolyakEstimate<shared_ptr<Eigen::VectorXd>>();
  
  py::class_<SmolyakQuadrature, shared_ptr<SmolyakQuadrature>,
  py::bases<SmolyakEstimate<Eigen::VectorXd>>, boost::noncopyable> exportQuad("SmolyakQuadrature", py::no_init);
  
  exportQuad.def("__init__",py::make_constructor(CreateQuadrature1));
  exportQuad.def("__init__",py::make_constructor(CreateQuadrature2));
  
  py::implicitly_convertible<shared_ptr<SmolyakQuadrature>, shared_ptr<SmolyakEstimate<Eigen::VectorXd>> >();
}

