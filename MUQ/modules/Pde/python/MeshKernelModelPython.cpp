
#include "MUQ/Pde/python/MeshKernelModelPython.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Geostatistics;
using namespace muq::Pde;

MeshKernelModelPython::MeshKernelModelPython(string const                              & paraName,
                                             shared_ptr<libMesh::EquationSystems> const& parasystem,
                                             vector<Eigen::VectorXd> const             & pts,
                                             unsigned int const                          numBasis,
                                             shared_ptr<CovKernel> const               & kernel) :
  MeshKernelModel(paraName, parasystem, pts, numBasis, kernel) {}

MeshKernelModelPython::MeshKernelModelPython(string const                              & paraName,
                                             shared_ptr<libMesh::EquationSystems> const& parasystem,
                                             vector<Eigen::VectorXd> const             & pts,
                                             unsigned int const                          numBasis,
                                             Eigen::VectorXd const                     & mean,
                                             shared_ptr<CovKernel> const               & kernel) :
  MeshKernelModel(paraName, parasystem, pts, numBasis, mean, kernel) {}

shared_ptr<MeshKernelModelPython> MeshKernelModelPython::ConstructNonzeroMean(
  shared_ptr<muq::Pde::GenericEquationSystems> const& parasystem,
  shared_ptr<muq::Geostatistics::CovKernel> const   & kernel,
  boost::python::list const                         & mean,
  boost::python::dict const                         & dict)
{
  boost::property_tree::ptree param = PythonDictToPtree(dict);

  const string paraName       = param.get<string>("MeshKernelModel.Name", "");
  const unsigned int order    = param.get<unsigned int>("MeshKernelModel.Order", 1);
  const unsigned int numBasis = param.get<unsigned int>("MeshKernelModel.KLModes", 10);

  const bool hasSys = parasystem->GetEquationSystemsPtr()->has_system(paraName);

  if (!hasSys) {
    libMesh::System& system = parasystem->GetEquationSystemsPtr()->add_system<libMesh::System>(paraName);

    if (order == 0) {
      system.add_variable(paraName, libMesh::CONSTANT, libMesh::MONOMIAL);
    } else {
      system.add_variable(paraName, libMesh::Order(order));
    }

    system.init();

    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }

    return make_shared<MeshKernelModelPython>(paraName,
                                              parasystem->GetEquationSystemsPtr(), pts, numBasis,
                                              GetEigenVector<Eigen::VectorXd>(mean), kernel);
  } else {
    libMesh::System& system       = parasystem->GetEquationSystemsPtr()->get_system<libMesh::System>(paraName);
    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info
        // for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }
    return make_shared<MeshKernelModelPython>(paraName, parasystem->GetEquationSystemsPtr(), pts, numBasis,
                                              GetEigenVector<Eigen::VectorXd>(mean), kernel);
  }
}

shared_ptr<MeshKernelModelPython> MeshKernelModelPython::ConstructZeroMean(
  shared_ptr<GenericEquationSystems> const& parasystem,
  shared_ptr<CovKernel> const             & kernel,
  boost::python::dict const               & dict)
{
  boost::property_tree::ptree param = PythonDictToPtree(dict);

  const string paraName       = param.get<string>("MeshKernelModel.Name", "");
  const unsigned int order    = param.get<unsigned int>("MeshKernelModel.Order", 1);
  const unsigned int numBasis = param.get<unsigned int>("MeshKernelModel.KLModes", 10);

  const bool hasSys = parasystem->GetEquationSystemsPtr()->has_system(paraName);

  if (!hasSys) {
    libMesh::System& system = parasystem->GetEquationSystemsPtr()->add_system<libMesh::System>(paraName);

    if (order == 0) {
      system.add_variable(paraName, libMesh::CONSTANT, libMesh::MONOMIAL);
    } else {
      system.add_variable(paraName, libMesh::Order(order));
    }

    system.init();

    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }

    return make_shared<MeshKernelModelPython>(paraName, parasystem->GetEquationSystemsPtr(), pts, numBasis, kernel);
  } else {
    libMesh::System& system       = parasystem->GetEquationSystemsPtr()->get_system<libMesh::System>(paraName);
    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info
        // for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }
    return make_shared<MeshKernelModelPython>(paraName, parasystem->GetEquationSystemsPtr(), pts, numBasis, kernel);
  }
}

void muq::Pde::ExportMeshKernelModel()
{
  boost::python::class_<MeshKernelModelPython,
                        shared_ptr<MeshKernelModelPython>,
                        boost::noncopyable,
                        boost::python::bases<KernelModel> >
  exportMeshKernel("MeshKernelModel",
                   boost
                   ::python::no_init);

  exportMeshKernel.def("__init__",
                       boost::python::make_constructor(&MeshKernelModelPython::ConstructZeroMean));
  exportMeshKernel.def("__init__",
                       boost::python::make_constructor(&MeshKernelModelPython::ConstructNonzeroMean));

  boost::python::implicitly_convertible<shared_ptr<MeshKernelModelPython>,
                                        shared_ptr<muq::Modelling::ModPiece> >();
  boost::python::implicitly_convertible<shared_ptr<MeshKernelModelPython>,
                                        shared_ptr<MeshKernelModel> >();
}

