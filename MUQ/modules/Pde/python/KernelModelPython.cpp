#include "MUQ/Pde/python/KernelModelPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Pde;
using namespace muq::Geostatistics;
using namespace muq::Utilities;

KernelModelPython::KernelModelPython(boost::python::list const  & pts,
                                     unsigned int                 numBasis,
                                     shared_ptr<CovKernel> const& kernel) :
  KernelModel(PythonListToVector(pts), numBasis, kernel)
{}

KernelModelPython::KernelModelPython(boost::python::list const  & pts,
                                     unsigned int                 numBasis,
                                     boost::python::list const  & mean,
                                     shared_ptr<CovKernel> const& kernel) :
  KernelModel(PythonListToVector(pts), numBasis, GetEigenVector<Eigen::VectorXd>(mean), kernel)
{}

boost::python::list KernelModelPython::PyGetCovariance() const
{
  return GetPythonMatrix(GetCovariance());
}

