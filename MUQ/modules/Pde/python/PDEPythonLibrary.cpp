#include "MUQ/Pde/python/GenericEquationSystemsPython.h"
#include "MUQ/Pde/python/MeshParameterPython.h"
#include "MUQ/Pde/python/KernelModelPython.h"
#include "MUQ/Pde/python/MeshKernelModelPython.h"
#include "MUQ/Pde/python/PointSensorPython.h"
#include "MUQ/Pde/python/LibMeshInitPython.h"
#include "MUQ/Pde/python/PDEModPiecePython.h"

using namespace std;
using namespace muq::Pde;

BOOST_PYTHON_MODULE(libmuqPde)
{
  ExportLibMeshInit();

  ExportGenericEquationSystems();
  ExportMeshParameter();
  ExportKernelModel();
  ExportMeshKernelModel();

  ExportPointSensor();

  ExportPDE();
}
