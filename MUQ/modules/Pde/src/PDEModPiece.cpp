#include "MUQ/Pde/PDEModPiece.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Pde;

shared_ptr<PDEModPiece<muq::Modelling::SolverModPiece> > muq::Pde::ConstructPDE(
  Eigen::VectorXi const                   & inputSizes,
  shared_ptr<GenericEquationSystems> const& system,
  boost::property_tree::ptree             & param)
{
  auto pde     = PDE::Create(inputSizes, system, param);
  auto monitor = make_shared<IdentityObserver>(pde->inputSizes, 0);

  return make_shared<PDEModPiece<muq::Modelling::SolverModPiece> >(pde, monitor, param);
}

shared_ptr<PDEModPiece<muq::Modelling::SolverModPiece> > muq::Pde::ConstructPDE(
  Eigen::VectorXi const                   & inputSizes,
  vector<Eigen::VectorXd> const           & pnts,
  shared_ptr<GenericEquationSystems> const& system,
  boost::property_tree::ptree             & param)
{
  auto pde     = PDE::Create(inputSizes, system, param);
  auto monitor = make_shared<PointObserver>(pde->inputSizes, pnts, 0, param.get<string>("PDE.Name"), system);

  assert(pde->GetName().compare(monitor->GetName()) == 0);

  return make_shared<PDEModPiece<muq::Modelling::SolverModPiece> >(pde, monitor, param);
}

shared_ptr<PDEModPiece<muq::Modelling::OdeModPiece> > muq::Pde::ConstructPDE(
  Eigen::VectorXi const                   & inputSizes,
  Eigen::VectorXd const                   & obsTimes,
  shared_ptr<GenericEquationSystems> const& system,
  boost::property_tree::ptree             & param)
{
  auto pde     = PDE::Create(inputSizes, system, param);
  auto monitor = make_shared<IdentityObserver>(pde->inputSizes, 1);

  return make_shared<PDEModPiece<muq::Modelling::OdeModPiece> >(pde, monitor, obsTimes, param);
}

shared_ptr<PDEModPiece<muq::Modelling::OdeModPiece> > muq::Pde::ConstructPDE(
  Eigen::VectorXi const                   & inputSizes,
  vector<Eigen::VectorXd> const           & pnts,
  Eigen::VectorXd const                   & obsTimes,
  shared_ptr<GenericEquationSystems> const& system,
  boost::property_tree::ptree             & param)
{
  auto pde     = PDE::Create(inputSizes, system, param);
  auto monitor = make_shared<PointObserver>(pde->inputSizes, pnts, 1, param.get<string>("PDE.Name"), system);

  assert(pde->GetName().compare(monitor->GetName()) == 0);

  return make_shared<PDEModPiece<muq::Modelling::OdeModPiece> >(pde, monitor, obsTimes, param);
}

