#include <boost/property_tree/ptree.hpp> // this needs to be here to avoid a weird "toupper" bug on osx

#include "MUQ/Pde/LibMeshInit.h"

#ifdef MUQ_MPI
#include "mpi.h"
#endif

using namespace std;
using namespace muq::Pde;

// initalize as null 
shared_ptr<libMesh::LibMeshInit> LibMeshInit::libmeshInit = nullptr;

LibMeshInit::LibMeshInit(int argc, char **argv)
{
  // start libmesh
  if( !libmeshInit ) {
#ifdef MUQ_MPI
    libmeshInit = make_shared<libMesh::LibMeshInit>(argc, argv, MPI_COMM_SELF);
#else 
     libmeshInit = make_shared<libMesh::LibMeshInit>(argc, argv);
#endif
	  
}
}

LibMeshInit::~LibMeshInit() 
{
  // delete object to prevent memory leaks
  libmeshInit.reset();
}
