
#include "MUQ/Pde/PointObserver.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Pde;

PointObserver::PointObserver(Eigen::VectorXi const& inputSizes, vector<Eigen::VectorXd> const& pnts,
                             unsigned int const stateIndex, string const& sysName,
                             shared_ptr<GenericEquationSystems> const eqnsystem) :
  ModPiece(inputSizes, pnts.size() * eqnsystem->GetEquationSystemsPtr()->get_system(sysName).n_vars(), false, false,
           false, false, false, sysName), stateIndex(stateIndex), system(eqnsystem->GetEquationSystemsPtr())
{
    assert(system->has_system(sysName));
    assert(system->get_system(sysName).solution->size() == inputSizes(stateIndex));

  points.resize(pnts.size());
  for (unsigned int i = 0; i < pnts.size(); ++i) {
    assert(pnts[i].size() == system->get_mesh().mesh_dimension());

    libMesh::Point pt;
    for (unsigned int j = 0; j < pnts[i].size(); ++j) {
      pt(j) = pnts[i](j);
    }
    points[i] = pt;
  }
}

Eigen::VectorXd PointObserver::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  libMesh::System& sys = system->get_system(GetName());

  Translater<Eigen::VectorXd, vector<double> > trans(inputs[stateIndex]);
  sys.solution->operator=(*trans.GetPtr());
  sys.update();

  vector<unsigned int> varNums(sys.n_vars());
  sys.get_all_variable_numbers(varNums);

  Eigen::VectorXd sensed(outputSize);
  for (unsigned int i = 0; i < points.size(); ++i) {
    for (unsigned int v = 0; v < sys.n_vars(); ++v) {
      sensed(i * sys.n_vars() + v) = sys.point_value(varNums[v], points[i]);
    }
  }

  return sensed;
}

