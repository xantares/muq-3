#include <iostream>

#include "MUQ/config.h"

#ifdef MUQ_MPI
# include <boost/mpi/environment.hpp>
#endif // ifdef MUQ_MPI

// include the google testing header
#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp> // this is needed to avoid a weird toupper bug on osx

#if HAVE_LIBMESH == 1
// declare libmesh for initialization
#include "MUQ/Pde/LibMeshInit.h"
#endif // ifdef HAVE_LIBMESH == 1

#include "MUQ/Utilities/LogConfig.h"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  muq::Utilities::InitializeLogging(argc, argv);

#if HAVE_LIBMESH == 1
  muq::Pde::LibMeshInit initLibMesh(argc, argv);
#endif // ifdef HAVE_LIBMESH == 1

#ifdef MUQ_MPI
  boost::mpi::environment env(argc, argv);
#endif // ifdef MUQ_MPI


  return RUN_ALL_TESTS();
}

