function samps = ReadSamples(filename)
%
% USAGE: samps = ReadSamples(filename)
%
% This function provides a way to read samples written by the MUQ
% EmpiricalRandVar into matlab.
%
% INPUT:
%   filename : the name of the binary file written by MUQ
%
% OUTPUT:
%   samps : a matrix containing the samples, each row contains one sample
%

% open the file
fid = fopen(filename);

% read in the number of samples
NumSamps = fread(fid,1,'int');

% read in the sample dimension
dim = fread(fid,1,'int');

% read in the samples
samps = fread(fid,[dim NumSamps],'double')';
fclose(fid);
