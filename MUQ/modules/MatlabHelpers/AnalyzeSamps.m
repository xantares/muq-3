% script to analyze the samples
FileName = 'FineSamples.dat';

% read the samples from a raw binary file
samps = ReadSamples('FineSamples.dat');

Nsamps = size(samps,1);
dim = size(samps,2);

% compute the effective sample size for each dimension
Ess = zeros(1,dim);
for d = 1:dim
    [~,~,~,tauint] = UWerr_fft(samps(:,d));
    Ess(d) = Nsamps./(2.*tauint);
end
fprintf('\nmin: %2.0f\nmed: %2.0f\nmax: %2.0f\n\n', min(Ess), median(Ess), max(Ess));