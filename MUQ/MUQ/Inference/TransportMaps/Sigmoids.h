#ifndef Sigmoids_h_
#define Sigmoids_h_

#include "MUQ/Inference/TransportMaps/UnivariateBasis.h"

namespace muq {
  namespace Inference{
    
    class SigmoidBase : public UnivariateBasis{
    public:
      
      SigmoidBase(int dimIn, double centerIn) : UnivariateBasis(dimIn), center(centerIn){};
      virtual ~SigmoidBase() = default;
      
      virtual double Evaluate(const double x) const = 0;
      virtual double Derivative(const double x) const = 0;
      virtual double SecondDerivative(const double x) const = 0;
      
    protected:
      double center;
      
    };
    
    
    class Sigmoid1 : public SigmoidBase{
    public:
      
      Sigmoid1(int dimIn, double centerIn, double alpha) : SigmoidBase(dimIn,centerIn), a(alpha){};
      virtual ~Sigmoid1() = default;
      
      std::shared_ptr<UnivariateBasis> Clone() const override{return std::make_shared<Sigmoid1>(dim,center,a);};
      
      double Evaluate(const double x) const override;
      double Derivative(const double x) const override;
      double SecondDerivative(const double x) const override;
      
    private:
      double a;
    };
    
    class Sigmoid2 : public SigmoidBase{
    public:
      
      Sigmoid2(int dimIn, double centerIn, double alpha) : SigmoidBase(dimIn,centerIn), a(alpha){};
      virtual ~Sigmoid2() = default;
      
      std::shared_ptr<UnivariateBasis> Clone() const override{return std::make_shared<Sigmoid2>(dim,center,a);};
      
      virtual double Evaluate(const double x) const override;
      virtual double Derivative(const double x) const override;
      virtual double SecondDerivative(const double x) const override;
      
    private:
      double a;
    };
    
    class Sigmoid3 : public SigmoidBase{
    public:
      
      Sigmoid3(int dimIn, double centerIn, double alpha) : SigmoidBase(dimIn,centerIn), a(alpha){};
      virtual ~Sigmoid3() = default;
      
      std::shared_ptr<UnivariateBasis> Clone() const override{return std::make_shared<Sigmoid3>(dim,center,a);};
      
      virtual double Evaluate(const double x) const override;
      virtual double Derivative(const double x) const override;
      virtual double SecondDerivative(const double x) const override;
      
    private:
      double a;
    };
    
  }// namespace Inference
}// namespace muq

#endif