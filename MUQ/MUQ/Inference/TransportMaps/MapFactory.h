
#ifndef MapFactory_h_
#define MapFactory_h_

// std library includes
#include <memory>
#include <vector>

// Eigen includes
#include <Eigen/Core>
#include <Eigen/Dense>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other MUQ includes
#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Inference/TransportMaps/TransportMap.h"
#include "MUQ/Inference/TransportMaps/BasisSet.h"
#include "MUQ/Modelling/TriangularLinearModel.h"

namespace muq {
  namespace Inference {
    class MapFactory {
      
    public:
      
      static std::shared_ptr<TransportMap> BuildToNormal(Eigen::MatrixXd const&                             samps,
                                                         std::vector<std::shared_ptr<muq::Utilities::MultiIndexSet>> const& multis,
                                                         std::vector<Eigen::VectorXi>                       optLevelsIn = std::vector<Eigen::VectorXi>(),
                                                         std::vector<Eigen::VectorXd>                       coeffGuess  = std::vector<Eigen::VectorXd>(),
                                                         boost::property_tree::ptree                        options     = boost::property_tree::ptree());
      
      
      static std::shared_ptr<TransportMap> BuildToNormal(Eigen::MatrixXd const&       samps,
                                                         std::vector<BasisSet> const& bases,
                                                         std::vector<Eigen::VectorXi> optLevelsIn = std::vector<Eigen::VectorXi>(),
                                                         std::vector<Eigen::VectorXd> coeffGuess  = std::vector<Eigen::VectorXd>(),
                                                         boost::property_tree::ptree  options     = boost::property_tree::ptree());
      
      
      
      //std::shared_ptr<TransportMap> AdaptSampsToStdSerial( const Eigen::MatrixXd      & samps,
      //                                                     const std::vector<BasisSet>& bases,
      //                                                     std::vector<Eigen::VectorXi> optLevelsIn,
      //                                                     std::vector<Eigen::VectorXd> coeffGuess  = std::vector<Eigen::VectorXd>(),
      //                                                     boost::property_tree::ptree  options     = boost::property_tree::ptree());
      
      
      static std::shared_ptr<TransportMap> RegressSampsToSamps(Eigen::MatrixXd const&                             sampsIn,
                                                               Eigen::MatrixXd const&                             sampsOut,
                                                               std::vector<std::shared_ptr<muq::Utilities::MultiIndexSet>> const& multis,
                                                               boost::property_tree::ptree                        options = boost::property_tree::ptree());
      
      static std::shared_ptr<TransportMap> CreateIdentity(std::vector<BasisSet> const& bases);
      
      static std::shared_ptr<TransportMap> CreateIdentity(std::vector<std::shared_ptr<muq::Utilities::MultiIndexSet>> const& multis,
                                                          boost::property_tree::ptree                        options = boost::property_tree::ptree());
      
      static std::vector<int> GetTriangularOrder(const Eigen::MatrixXd & samps);
      
    private:
      static std::shared_ptr<TransportMap> CreateZeroMap(std::vector<std::shared_ptr<muq::Utilities::MultiIndexSet>> const& multis,
                                                         boost::property_tree::ptree                        options = boost::property_tree::ptree());
      
      static std::shared_ptr<TransportMap> CreateZeroMap(std::vector<BasisSet> const& bases);
      
      static Eigen::VectorXd GetIdentityCoeff(std::shared_ptr<muq::Utilities::MultiIndexSet> const& multis, int dim, double linearScale=1.0);
      static void            ConcatMatrices(Eigen::MatrixXd& allMats, const Eigen::MatrixXd& localMat, bool toRootOnly = false);
      
    }; // class MapFactory
  } // namespace Inference
} // namesapce muq

#endif // ifndef MapFactory_h_
