#ifndef SAMPLINGPROBLEM_H
#define SAMPLINGPROBLEM_H

#include <memory>

#include "MUQ/Modelling/Density.h"
#include <MUQ/Modelling/ModGraph.h>
#include <MUQ/Utilities/HDF5Logging.h>
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"

namespace muq {
namespace Inference {
class MCMCState;
class PreMALA;

class AbstractSamplingProblem {
public:

  friend class PreMALA;


  AbstractSamplingProblem(int const samplingDim, std::shared_ptr<muq::Modelling::ModGraph> posteriorGraph,
                          std::shared_ptr<muq::Inference::AbstractMetric> metricObject);

  virtual ~AbstractSamplingProblem() {}

  /**
   * This method might return a null pointer - this means that the prior or likelihood was not finite, and proposal
   *therefore cannot be used.
   **/

  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) = 0;

  ///MCMC methods should set this to specify which information the MCMCState should contain
  virtual void                       SetStateComputations(bool const likelihoodGrad,
                                                          bool const priorGrad,
                                                          bool const metric);

  virtual void CopyStateComputations(
    std::shared_ptr<AbstractSamplingProblem> const otherSamplingProblem);

  bool         ComputesGrad()      const
  {
    return useLikelihoodGrad;
  }

  bool ComputesPriorGrad() const
  {
    return usePriorGrad;
  }

  bool ComputesMetric()    const
  {
    return useMetric;
  }

  const int samplingDim;

  ///Just calls the implementation method, SetGraphImpl.
  virtual std::shared_ptr<muq::Modelling::ModGraph>                GetGraph();
  virtual void                                    SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph);
  ///Force the caches to invalidate.
  virtual void                                    InvalidateCaches() = 0;

  std::shared_ptr<muq::Inference::AbstractMetric> GetMetric()
  {
    return metricObject;
  }

  void SetMetric(std::shared_ptr<muq::Inference::AbstractMetric> metricObjectIn)
  {
    metricObject = metricObjectIn;
  }

protected:

  ///The core element of the sampling problem
  std::shared_ptr<muq::Modelling::ModGraph> posteriorGraph;

  ///Set the graph. This is necessary so that the virtual function isn't called during construction.
  void SetGraphImpl(std::shared_ptr<muq::Modelling::ModGraph> graph);

  std::shared_ptr<muq::Inference::AbstractMetric> metricObject;


  bool useLikelihoodGrad = false;
  bool usePriorGrad      = false;
  bool useMetric         = false;
};


class Sampling2Opt;

class SamplingProblem : public AbstractSamplingProblem {
public:

  friend Sampling2Opt;
  friend class TransportMapMCMC;

  SamplingProblem(std::shared_ptr<muq::Modelling::Density>        density,
                  std::shared_ptr<muq::Inference::AbstractMetric> metricObject =
                    std::                                         make_shared<muq::Inference::EuclideanMetric>());

  SamplingProblem(std::shared_ptr<muq::Modelling::ModGraph>                      posteriorGraph,
                  std::shared_ptr<muq::Inference::AbstractMetric> metricObject =
                    std::                                         make_shared<muq::Inference::EuclideanMetric>());

  virtual ~SamplingProblem();

  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  virtual void                       InvalidateCaches();

  virtual void                       SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph) override;

protected:

  ///the density to sample
  std::shared_ptr<muq::Modelling::Density> density;

  ///Set the graph. This is necessary so that the virtual function isn't called during construction.
  void SetGraphImpl(std::shared_ptr<muq::Modelling::ModGraph> graph);

private:
};

class TemperedSamplingProblem : public AbstractSamplingProblem {
public:

  TemperedSamplingProblem(std::shared_ptr<AbstractSamplingProblem> baseProblem, double const temperature);
  virtual ~TemperedSamplingProblem();

  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  virtual void                       SetStateComputations(bool const likelihoodGrad,
                                                          bool const priorGrad,
                                                          bool const metric);
  virtual void                       CopyStateComputations(
    std::shared_ptr<AbstractSamplingProblem> const otherSamplingProblem);

  virtual void                       InvalidateCaches();

  virtual void                       SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph) override;

private:

  ///Set the graph. This is necessary so that the virtual function isn't called during construction.
  void SetGraphImpl(std::shared_ptr<muq::Modelling::ModGraph> graph);

  std::shared_ptr<AbstractSamplingProblem> baseProblem;
  double const temperature;
};
}
}

#endif // SAMPLINGPROBLEM_H
