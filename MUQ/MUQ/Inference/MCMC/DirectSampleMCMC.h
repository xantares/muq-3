
#ifndef DIRECTSAMPLEMCMC_H
#define DIRECTSAMPLEMCMC_H

#include "MUQ/Inference/MCMC/SingleChainMCMC.h"

namespace muq {
namespace Modelling {
class RandVar;
}

namespace Inference {
///This is sort of a strange class, as it just samples from a provided RV. Therefore, we do not register it as an MCMC.
// It's here for tempering with temp=0.
class DirectSampleMCMC : public SingleChainMCMC {
public:

  DirectSampleMCMC(std::shared_ptr<AbstractSamplingProblem> density,
                   boost::property_tree::ptree            & properties,
                   std::shared_ptr<muq::Modelling::RandVar> randVar);
  virtual ~DirectSampleMCMC();

private:

  virtual ProposalResult MakeProposal() override;


  std::shared_ptr<muq::Modelling::RandVar> randVar;
};
}
}

#endif // DIRECTSAMPLEMCMC_H
