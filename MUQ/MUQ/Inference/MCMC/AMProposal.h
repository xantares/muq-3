
#ifndef AMProposal_H_
#define AMProposal_H_

// standard library includes
#include <vector>

// other muq related includes
#include "MUQ/Inference/MCMC/MHProposal.h"


namespace muq {
namespace Inference {
/**\class AM
 *  \author Matthew Parno
 *  \brief Basic adaptive Metropolis MCMC implementation
 *  This class implements the original adaptive Metropolis algorithm proposed by Haario et al.  The proposal covariance
 *     is updated to approach the posterior covariance (or some scaled version of the posterior covariance) as the chain
 *     progresses.  While this breaks the Markov property of the chain, Haario proved that the resulting chain is
 * ergodic
 *     and converges has the posterior as a stationary distribution.  This implementation does not use the Stochastic
 *     Approximation interpretation of the Haario algorithm.
 */
class AM : public MHProposal {
public:

  /** Standard MCMC constructor. */
  AM(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
     boost::property_tree::ptree                            & properties);


  /** Destructor. */
  virtual ~AM();

  ///Update the proposal using the functions below
  virtual void PostProposalProcessing(std::shared_ptr<muq::Inference::MCMCState> const newState,
                                      MCMCProposal::ProposalStatus const               proposalAccepted,
                                      int const                                        iteration,
                                      std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry) override;

  /** Write important information about the settings in this proposal as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const override;

private:

  void updateSampleMeanAndCov(int const currIteration, std::shared_ptr<muq::Inference::MCMCState> const currentState);
  void updateProp();


  // symmetric matrix to hold chain covariance
  Eigen::MatrixXd sampleCov;

  // vector to hold chain mean
  Eigen::VectorXd sampleMean;

  int adaptSteps, adaptStart;
  double adaptScale;
};
} //namespace inference
} //namespace muq

#endif /* AMProposal_H_ */
