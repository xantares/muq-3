#ifndef GaussianIndependence_H
#define GaussianIndependence_H

#include "MUQ/Inference/MCMC/MCMCProposal.h"
#include "MUQ/Modelling/GaussianPair.h"

namespace muq {
namespace Inference {
class GaussianIndependence : public MCMCProposal {
public:

  ///Accept the inference problem and configure what is required.
  GaussianIndependence(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                       boost::property_tree::ptree            & properties);

  virtual ~GaussianIndependence() {}


  virtual std::shared_ptr<muq::Inference::MCMCState> DrawProposal(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
    int const currIteration = 0) override;


  virtual double ProposalDensity(
    std::shared_ptr<muq::Inference::MCMCState> currentState,
    std::shared_ptr<muq::Inference::MCMCState> proposedState) override;

  /** Write important information about the settings in this proposal as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName,
                               std::string        prefix = "") const;

protected:

  double propVar;
  std::shared_ptr<muq::Modelling::GaussianPair> prop;
};
}
}


#endif // GaussianIndependence_H
