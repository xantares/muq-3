
#ifndef _pCN_h
#define _pCN_h

// include other MUQ related headers
#include "MUQ/Inference/MCMC/Langevin.h"


namespace muq {
namespace Inference {
/** \class pCN
 *  \author Matthew Parno
 *  \brief preconditioned Crank-Nicholson proposal from the work of Cotter, Roberts, Stuart and White
 *  This class uses the infinite dimensional analysis done by Cotter et. al. to provide a shift to the standard MH
 *     proposal based on the current state.  As Cotter et al showed, this shift helps the method have an acceptance
 *     probability independent of mesh refinement for high dimensional problems coming from spatial discretizations or
 *     similar problems.
 */
class pCN : public Langevin {
public:

  /** Default constructor. */
  pCN();

  /** destructor. */
  virtual ~pCN();

  /** Construct an instance by reading in Langevin-specific parameters from a parameter list.
   *  @param[in] paramList Parameter list containing MCMC parameters
   */
  pCN(muq::utilities::ParameterList& paramList, const Eigen::MatrixXd& Sigma);

  /** Set the Langevin parameters from a ParameterList
   *  @param[in] paramList Parameter list containing MCMC parameters
   */
  virtual void   setParms(muq::utilities::ParameterList& paramList);

  /** Set up the Langevin MCMC algorithm by defining the initial proposal from the initial gradient of PostIn at xStart.
   *  @param[in] PostIn The density to sample
   *  @param[in] xStart The starting point of the chain.
   */
  virtual void   setup(muq::Modelling::Density& PostIn, const Eigen::VectorXd& xStart);

  /** Set up the proposal at xc using the gradient of PostDens
   *  @param[in] PostDens The density being sampled
   *  @param[in] xc The current point in the chain, or the point to be used to define the proposal
   *  @param[out] proposal The updated pCN proposal.
   *  @return Evaluation of the proposal density
   */
  virtual double setProp(muq::Modelling::Density     & PostDens,
                         const Eigen::VectorXd       & xc,
                         std::shared_ptr<GaussianPair> proposal);

protected:

  Eigen::MatrixXd PriorCov;
  double beta;
};
} //namespace Inference
} // namespace muq


#endif // ifndef _pCN_h
