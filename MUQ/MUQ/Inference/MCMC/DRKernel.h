
#ifndef DRKernel_H_
#define DRKernel_H_

// standard library includes
#include <vector>

// other muq related includes
#include "MUQ/Inference/MCMC/MCMCProposal.h"
#include "MUQ/Inference/MCMC/MCMCKernel.h"
#include "MUQ/Inference/MCMC/MHProposal.h"


namespace muq {
namespace Inference {
/**
 *  \class DR
 *  \author Matthew Parno
 *  \brief Delayed rejection MCMC class
 *  This class implements the delayed rejection Metropolis-Hastings MCMC algorithm proposed by Mira in 2000.  The
 *     implementation is general enough to handle an arbitrary number of delayed rejection stages by using the
 *     SubContainer class to keep track of the of the numerator and denominator in the acceptance ratio.  However, it is
 *     assumed that the proposal at each stage independently draws from the start state, because this is much simpler.
 */
class DR : public MCMCKernel {
public:

  /** Default constructor. */
  DR(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
     boost::property_tree::ptree                            & properties);

  /** Destructor. */
  virtual ~DR();

  virtual void                                       PrintStatus() const override;

  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName,
                               std::string        prefix = "") const override;

private:

  void UpdateProposals(std::shared_ptr<muq::Inference::MCMCState> const newState,
                       std::vector<MCMCProposal::ProposalStatus> const& proposalAccepted,
                       int const                                        iteration,
                       std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry);

  std::vector < std::shared_ptr < MCMCProposal >> proposals;

  int drEnd;
  int drStages;


  // utility functions for recursively computing DR acceptance probabilities
  double alphafun(std::vector<double> likelies, std::vector < std::shared_ptr < MCMCState >> proposed_points) const;
  double qfun(std::vector < std::shared_ptr < MCMCState >> proposed_points) const;

  // store how many times each proposal has been called
  Eigen::VectorXi numProposalCalls;

  // store how many times each proposal has been accepted
  Eigen::VectorXi numProposalAccepts;
};
} //namespace inference
} //namespace muq

#endif /* DRKernel_H_ */
