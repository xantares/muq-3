/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *  USA.
 *
 *  MIT UQ Library
 *  Copyright (C) 2013 MIT
 */

#ifndef METRICPYTHON_H_
#define METRICPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"
#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"

namespace muq {
namespace Inference {
/// Python wrapper around muq::inference::EuclideanMatric
class EuclideanMetricPython : public EuclideanMetric, public boost::python::wrapper<EuclideanMetric> {
public:

  EuclideanMetricPython();

  EuclideanMetricPython(boost::python::list const& met);

  boost::python::list PyComputeMetric(boost::python::list const& point);
};

/// Python wrapper around muq::inference::GaussianFisherInformationMetric
class GaussianFisherInformationMetricPython : public GaussianFisherInformationMetric,
                                              public boost::python::wrapper<GaussianFisherInformationMetric> {
public:

  GaussianFisherInformationMetricPython(std::shared_ptr<muq::Modelling::GaussianDensity>      density,
                                        std::shared_ptr<muq::Modelling::ModPiece> const& model);

  boost::python::list PyComputeMetric(boost::python::list const& point);

  void                PySetMask(boost::python::list const& mask);
};

void           ExportMetric();
} // namespace inference
} // namespace muq

#endif // ifndef METRICPYTHON_H_
