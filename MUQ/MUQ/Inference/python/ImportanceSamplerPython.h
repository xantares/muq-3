#ifndef IMPORTANCESAMPLERPYTHON_H_
#define IMPORTANCESAMPLERPYTHON_H_ 

#include "MUQ/Inference/ImportanceSampling/ImportanceSampler.h"

namespace muq {
  namespace Inference {
    void ExportImportanceSampler();
  } // namespace Inference 
} // namespace muq

#endif
