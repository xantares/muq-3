#ifndef POLYNOMIALBASISPYTHON_H_
#define POLYNOMIALBASISPYTHON_H_

#include "MUQ/Inference/TransportMaps/PolynomialBasis.h"

namespace muq {
namespace Inference {
template<typename polyType1d>
class GeneralPolynomialBasisPython : public GeneralPolynomialBasis<polyType1d>, public boost::python::wrapper<
                                       GeneralPolynomialBasis<polyType1d> > {
public:

  GeneralPolynomialBasisPython(boost::python::list const& multiIndex) :
    GeneralPolynomialBasis<polyType1d>(muq::Utilities::GetEigenVector<Eigen::RowVectorXu>(multiIndex)) {}

  double PyEvaluate(boost::python::list const& input) const
  {
    return GeneralPolynomialBasis<polyType1d>::evaluate(muq::Utilities::GetEigenVector<Eigen::VectorXd>(input));
  }

private:
};

void ExportPolyBasis();

template<typename polyType1d>
void ExportGenPolyBasis(std::string const& className)
{
  boost::python::class_<GeneralPolynomialBasisPython<polyType1d>,
                        std::shared_ptr<GeneralPolynomialBasisPython<polyType1d> >,
                        boost::python::bases<PolynomialBasisBase, TransportMapBasis>, boost::noncopyable>
  exportGenPoly(className.c_str(), boost::python::init<boost::python::list const&>());

  exportGenPoly.def("evaluate", &GeneralPolynomialBasisPython<polyType1d>::PyEvaluate);

  boost::python::implicitly_convertible<std::shared_ptr<GeneralPolynomialBasisPython<polyType1d> >,
                                        std::shared_ptr<PolynomialBasisBase> >();
}
} // namespace Inference
} // namespace muq

#endif // ifndef POLYNOMIALBASISPYTHON_H_
