
#ifndef _AugLag_h
#define _AugLag_h

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"
#include "MUQ/Optimization/Problems/AugLagProb.h"

namespace muq {
namespace Optimization {
/** @class AugLag
 *   @ingroup Optimization
 *   @brief Augmented Lagrangian algorithm for solving general constrained nonlinear optimization problems
 *  @details This class implements an Augmented Lagrangian method based on "A globally convergent augmented Lagrangian
 *     pattern search algorithm for optimization with general constraints and simple bounds" by Lewis and Torczon.  The
 *     general idea is to create a series of unconstrained subproblems that are solved by any unconstrained optimizer.
 *      Note that if the optimizer requires derivatives, then derivatives of both the constraints and objective will be
 *     required.  Similarly, if the optimizer is a derivative free algorithm, then no derivative information about the
 *     constraints or the objective will be used.  Furthermore, the way Augmented Lagrangian solvers handle constraints
 *     does not guarantee that the objective will only be evaluated in the feasible region, the constraints must be
 *     relaxable.
 */
class AugLag : public OptAlgBase {
public:

  REGISTER_OPT_CONSTRUCTOR(AugLag)
  /** Construct an Augmented Lagrangian based optimization solver from the settings in a ptree.  The ptree should use
   *  some base optimization algorithm as Opt.Method and should have an Opt.ConstraintHandler set to AugLag.
   *  @param[in] ProbPtr A shared pointer to the optimization problem object
   *  @param[in] properties Parameter list containing optimization method and parameters
   */
  AugLag(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);


  /** Solve the optimization problem using x0 as a starting point for the iteration.
   *  @param[in] x0 The initial point for the optimizer.
   *  @return A vector holding the best point found by the optimizer.  Note that the optimizer may not have converged
   * and
   *     the GetStatus() function should be called to ensure that the algorithm converged and this vector is meaningful.
   */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& x0);

protected:

  // some augmented lagrangian specific parameters
  double auglag_gamma;
  double auglag_tau;

  int maxOuterIts;

  // property list for unconstrained solver
  boost::property_tree::ptree uncProp;

private:

  REGISTER_OPT_DEC_TYPE(AugLag)
};

//class AugLag
} // namespace Optimization
} // namespace muq

#endif // ifndef _AugLag_h
