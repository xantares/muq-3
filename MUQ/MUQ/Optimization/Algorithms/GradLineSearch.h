
#ifndef _GradLineSearch_h
#define _GradLineSearch_h

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

namespace muq {
namespace Optimization {
/**
 *  @class GradLineSearch
 *  @ingroup Optimization
 *  @brief An abstract class to define a backtracing line search method that will satisfy the Armijo condition.
 *
 *  @details Many gradient based methods rely on a line search to ensure global convergence (in the sense that the
 * method
 *     will converge to a local min from any starting point, not that it will converge to the global minimum).  This
 *     class provides the line-search routines necessary to satisfy the Armijo condition but lacks the routine that
 *     provides the descent direction.  Thus, child classes will only need to define a function that returns some
 * descent
 *     direction and this class will add the outer loop and linesearch routines to run the optimization.  Child classes
 *     need to implement:
 *  <ul>
 *   <li> The step function to provide some pre line-search descent direction.</li>
 *  </ul>
 */
class GradLineSearch : public OptAlgBase {
public:

  /** Construct the line search method from a pointer to the optimization problem and a parameter list containing the
   *  number of line search iterations, backtracing method, etc...
   *  @param[in] ProbPtr A shared pointer to the optimization problem
   *  @param[in] properties A boost ptree holding the line search settings
   */
  GradLineSearch(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);

  /** Unconstrained minimization routine that uses child class descent direction and a backtracing Armijo line-search
   *  @parm[in] x0 Starting point for the optimization
   *  @return The point found by the optimization algorithm.  Note that you could GetStatus() after running solve to
   * make
   *     sure this point is a local min.
   */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& x0);

  /** This function is the inner-most loop of the of the optimization procedure.  This is specific to each method such
   *  as steepest descent, bfgs, etc...  The point of this function is to replace delta with a descent direction, i.e.
   *  -Gradient or Newton Direction.  FYI: if a legitimate descent direction is not given, the line-search routine will
   *  likely fail and throw an exception. NOTE: this function must update the value of the objective at xc as well as
   * the
   *  gradient (gc) at xc.
   *  @return The pre line-search descent direction
   */
  virtual Eigen::VectorXd step() = 0;

protected:

  /// the current optimization location
  Eigen::VectorXd xc;

  /// the gradient of the objective at the optimization location
  Eigen::VectorXd gc;

  /** perform a line search, just updates the state given a descent direction.  This function updates xc with the new
   *  step and fnew with the new function value.
   *  @param[in] descDir An initial descent direction at xc
   *  @return A success flag.  True if the line search successfully found a significant decrease and false otherwise.
   */
  bool lineSearch(const Eigen::VectorXd& descDir);

  // old function value and new function value
  double fold, fnew;

  // maximum number of line search steps
  int maxLineIts;

  // step size reduction parameter
  double beta;

  // the multiplier used in the line search.  After lineSearch is called, this will contain the final coefficient
  // multiplying the descent direction
  double lambda;

  // coefficients for the Armijo rule
  double c1 = 1e-4;
};

// class GradLineSearch
} // namespace Optimization
} // namespace muq
#endif // ifndef _GradLineSearch_h
