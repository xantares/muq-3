
#ifndef OPTALGBASEPYTHON_H_
#define OPTALGBASEPYTHON_H_

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

namespace muq {
namespace Optimization {
inline std::shared_ptr<OptAlgBase> CreateOptAlgBase(std::shared_ptr<OptProbBase> probPtr,
                                                    boost::python::dict const  & dict)
{
  boost::property_tree::ptree pt = muq::Utilities::PythonDictToPtree(dict);

  return OptAlgBase::Create(probPtr, pt);
}

inline std::shared_ptr<OptAlgBase> CreateOptAlgBaseFromFile(std::shared_ptr<OptProbBase> probPtr,
                                                            std::string const          & inputFile)
{
  // check to make sure the file exists
  std::ifstream ifile(inputFile.c_str());
  boost::property_tree::ptree pt;

  if (ifile.good()) {
    ifile.close();

    // read the file
    boost::property_tree::read_xml(inputFile, pt);
  } else {
    std::cerr << "\nWARNING: Not able to open  " << inputFile <<
    "  please make sure the file exists.  This may cause the program to crash.\n\n";
  }

  return OptAlgBase::Create(probPtr, pt);
}

void ExportOptAlgBase();
} // namespace optimization
} // namespace muq

#endif // ifndef OPTALGBASEPYTHON_H_
