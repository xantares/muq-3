
#ifndef _SAAProb_h
#define _SAAProb_h

#include <memory>

#include "MUQ/Optimization/Problems/OptProbBase.h"


namespace muq {
namespace Optimization {
// An optimization problem used
class SAAProb : public OptProbBase {
public:

  SAAProb(std::shared_ptr<OptProbBase> StochProbPtrIn, int seedVal);

  /** Evaluate the objective by fixing the random seed .
   *  @param[in] xc The location to evaluate the objective.
   *  @return The value of the objective at xc.
   */
  virtual double eval(const Eigen::VectorXd& xc);

  /** Evaluate the gradient
   *  @param[in] xc The location to compute the objective and gradient.
   *  @param[out] gradient A vector that will be filled with the gradient.
   *  @return xc The value of the objective at xc.
   */
  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient);

private:

  const int seed;
  std::shared_ptr<OptProbBase> StochProbPtr;
};
} // namespace Optimization
} // namespace muq


#endif // ifndef _SAAProb_h
