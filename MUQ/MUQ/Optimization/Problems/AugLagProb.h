
#ifndef _AugLagProb_h
#define _AugLagProb_h

#include "MUQ/Optimization/Problems/OptProbBase.h"

namespace muq {
namespace Optimization {
/** @class AugLagProb
 *   @ingroup Optimization
 *   @brief An unconstrained Augmented Lagrangian subproblem
 *   @details This class provides the definition of an unconstrained Augmented Lagrangian subproblem that is used inside
 *      the Augmented Lagrangian optimization problem to solve generally constrained optimization problems using an
 *      arbitrary optimization solver that cannot or will not handle the constraints directly.
 */
class AugLagProb : public OptProbBase {
public:

  /** Construct the augmented lagrangian subproblem using the original objective and constraints defined in prob, as
   *  well as the penalty and approximate lagrange multipliers given in muIn and lambdaIn.
   *  @param[in] prob The original constrained optimization problem.
   *  @param[in] muIn penalty parameter set in Augmented Lagrangian outer iteration
   *  @param[in] lambdaIn Augmented Lagrangian approximated Lagrange multipliers found by outer AugLag iteration.
   */
  AugLagProb(std::shared_ptr<OptProbBase> prob,
             const Eigen::VectorXd      & muIn,
             const Eigen::VectorXd      & lambdaIn,
             double                       rhoIn);

  /** Evaluate the augmented-lagrangian.
   *  @param[in] xc The location to evaluate the objective.
   *  @return The value of the augmented objective at xc.
   */
  virtual double          eval(const Eigen::VectorXd& xc) override;

  /** Evaluate the gradient.
   *  @param[in] xc The location to compute the objective and gradient.
   *  @param[out] gradient A vector that will be filled with the gradient.
   *  @return xc The value of the augmented objective at xc.
   */
  virtual double          grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override;

  /** Apply the Hessian of the Augmented-Lagrangian subproblem to an input vector.
   * @param[in] xc The location where Hessian is defined
   * @param[in] vecIn The input vector.
   * @return H*vecIn The Hessian applied to the input vector.
   */
  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc) override;

private:

  Eigen::VectorXd mu;
  Eigen::VectorXd lambda;

  // the underlying objective function and constraints
  std::shared_ptr<OptProbBase> baseProb;

  double rho;
};

// class AugLagProb
} //namespace Optimization
} // namespace muq

#endif // ifndef _AugLagProb_h
