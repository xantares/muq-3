#ifndef POLYNOMIALAPPROXIMATORPYTHON_H_
#define POLYNOMIALAPPROXIMATORPYTHON_H_

#include "MUQ/Approximation/Incremental/PolynomialApproximator.h"

namespace muq {
  namespace Approximation {
    void ExportPolynomialApproximator();
    void ExportGPApproximator();
  } // namespace Approximation
} // namespace muq 

#endif
