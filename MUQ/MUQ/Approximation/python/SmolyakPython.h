#ifndef SMOLYAKPYTHON_H_
#define SMOLYAKPYTHON_H_


namespace muq {
  namespace Approximation {
    void ExportSmolyakPCEFactory();
    void ExportSmolyakQuadrature();
    
  } // namespace Approximation
} // namespace muq

#endif
