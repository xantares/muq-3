
#ifndef GENZFUNCTIONS_H_
#define GENZFUNCTIONS_H_

#include <functional>

#include <Eigen/Dense>

/**
 * @brief Return a random instance of the Genz function requested.
 *
 * Uses boost::function pointers to return an instance that has used boost::bind to set
 * the w and c parameters. Thus, you get a colvec->colvec function, as needed by the package.
 * Works for arbitrary input dimension.
 *
 * Applies normalization, as taken from: Algorithm 847: spinterp: Piecewise
 * Multilinear Hierarchical Sparse Grid Interpolation in MATLAB by ANDREAS KLIMKE and BARBARA WOHLMUTH
 *
 * Note that the functions produced are shifted from interval [0,1] as their domain to [-1,1].
 *
 * @param type Indicates which type of function you want.
 * @param dim The dimension of the input.
 * @return :function The constructed random Genz function.
 **/
std::function<Eigen::VectorXd(Eigen::VectorXd)> GetRandomGenzFunction(unsigned int type, unsigned int dim);


/**
 * @brief Impelment the six Genz functions.
 *
 * @param x The input of the function.
 * @param w The first parameter.
 * @param c The second parameter.
 * @return :colvec
 **/
Eigen::VectorXd genzOsc(Eigen::VectorXd x, Eigen::VectorXd  w, Eigen::VectorXd  c);
Eigen::VectorXd genzProdPeak(Eigen::VectorXd x, Eigen::VectorXd  w, Eigen::VectorXd  c);
Eigen::VectorXd genzCornerPeak(Eigen::VectorXd x, Eigen::VectorXd  w, Eigen::VectorXd  c);
Eigen::VectorXd genzGaussian(Eigen::VectorXd x, Eigen::VectorXd  w, Eigen::VectorXd  c);
Eigen::VectorXd genzContinuous(Eigen::VectorXd x, Eigen::VectorXd  w, Eigen::VectorXd  c);
Eigen::VectorXd genzDiscontinuous(Eigen::VectorXd x, Eigen::VectorXd  w, Eigen::VectorXd  c);


/**
 * @brief An extra Genz style function, which has independent terms for the first three inputs, and the rest.
 *
 * Constructed as the sum of genzOsc for the first three inputs and genzCornerPeak for the rest. I made up
 * the parameter normalizations.
 **/
Eigen::VectorXd genzMixed(Eigen::VectorXd x, Eigen::VectorXd  w, Eigen::VectorXd  c);
#endif /* GENZFUNCTIONS_H_ */
