#ifndef EXPANSIONBASE_H_
#define EXPANSIONBASE_H_

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Utilities/EigenUtils.h"

namespace muq{
namespace Approximation{
  
  /** @class ExpansionBase
      @ingroup PolynomialChaos
      @ingroup TransportMaps
      @brief Provides an abstract interface for finite expansions such as polynomial expansions and radial basis expansions
      @details This class provides a virtual interface for finite expansions such as polynomial (or polynomial chaos) expansions and radial basis function (RBF) expansions.  Let \f$h_i(x)\f$ be the \f$i^{\mbox{th}}\f$ output of expansion evaluated at a point \f$x\f$.  Furthermore, let \f$f_j(x)\f$ be the \f$j^{\mbox{th}}\f$ basis function in the expansion.  The, the expansions represented by this class take the form
   \f[
   h_i(x) = \sum_{j=1}^{N}a_{ij}f_j(x)
   \f]
   where \f$a_{ij}\f$ is a scalar coefficient ouput \f$i\f$ and term \f$j\f$.  Internally, these coefficients are stored in a matrix, \f$A\f$.
   */
  class ExpansionBase : public muq::Modelling::OneInputJacobianModPiece {
    friend class boost::serialization::access;
  
  public:
    
    /** Basic constructor.
     @param[in] inputDim The dimension of the input, i.e. Each term in the expansion takes an argument with this dimension.
     @param[in] outputDim The output dimension.  If the expansion is being used to approximate a function, this is the output dimension of that function.
     @param[in] numTerms The number of terms in the expansion.  This is used to allocate space in the matrix of coefficients.
     */
    ExpansionBase(int inputDim, int outputDim, int numTerms);
    
    /** Construct the expansion with known coefficients.
     @param[in] inputDim The dimension of the input, i.e. Each term in the expansion takes an argument with this dimension.
     @param[in] coeffsIn The expansion coefficients.  Rows represent coefficients for each output and columns represent the coefficients for each basis function.
     */
    ExpansionBase(int inputDim, Eigen::MatrixXd const& coeffsIn);
    
    virtual ~ExpansionBase() = default;
    
    /** This function evaluates all of the basis functions for each point in the xs matrix.
        @param[in] xs A \f$D\times K\f$ matrix of points to evaluate the basis functions, where \f$D\f$ is the dimension of the input \f$x\f$ and \f$K\f$ is the number of points we want to evaluate.  Notice that each column of the matrix represents a point.
        @return An \f$N\timesK\f$ matrix \f$F\f$, where each component of \f$F\f$ is given by
     \f[
     F_{jk} = f_j(x^{(k)})
     \f]
     In this context, \f$f_j\f$ is the \f$j^{\mbox{th}}\f$ term in the expansion, and \f$x^{(k)}\f$ is the \f$k^{\mbox{th}}\f$ column of the input matrix xs.
     */
    virtual Eigen::MatrixXd EvaluateTerms(Eigen::MatrixXd const& xs) const = 0;
    
    /** This function is analogous to EvaluateTerms, except for the derivative of each term with respect to \f$x_m\f$.
     @param[in] xs A \f$D\times K\f$ matrix of points to evaluate the basis function derviatives.  Here, \f$D\f$ is the dimension of the input \f$x\f$ and \f$K\f$ is the number of points we want to evaluate.  Notice that each column of the matrix represents a point.
     @param[in] dimWrt The value of \f$m\f$, i.e., the dimension of \f$x\f$ to take the derivative with respect to.
     @return A matrix \f$G\f$ defined by
     \f[
     G_{jk} = \frac{\partial f_j}{\partial x_m}(x^{(k)})
     \f]
     */
    virtual Eigen::MatrixXd EvaluateTermDerivs(Eigen::MatrixXd const& xs, int dimWrt) const = 0;
  
    /** Get the number of terms in the expansion. */
    int GetNumTerms() const {return coeffs.cols();};
    
    /** Get a copy of the coefficents for this expansion. */
    Eigen::MatrixXd GetCoefficients() const{return coeffs;};
    
  protected:
    
    Eigen::MatrixXd coeffs;
    
  private:
    
    //virtual Eigen::MatrixXd EvaluateMultiImpl(Eigen::MatrixXd const& xs) override;
    
    virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& x) override;
    
    virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& x) override;
    
  }; // class ExpansionBase
  
} // namespace Utilities
} // namespace muq


#endif // EXPANSIONBASE_H_