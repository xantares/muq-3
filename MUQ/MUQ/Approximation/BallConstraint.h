#ifndef BALLCONSTRAINT_H_
#define BALLCONSTRAINT_H_

#include "MUQ/Optimization/Constraints/ConstraintBase.h"

namespace muq {
  namespace Approximation {
    /// The constraint for the space filling algorithm 
    /**
       Ensure the point stays inside the ball.
     */
    class BallConstraint : public muq::Optimization::ConstraintBase {
    public: 

      /**
	 @param[in] dim The input dimension
       */
      BallConstraint(unsigned int const dim);

    private:
      
      /// Evaluate the constraint at a point 
      /**
	 @param[in] point The point 
	 \return The constraint value
       */
      virtual Eigen::VectorXd eval(Eigen::VectorXd const& point) override;

      /// Evaluate the Jacobian action at a point 
      /**
	 @param[in] point The point 
	 @param[in] target The Jacobian is acting on this vector
	 \return The Jacobian action
       */
      virtual Eigen::VectorXd ApplyJacTrans(Eigen::VectorXd const& point, Eigen::VectorXd const& target) override;

    };
  } // namespace Approximation
} // namespace muq

#endif
