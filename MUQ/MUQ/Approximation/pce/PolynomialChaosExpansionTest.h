
#ifndef POLYNOMIALCHAOSEXPANSIONTEST_H_
#define POLYNOMIALCHAOSEXPANSIONTEST_H_

#include <Eigen/Core>

Eigen::VectorXd pceTestConstantine1(Eigen::VectorXd const& val);
Eigen::VectorXd pceTestConstantine2(Eigen::VectorXd const& val);
Eigen::VectorXd pceTestConstantine3(Eigen::VectorXd const& val);
Eigen::VectorXd pceTestConstantine4(Eigen::VectorXd const& val);


#endif /* POLYNOMIALCHAOSEXPANSIONTEST_H_ */
