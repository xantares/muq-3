
#ifndef _ConvexHull_h
#define _ConvexHull_h

#include <vector>
#include <list>
#include <Eigen/Core>

namespace muq {
namespace Utilities {
/** \author Matthew Parno
 *  \class ConvexHull
 *  \brief A class for computing and storing the convex hull of a collection 1, 2, or 3 dimensional points.
 *  \details This class provides the ability to compute, store, and merge the convex hulls of point collections.  This
 *     class was originally designed for use with the Hmatrix class to efficiently compute distances between groups of
 *     FEM elements.
 */
template<unsigned int dim>
class ConvexHull {
public:

  /** Create an empty convex hull. */
  ConvexHull() {}

  /** Copy constructor */
  ConvexHull(const ConvexHull& rhs)
  {
    hullpts = rhs.hullpts;
  }

  /** Construct the convex hull from the pts stored in a vector. */
  ConvexHull(std::vector < Eigen::Matrix < double, dim, 1 >>& AllPts)
  {
    BuildHull(AllPts);
  }

  /** Build the convex hull from the pts stored in a vector. */
  void BuildHull(std::vector < Eigen::Matrix < double, dim, 1 >> &AllPts);

  /** Add on to this convex hull. */
  ConvexHull<dim>& operator+=(const ConvexHull<dim>& Rhs);

  /** Merge two convex hulls into one. */
  ConvexHull<dim>  operator+(const ConvexHull<dim>& Rhs);

  /** Copy a different convex hull */
  ConvexHull<dim>& operator=(const ConvexHull<dim>& Rhs);

  /** Compute and return the diameter of this convex hull. */
  double           GetDiam() const;

  /** Compute and return the minimum distance between points in this hull and another hull*/
  double           GetDist(const ConvexHull<dim>& Rhs) const;

private:

  void GrahamScan(std::vector < Eigen::Matrix < double, dim, 1 >> &AllPts);


  // the points on the convex hull
  std::vector < Eigen::Matrix < double, dim, 1 >> hullpts;
};

// class ConvexHull
} // namespace Utilities
} // namespace muq


#endif // ifndef _ConvexHull_h
