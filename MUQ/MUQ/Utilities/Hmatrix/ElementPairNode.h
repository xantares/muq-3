#ifndef _ElementPairNode_h
#define _ElementPairNode_h

#include "MUQ/Utilities/Hmatrix/ElementTree.h"

namespace muq {
namespace Utilities {
template<unsigned int dim>
class ElementNode;

template<unsigned int dim>
class ElementPairNode {
public:

  ElementPairNode(double                                  eta,
                  std::shared_ptr < ElementTree < dim >>& Tree,
                  std::shared_ptr < ElementNode < dim >>& Node1In,
                  std::shared_ptr < ElementNode < dim >>& Node2In);

  void RecurseMatMult(std::shared_ptr < ElementTree < dim >> &ClusterTree,
                      const Eigen::VectorXd & x,
                      Eigen::VectorXd & b) const;

  // store the pair
  std::shared_ptr < ElementNode < dim >> Node1;
  std::shared_ptr < ElementNode < dim >> Node2;

  // store the children in the tree
  std::shared_ptr < ElementPairNode < dim >> child11;
  std::shared_ptr < ElementPairNode < dim >> child12;
  std::shared_ptr < ElementPairNode < dim >> child21;
  std::shared_ptr < ElementPairNode < dim >> child22;

  // if the pair is admissable, then we will use a rank k matrix stored as two small matrices
  Eigen::MatrixXd u;
  Eigen::MatrixXd v;

  void BuildMatParts(std::shared_ptr < ElementTree < dim >> &Tree, muq::Geostatistics::CovKernelPtr & CovKernel);

  void RecurseMatConstruct(std::shared_ptr < ElementTree < dim >> &Tree, muq::Geostatistics::CovKernelPtr & CovKernel);

  // if the pair is not admissable, then we will use a Dense matrix
  Eigen::MatrixXd DenseCovPart;

private:

  // store the admissibility of this pair
  bool Admiss;
  bool isLeaf;

  // build up the children nodes recursively (used in constructor)
  void BuildChildren(std::shared_ptr < ElementNode < dim >> &Node1In, std::shared_ptr < ElementNode < dim >> &Node2In);
};
}
}
#endif // ifndef _ElementPairNode_h
