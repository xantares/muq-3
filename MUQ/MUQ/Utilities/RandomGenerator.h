
#ifndef _RandomGenerator_h
#define _RandomGenerator_h

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#endif

#include <fstream>
#include <iostream>
#include <random>
#include <mutex>
#include <ctime>
#include <boost/graph/graph_concepts.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <Eigen/Dense>
#include <array>

namespace muq {
namespace Utilities {

/**
 * Call this class to temporarily set the seed. The previous state is restored when the object
 * is destructed. This is a fairly low quality way to do this, but is fine for testing. 
 */
class RandomGeneratorTemporarySetSeed
{
public:
	RandomGeneratorTemporarySetSeed(int seed);

	virtual ~RandomGeneratorTemporarySetSeed();
private:
	std::mt19937 oldGenerator;
};



/** The RandGen generator class is a wrapper around the std::random number generation library.  The point of this class
 *  is to provide a super easy to use interface that can easily be mimiced when Boost is not available. These static
 *     methods
 *  will provide safe random numbers, in that the global state of the random number generator is maintained. Thread
 * safe.
 *
 *  NOTE: must call random numbers with dist(engine), not with the bind strategy, as that ruins the global
 *  shared random state. Mutex strategy taken from boost synchronization page tutorial.
 */
class RandomGenerator {
public:
	
	typedef std::mt19937 GeneratorType;

  /** Get a standard Gaussian distribution sample. */
  static double          GetNormal();

  /** Get a uniformly distributed real sample in [0,1). */
  static double          GetUniform();

  static double          GetGamma(double const alpha, double const beta);

  /** Get a random integer, distributed uniformly, between the bounds. */
  static int             GetUniformInt(int lb, int ub);

  ///Convenience method - return a vector of random numbers
  static Eigen::VectorXd GetUniformRandomVector(int const n);

  ///Convenience method - return a matrix of random numbers
  static Eigen::MatrixXd GetUniformRandomMatrix(int const m, int const n);

  /// Generate N unique random integers from the range lb, ub.  This function is similar to randperm in matlab
  static Eigen::VectorXi GetUniqueUniformInts(int lb, int ub, int N);

  ///Convenience method - return a vector of random numbers
  static Eigen::VectorXd GetNormalRandomVector(int const n);

  ///Convenience method - return a matrix of random numbers
  static Eigen::MatrixXd GetNormalRandomMatrix(int const m, int const n);

  /// Set the seed for the random number generator. This is a fairly low quality way to do this, but is fine for testing.
  static void            SetSeed(int seedval);
  
  ///Store a copy of the generator, designed for use with SetGenerator. Be very careful in using this, or you could wreck the library's access to random numbers.
  static GeneratorType CopyGenerator();
  
  ///Set the state of the generator, designed for use with CopyGenerator. Be very careful in using this, or you could wreck the library's access to random numbers.
  static void SetGenerator(GeneratorType);

private:

  static std::mutex  & GetGeneratorMutex();

  friend RandomGeneratorTemporarySetSeed;
  static GeneratorType & GetGenerator();
};


class SeedGenerator {
public:

  SeedGenerator();
  SeedGenerator(const std::array<std::seed_seq::result_type, RandomGenerator::GeneratorType::state_size> &seed_data);
  //draw a seed for the entire state, as per: http://stackoverflow.com/questions/15509270/does-stdmt19937-require-warmup
  std::seed_seq seed_seq;
  virtual ~SeedGenerator() = default;
};



} // namespace GeneralUtilties
} //namespace muq

#endif // ifndef _RandomGenerator_h
