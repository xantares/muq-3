#ifndef MULTIINDEX_H_
#define MULTIINDEX_H_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1

#include <boost/property_tree/ptree.hpp> // this include is weird, but must be here to avoid a strange bug on osx
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1


#include <vector>

#include "MUQ/Utilities/EigenUtils.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>

/** @defgroup MultiIndices
    @ingroup Utilities
    @ingroup PolynomialChaos
    @ingroup TransportMaps
    @brief Provides mechanisms for storing and manipulating the multiindices that commonly define multivariate polynomial expansions.
    @details
 <h2>Overview</h2>
 Multiindices are useful in many settings; however, in MUQ, they are primarily used for defining multivariate polynomial expansions, which arise in both polynomial chaos expansions and transport maps.  Multiindices themselves are defined in the MultiIndex class, while groups of multiindices are managed by the MultiIndexSet class.  Most instances of the MultiIndexSet class are created using a member of MultiIndexFactory or as part of constructing a polynomial chaos expansion or transport map.
 
 <h2>Typical usage</h2>
 To create a typical total order limited in 10 dimensions with a maximum order of 5, the following code could be used:
\code{.cpp}
 int dim      = 10;
 int maxOrder = 5;
 auto set = MultiIndexFactory::CreateTotalOrder(dim,maxOrder);
\endcode
 Similarly, we could create a much larger tensor product set by using
 \code{.cpp}
 auto set = MultiIndexFactory::CreateFullTensor(dim,maxOrder);
 \endcode
 To subsequently print all of the multiindices in the set, we could use
 \code{.cpp}
 Eigen::MatrixXu allMultiVecs = set->GetAllMultiIndices();
 cout << allMultiVecs << endl;
 \endcode
 where each row of allMultiVecs corresponds to a multiindex.  Internally, the MultiIndex class stores multiindices in a compressed format.  To get a shared_ptr to a multiindex at index \f$i\f$ of the set, we could run
 \code{.cpp}
 int i = 2;
 shared_ptr<MultiIndex> multi = set->IndexToMultiPtr(i);
 \endcode
 or equivalently,
 \code{.cpp}
 int i = 2;
 shared_ptr<MultiIndex> multi = set->at(i);
 \endcode
 To instead get access to a row vector of unsigned integers representing the set, we could call
 \code{.cpp}
 Eigen::RowVectorXu multiVec = set->IndexToMult(i);
 \endcode
 This is equivalent to calling the GetMulti() function of the MultiIndex class:
 \code{.cpp}
 Eigen::RowVectorXu multiVec2 = set->IndexToMult(i)->GetMulti();
 \endcode
 Like a std::vector, the size() function returns the number of active indices in the MultiIndexSet.  To manually loop over each active multiindex in the set, we could run
 \code{.cpp}
 for(int i=0; i<set->size(); ++i)
   cout << "Index " << i << " maps to multiindex " << set->at(i)->GetMulti() << std::endl;
 \endcode
 For more information on all the capabilities of MUQ's multiindices, check out the documentation for muq::Utilities::MultiIndexFactory, muq::Utilities::MultiIndexSet, muq::Utilities::MultiIndex, and muq::Utilities::MultiIndexLimiter.
 */
namespace muq {
  
  namespace Approximation{
    class PolynomialExpansion;
    class PolynomialChaosExpansion;
  }

  namespace Utilities{

    class MultiIndexSet;
    
    /** @class MultiIndex
        @ingroup MultiIndices
        @brief A class for holding and accessing a single multiindex.
        @details <p> In its simplest form, a multiindex is simply a vector of nonnegative integers, say \f$ \mathbf{j}=[j_1,j_2,\dots,j_D]\f$, where \f$D\f$ is a user-specified dimension.  In MUQ, these multiindices are used to define multivariate polynomial expansions.</p>
     
          <p> The reason we use this class, instead of just storing the vectors directly, is that this class provides sparse storage scheme for the multiindex; only elements of \f$\mathbf{j}\f$ that are nonzero are actually stored.  This type of sparse storage is particularly advantageous for polynomial expansions that do not have a large number of cross terms, e.g., diagonal transport maps or highly anisotropic polynomial chaos expansions.
          </p>
     
          <p> This class is mostly used behind the scenes.  However, the GetMulti() function may come in useful for users that need to extract the multiindex vector. </p>
     */
    class MultiIndex{
      friend class muq::Approximation::PolynomialExpansion;
      friend class muq::Approximation::PolynomialChaosExpansion;
      
      friend class MultiIndexSet;
      friend class boost::serialization::access;
      
    public:
      
      /** Constructor that creates a multiindex of all zeros.
          @param[in] dimensionIn The dimension of the zero multiindex.
       */
      MultiIndex(int dimensionIn);
      
      /** Constructor that takes a dense vector description of the multiindex and extracts the nonzero components.
          @param[in] indIn Row vector of unsigned integers defining the multiindex.
       */
      MultiIndex(Eigen::RowVectorXu const& indIn);
      
      /** Copy Constructor.  This function performs a deep copy of another MultiIndex.
       @param[in] indIn Another instance of a MultiIndex.
       */
      MultiIndex(MultiIndex const& indIn);
      
      /** Create a deep copy of MultiIndex pointed to by the input.
          @param[in] indIn A shared_ptr to a MultiIndex instance
          @return A shared_ptr to a new MultiIndex instance containing the same information as the input.
       */
      static std::shared_ptr<MultiIndex> Clone(std::shared_ptr<MultiIndex> const& indIn){return std::make_shared<MultiIndex>(*indIn);};
      
      /** Default irtual destructor.  */
      virtual ~MultiIndex() = default;
     
      /** Get the dense representation of this multiindex.
          @return A row vector of unsigned integers containing the multiindex.
       */
      Eigen::RowVectorXu GetMulti() const;
      
      /** Get the order of this multiindex: the \f$\ell_1\f$ norm.
       @return The sum of the nonzero components: the total order of this multiindex.
       */
      int GetOrder() const{return totalOrder;};
      
      /** This function returns the maximum of this multiindex: the \f$\ell_\infty\f$ norm.
       @return The maximum value of the multiindex.
       */
      int GetMax() const{return maxIndex;};
      
      /** Use this function to set the value of the an entry in the multiindex.
       @param[in] dim The dimension of the multiindex to set (starting with 0).
       @param[in] val A non-negative value for the dim component of the multiindex.
       @return True if this function updated an already nonzero component, or false if this function added a new nonzero entry.
       */
      bool SetValue(int dim, int val);
      
      /** Obtain the a particular component of the multiindex.  Notice that this function can be slow for multiindices with many nonzero components.  The worst case performance requires \f$O(|\mathbf{j}|_0)\f$ integer comparisons, where \f$|\mathbf{j}|_0\f$ denotes the number of nonzero entries in the multiindex.
       @param[in] dim The component to return.
       @return The integer stored in component dim of the multiindex.
       */
      int GetValue(int dim) const;
      
      
      void SetDimension(int newDim);
      unsigned int GetDimension() const{return dimension;};
      
      bool operator==(const MultiIndex &b);
      bool operator!=(const MultiIndex &b);
      bool operator<(const MultiIndex &b);
      
      std::vector<std::pair<unsigned,unsigned>>::const_iterator GetNzBegin() const{return nzInds.begin();};
      std::vector<std::pair<unsigned,unsigned>>::const_iterator GetNzEnd() const{return nzInds.end();};

#if MUQ_PYTHON == 1
      boost::python::list PyGetMulti() const;
      static std::shared_ptr<MultiIndex> PyCreate(boost::python::list const& multiIn);
      boost::python::list GetNz() const;
#endif
      
    private:
      
      unsigned int dimension;
      
      /// a vector holding pairs of (dimension,index) for nonzero values of index.
      std::vector<std::pair<unsigned,unsigned>> nzInds;
      
      /// The maximum index over all nzInds pairs.
      int maxIndex;
      
      // the total order of the multiindex (i.e. the sum of the indices)
      int totalOrder;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version){
        ar & nzInds;
        ar & maxIndex;
        ar & totalOrder;
      };
      
    }; // class MultiIndex
    
    
    struct MultiPtrComp{
      bool operator()(std::shared_ptr<MultiIndex> a, std::shared_ptr<MultiIndex> b) const{return (*a)<(*b);};
    };
    
    
    
  } // namespace Utilities
} // namespace muq

namespace boost { 
namespace serialization {

  template<class Archive>
  inline void save_construct_data(
    Archive & ar, const muq::Utilities::MultiIndex * t, const unsigned int file_version){
    int dim = t->GetDimension();
    ar << dim;
  }

  template<class Archive>
  inline void load_construct_data(
    Archive & ar, muq::Utilities::MultiIndex * t, const unsigned int file_version){
    int dimension;
    ar >> dimension;
    ::new(t)muq::Utilities::MultiIndex(dimension);
  }

} // namespace serialization
} // namespace boost

#endif