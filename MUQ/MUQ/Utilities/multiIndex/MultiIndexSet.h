#ifndef MULTIINDEXSET_H_
#define MULTIINDEXSET_H_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1

#include <boost/property_tree/ptree.hpp> // this include is weird, but must be here to avoid a strange bug on osx
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1


#include <vector>
#include <memory>

#include <boost/bimap.hpp>
#include <boost/graph/graph_concepts.hpp>
#include "MUQ/Utilities/multiIndex/MultiIndex.h"

#include "MUQ/Utilities/multiIndex/MultiIndexLimiter.h"
#include "MUQ/Utilities/multiIndex/MultiIndexPool.h"


#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace muq{
  namespace Utilities{
    
    class MultiIndexSet;
    class MultiIndexFactory;
    

    std::shared_ptr<MultiIndexSet> operator+=( std::shared_ptr<MultiIndexSet> x, const std::shared_ptr<MultiIndexSet>& y);
    std::shared_ptr<MultiIndexSet> operator+=( std::shared_ptr<MultiIndexSet> x, const std::shared_ptr<MultiIndex>& y);
    
    /** @class MultiIndexSet
     @ingroup MultiIndices
     @brief A class for holding, sorting, and adapting sets of muliindices.
     @details <p>In the context of polynomial expansions, a multiindex defines a single multivariate polynomial.  A finite expansion of multivariate polynomials is then defined by a collection of multiindices, one for each term in the expansion.  This class is a tool for defining such a multiindex set, relating members within the set (i.e. defining neighbors), and expanding the set. </p>
     
     <p>Let \f$\mbox{j}=[j_1,j_2,\dots,j_D]\f$ be a \f$D\f$-dimensional multiindex.  The backwards neighbors of $\mbox{j}$ are the multiindices given by multiindices who are only different from \f$\mbox{j}\f$ in one component, and in that component, the difference is -1.  For example, \f$[j_1-1, j_2,\dots,j_D]\f$ and \f$[j_1, j_2-1,\dots,j_D]\f$ are backwards neighbors of \f$\mbox{j}\f$, but \f$[j_1-1, j_2-1,\dots,j_D]\f$ and \f$[j_1, j_2-2,\dots,j_D]\f$ are not.  Forwar neighbors are similarly defined, but with +1. Examples of forward neighbors include \f$[j_1+1, j_2,\dots,j_D]\f$ and \f$[j_1, j_2+1,\dots,j_D]\f$.   As far as this class is concerned, multiindices can be in three different categories: active, inactive, and/or admissable.  Active multiindices are those that are currently being used to define a polynomial expansion, inactive multiindices are not currently used but are being tracked, and admissable multiindices are inactive multiindices whose backward neighbors are all active.<p>
     
     <p>This class keeps track of both active and admissable multiindices, but only active indices are included in the linear indexing.  Nonactive indices are hidden (i.e. not even considered) in all the public members of this class.  For example, the GetAllMultiIndices function will return all active multiindices, and the IndexToMulti function will return \f$i^\mbox{th}\f$ active multiindex.  Inactive multiindices are used to check admissability and are added to the active set during adaptation.
     </p>
     
     <p>In general, members of the MultiIndexFactory class should be used to construct MultiIndexSets directly.<p>
     */
    class MultiIndexSet{
      
      friend class MultiIndexFactory;
      friend class boost::serialization::access;
      
    public:
      
      MultiIndexSet(const unsigned dimIn,
                    std::shared_ptr<MultiIndexLimiter> limiterIn = std::make_shared<NoLimiter>(),
                    std::shared_ptr<MultiIndexPool> poolIn = std::make_shared<MultiIndexPool>());
      
      /// NOTE: does not perform a deep copy of the multiindices themselves, only pointers to the multiindices
      static std::shared_ptr<MultiIndexSet> CloneExisting(std::shared_ptr<MultiIndexSet> original);
      
      /** Default virtual destructor */
      virtual ~MultiIndexSet() = default;
      
      /** Set the limiter of this MultiIndexSet.  This function will check to make sure that all currently active nodes are still feasible with the new limiter.  If this is not the case, an assert will be thrown.
          @param[in] limiterIn A shared pointer to the new limiter.
       */
      virtual void SetLimiter(std::shared_ptr<MultiIndexLimiter> limiterIn);
      virtual std::shared_ptr<MultiIndexLimiter> GetLimiter() const{return limiter;};
      
      // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      // * * * FIXED COMPONENTS
      // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      
      /** Get all the multiindices in this set, returned as a dense matrix.
       @return A \f$K\times D\f$ matrix of unsigned integers, where \f$D\f$ is the dimension of each multiindex, and \f$K\f$ is the number of multiindices in this set.  Note that each row of the returned matrix is a multiindex.
       */
      virtual Eigen::MatrixXu GetAllMultiIndices() const;
      
      /** Return \f$K\f$, the number of multiindices in this set. */
      virtual unsigned int GetNumberOfIndices() const{return active2local.size();};
      
      /** Given an index into the set, return the corresponding multiindex as a row vector of unsigned integers. If all the multiindices were stored in a vector called multiVec, the functionality of this method would be equivalent to multiVec[activeIndex].
       @param[in] activeIndex Linear index of interest.
       @return A row vector containing the multiindex corresponding to activeIndex.
       */
      virtual Eigen::RowVectorXu IndexToMulti(unsigned int const activeIndex) const;
      
      
      /** Given an index into the set, return the corresponding multiindex as an instance of the MultiIndex set. If all the multiindices were stored in a vector called multiVec, the functionality of this method would be equivalent to multiVec[activeIndex].
       @param[in] activeIndex Linear index of interest.
       @return A shared pointer to a constant instance of the MultiIndex class.
       */
      virtual const std::shared_ptr<MultiIndex> IndexToMultiPtr(unsigned int const activeIndex) const{return pool->GetMulti(local2global.at(active2local.at(activeIndex)));};
      
      
      /** Given a multiindex, return the linear index where it is located.
       @param[in] input A shared pointer to an instance of the MultiIndex class.
       @return If the multiindex was found in this set, a nonnegative value containing the linear index is returned.  However, if the set does not contain the multiindex, -1 is returned.
       */
      virtual int MultiToIndex(const std::shared_ptr<MultiIndex> input) const;
      
      /** Given a multiindex, return the linear index where it is located.
       @param[in] input A row vector of unsigned integers representing the multiindex.
       @return If the multiindex was found in this set, a nonnegative value containing the linear index is returned.  However, if the set does not contain the multiindex, -1 is returned.
       */
      virtual int MultiToIndex(Eigen::RowVectorXu const& multiIn) const;
      
      /** Get the dimension of the multiindex, i.e. how many components does it have? */
      virtual unsigned int GetMultiIndexLength() const{return dim;};
      
      /** Assume the \f$\mathbf{j}^{\mbox{th}}\f$ multiindex in this set is given by \f$\mathbf{j}=[j_1,j_2,\dots,j_D]\f$.  This function returns a vector containing the maximum value of any multiindex in each direction, i.e., a vector \f$\mathbf{m}=[m_1,m_2,\dots,m_D]\f$ where \f$m_d = \max_{\mathbf{j}} j_d\f$.
       @return The vector \f$\mathbf{m}\f$.
       */
      virtual Eigen::VectorXu GetMaxOrders() const{return maxOrders;};
      
      /**
       * This function provides access to each of the MultiIndices.
       @param[in] outputIndex The index of the active MultiIndex to return.
       @return A pointer to the MultiIndex at index outputIndex.
       */
      virtual std::shared_ptr<MultiIndex>& at(int outputIndex){return pool->at(local2global.at(active2local.at(outputIndex)));};
      
      /**
       * This function provides constant access to each of the MultiIndices.
       @param[in] outputIndex The index of the MultiIndex to return.
       @return A pointer to the MultiIndex at index outputIndex.
       */
      virtual const std::shared_ptr<MultiIndex>& at(int outputIndex) const{return pool->at(local2global.at(active2local.at(outputIndex)));};
      
      
      /**
       * This function provides access to each of the MultiIndices without any bounds checking on the vector.
       @param[in] outputIndex The index of the active MultiIndex we want to return.
       @return A pointer to the MultiIndex at index outputIndex.
       */
      virtual std::shared_ptr<MultiIndex>& operator[](int outputIndex){return (*pool)[local2global[active2local[outputIndex]]];};
      
      /**
       * This function provides constant access to each of the basis functions without any bounds checking on the vector.
       @param[in] outputIndex The index of the basis function we want to return.
       @return A pointer to the basis function at index outputIndex.
       */
      virtual const std::shared_ptr<MultiIndex>& operator[](int outputIndex) const{return (*pool)[local2global[active2local[outputIndex]]];};
      
      /**
       * Get the number of active MultiIndices in this set.
       @return An unsigned integer with the number of active MultiIndices in the set.
       */
      virtual unsigned int size() const{return active2local.size();};
      
      /** 
       * Get a pointer to the pool of multiindices related to this set.
         @return A constant shared_ptr to the pool.
       */
      const std::shared_ptr<MultiIndexPool> GetPool() const{return pool;};
      int GetPoolId() const{return setID;};
      
      // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      // * * * ADAPTIVE COMPONENTS
      // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      
      /** @brief Add another set of multiindices to this one.
       @details Any basis functions in the rhs MultiIndexSet that are not equivalent to basis functions in this instance are added.
       @param[in] rhs Another MultiIndex set to add to this one
       @return A reference to this MultiIndex set, which now contains the union of this set and rhs.
       */
      virtual MultiIndexSet& operator+=(const MultiIndexSet &rhs);
      
      /** @brief Add a single MultiIndex to the set.
       @details This functions checks to see if the input basis function is already in the set and if the input function is unique, it is added to the set.
       @param[in] rhs A shared_ptr to the MultiIndex we want to add to the set.
       @return A reference to this MultiIndex set, which may now contain the new MultiIndex in rhs.
       */
      virtual MultiIndexSet& operator+=(std::shared_ptr<MultiIndex> rhs);
      
      /** @brief Add a single MultiIndex to the set.
       @details This function creates an instance of MultiIndex from the row vector and calls the other += operator.
       @param[in] multiIndex A row vector of unsigned integers represent the multiindex.
       @return A reference to this MultiIndex set, which may now contain the new MultiIndex in multiIndex.
       */
      virtual MultiIndexSet& operator+=(Eigen::RowVectorXu const& multiIndex);
      
      /** @brief Add all terms in rhs to this instance.
       @details This function adds all unique MultiIndices from the rhs into this MultiIndexSet.  In the event that a multiindex is active in one set, but not the other, the union will set that multiindex to be active.
       @param[in] rhs The MultiIndex set we want to add to this instance.
       @return The number of unique terms from rhs that were added to this set.
       */
      virtual int Union(const MultiIndexSet &rhs);
      
      
      /**
       * Make the multi-index active. Assumes that the multiIndex input is already
       * admissible, as checked by an assertion.  To be admissable (according to this function), the multiIndex must already exist
       * as an inactive member of this set.  If that is not the case, use the AddActive function instead.
       * @param[in] multiIndex A multiindex to make active.  Note that an assert will fail if multiIndex is not admissable.
       */
      virtual void Activate(std::shared_ptr<MultiIndex> multiIndex);
      
      /**
       * Make the multi-index active. Assumes that the multiIndex input is already
       * admissible, as checked by an assertion.  Notice that this function simply creates an instance of MultiIndex and calls the Activate(std::shared_ptr<MultiIndex> multiIndex) function.
       * @param[in] multiIndex A row vector of unsigned integers representing the multiindex.
       */
      virtual void Activate(Eigen::RowVectorXu const& multiIndex){Activate(std::make_shared<MultiIndex>(multiIndex));};
      
      /**
       * Add the given multiindex to the set and make it active.  The functionality of this function is very similar to Activate; however,
       * this function will add the multiIndex if it does not already exist in the set.  This function does not check for admissability.  Instead, it will add the multiindex to the set and add all neighbors as inactive.  Be careful when using this function as it is possible to create a set with active multiindices that are not active!
       @param[in] newNode A multiindex we want to add as an active member of the set.
       @return An integer specifying the linear index of the now active multiindex.
       */
      virtual int AddActive(std::shared_ptr<MultiIndex> newNode);
      
      /** This function simply creates an instance of MultiIndex and calls the AddActive(std::shared_ptr<MultiIndex> newNode) function.
       @param[in] multiIndex A row vector of unsigned integers representing the multiindex to add to the set.
       @return An integer specifying the linear index of the now active multiindex.
       */
      virtual int AddActive(Eigen::RowVectorXu const& multiIndex){return AddActive(std::make_shared<MultiIndex>(multiIndex));};
      
      
      /**
       * If possible, make the neighbors of this index active, and return any that become active.
       * Do not activate a neighbor that is alxready part of the family
       * @param activeIndex The linear index of the active multiindex to expand.
       * @return A vector containing the linear index of any multiindices activated because of the expansion.
       */
      virtual Eigen::VectorXu Expand(unsigned int activeIndex);
      
      /**
       * Completely expands an index, whether or not it is currently expandable. In order to maintain admissability
       * of the set, it will add backward neighbors needed recursively, and return a list of all the
       * indices it adds.
       * @param activeIndex The linear index of the active multiindex to expand.
       * @return A vector containing the linear index of any multiindices activated because of the expansion.
       */
      virtual Eigen::VectorXu ForciblyExpand(unsigned int const activeIndex);
      
      /**
       * Add the given multi-index to the active set regardless of whether it's currently admissible.
       * To keep the whole set admissible, recursively add any backward neighbors necessary.
       * Returns a list of indices of any newly added elements, including itself.
       * @param multiIndex The MultiIndex to forcibly add, make active, and make admissable.
       * @return A vector of linear indices indicating all the active MultiIndices added to the set in order to make the given multiIndex admissable.
       */
      virtual Eigen::VectorXu ForciblyActivate(std::shared_ptr<MultiIndex> multiIndex);
      
      /**
       * This function simply creates an instance of the MultiIndex class and calls ForciblyActivate(std::shared_ptr<MultiIndex> multiIndex).
       * @param multiIndex A row vector of unsigned integers defining the multiindex.
       * @return A vector of linear indices indicating all the active MultiIndices added to the set in order to make the given multiIndex admissable.
       */
      virtual Eigen::VectorXu ForciblyActivate(Eigen::RowVectorXu const& multiIndex){return ForciblyActivate(std::make_shared<MultiIndex>(multiIndex));};
      
      /** This function returns the admissable forward neighbors of an active multiindex.
       @param[in] activeIndex The linear index of the active multiIndex under consideration.
       @return A matrix of admissible forward neighbors.  Each row is is a MultiIndex.
       */
      virtual Eigen::MatrixXu  GetAdmissibleForwardNeighbors(unsigned int activeIndex);
      
      //*********************************************************
      //Testing properties of an index/multiIndex
      //*********************************************************
      
      ///Determines whether the input multiIndex is currently admissible.
      virtual bool IsAdmissible(std::shared_ptr<MultiIndex> multiIndex) const;
      
      ///Determines whether the input multiIndex is currently admissible.
      virtual bool IsAdmissible(Eigen::RowVectorXu const& multiIndex) const;
      
      
      ///Return true if one of the forward neighbors of index is admissible but not active.
      virtual bool IsExpandable(unsigned int activeIndex) const;
      
      ///Return true if the multiIndex is active
      virtual bool IsActive(Eigen::RowVectorXu const& multiIndex) const{return IsActive(std::make_shared<MultiIndex>(multiIndex));};
      
      ///Return true if the multiIndex is active
      virtual bool IsActive(std::shared_ptr<MultiIndex> multiIndex) const;
      //
      //
      //      //*********************************************************
      //      //Select groups of indices
      //      //*********************************************************
      //
      //
      //      ///Return a 0/1 vector with 1s on the expandable indices and 0s on the expandable indices.
      //
      //      /**
      //       * The idea here is that if you have a colvec of values associated with the indices, you
      //       * could select them out by performing termwise multiplication (as .* in matlab)
      //       */
      //      //Eigen::VectorXd    SelectExpandable() const;
      //
      //      ///Get a row vector of the indices of the backward neighbors of an input index
      //      //Eigen::RowVectorXu GetActiveBackwardNeighborIndices(unsigned int const index) const;
      
#if MUQ_PYTHON == 1
      boost::python::list PyGetAllMultiIndices() const;
      boost::python::list PyIndexToMulti(unsigned int const activeIndex) const;
      int PyMultiToIndex1(boost::python::list const& multiIn) const;
      int PyMultiToIndex2(std::shared_ptr<MultiIndex> multiIndex) const;
      boost::python::list PyGetMaxOrders() const;
      void PyActivate1(boost::python::list const& multiIndex);
      void PyActivate2(std::shared_ptr<MultiIndex> multiIndex);
      int PyAddActive1(boost::python::list const& multiIndex);
      int PyAddActive2(std::shared_ptr<MultiIndex> multiIndex);
      boost::python::list PyExpand(unsigned int activeIndex);
      boost::python::list PyForciblyExpand(unsigned int activeIndex);
      boost::python::list PyForciblyActivate1(std::shared_ptr<MultiIndex> multiIndex);
      boost::python::list PyForciblyActivate2(boost::python::list const& multiIndex);
      boost::python::list PyGetAdmissibleForwardNeighbors(unsigned int activeIndex);
      bool PyIsAdmissible1(boost::python::list const& multiIndex) const;
      bool PyIsAdmissible2(std::shared_ptr<MultiIndex> multiIndex) const;
      bool PyIsActive1(boost::python::list const& multiIndex) const;
      bool PyIsActive2(std::shared_ptr<MultiIndex> multiIndex) const;
#endif
    protected:
      
      int AddInactive(std::shared_ptr<MultiIndex> newNode);
      int AddInactive(Eigen::RowVectorXu const& multiIndex){return AddInactive(std::make_shared<MultiIndex>(multiIndex));};
      
      // loop through the multis vector and update the maximum order for each dimension
//      void UpdateMaxOrders();
//      
//      void UpdateMultiMap();
//      
      virtual bool IsAdmissible(unsigned int localIndex) const;
      virtual bool IsActive(unsigned int localIndex) const;
      
      //int AddActiveNode(std::shared_ptr<MultiIndex> newNode);
      int AddActive(std::shared_ptr<MultiIndex>                                        newNode,
                    std::map<std::shared_ptr<MultiIndex>, unsigned int, MultiPtrComp>::iterator iter);
      
      int AddInactive(std::shared_ptr<MultiIndex>                                        newNode,
                      std::map<std::shared_ptr<MultiIndex>, unsigned int, MultiPtrComp>::iterator iter);
      
      void AddForwardNeighbors(unsigned int localIndex, bool addInactive);
      void AddBackwardNeighbors(unsigned int localIndex, bool addInactive);
      
      void Activate(int localIndex);
      void ForciblyActivate(int localIndex, std::vector<unsigned int> &newInds);
      
      
      
      // a vector defining the map between active indices and the local indexing of all active and inactive indices
      std::vector<unsigned int> active2local;
      
      // a vector defining the map between the local indexing scheme and the global indexing scheme of the pool
      std::vector<unsigned int> local2global;
      
      // a map from all local indices to active indices, a negative value is used for non-active indices
      std::vector<int> local2active;
      
      // a vector of sets for the input and output edges.
      std::vector<std::set<int>> outEdges;
      std::vector<std::set<int>> inEdges;
      
      Eigen::VectorXu maxOrders; // the maximum order in each dimension
      
      // the dimension of each multi index
      unsigned int dim;
      
      // the ID of this set into it's pool
      unsigned int setID;
      std::shared_ptr<MultiIndexPool> pool;
      
      // store a MultiIndexLimiter that will tell us the feasible set (i.e. simplex, exp limited, etc...)
      std::shared_ptr<MultiIndexLimiter> limiter;
      
    private:
      
      std::map<std::shared_ptr<MultiIndex>, unsigned int, MultiPtrComp> multi2local; // map from a multiindex to an integer
      
      //std::vector<std::shared_ptr<MultiIndex>> allMultis;
      
      
      MultiIndexSet() = default;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version){
        ar & pool;
        ar & setID;
        ar & active2local;
        ar & local2global;
        ar & local2active;
        ar & outEdges;
        ar & inEdges;
        ar & multi2local;
        ar & maxOrders;
        ar & dim;
        ar & limiter;
      };
      
    }; // class MultiIndexSet
    
  } // namespace Utilities
} // namespace muq




#endif // MULTIINDEXSET_H_