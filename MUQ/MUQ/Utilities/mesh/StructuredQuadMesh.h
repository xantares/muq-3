
#ifndef _StructuredQuadMesh_h
#define _StructuredQuadMesh_h

#include "MUQ/Utilities/mesh/Mesh.h"

namespace muq {
namespace Utilities {
/** \author Matthew Parno
 *  \brief Structured quadrilateral mesh
 *  \details This class implements a structured quadrilater mesh class over a rectangular domain.  It is assumed that
 * all
 *     elements are the same size.  Using this class is very efficient because no lists of nodes or lists of elements
 * are
 *     needed.
 */
template<unsigned int dim>
class StructuredQuadMesh : public Mesh<dim> {
public:

  typedef std::shared_ptr<StructuredQuadMesh> Ptr;

  /** Default constructor to build an empty mesh with no nodes or elements. */
  StructuredQuadMesh();

  /** The usual constructor taking the bounds and number or elements.
   *  @param[in] lbIn Lower bounds
   *  @param[in] ubIn Upper bounds
   *  @param[in] NIn Number of elements in each direction
   */
  StructuredQuadMesh(const Eigen::Matrix<double, dim, 1>& lbIn,
                     const Eigen::Matrix<double, dim, 1>& ubIn,
                     const Eigen::Matrix<unsigned int, dim, 1>& NIn);

  /** Get the position of the a node in the mesh
   *  @param[in] node The node number
   *  @return A vector holding the spatial coordinates of the node.
   */
  virtual Eigen::Matrix<double, dim, 1>                               GetNodePos(unsigned int node) const;

  /** Get the geometric center of an element in the mesh.
   *  @param[in] ele The element number
   *  @return A vector holding the spatial coordinates of the element center.
   */
  virtual Eigen::Matrix<double, dim, 1>                               GetElePos(unsigned int ele) const;

  /** Return a vector of unsigned integers for the center nodes around an element.
   *  @param[in] ele Element of interest
   *  @return List of nodes for element ele.
   */
  virtual Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> GetNodes(unsigned int ele) const;

  /** Get the area or volume of an element.
   *  @param[in] ele Element of interest.
   *  @return The length/area/volume of element ele.
   */
  virtual double                                                      EleSize(unsigned int ele) const;

  /** Get the number of nodes in the mesh. */
  virtual unsigned int                                                NumNodes() const;

  /** Get the number of elements in the mesh. */
  virtual unsigned int                                                NumEles() const;

  /** Return the one dimensional mesh definitions. */
  virtual std::vector<Eigen::VectorXd>                                GetSeparableMeshes() const;

  virtual Eigen::Matrix<double, dim, 1>                               GetLowerBounds() const
  {
    return lb;
  }

  virtual Eigen::Matrix<double, dim, 1> GetUpperBounds() const
  {
    return ub;
  }

protected:

  // number of elements in each direction
  Eigen::Matrix<unsigned int, dim, 1> N;

  // bounds and size in each direction
  Eigen::Matrix<double, dim, 1> lb;
  Eigen::Matrix<double, dim, 1> ub;
  Eigen::Matrix<double, dim, 1> dx;
};

// class StructuredQuadMesh
} // namespace Utilities
} // namespace muq

#endif // ifndef _StructuredQuadMesh_h
