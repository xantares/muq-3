#ifndef QUADRATUREPYTHON_H_
#define QUADRATUREPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

namespace muq {
  namespace Utilities {
    void ExportQuadratureFamily();//
    void ExportClenshawCurtisQuadrature();//
    void ExportExpGrowthQuadratureFamily();//
    void ExportGaussChebyshevQuadrature();//
    void ExportGaussHermiteQuadrature();//
    void ExportGaussProbHermiteQuadrature();//
    void ExportGaussianQuadratureFamily();//
    void ExportGaussLegendreQuadrature();//
    void ExportGaussPattersonQuadrature();//
    void ExportLinearGrowthQuadratureFamily();//
    //void ExportQuadrature();
  }
}

#endif