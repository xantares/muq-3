#include <iostream>
#include <boost/function_types/components.hpp>
#include <boost/function_types/result_type.hpp>
#include <boost/make_shared.hpp>
#include <boost/mpl/insert.hpp>
#include <boost/python.hpp>

namespace muq {
namespace Utilities {

/// Force the initializeiation of boost::python::wrapper 
/**
   If a class inherits boost::python::wrapper it does not always initialize it correctly (especially if it is created with static or external functions).  This class helps force it to initialize.
 */
template <typename Fn>
class WrapperConstructor
{
public:

  typedef typename boost::function_types::result_type<Fn>::type result_type;

  /// Constructor.
  WrapperConstructor(Fn fn) : Constructor(boost::python::make_constructor(fn)) {}

  /// Construct and initialize python object.
  result_type operator()(boost::python::object self)
  {
    Constructor(self);
    return initialize(self);
  }

  /// Construct and initialize python object.
  /**
     Uses the python object's construction to make the object.
   */
  template<typename ... Args>
    result_type operator()(boost::python::object self, Args ... args) 
    {
      Constructor(self, args ...);

      return initialize(self);
    }

private:

  /// Explicitly initialize the wrapper.
  static result_type initialize(boost::python::object self)
  {
    // Extract holder from self.
    result_type ptr = boost::python::extract<result_type>(self);

    // Explicitly initialize the boost::python::wrapper hierarchy.
    initialize_wrapper(self.ptr(),        // PyObject.
                       get_pointer(ptr)); // wrapper hierarchy.

    return ptr;
  }

private:
  boost::python::object Constructor;
};

/// Makes a wrapper constructor for a python wrapper class
/**
   The boost::python::wrapper class, a parent class for python wrappers, is not always initialized correctly when the class is created with static or external Create or Construct methods, especially if the class requires the user to implement a virtual function in python.  In such cases, use this function instead of boost::python::make_constructor forces boost::python::wrapper to initialize correctly.
 */
template <typename Fn>
boost::python::object MakeStaticConstructor(Fn fn)
{
  // Python constructors take the instance/self argument as the first
  // argument.  Thus, inject the 'self' argument into the provided
  // constructor function type.
  typedef typename boost::function_types::components<Fn>::type components_type;
  typedef typename boost::mpl::begin<components_type>::type begin;
  typedef typename boost::mpl::next<begin>::type self_pos;
  typedef typename boost::mpl::insert<components_type, self_pos, boost::python::object>::type signature_type;

  // Create a callable python object that defers to the WrapperConstructor.
  return boost::python::make_function(WrapperConstructor<Fn>(fn), boost::python::default_call_policies(), signature_type());
}

} // namespace muq
} // namespace Utilities
