
#ifndef PYTHONTRANSLATOR_H_
#define PYTHONTRANSLATOR_H_

#include <boost/property_tree/ptree.hpp>


#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/VectorTranslater.h"

namespace muq {
namespace Utilities {
/// Try to add an element from a python dictionary to a boost ptree
template<typename T>
bool TrySetPtreeVal(boost::property_tree::ptree& pt, std::string const& key, boost::python::object& val)
{
  boost::python::extract<T> ex(val);

  if (ex.check()) {
    pt.put<T>(key, ex());
    return true;
  }
  return false;
}

/// Translate python dictionary into boost ptree
boost::property_tree::ptree PythonDictToPtree(boost::python::dict const& dict);

/// Translate Eigen::Vector to boost::python::list
template<typename EigenVec>
inline boost::python::list GetPythonVector(EigenVec& input)
{
  muq::Utilities::Translater<EigenVec, boost::python::list> trans(input);

  return *trans.GetPtr();
}

/// Translate constant Eigen::Vector to boost::python::list
template<typename EigenVec>
inline boost::python::list GetPythonVector(EigenVec const& input)
{
  muq::Utilities::Translater<EigenVec, boost::python::list> trans(input);

  return *trans.GetPtr();
}

/// Translate boost::python::list to Eigen::Vector
template<typename EigenVec>
inline EigenVec GetEigenVector(boost::python::list& input)
{
  muq::Utilities::Translater<boost::python::list, EigenVec> trans(input, (int)boost::python::len(input));

  return *trans.GetPtr();
}

/// Translate constant boost::python::list to Eigen::Vector
template<typename EigenVec>
inline EigenVec GetEigenVector(boost::python::list const& input)
{
  muq::Utilities::Translater<boost::python::list, EigenVec> trans(input, (int)boost::python::len(input));

  return *trans.GetPtr();
}

/// Convert a list of python lists to a std::vector of Eigen::VectorXds
template<typename vec = Eigen::VectorXd>
inline std::vector<vec> PythonListToVector(boost::python::list const& pyInputs)
{
  const int numInputs = boost::python::len(pyInputs);

  std::vector<vec> inputs(numInputs);

  for (int i = 0; i < numInputs; ++i) {
    inputs[i] =
      muq::Utilities::GetEigenVector<vec>(boost::python::extract<boost::python::list>(pyInputs[i]));
  }

  return inputs;
}

/// Convert vectors of Eigen::Vectors to lists of lists
boost::python::list VectorToPythonList(std::vector<Eigen::VectorXd> const& input);

/// Translate double boost::python::list (list of lists) to Eigen::MatrixXd
template<typename scalarType=double>
Eigen::Matrix<scalarType,Eigen::Dynamic,Eigen::Dynamic> GetEigenMatrix(boost::python::list& pyMatrix);

/// Translate constant double boost::python::list (list of lists) to Eigen::MatrixXd
template<typename scalarType=double>
Eigen::Matrix<scalarType,Eigen::Dynamic,Eigen::Dynamic> GetEigenMatrix(boost::python::list const& pyMatrix);

/// Translate Eigen::MatrixXd to a double boost::python::list (list of lists)
template<typename scalarType>
boost::python::list GetPythonMatrix(Eigen::MatrixXd& input){
  
  boost::python::list pyMatrix;
  
  for (int i = 0; i < input.rows(); ++i) {
    boost::python::list row = GetPythonVector<Eigen::Matrix<scalarType,Eigen::Dynamic,1>>(input.row(i));
    pyMatrix.append(row);
  }
  return pyMatrix;
}

/// Translate constant Eigen::MatrixXd to a double boost::python::list (list of lists)
template<typename scalarType>
  boost::python::list GetPythonMatrix(Eigen::Matrix<scalarType,Eigen::Dynamic,Eigen::Dynamic> const& input){
  boost::python::list pyMatrix;
  
  for (int i = 0; i < input.rows(); ++i) {
    boost::python::list row = GetPythonVector<Eigen::Matrix<scalarType,Eigen::Dynamic,1>>(input.row(i));
    pyMatrix.append(row);
  }
  return pyMatrix;
}
  
/// Convert boost::python scalar into unsigned int
template<>
inline unsigned int ConvertScalar<>(boost::python::api::proxy<boost::python::api::const_item_policies> const& v2)
{
  return boost::python::extract<unsigned int>(v2);
}

/// Convert boost::python scalar into int
template<>
inline int ConvertScalar<>(boost::python::api::proxy<boost::python::api::const_item_policies> const& v2)
{
  return boost::python::extract<int>(v2);
}

/// Convert boost::python scalar into double
template<>
inline double ConvertScalar<>(boost::python::api::proxy<boost::python::api::const_item_policies> const& v2)
{
  return boost::python::extract<double>(v2);
}

/// translate Eigen::VectorXd to a boost::python::list
void VectorTranslate(Eigen::VectorXd const& Input, boost::python::list& Output, int size);

/// translate generic vector to a boost::python::list
template<typename vec>
inline void VectorTranslate(vec const& Input, boost::python::list& Output, int size)
{
  for (int i = 0; i < size; ++i) {
    Output.append(Input[i]);
  }
}

/// translater specialized for boost::python::list->T2
template<typename T2>
class Translater<boost::python::list, T2> : public EigenTranslater {
public:

  /// Const Constructor

  /**
   *  @param[in] vec1 A vector of type boost::python that we wish to translate to a vector of type T2.
   */
  Translater(boost::python::list const& vec1, int VecSizeIn)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<boost::python::list *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
    ForwardTranslate(); // translate from boost::python::list to T2
  }

  /// Usual Constructor

  /**
   *  @param[in] vec1 A vector of type boost::python that we wish to translate to a vector of type T2.
   */
  Translater(boost::python::list& vec1, int VecSizeIn)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<boost::python::list *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
    ForwardTranslate(); // translate from boost::python::list to T2
  }

  /// Destructor.
  ~Translater() {}

  /// Return a pointer to the translated vector.

  /**
   *  @return A pointer to a T2 vector translated from constructor input
   */
  T2* GetPtr()
  {
    return vec2ptr;
  }

  virtual Eigen::VectorXd* GetEigenPtr()
  {
    if (std::is_same<T2, Eigen::VectorXd>::value) {
      return reinterpret_cast<Eigen::VectorXd *>(vec2ptr);
    } else {
      std::cerr << "ERROR: Attempt to access Eigen::VectorXd pointer from different type.\n";
      throw("Invalid template parameter.");
      return nullptr;
    }
    return nullptr;
  }

private:

  /// Translate the boost::python::list type vector to an internally stored T2 type vector.
  void ForwardTranslate() // strange to native
  {
    VectorTranslate(*vec1ptr, this->vec2, this->VecSize);
    this->vec2ptr = &(this->vec2);
  }

  // keep a pointer to the original untranslated vector
  boost::python::list *vec1ptr;

  // keep a pointer tot he translated vector
  T2 *vec2ptr;

  // in some cases we will create a vector of type T2 to hold the new vector
  T2 vec2;

  // store the size of the vector -- this is important for handling double pointers
  unsigned int VecSize;
};

// end of class translater specialized for boost::python::list->T2

/// translater specialized for T1->boost::python::list
template<typename T1>
class Translater<T1, boost::python::list> : public EigenTranslater {
public:

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type boost::python::list.
   */
  Translater(const T1& vec1)
  {
    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
    ForwardTranslate(); // translate from T1 to boost::python::list
  }

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type boost::python::list.
   */
  Translater(const T1& vec1, int VecSizeIn)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
    ForwardTranslate(); // translate from T1 to boost::python::list
  }

  /** Destructor. */
  ~Translater() {}

  /** Return a pointer to the translated vector.
   *  @return A pointer to a boost::python::list vector translated from constructor input
   */
  boost::python::list* GetPtr()
  {
    return vec2ptr;
  }

  virtual Eigen::VectorXd* GetEigenPtr()
  {
    if (std::is_same<boost::python::list, Eigen::VectorXd>::value) {
      return reinterpret_cast<Eigen::VectorXd *>(vec2ptr);
    } else {
      std::cerr << "ERROR: Attempt to access Eigen::VectorXd pointer from different type.\n";
      throw("Invalid template parameter.");
      return nullptr;
    }
    return nullptr;
  }

private:

  /** Translate the T1 type vector to an internally stored boost::python::list type vector. */
  void ForwardTranslate() // strange to native
  {
    VectorTranslate(*vec1ptr, this->vec2, this->VecSize);
    this->vec2ptr = &(this->vec2);
  }

  /** Translate the potentially changed boost::python::list type vector to a T1 type vector. */
  void BackwardTranslate() // native to strange
  {
    VectorTranslate(vec2, *vec1ptr, VecSize);
  }

  // keep a pointer to the original untranslated vector
  T1 *vec1ptr;

  // keep a pointer tot he translated vector
  boost::python::list *vec2ptr;

  // in some cases we will create a vector of type boost::python::list to hold the new vector
  boost::python::list vec2;

  // store the size of the vector -- this is important for handling double pointers
  unsigned int VecSize;
};

// end translater for T1->boost::python::list
}
}

#endif // ifndef PYTHONTRANSLATOR_H_
