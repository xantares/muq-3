#ifndef MULTIINDEXPYTHON_H_
#define MULTIINDEXPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

namespace muq {
  namespace Utilities {
    void ExportMultiIndex();
    void ExportMultiIndexSet();
    void ExportMultiIndexFactory();
  }
}

#endif