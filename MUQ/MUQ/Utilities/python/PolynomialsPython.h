#ifndef POLYNOMIALSPYTHON_H_
#define POLYNOMIALSPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/ProbabilistHermite.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/Monomial.h"

namespace muq {
  namespace Utilities {
    void ExportRecursive();
    void ExportHermite();
    void ExportProbHermite();
    void ExportLegendre();
    void ExportMonomial();
  }
}

#endif