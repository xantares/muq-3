
#ifndef JOBMANAGER_H
#define JOBMANAGER_H

#include <map>

#include <boost/property_tree/ptree.hpp>


namespace muq {
namespace Utilities {
///Uses all the available MPI nodes to evaluate the jobs provided. Calls the provided function
///to test whether the job's HDF5 result has been recorded already.
class JobManager {
public:

  JobManager() = default;

  virtual ~JobManager() = default;

  /**
   * Takes XML like this:
   *
   * <Job>
   * <Function>RegisteredFnName</Function>
   * <Data> whatever, including subtree </Data>
   * <NumberOfCopies>1</NumberOfCopies> [optional]
   * </Job>
   *
   * Calls the function with everything below the data tag. Inserts CopyNumber
   * tag into data.
   **/
  static void RunJobs(boost::property_tree::ptree const& jobList);

private:

  typedef std::map < std::string, std::pair < std::function<void (boost::property_tree::ptree&)>,
    std::function<std::string(boost::property_tree::ptree&) >>> AvailableJobMap;

public:

  ///Globally available way to get the map that tells you what MCMC methods are available.
  static std::shared_ptr<AvailableJobMap> GetJobMap();
};
}

//Pass in the function name, and a dataset name generating function, as desired, or nullptr
#define REGISTER_JOB_FUNCTION(FNNAME, DATASETNAME) \
  static auto registered_pair_ ## FNNAME =         \
    JobManager::GetJobMap()->insert(std::make_pair(# FNNAME, std::make_pair(FNNAME, DATASETNAME)));
}

#endif // JOBMANAGER_H
