#ifndef VARIABLECOLLECTION_H_
#define VARIABLECOLLECTION_H_

#include "MUQ/Utilities/Variable.h"

#include <vector>


#include <boost/serialization/access.hpp>
#include <boost/serialization/export.hpp>

namespace muq {
 namespace Utilities {

class RecursivePolynomialFamily1D;
class QuadratureFamily1D;

/**@class VariableCollection
    @ingroup Quadrature
    @ingroup PolynomialChaos
 @brief A class to hold several instances of the Variable class.
 @details A set of variables used to define custom functions or
 * representations because each dimension may have different properties, i.e. different quadrature rules and polynomial values.  The implementation of VariableCollection is essentially a vector of shared_ptr's to instances of the Variable class.  Thus, the PushVariable methods have the same functionality as std::vector's push_back, and the EmplaceVariable function has the same functionality of std::vector's emplace_back.
 
     If possible, the recommended way of adding to the VariableCollection is through the EmplaceVariable function, which minimizes the number of temporary variables needed to grow the VariableCollection.
 @see Variable
 */
class VariableCollection {
public:

  typedef std::shared_ptr<VariableCollection> Ptr;

  VariableCollection();
  virtual ~VariableCollection();

  ///Add a new variable to the back.

  /** Add a previously defined variable to this collection.
   * @param[in] newVar probably called with "make_shared<Variable>(...)".
   */
  void PushVariable(Variable::Ptr newVar);
  
  
  /** Construct a variable and add it to the VariableCollection.  Note that versions of this function exist to mimic the structure of all Variable constructors.
   @param[in] name The name of the variable to create
   @param[in] polyFamily A shared_ptr to the polynomial family used by the created variable
   @param[in] quadFamily A shared_ptr to the quadrature family used by the created variable
   */
  void PushVariable(std::string                                  name,
                    std::shared_ptr<RecursivePolynomialFamily1D> polyFamily,
                    std::shared_ptr<QuadratureFamily1D>          quadFamily);
  
  /** Construct a variable and add it to the VariableCollection.  Note that versions of this function exist to mimic the structure of all Variable constructors.
   @param[in] name The name of the variable to create
   @param[in] quadFamily A shared_ptr to the quadrature family used by the created variable
   */
  void PushVariable(std::string                                  name,
                    std::shared_ptr<QuadratureFamily1D>          quadFamily);
  
  /** Construct a variable and add it to the VariableCollection.  Note that versions of this function exist to mimic the structure of all Variable constructors.
   @param[in] name The name of the variable to create
   @param[in] polyFamily A shared_ptr to the polynomial family used by the created variable
   */
  void PushVariable(std::string                                  name,
                    std::shared_ptr<RecursivePolynomialFamily1D> polyFamily);
  
  
  ///Return the number of variables.
  unsigned int  length();

  ///return a reference to the ith variable.
  Variable::Ptr GetVariable(unsigned int i);

private:

  ///Make estimates serializable
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);

  ///The list of variables
  std::vector<Variable::Ptr> variables;
};
}
}


//BOOST_CLASS_EXPORT_KEY(VariableCollection)

#endif /* VARIABLECOLLECTION_H_ */
