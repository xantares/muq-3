
#ifndef _LanczosEigenSolver_h
#define _LanczosEigenSolver_h

#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/src/Jacobi/Jacobi.h>
#include <Eigen/Geometry>
#include <boost/graph/buffer_concepts.hpp>
#include <exception>

namespace muq {
namespace Utilities {
/** Exception thrown when the implicitly restarted Lanczos iteration fails to converge. */
class LanczosFail : public std::exception {
  virtual const char* what() const throw()
  {
    return "The Lanczos iteration failed to converge.  Make sure the input matrix is positive definite.";
  }
};

//        double PolyEval(const double &x, const Eigen::VectorXd &a, const Eigen::VectorXd &b, int r){
//
//
//            if(r==-1){
//                return 1;
//            }else if(r==0){
//                return (a[r] - x)*PolyEval(x,a,b,r-1);
//            }else{
//                return (a[r] - x)*PolyEval(x,a,b,r-1) - b[r-1]*b[r-1]*PolyEval(x,a,b,r-2);
//            }
//        }
//
//        unsigned int SignCount(const double &x, const Eigen::VectorXd &a, const Eigen::VectorXd &b, int r){
//
//            int aout = 0;
//            double q = 1;
//            q = a[0] - x;
//            if(q<0)
//                ++aout;
//
//            for(int i=1; i<=r; ++i){
//                if(q!=0){
//                    q = a[i] - x - b[i-1]*b[i-1]/q;
//                }else{
//                    q = a[i] - x - fabs(b[i])/1.0e-14;
//                }
//
//                if(q<0)
//                    ++aout;
//            }
//
//            return aout;
//
//        }

/** This class computes the eigenvalues and eigenvectors of a Symmetric Positive Definite matrix using the Lanczos
 *  method.  All that is required from the Matrix type is a * operator taking in and Eigen::VectorXd and returning and
 *  Eigen::VectorXd.  The syntax of this class tries to follow the Eigen::EigenSolver class by providing the
 *  "eigenvalues()" function and "eigenvectors()" function.
 */
class LanczosEigenSolver {
public:

  LanczosEigenSolver(int N = 1) : Nkeep(N) {}

  /** Compute the eigen values and vectors given a matrix of type MatType.  Note that MatType only needs to offer a
   *  multiplication operator "*" that takes an Eigen::VectorXd as input and returns an Eigen::VectorXd of the same
   *  dimension.  The rows() and cols() functions also need to be provided by the matrix.*/
  template<typename MatType>
  void compute(const MatType& matrix)
  {
    // get the problem size
    dim = matrix.rows();
    double mult    = std::max(sqrt(dim), 10.0);
    unsigned int p = std::ceil(mult * Nkeep); // this is a pure heuristic I'm using after some trial and error
    unsigned int m = std::min(Nkeep + p, dim);
    p = m - Nkeep;

    // bookkeeping vectors
    //v = Eigen::VectorXd::Zero(dim);
    beta  = Eigen::VectorXd::Zero(m - 1);
    alpha = Eigen::VectorXd::Zero(m);

    // start with a random vector
    wz = Eigen::MatrixXd::Zero(dim, m);
    wz.col(0).setRandom();
    wz.col(0) /= wz.col(0).norm();

    Eigen::VectorXd r = Eigen::VectorXd::Zero(dim);
    MstepLanczos(m, 0, matrix, r);

    if (m == dim) {
      Eigen::VectorXd ritzVals; // ritz values
      Eigen::MatrixXd ritzVecs; // rotated ritz vectors
      tridiagonal_eig(alpha, beta, m, ritzVals, ritzVecs);

      EigenValues  = ritzVals.head(Nkeep);
      EigenVectors = wz * ritzVecs.topLeftCorner(m, Nkeep);
      return;
    }

    unsigned int MaxIts = 1000;
    unsigned int ii;
    for (ii = 0; ii < MaxIts; ++ii) {
      // compute the shifts
      Eigen::VectorXd mu = tridiagonal_eig(alpha, beta, m);

      // implicit qr algorithm
      Eigen::MatrixXd Q = Eigen::MatrixXd::Identity(m, m);
      for (unsigned int j = 0; j < p; ++j) {
        tridiagonal_qr_step_shift<Eigen::ColMajor>(alpha.data(), beta.data(), 0, m - 1, Q.data(), m, mu(j + Nkeep));
      }

      r                            = (wz.col(Nkeep) * beta[Nkeep - 1] + r * Q(m - 1, Nkeep - 1)).eval();
      wz.topLeftCorner(dim, Nkeep) = (wz * Q.topLeftCorner(m, Nkeep)).eval();


      beta[Nkeep - 1] = r.norm();

      // perform some more Lanczos iterations
      MstepLanczos(m, Nkeep, matrix, r);

      // check for convergence
      if ((beta.head(Nkeep).maxCoeff() < 1e-8) || (beta(Nkeep - 1) == 0)) {
        break;
      }
    }


    if (ii >= MaxIts) {
      throw LanczosFail();
    } else {
      Eigen::VectorXd ritzVals; // ritz values
      Eigen::MatrixXd ritzVecs; // rotated ritz vectors
      tridiagonal_eig(alpha, beta, m, ritzVals, ritzVecs);

      EigenValues  = ritzVals.head(Nkeep);
      EigenVectors = wz * ritzVecs.topLeftCorner(m, Nkeep);
    }
  }

  /** This function returns the eigen values in a vector.*/
  Eigen::VectorXd& eigenvalues()
  {
    return EigenValues;
  }

  /** This function returns the eigen matrix in a vector. */
  Eigen::MatrixXd& eigenvectors()
  {
    return EigenVectors;
  }

private:

  /** Compute and M-step lanczos factorization starting at index k+1 and ending at M */
  template<typename MatType>
  void MstepLanczos(unsigned int M, unsigned int kstart, const MatType& A, Eigen::VectorXd& v)
  {
    unsigned int k = kstart;

    while (k < M) {
      v = A * wz.col(k);
      if (k != 0) {
        v -= beta[k - 1] * wz.col(k - 1);
      }
      alpha[k] = v.dot(wz.col(k));
      v       -= alpha[k] * wz.col(k);

      if (k == M - 1) {
        break;
      }

      if (k != 0) {
        v -= (v.dot(wz.col(k - 1)) * wz.col(k - 1)).eval();
      }

      v -= (v.dot(wz.col(k)) * wz.col(k)).eval();

      // reorthoganalize v
      for (unsigned int ii = 0; ii < k; ++ii) {
        v -= (v.dot(wz.col(ii)) * wz.col(ii)).eval();
      }

      beta[k]       = v.norm();
      wz.col(k + 1) = v / beta[k];
      ++k;

      //std::cout << alpha.transpose() << std::endl << std::endl;
      //                     if(k!=0){
      //                         Eigen::VectorXd t = wz.col(k);
      //                         wz.col(k) = v/beta[k-1];
      //                         v = -beta[k-1]*t;
      //                     }
      //
      //                     v += A*wz.col(k);
      //
      //                     // get the projection of v onto w
      //                     alpha[k] = wz.col(k).dot(v);
      //
      //                     // remove this component of v
      //                     v -= alpha[k]*wz.col(k);
      //
      //
      //                     if(k==M-1)
      //                         break;
      //
      //                     // reorthoganalize wz
      //                  for(unsigned int jj=0; jj<k; ++jj){
      //                      wz.col(k) -= (wz.col(k).dot(wz.col(jj))*wz.col(jj)).eval();
      //                  }
      //
      //                    // reorthoganalize
      //                    if(k>2){
      //
      //
      //                        // use a modified gram-schmidt procedure to orthoganalize v
      //                        Eigen::VectorXd newv = v;
      //                        for(unsigned int i=0; i<k; ++i){
      //                          double mag = newv.dot(wz.col(i));
      //                          newv -= mag*wz.col(i);
      //                        }
      //                            /*
      //                            Eigen::VectorXd s = wz.topLeftCorner(dim,k).transpose()*v;
      //                            v -= wz.topLeftCorner(dim,k)*s;*/
      //                          alpha[k] += newv.dot(wz.col(k-1));//s[k-1];
      //                          beta[k] += newv.dot(wz.col(k-2));//s[k-2];
      //                          v = newv;
      //
      //                    }else if(k==1){
      //                      Eigen::VectorXd newv = v;
      //                      double mag = newv.dot(wz.col(0));
      //                      newv -= mag*wz.col(0);
      //                      alpha[k] += newv.dot(wz.col(0));
      //                      v = newv;
      //                    }
      //
      //                    wz.col(k+1) = wz.col(k);
      //
      //                     // get the scale
      //                     beta[k] = v.norm();
      //
      //                     ++k;
    }


    //       double rho = 0.1;
    //
    //                for(unsigned int j=k+1; j<M; ++j){
    //            v.col(j) = r/beta[j-1];
    //            r = A*v.col(j) - v.col(j-1)*beta[j-1];
    //            alpha[j] = v.col(j).dot(r);
    //            r -= v.col(j)*alpha[j];
    //
    //            if(r.norm()<rho*sqrt(alpha[j]*alpha[j] + beta[j-1]*beta[j-1])){
    //
    //              Eigen::VectorXd s = v.topLeftCorner(dim,j).transpose()*r;
    //              r -= v.topLeftCorner(dim,j)*s;
    //              alpha[j] += s[j];
    //              beta[j] += s[j-1];
    //            }
    //
    //         }
  }

  static void tridiagonal_eig(const Eigen::VectorXd& alphaIn,
                              const Eigen::VectorXd& betaIn,
                              int                    MaxJ,
                              Eigen::VectorXd      & eigVals,
                              Eigen::MatrixXd      & eigVecs)
  {
    Eigen::VectorXd alpha = alphaIn;
    Eigen::VectorXd beta  = betaIn;

    int n = std::min(MaxJ, int(alpha.size()));

    eigVecs = Eigen::MatrixXd::Identity(n, n);

    // use the internal Eigen qr step to get the eigenvalues and eigenvectors of the tridiagonal system
    int end   = n - 1;
    int start = 0;
    int iter  = 0; // total number of iterations

    // QR algorithm loop -- note that this calls the internal Eigen tridiagonal_qr_step function
    while (end > 0) {
      for (int i = start; i < end; ++i) {
        if (Eigen::internal::isMuchSmallerThan(abs(beta[i]), (abs(alpha[i]) + abs(alpha[i + 1])))) {
          beta[i] = 0;
        }
      }

      // find the largest unreduced block
      while (end > 0 && beta[end - 1] == 0) {
        end--;
      }
      if (end <= 0) {
        break;
      }

      // if we spent too many iterations, we give up
      iter++;
      if (iter > 2 * n) {
        break;
      }

      start = end - 1;
      while (start > 0 && beta[start - 1] != 0) {
        start--;
      }

      Eigen::internal::tridiagonal_qr_step<Eigen::ColMajor>(alpha.data(), beta.data(), start, end, eigVecs.data(), n);
    }

    eigVals = alpha;

    // Sort eigenvalues and corresponding vectors in order of ascending eigevalue.
    for (int i = 0; i < n - 1; ++i) {
      int k;
      eigVals.segment(i, n - i).maxCoeff(&k);
      if (k > 0) {
        std::swap(eigVals[i], eigVals[k + i]);
        eigVecs.col(i).swap(eigVecs.col(k + i));
      }
    }
  }

  // Return a vector holding the p smallest eigenvalues in the tridiagonal matrix
  static Eigen::VectorXd tridiagonal_eig(const Eigen::VectorXd& diag, const Eigen::VectorXd& sub, unsigned int p)
  {
    Eigen::VectorXd alpha = diag;
    Eigen::VectorXd beta  = sub;

    // we want to compute the p smallest eigenvalues
    int n = std::min(int(p), int(alpha.size()));

    // use the internal Eigen qr step to get the eigenvalues and eigenvectors of the tridiagonal system
    int end   = n - 1;
    int start = 0;
    int iter  = 0; // total number of iterations

    // QR algorithm loop -- note that this calls the internal Eigen tridiagonal_qr_step function
    while (end > 0) {
      for (int i = start; i < end; ++i) {
        if (Eigen::internal::isMuchSmallerThan(abs(beta[i]), (abs(alpha[i]) + abs(alpha[i + 1])))) {
          beta[i] = 0;
        }
      }

      // find the largest unreduced block
      while (end > 0 && beta[end - 1] == 0) {
        end--;
      }
      if (end <= 0) {
        break;
      }

      // if we spent too many iterations, we give up
      iter++;
      if (iter > 2 * n) {
        break;
      }

      start = end - 1;
      while (start > 0 && beta[start - 1] != 0) {
        start--;
      }

      Eigen::internal::tridiagonal_qr_step<Eigen::ColMajor>(alpha.data(), beta.data(), start, end, (double *)0, n);
    }

    // Sort eigenvalues and corresponding vectors in order of ascending eigevalue.
    for (int i = 0; i < n - 1; ++i) {
      int k;
      alpha.segment(i, n - i).maxCoeff(&k);
      if (k > 0) {
        std::swap(alpha[i], alpha[k + i]);
      }
    }

    return alpha.head(p);
  }

  // slightly modified version of the Eigen::internal tridiagonal_qr_step that uses an explicit shift instead of the
  // wilkinson shift
  template<int StorageOrder>
  static void tridiagonal_qr_step_shift(double *diag,
                                        double *subdiag,
                                        int     start,
                                        int     end,
                                        double *matrixQ,
                                        int     n,
                                        double  mu)
  {
    // Note that thanks to scaling, e^2 or td^2 cannot overflow, however they can still
    // underflow thus leading to inf/NaN values when using the following commented code:
    //   RealScalar e2 = abs2(subdiag[end-1]);
    //   RealScalar mu = diag[end] - e2 / (td + (td>0 ? 1 : -1) * sqrt(td*td + e2));
    // This explain the following, somewhat more complicated, version:
    //RealScalar mu = diag[end] - (e / (td + (td>0 ? 1 : -1))) * (e / hypot(td,e));

    double x = diag[start] - mu;
    double z = subdiag[start];

    for (int k = start; k < end; ++k) {
      Eigen::JacobiRotation<double> rot;
      rot.makeGivens(x, z);

      // do T = G' T G
      double sdk  = rot.s() * diag[k] + rot.c() * subdiag[k];
      double dkp1 = rot.s() * subdiag[k] + rot.c() * diag[k + 1];

      diag[k] = rot.c() *
                (rot.c() * diag[k] - rot.s() * subdiag[k]) - rot.s() * (rot.c() * subdiag[k] - rot.s() * diag[k + 1]);
      diag[k + 1] = rot.s() * sdk + rot.c() * dkp1;
      subdiag[k]  = rot.c() * sdk - rot.s() * dkp1;


      if (k > start) {
        subdiag[k - 1] = rot.c() * subdiag[k - 1] - rot.s() * z;
      }

      x = subdiag[k];

      if (k < end - 1) {
        z              = -rot.s() * subdiag[k + 1];
        subdiag[k + 1] = rot.c() * subdiag[k + 1];
      }

      // apply the givens rotation to the unit matrix Q = Q * G
      if (matrixQ) {
        // FIXME if StorageOrder == RowMajor this operation is not very efficient
        Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, StorageOrder> > q(matrixQ, n, n);
        q.applyOnTheRight(k, k + 1, rot);
      }
    }
  }

  // how many eigenvalues/eigenvectors do we want to keep?  If negative, will use a tolerance on the eigenvalues
  int Nkeep;
  unsigned int dim;

  Eigen::VectorXd alpha, beta;
  Eigen::MatrixXd wz;

  // store the eigenvalues in a vector
  Eigen::VectorXd EigenValues;

  // store the eigenvectors in a matrix
  Eigen::MatrixXd EigenVectors;
};

// class LanczosEigenSolver
} // namespace Utilities
} // namespace muq


#endif // ifndef _LanczosEigenSolver_h
