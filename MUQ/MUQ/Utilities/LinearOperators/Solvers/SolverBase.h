
#ifndef _SolverBase_h
#define _SolverBase_h

#include <memory>

namespace muq {
namespace Utilities {
class LinOpBase;

class SolverBase {
public:

  SolverBase() {}

  SolverBase(std::shared_ptr<LinOpBase> Ain) : A(Ain) {}

  virtual void compute(std::shared_ptr<LinOpBase> Ain)
  {
    A = Ain;
  }

  virtual Eigen::VectorXd solve(const Eigen::VectorXd& b) = 0;

protected:

  std::shared_ptr<LinOpBase> A;
};

// class SolverBase
} // namespace Utilities
} // namespace muq

#endif // ifndef _SolverBase_h
