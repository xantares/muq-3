
#ifndef _BiCG_h
#define _BiCG_h

#include <Eigen/Core>

#include "MUQ/Utilities/LinearOperators/Solvers/SolverBase.h"

namespace muq {
namespace Utilities {
class BiCG : public SolverBase {
public:

  BiCG() {}

  BiCG(std::shared_ptr<LinOpBase> Ain) : SolverBase(Ain) {}

  /** Solve the system using the conjugate gradient method. */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& b) override;


protected:

  double tol = 5e-4;
};

// class BiCG
} // namespace Utilities
} // namespace muq

#endif // ifndef _BiCG_h
