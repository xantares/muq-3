
#ifndef _CG_h
#define _CG_h

#include <Eigen/Core>

#include "MUQ/Utilities/LinearOperators/Solvers/SolverBase.h"

namespace muq {
namespace Utilities {
class CG : public SolverBase {
public:

  CG() {}

  CG(std::shared_ptr<LinOpBase> Ain) : SolverBase(Ain) {}

  /** Solve the system using the conjugate gradient method. */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& b) override;

  virtual Eigen::VectorXd sampleCov(const Eigen::VectorXd& b);
  virtual Eigen::VectorXd samplePrec(const Eigen::VectorXd& b);

protected:

  double tol = 1e-13;
};

// class CG
} // namespace Utilities
} // namespace muq

#endif // ifndef _CG_h
