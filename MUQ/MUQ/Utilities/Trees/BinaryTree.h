
#ifndef _BinaryTree_h
#define _BinaryTree_h


// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/any.hpp>

// other muq includes
#include "MUQ/Utilities/Trees/IndexType.h"
#include "MUQ/Utilities/Trees/Splitters.h"
#include "MUQ/Utilities/Trees/Stoppers.h"


#define  REGISTER_TERMINAL_DEC_TYPE(NAME, OutType) \
  static std::shared_ptr < muq::Utilities::DerivedTerminalRegister < OutType, TfrainingTypes ... >> reg;

namespace muq {
namespace Utilities {
template<typename OutType, typename ... TrainingTypes>
class BinaryTreeNode;

template<typename OutType, typename ... TrainingTypes>
struct DerivedTerminalRegister;

/** Define a terminal node -- perhaps where regression occurs. */
template<typename OutType, typename ... TrainingTypes>
class TerminalNode : public BinaryTreeNode<OutType, TrainingTypes ...> {
public:

  virtual OutType Eval(const Eigen::VectorXd& x) const = 0;


  // /////////////////////////////////////////////////////////
  // Register and create code
  // /////////////////////////////////////////////////////////
  typedef std::function < std::shared_ptr <
    TerminalNode<OutType, TrainingTypes ... >>
                 (const Eigen::MatrixXd&, const muq::Utilities::TreeIndexContainer&, int,
                  boost::property_tree::ptree&,
                  TrainingTypes ...)> CreateFuncType;

  ///Typedef for the map used to track available terminal node types.   Not user code.
  typedef std::map<std::string, CreateFuncType> Name2ConstructorMapType;


  static std::shared_ptr < TerminalNode < OutType, TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                               const TreeIndexContainer &inds,
                                                                               int depth,
                                                                               boost::property_tree::ptree & properties,
                                                                               TrainingTypes ... trainInfo)
  {
    // extract the create function object from the register
    auto f =
      TerminalNode<OutType, TrainingTypes ...>::GetConstructorMap()->at(properties.get("Trees.LeafMethod",
                                                                                       "MeanTerminalNode"));

    return f(pts, inds, depth, properties, trainInfo ...);
  };

  ///Globally available way to get the map that tells you what terminal node methods are available.
  static std::shared_ptr<Name2ConstructorMapType> GetConstructorMap()
  {
    static std::shared_ptr<typename TerminalNode<OutType, TrainingTypes ...>::Name2ConstructorMapType> map;

    // if the map doesn't yet exist, create it
    if (!map) {
      map = std::make_shared<typename TerminalNode<OutType, TrainingTypes ...>::Name2ConstructorMapType>();
    }
    return map;
  }

  // /////////////////////////////////////////////////////////
  // /////////////////////////////////////////////////////////
};

// class TerminalNode


template<typename OutType, typename ... TrainingTypes>
struct DerivedTerminalRegister {
  DerivedTerminalRegister(std::string s, typename TerminalNode<OutType, TrainingTypes ...>::CreateFuncType func)
  {
    TerminalNode<OutType, TrainingTypes ...>::GetConstructorMap()->insert(std::make_pair(s, func));
  }
};


template<typename OutType, typename ... TrainingTypes>
class SplitTreeNode : public BinaryTreeNode<OutType, TrainingTypes ...> {
public:

  static std::shared_ptr < SplitTreeNode < OutType, TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                                const TreeIndexContainer &inds,
                                                                                int depth,
                                                                                boost::property_tree::ptree & properties,
                                                                                TrainingTypes ... params)
  {
    // first, create the splitter
    auto tempSplit = Splitter<TrainingTypes ...>::Create(pts, inds, depth, properties, params ...);

    // now split the indices
    auto splitInds = tempSplit->Split(pts, inds);

    // create the left and right nodes
    auto tempLeftNode = BinaryTreeNode<OutType, TrainingTypes ...>::Create(pts,
                                                                           splitInds.first,
                                                                           depth + 1,
                                                                           properties,
                                                                           params ...);
    auto tempRightNode = BinaryTreeNode<OutType, TrainingTypes ...>::Create(pts,
                                                                            splitInds.second,
                                                                            depth + 1,
                                                                            properties,
                                                                            params ...);

    assert(tempLeftNode != nullptr);
    assert(tempRightNode != nullptr);

    return std::make_shared < SplitTreeNode < OutType, TrainingTypes ... >> (tempSplit, tempLeftNode, tempRightNode);
  };


  SplitTreeNode(std::shared_ptr < Splitter < TrainingTypes ... >> SplitIn,
                std::shared_ptr < BinaryTreeNode <                OutType,
                TrainingTypes ... >>                              LeftNodeIn,
                std::shared_ptr < BinaryTreeNode <                OutType,
                TrainingTypes ... >>                              RightNodeIn) : Split(SplitIn), LeftNode(LeftNodeIn),
                                                                                 RightNode(
                                                                                   RightNodeIn)
  {}

  virtual OutType Eval(const Eigen::VectorXd& x) const override
  {
    if (Split->Test(x)) {
      return LeftNode->Eval(x);
    } else {
      return RightNode->Eval(x);
    }
  }

private:

  // create
  std::shared_ptr < Splitter < TrainingTypes ... >> Split;
  std::shared_ptr < BinaryTreeNode < OutType, TrainingTypes ... >> LeftNode;
  std::shared_ptr < BinaryTreeNode < OutType, TrainingTypes ... >> RightNode;
};

// class SplitTreeNode


template<typename OutType, typename ... TrainingTypes>
class BinaryTreeNode {
public:

  /** Create a node (either Splitter or TerminalNode) using the inds columns of pts and the properties in the boost
   *  ptree */
  static std::shared_ptr < BinaryTreeNode < OutType, TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                                 const TreeIndexContainer &inds,
                                                                                 int depth,
                                                                                 boost::property_tree::ptree & properties,
                                                                                 TrainingTypes ... params)
  {
    // first, create the stopper based on the name given in the property tree, if the stopper decides to continue, we'll
    // create the splitter and child nodes
    auto CurrStop = Stopper<TrainingTypes ...>::Create(pts, inds, depth, properties, params ...);

    std::shared_ptr < BinaryTreeNode < OutType, TrainingTypes ... >> outPtr;

    if (CurrStop->StopSplitting()) {
      return std::dynamic_pointer_cast < BinaryTreeNode < OutType, TrainingTypes ... >>
             (TerminalNode<OutType, TrainingTypes ...>::Create(pts, inds, depth, properties, params ...));
    } else {
      return std::dynamic_pointer_cast < BinaryTreeNode < OutType, TrainingTypes ... >>
             (SplitTreeNode<OutType, TrainingTypes ...>::Create(pts, inds, depth, properties, params ...));
    }
  };


  virtual OutType Eval(const Eigen::VectorXd& x) const = 0;
};

// class BinaryTreeNode


template<typename OutType, typename ... TrainingTypes>
class BinaryTree {
public:

  BinaryTree(const Eigen::MatrixXd& pts, boost::property_tree::ptree& properties, TrainingTypes ... args)
  {
    // create a set of all the indices
    TreeIndexContainer inds;

    for (int i = pts.cols() - 1; i >= 0; --i) {
      inds.push_front(i);
    }

    // create the root node (this call will recursively build the tree)
    RootNode = BinaryTreeNode<OutType, TrainingTypes ...>::Create(pts, inds, 0, properties, args ...);
  }

  std::shared_ptr < BinaryTreeNode < OutType, TrainingTypes ... >> RootNode;
};

// class BinaryTree
} // namespace Utilities
} // namespace muq

#endif // ifndef _BinaryTree_h
