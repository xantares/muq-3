#ifndef RECURSIVEPOLYNOMIALFAMILY1D_H_
#define RECURSIVEPOLYNOMIALFAMILY1D_H_

#include <boost/property_tree/ptree.hpp> // needed here to avoid weird toupper bug on osx

#include "MUQ/config.h"
#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include <Eigen/Core>

#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>



///An abstract class for stable evaluation of orthogonal 1D polynomial families.

/**
 @defgroup Polynomials1D
 @ingroup Utilities
 @brief Definitions of 1D polynomials and tools for manipulating them.
 @details One dimensional polynomials are a basic building block for polynomial chaos expansions, transport maps, and many other settings.  This group contains classes that define polynomials through three-term recurrence relations.  Evaluations, derivatives, and second derivatives are all supported, as well as root finding and translation to monomials.
*/

namespace muq {
  namespace Utilities {
    
    /**
     * @class RecursivePolynomialFamily1D
     * @ingroup Polynomials1D
     * @brief Base class for 1D polynomials defined by a three-term recurrence.
     * @details
     * Evaluate a polynomial family based on the three term recurrence:
     * phi_{k+1}(x) + alpha_k(x)*phi_k(x) + beta_k(x)*phi_{k-1}(x) = 0;
     *
     * Subclasses specialize for particular families of polynomials, and only provide
     * a specification of the polynomial family. Specifically, sub-classes provide
     * implementations of alpha, beta, and the base terms, phi_0(x) and phi_1(x).
     *
     * Subclasses are also expected to provide a way of determining the normalization
     * of the polynomial.
     *
     * For single evaluations (the evaluate method), this class implements the highly
     * stable Clenshaw algorithm, which recursively applies the recurrence to determine
     * the values. However, when multiple evaluations are computed (using evaluateAll),
     * the basic three term recurrence is used directly.  For high orders, or evaluations
     * near the roots of the polynomial, this direct use can be unstable.  That said,
     * implementations should independently verify accuracy for their particular families.
     *
     * From our limited tests with Hermite and Legendre polynomials, we have found the two approaches
     * to produce nearly identical results (to within floating point precision) for orders up
     * to around 7. After this, there was a very minor difference between the two approaches for some
     * evaluation points.  However, the difference was minor (~1e-14) for orders into the 50s.
     *
     * Clenshaw algorithm from: http://en.wikipedia.org/wiki/Clenshaw_algorithm
     */
    class RecursivePolynomialFamily1D{
    public:
      
      typedef std::shared_ptr<RecursivePolynomialFamily1D> Ptr;
      
      RecursivePolynomialFamily1D() = default;
      virtual ~RecursivePolynomialFamily1D() = default;
      
      virtual double evaluate(unsigned int order, double x) const = 0;
      virtual Eigen::VectorXd evaluateAll(unsigned int maxOrder, double x) const = 0;
      virtual double GetNormalization(unsigned int n) const = 0;
      virtual double gradient(unsigned int order, double x) const = 0;
      virtual double secondDerivative(unsigned int order, double x) const = 0;
      
    protected:
      static double MonomialEvaluate(const Eigen::VectorXd &P, double x);
      static void MonomialDivision(const Eigen::VectorXd &A, const Eigen::VectorXd &B, Eigen::VectorXd &Q, Eigen::VectorXd &R);
      static Eigen::VectorXd MonomialRoots(const Eigen::VectorXd &Pin, double tol);
      
      
    private:
      
      static inline int sgn(double val) {
        return ((0.0 < val) - (val < 0.0));
      };

      
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive& ar, const unsigned int version){};
    };
    
    
    /**
     @class Static1DPolynomial
     @ingroup Polynomials1D
     @brief Child of RecursivePolynomialFamily1D enabling the curiously recuring template patter.
     @details This class allows children to implement their methods as static members.  In some cases this can speed up evaluation and in other cases it allows for easier parallelism because the polynomials can be evaluated without having to instantiate a class.
     */
    template<class PolyChild>
    class Static1DPolynomial : public RecursivePolynomialFamily1D{
    public:
      
      typedef std::shared_ptr<Static1DPolynomial> Ptr;
      
      Static1DPolynomial() = default;
      virtual ~Static1DPolynomial() = default;
      
      
      virtual double evaluate(unsigned int order, double x) const override{return evaluate_static(order,x);};
      virtual Eigen::VectorXd evaluateAll(unsigned int maxOrder, double x) const override{return evaluateAll_static(maxOrder,x);};
      virtual double GetNormalization(unsigned int n) const override{return PolyChild::GetNormalization_static(n);};
      virtual double gradient(unsigned int order, double x) const override{return PolyChild::gradient_static(order,x);};
      virtual double secondDerivative(unsigned int order, double x) const override{return PolyChild::secondDerivative_static(order,x);};
      
      ///Evaluate the order polynomial at x.
      static double evaluate_static(unsigned int order, double x);
      
      /// Evaluate all polynomials up to (and including) order maxOrder at the point x
      static Eigen::VectorXd evaluateAll_static(unsigned int maxOrder, double x);
      
      
      /// Get the roots of the polynomial
      static Eigen::VectorXd Roots(const Eigen::VectorXd& coeffs, double tol=1e-4);
      
      
#if MUQ_PYTHON == 1
      static boost::python::list PyRootsTol(boost::python::list const& coeffs, double tol=1e-4);
#endif // MUQ_PYTHON == 1
      
      
    private:
      
      ///Make class serializable
      friend class boost::serialization::access;
      
      template<class Archive>
      void serialize(Archive& ar, const unsigned int version){
        ar& boost::serialization::base_object<RecursivePolynomialFamily1D>(*this);
      };
      
      ///Methods that define the polynomial.  These should be defined by all children of this class
      //static double alpha(double x, unsigned int k);
      //static double beta(double x, unsigned int k);
      //static double phi0(double x);
      //static double phi1(double x);
      
    }; // class definition
    
  } // namespace Utilities
}// namespace muq

BOOST_CLASS_EXPORT_KEY(muq::Utilities::RecursivePolynomialFamily1D)

#endif /* RECURSIVEPOLYNOMIALFAMILY1D_H_ */
