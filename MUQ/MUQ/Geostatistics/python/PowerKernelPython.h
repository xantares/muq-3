
#ifndef POWERKERNEL_H_
#define POWERKERNEL_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Geostatistics/PowerKernel.h"

//#include "MUQ/Geostats/CovKernelPython.h"

namespace muq {
namespace Geostatistics {
void ExportPowerKernel();
} // namespace muq
} // namespace geostats

#endif // ifndef POWERKERNEL_H_
