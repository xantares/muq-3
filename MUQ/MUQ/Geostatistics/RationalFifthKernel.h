
#ifndef _RationalFifthKernel_h
#define _RationalFifthKernel_h

#include "MUQ/Geostatistics/IsotropicCovKernel.h"

namespace muq {
namespace Geostatistics {
/**
 *  @class RationalFifthKernel
 *
 *  @brief This is a basic class for building covariance matrices from a 5th order piecewise function.
 *
 *  Following equation 3.6 of http://people.cs.vt.edu/~asandu/Public/For/Ahmed/Petrie_2008_MS_KF.pdf, This class
 *implements covariance kernels of the form:
 *  /f[
 *  cov(x,y) = \left\{\begin{array}{ll}-\frac{1}{4}(d/c)^5 + \frac{1}{2}(d/c)^4 + \frac{5}{8}(d/c)^3 -
 *\frac{5}{3}(d/c)^2 + 1 & 0\leq d \leq c\\
 *                  \frac{1}{12}(d/c)^5 - \frac{1}{2}(d/c)^4 + \frac{5}{8}(d/c)^3 + \frac{5}{3}(d/c)^2
 *-5(d/c)+4-\frac{2}{3}(c/d) & c < d \leq 2c \\
 *                  0  & 2c < d \end{array}\right.
 *  /f]
 *
 *  @author Matthew Parno, Patrick Conrad
 *
 */
class RationalFifthKernel : public IsotropicCovKernel {
public:

  REGISTER_KERNEL_CONSTRUCTOR(RationalFifthKernel)
  /** Construct the kernel from the characteristic length scale, and variance.
   *  @param[in] cIn characteristic length scale
   *  @param[in] VarIn variance - i.e. diagonal of covariance matrix
   */
  RationalFifthKernel(boost::property_tree::ptree& ptree);

  RationalFifthKernel(double cIn = 1, double VarIn = 1);

  virtual ~RationalFifthKernel() = default;

  /** This function evaluates the power kernel.
   *  @param[in] d Distance between two points
   *  @return covariance of random field between two points at a distance d
   */
  virtual double          DistKernel(double d) const override;

  /** Set the covariance kernel parameters (c, variance) from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void            SetParms(const Eigen::VectorXd& theta) override;

  virtual void            GetGrad(double d, Eigen::VectorXd& grad) const override;

  virtual Eigen::VectorXd GetParms() override
  {
    Eigen::VectorXd Out(2); Out(0) = logC; Out(1) = logVar; return Out;
  }

  virtual double KernelRadialDerivative(double const r) override;

protected:

  double logC;
  double logVar;
};
} // namespace Geostatistics
} // namespace muq

#endif // ifndef _RationalFifthKernel_h
