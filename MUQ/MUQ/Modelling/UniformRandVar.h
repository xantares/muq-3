
#ifndef _UniformRandVar_h
#define _UniformRandVar_h

#include <Eigen/Dense>

#include "MUQ/Modelling/RandVar.h"
#include "MUQ/Modelling/UniformSpecification.h"

namespace muq {
namespace Modelling {
class UniformRandVar : public RandVar {
public:

  UniformRandVar(std::shared_ptr<UniformSpecification> specification) : RandVar(
                                                                          Eigen::VectorXi(), specification->dim, false, false, false,
                                                                          false),
                                                                        specification(specification)
  {}

  virtual ~UniformRandVar() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

  std::shared_ptr<UniformSpecification> specification;
};
} // namespace Modelling
} // namespace muq


#endif // ifndef _UniformRandVar_h
