#ifndef GAUSSIANPAIR_H_
#define GAUSSIANPAIR_H_

#include "MUQ/Modelling/GaussianRV.h"
#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/DensityRVPair.h"

namespace muq {
namespace Modelling {
typedef DensityRVPair<GaussianRV, GaussianDensity, GaussianSpecification> GaussianPair;
}
}

#endif // ifndef GAUSSIANPAIR_H_
