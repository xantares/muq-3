#ifndef MODGRAPHOPERATORS_H
#define MODGRAPHOPERATORS_H

#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/ModPiece.h"


namespace muq {
namespace Modelling {
	
	/**
	 * These are free functions that provide the ability to operate on ModPieces and ModGraphs with mathematical
	 * operators. For example, you can do:
	 * \code{.cpp} 
	 * - Sin(1.+2.*make_shared<VectorPassthrough>(3)) 
	 * \endcode
	 * 
	 * See Modelling/AnalyticFunctions for the special functions that allow this. You can also perform linear
	 * algebra naturally:
	 * \code{.cpp} 
	 * MatrixXd A;
	 * VectorXd b;
	 * A*make_shared<VectorPassthrough>(3) + b 
	 * \endcode
	 * 
	 * These are provided as free functions because they don't actually require member access. All these functions
	 * are focused around ModGraphs; all the overloads that take ModPieces directly just turn them into one node
	 * ModGraphs, then call the ModGraph function. These operators add to the "end" of the ModGraph, so they 
	 * need to be given graphs with a single output.
	 * 
	 * Remember that ModGraph nodes must have unique names. The default behavior gives all constructed ModPieces
	 * unique names, which are used as their node names. However, some operators, like making ModGraphPieces, 
	 * typically require that you know the names of nodes. You can either set the names of the ModPieces, before
	 * incorporating them into the graph, or use SetOutputNodeName() method to name the output node you just created.
	 * GetOutputNodeName() also helps if you need easy access to the name.
	 * 
	 * When multiple graphs are combined, their nodes and edges are merged, then the new node and edges required are
	 * added. If a node exists in two graphs with the same name and the same underlying object, then the node is treated as a 
	 * duplicate and is merged. This allows you to have diamond graphs. If you want a graph with multiple outputs, create 
	 * the graphs for all the outputs separately, then use FormUnion(). If you have a graph with multiple outputs, and 
	 * want to use these methods, use DependentCut to get singe output graphs, then merge them again using FormUnion. 
	 * Unfortunately this process is a little indirect, but we cannot overload operators like +,- for multi-output
	 * graphs without ambiguous behavior.
	 * 
	 * It also provides a more natural syntax for hooking up the inputs of a node, GraphCompose. See below for how to
	 * to call it. The versions for ModPiece and ModGraph are a bit different. Unfortunately, we cannot use something 
	 * as simple as the () operator because that cannot be a free function, and we can't add it to the 
	 * std::shared_ptr<ModPiece> objects.
	 * 
	 * NOTE: ModGraphPieces are treated as ModPieces, so their internal graphs are left untouched, which
	 * creates a "nested" graph. ModGraphPieces modify their internal graph to replace inputs with ModParameters,
	 * hence their internal graphs cannot be used with these operations. The usual procedure, then, is to
	 * make any ModGraphPieces needed as a final step, after the graph has been assembled.
	 * 
	 */
	
	
	
	
///Provide natural calling syntax for graphs.
/**
 * Creates a graph, filling the input for the modToBind with the input args. 
 * Called: GraphCompose(newNode, input1, input2, input3....). 
 * The inputs may be either ModGraphs with one output or a ModPiece. All the input graphs
 * are merged, and hence must satisfy the unique naming rules. You must provide
 * all the inputs the ModPiece takes at once.
 * 
 * The vararg template allows this calling syntax. It uses several helper templates to 
 * strip the inputs off, one at a time, storing them into a vector. 
 */
template<typename ... Args>
std::shared_ptr<ModGraph> GraphCompose(std::shared_ptr<ModPiece> const& modToBind, Args ... args)
{
  std::vector < std::shared_ptr < ModGraph >> capturedArgs;


  return GraphCompose_piece_impl(modToBind, capturedArgs, args ...);
}

/**
 * Helper function for GraphCompose. Capture one input, if it's a ModPiece, and add it to the vector
 * of the inputs so far.
 */
template<typename ... Args>
std::shared_ptr<ModGraph> GraphCompose_piece_impl(std::shared_ptr<ModPiece> const           & modToBind,
                                                  std::vector < std::shared_ptr < ModGraph >> collectingVector,
                                                  std::shared_ptr<ModPiece> const           & nextMod,
                                                  Args ...                                    args)
{
  collectingVector.push_back(std::make_shared<ModGraph>(nextMod));
  return GraphCompose_piece_impl(modToBind, collectingVector, args ...);
}

/**
 * Helper function for GraphCompose. Capture one input, if it's a ModGraph, and add it to the vector
 * of the inputs so far.
 */
template<typename ... Args>
std::shared_ptr<ModGraph> GraphCompose_piece_impl(std::shared_ptr<ModPiece> const           & modToBind,
                                                  std::vector < std::shared_ptr < ModGraph >> collectingVector,
                                                  std::shared_ptr<ModGraph> const           & nextMod,
                                                  Args ...                                    args)
{
  collectingVector.push_back(nextMod);
  return GraphCompose_piece_impl(modToBind, collectingVector, args ...);
}

/**
 * Helper function for GraphCompose. Bottom of the recursion. Now we have the vector of all the inputs, so create the graph.
 */
std::shared_ptr<ModGraph> GraphCompose_piece_impl(std::shared_ptr<ModPiece> const & modToBind,
                                                  std::vector < std::shared_ptr < ModGraph >> collectingVector);


///Provide natural calling syntax for graphs.
/**
 * Creates a graph, filling the input for the graphToBind with the input args. 
 * Called: GraphCompose(graphToBind, input1, input2, input3...., inputNames, inputDims). 
 * You may have as many inputs as needed, where each may be either ModGraphs with one 
 * output or a ModPiece. All the input graphs are merged, and hence must satisfy the 
 * unique naming rules. You must provide all the inputs the ModPiece takes at once.
 * Since the graph inputs are not uniquely ordered, you may provide inputNames and inputDims
 * to specify this; both are optional inputs, so you may omit them, as follows:
 * 
 * inputNames is a std::vector<std::string> of the node names whose inputs are provided, in the same order as the 
 * inputs. It should only be omitted if there is exactly one input to provide. As with ModGraphPieces,
 * the default behavior will simply choose an ordering, but is not guaranteed to be stable or sensible.
 * 
 * inputDims is a std::vector<int> of the dimensions of the inputs to provide - remember that nodes
 * can have more than one vector of inputs. It may be omitted if all the inputs only take one vector.
 * 
 * The vararg template allows this calling syntax. It uses several helper templates to 
 * strip the inputs off, one at a time, storing them into a vector. 
 */
template<typename ... Args>
std::shared_ptr<ModGraph> GraphCompose(std::shared_ptr<ModGraph> const& graphToBind, Args ... args)
{
  std::vector < std::shared_ptr < ModGraph >> capturedArgs;


  return GraphCompose_graph_impl(graphToBind, capturedArgs, args ...);
}

/**
 * Helper function for GraphCompose. Capture one input, if it's a ModPiece, and add it to the vector
 * of the inputs so far.
 */
template<typename ... Args>
std::shared_ptr<ModGraph> GraphCompose_graph_impl(std::shared_ptr<ModGraph> const           & modToBind,
                                                  std::vector < std::shared_ptr < ModGraph >> collectingVector,
                                                  std::shared_ptr<ModPiece> const           & nextMod,
                                                  Args ...                                    args)
{
  collectingVector.push_back(std::make_shared<ModGraph>(nextMod));
  return GraphCompose_graph_impl(modToBind, collectingVector, args ...);
}

/**
 * Helper function for GraphCompose. Capture one input, if it's a ModGraph, and add it to the vector
 * of the inputs so far.
 */
template<typename ... Args>
std::shared_ptr<ModGraph> GraphCompose_graph_impl(std::shared_ptr<ModGraph> const           & modToBind,
                                                  std::vector < std::shared_ptr < ModGraph >> collectingVector,
                                                  std::shared_ptr<ModGraph> const           & nextMod,
                                                  Args ...                                    args)
{
  collectingVector.push_back(nextMod);
  return GraphCompose_graph_impl(modToBind, collectingVector, args ...);
}

/**
 * Helper function for GraphCompose. Bottom of the recursion. Now we have the vector of all the inputs,
 * and maybe the ordering/dim vectors, so actually create the graph.
 */
std::shared_ptr<ModGraph> GraphCompose_graph_impl(std::shared_ptr<ModGraph> const & modToBind,
                                                  std::vector < std::shared_ptr < ModGraph >> collectingVector,
                                                  std::vector<std::string> inputOrder = std::vector<std::string>(),
                                                  std::vector<int> inputDims = std::vector<int>());


///Add graph
std::shared_ptr<ModGraph> operator+(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator+(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator+(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModPiece>& b);
std::shared_ptr<ModGraph> operator+(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModPiece>& b);

///Subtract graph
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModPiece>& b);
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModPiece>& b);

///Component-wise multiply graphs
std::shared_ptr<ModGraph> operator*(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator*(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator*(const std::shared_ptr<ModGraph>& a, const std::shared_ptr<ModPiece>& b);
std::shared_ptr<ModGraph> operator*(const std::shared_ptr<ModPiece>& a, const std::shared_ptr<ModPiece>& b);

///Linear operators
std::shared_ptr<ModGraph> operator*(const Eigen::MatrixXd& A, std::shared_ptr<ModGraph> x);
std::shared_ptr<ModGraph> operator*(const Eigen::MatrixXd& A, std::shared_ptr<ModPiece> x);
std::shared_ptr<ModGraph> operator+(const Eigen::VectorXd& b, std::shared_ptr<ModGraph> x);
std::shared_ptr<ModGraph> operator+(const Eigen::VectorXd& b, std::shared_ptr<ModPiece> x);
std::shared_ptr<ModGraph> operator+(std::shared_ptr<ModGraph> x, const Eigen::VectorXd& b);
std::shared_ptr<ModGraph> operator+(std::shared_ptr<ModPiece> x, const Eigen::VectorXd& b);
std::shared_ptr<ModGraph> operator-(std::shared_ptr<ModGraph> x, const Eigen::VectorXd& b);
std::shared_ptr<ModGraph> operator-(std::shared_ptr<ModPiece> x, const Eigen::VectorXd& b);
std::shared_ptr<ModGraph> operator-(const Eigen::VectorXd& b, std::shared_ptr<ModGraph> x);
std::shared_ptr<ModGraph> operator-(const Eigen::VectorXd& b, std::shared_ptr<ModPiece> x);


///The following batch are all just linear operators, where we promote scalars to matrices or vectors

///Negate graph
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModGraph>& a);
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModPiece>& a);

///Add a constant
std::shared_ptr<ModGraph> operator+(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraph> operator+(const std::shared_ptr<ModPiece>& a, const double& b);
std::shared_ptr<ModGraph> operator+(const double& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator+(const double& a, const std::shared_ptr<ModPiece>& b);

///Subtract a constant
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraph> operator-(const std::shared_ptr<ModPiece>& a, const double& b);
std::shared_ptr<ModGraph> operator-(const double& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator-(const double& a, const std::shared_ptr<ModPiece>& b);

///Multiply by a constant
std::shared_ptr<ModGraph> operator*(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraph> operator*(const std::shared_ptr<ModPiece>& a, const double& b);
std::shared_ptr<ModGraph> operator*(const double& a, const std::shared_ptr<ModGraph>& b);
std::shared_ptr<ModGraph> operator*(const double& a, const std::shared_ptr<ModPiece>& b);

///Divide by a constant
std::shared_ptr<ModGraph> operator/(const std::shared_ptr<ModGraph>& a, const double& b);
std::shared_ptr<ModGraph> operator/(const std::shared_ptr<ModPiece>& a, const double& b);
}
}
#endif // MODGRAPHOPERATORS_H
