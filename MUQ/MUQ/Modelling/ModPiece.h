#ifndef ModPiece_h_
#define ModPiece_h_

// standard library includes
#include <math.h>
#include <iostream>
#include <vector>
#include <memory>
#include <map>

#include "MUQ/config.h"

#if MUQ_PYTHON == 1

#include <boost/property_tree/ptree.hpp> // this include is weird, but must be here to avoid a strange bug on osx
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

// external library includes
#include <Eigen/Core>
#include <boost/optional.hpp>

namespace muq {
namespace Modelling {
class ModPieceOneStepCache;

///The core Modelling class.

/**
 * Note that users typically will not subclass from this directly. Instead, they'll use one of the templates provided.
 *
 * Provides one step of caching, for non-random functions, as defined by isRandom. For example, Evaluate may be called
 * twice in a row, and the second call will just return the prior value without any expensive computation. However, if
 * a!=b, Evaluate(a); Evaluate(b);Evaluate(a); generates three
 * runs of the implementation function.
 *
 * Three core methods. Evaluate, Gradient, and Jacobian. They are split into the functions you call, and the Impl
 * versions that children must implement. The base name ones handle certain tasks you always want. For example, they
 * perform the actual caching and thorough error testing. Hence the user-written Impl variants need not worry about
 *this,
 * as the inputs are guaranteed to be of the correct sizes.
 *
 * Method routing:
 * Evaluate: must be provided Gradient: direct if possible, else from the jacobian if there, else by finite difference
 * Jacobian: direct if possible, else by repeated gradient JacobianAction: direct if possible, else by forming jacobian
 **/
class ModPiece {
public:

  /**
   * This constructor provides a unique id to every ModPiece created.
   * NOTE: Do not create another construtor without ensuring the unique id strategy is adjusted.
   **/
  ModPiece(Eigen::VectorXi const& inputSizes,
           int const              outputSize,
           bool const             hasDirectGradient,
           bool const             hasDirectJacobian,
           bool const             hasDirectJacobianAction,
           bool const             hasDirectHessian,
           bool const             isRandom,
           std::string const    & name = "");


  virtual ~ModPiece() = default;

  /** This function provides an alternative to evaluating the "Evaluate" function multiple times, as in a for loop.  This function takes many copies of the inputs (in the form of a MatrixXd) and returns evaluations of the ModPiece for each input.  The std::vector is used for multiple inputs to the ModPiece (i.e., something like f(x,y)), while the matrix inputs correspond to multiple copies of the input variables (i.e. x_i, y_i).  Each column of the matrices contains a single copy.  This function calls the EvaluateMultiImpl function, which defaults to a for loop around the EvaluateImpl function.
   @param[in] input A std::vector containing matrices of input copies. 
   @return A matrix containing evaluations of the ModPiece for each input.  If F is the output matrix and f is ModPiece, then F.col(i)=f(input[0].col(i),input[1].col(i),...).
   */
  Eigen::MatrixXd EvaluateMulti(std::vector<Eigen::MatrixXd> const& input);
  
  ///The three primary methods a ModPiece is guaranteed to provide.

  /**
   * These methods should always be called because they handle one-step caching and sanitation of inputs/outputs. If
   * subclasses want to provide nicer interfaces, be very careful about providing an overload, as you can hide these
   * methods entirely. Better to create a method with a different name that calls this one.
   *
   * Methods below allow you to call these with Evaluate(vec1, ... vecN) instead of the vector or vector notation.
   **/
  Eigen::VectorXd Evaluate(std::vector<Eigen::VectorXd> const& input);

  Eigen::VectorXd Gradient(std::vector<Eigen::VectorXd> const& input,
                           Eigen::VectorXd const             & sensitivity,
                           int const                           inputDimWrt = 0);


  Eigen::MatrixXd Jacobian(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt = 0);

  Eigen::VectorXd JacobianAction(std::vector<Eigen::VectorXd> const& input,
                                 Eigen::VectorXd const             & target,
                                 int const                           inputDimWrt = 0);

  Eigen::MatrixXd Hessian(std::vector<Eigen::VectorXd> const& input,
                          Eigen::VectorXd const             & sensitivity,
                          int const                           inputDimWrt = 0);

#if MUQ_PYTHON == 1

  /// Python implementation of Evaluate
  virtual boost::python::list PyEvaluate(boost::python::list const& inputs);

  /// Allow the user to call the gradient function from python
  virtual boost::python::list PyGradient(boost::python::list const& pyInputs,
                                         boost::python::list const& pySensitivity,
                                         int const                  inputDimWrt = 0);

  /// Allow the user to call the Jacobian function from python
  virtual boost::python::list PyJacobian(boost::python::list const& pyInputs, int const inputDimWrt = 0);

  /// Allow the user to call the Jacobian action function from python
  virtual boost::python::list PyJacobianAction(boost::python::list const& pyInputs,
                                               boost::python::list const& pyTarget,
                                               int const                  inputDimWrt = 0);

  /// Allow the user to call the Hessian function from python
  virtual boost::python::list PyHessian(boost::python::list const& pyInputs,
                                        boost::python::list const& pySensitivity,
                                        int const                  inputDimWrt = 0);

#endif // if MUQ_PYTHON == 1

  ///The length of the vector is the number of separate vector inputs and the values are the lengths of each
  const Eigen::VectorXi inputSizes;

#if MUQ_PYTHON == 1

  /// translate inputSizes array to python list and expose it to python
  boost::python::list GetInputSizes() const;
#endif // if MUQ_PYTHON == 1

  /// Get the name of the ModPiece
  /**
   *  Every ModPiece has an optional, user defined name.  This function returns that name.  If no name has been defined
   * it returns the name of the class, postfixed with the id of the ModPiece.
   *  \return ModPiece name.
   */
  std::string GetName() const;

  /// Turn the one step caching off
  void TurnCachingOff(){useCache = false;};
  
  // The the one step caching on
  void TurnCachingOn(){useCache = true;};
  
  /// Name the ModPiece
  /**
   *  Sets myName to a user defined name.
   *  @param[in] name The new name of the ModPiece
   */
  void SetName(std::string const& name);

  ///The length of the output vector
  const int outputSize;

  ///Constants that reflect properties.
  const bool hasDirectGradient;

  const bool hasDirectJacobian;

  const bool hasDirectJacobianAction;

  const bool hasDirectHessian;

  /**
   * When this is true, caching can't help because the output changes every time it's run. Be careful about re-using
   * values. Gradients of random things are not necessarily handled as you want.
   **/
  const bool isRandom;

  ///Parameters users may set to alter default behavior of FD routines, especially used when Gradient calls FD
  const double FdRelTolDefault = 1e-4;
  const double FdMinTolDefault = 1e-6;

  ///Specifically request a gradient is computed by finite difference.

  /**
   * Users probably use this for debugging, or else is called indirectly by Gradient() when there is no direct gradient
   * method implemented.
   *
   * Note that this uses repeated Evalute calls, which will override the one-step cache
   **/
  Eigen::VectorXd GradientByFD(std::vector<Eigen::VectorXd> const& input,
                               Eigen::VectorXd const             & sensitivity,
                               int const                           inputDimWrt,
                               double                              FdRelTol,
                               double                              FdMinTol);
  
  /** Calls the other GradientByFD function will default values for FdRelTol and FdMinTol. */
  Eigen::VectorXd GradientByFD(std::vector<Eigen::VectorXd> const& input,
                               Eigen::VectorXd const             & sensitivity,
                               int const                           inputDimWrt = 0);

#if MUQ_PYTHON == 1

  /// Allow the user to call the gradient by finite difference function from python
  virtual boost::python::list PyGradientByFD(boost::python::list const& inputs,
                                             boost::python::list const& sensIn,
                                             int const                  inputDimWrt);
  
  virtual boost::python::list PyJacobianByFD(boost::python::list const& inputs,
                                             int const                  inputDimWrt);

  /// Allow the user to call the Hessian by finite difference function from python
  virtual boost::python::list PyHessianByFD(boost::python::list const& inputs,
                                            boost::python::list const& sensIn,
                                            int const                  inputDimWrt);

#endif // if MUQ_PYTHON == 1

  /** Compute the Jacobian matrix using finite differences with the tolerances specified as inputs.  This method uses
      forward differences to compute the Jacobian matrix.  This function will require inputSizes(inputDimWrt)+1 evaluations of EvaluateImpl to 
      compute the Jacobian.  The step size, \f$\delta x\f$ is computed using the expression 
  \f[
  \delta x = \max(\text{FdMinTol},|\text{FdRelTol}*x|) 
  \f]
      @param[in] input A vector of inputs to this ModPiece.  This defines where the Jacobian is to be computed.
      @param[in] inputDimWrt Of the possibly many inputs, which one do we want to compute the Jacobian wrt?  Note that all inputs and the output of the ModPiece are vector-valued.  This means that the output of this function can always be a Matrix.
      @param[in] FdRelTol  Relative tolerance used to define the finite difference step.   
      @param[in] FdMinTol The minimum step sized allowed.
  @see muq::Modelling::ModPiece::JacobianByFD, muq::Modelling::ModPiece::Jacobian
  */
  Eigen::MatrixXd JacobianByFD(std::vector<Eigen::VectorXd> const& input,
                               int const                           inputDimWrt,
                               double                              FdRelTol,
                               double                              FdMinTol);

  /** Calls the other JacobianByFD function with default values for FdRelTol and FdMinTol.  @see muq::Modelling::ModPiece::JacobianByFD */
  Eigen::MatrixXd JacobianByFD(std::vector<Eigen::VectorXd> const& input,
                               int const                           inputDimWrt = 0);
                             
   /** Call the other JacobianActionByFD function with default values for FdRelTol and FdMinTol. @see muq::Modelling::ModPiece::JacobianActionByFd */   
   Eigen::VectorXd JacobianActionByFD(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & target,
                                      int const                           inputDimWrt = 0);
                                      
  /** This function computes the action of the Jacobian on a vector using finite differences.  Notice that the action of the Jacobian on a vector is just a directional derivative.  Thus, only 2 calls to the Evaluate function are needed to evaluate this function.The step size, \f$\delta x\f$ is computed using the expression 
   \f[
   \delta x = \max(\text{FdMinTol},|\text{FdRelTol}*x|) 
   \f]
      @param[in] input A vector of inputs to this ModPiece.  This defines where the JacobianAction is to be computed.
      @param[in] target The vector we want to apply the Jacobian matrix to. 
      @param[in] inputDimWrt Of the possibly many inputs, which one do we want to compute the Jacobian wrt?
      @param[in] FdRelTol  Relative tolerance used to define the finite difference step.   
      @param[in] FdMinTol The minimum step sized allowed.
    @see muq::Modelling::ModPiece::JacobianActionByFD, muq::Modelling::ModPiece::JacobianAction
   */
   Eigen::VectorXd JacobianActionByFD(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & target,
                                      int const                           inputDimWrt,
                                      double                              FdRelTol,
                                      double                              FdMinTol);
                                      
  ///Specifically request a Hessian is computed by finite difference

  /**
   * Users probably use this for debugging, or else is called indirectly by Hessian() when there is no direct Hessian
   * method implemented.
   *
   * Note that this uses repeated Evalute calls, which will mess up the cache
   **/

  Eigen::MatrixXd HessianByFD(std::vector<Eigen::VectorXd> const& input,
                              Eigen::VectorXd const             & sensitivity,
                              int const                           inputDimWrt,
                              double                              FdRelTol,
                              double                              FdMinTol);
  Eigen::MatrixXd HessianByFD(std::vector<Eigen::VectorXd> const& input,
                              Eigen::VectorXd const             & sensitivity,
                              int const                           inputDimWrt=0);
  

  ///A generic way to call EvaluateMulti(vec1, vec2, ...)
  template<typename ... Args>
  Eigen::MatrixXd EvaluateMulti(Eigen::MatrixXd const& firstInput, Args ... args)
  {
    std::vector<Eigen::MatrixXd> startVec;
    
    startVec.push_back(firstInput);
    return EvaluateMulti(startVec, args ...);
  }
  
  
  template<typename ... Args>
  Eigen::MatrixXd EvaluateMulti(std::vector<Eigen::MatrixXd> collectingVector,
                                Eigen::MatrixXd const      & nextInput,
                                Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return EvaluateMulti(collectingVector, args ...);
  }
  
  ///A generic way to call Evaluate(vec1, vec2, ...)
  template<typename ... Args>
  Eigen::VectorXd Evaluate(Eigen::VectorXd const& firstInput, Args ... args)
  {
    std::vector<Eigen::VectorXd> startVec;

    startVec.push_back(firstInput);
    return Evaluate(startVec, args ...);
  }

  
  template<typename ... Args>
  Eigen::VectorXd Evaluate(std::vector<Eigen::VectorXd> collectingVector,
                           Eigen::VectorXd const      & nextInput,
                           Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return Evaluate(collectingVector, args ...);
  }

  ///Allow call to evaluate with no inputs, which will fail if the ModPiece requires inputs
  Eigen::VectorXd Evaluate()
  {
    return Evaluate(std::vector<Eigen::VectorXd>());
  }

  ///A generic way to call Gradient(vec1, vec2, ..., sens, dimWrt)
  template<typename ... Args>
  Eigen::VectorXd Gradient(Eigen::VectorXd const& firstInput, Args ... args)
  {
    std::vector<Eigen::VectorXd> startVec;

    startVec.push_back(firstInput);
    return Gradient(startVec, args ...);
  }

  template<typename ... Args>
  Eigen::VectorXd Gradient(std::vector<Eigen::VectorXd> collectingVector,
                           Eigen::VectorXd const      & nextInput,
                           Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return Gradient(collectingVector, args ...);
  }

  ///A generic way to call GradientByFD(vec1, vec2, ..., sens, dimWrt)
  template<typename ... Args>
  Eigen::VectorXd GradientByFD(Eigen::VectorXd const& firstInput,  Eigen::VectorXd const & secondInput, Args ... args)
  {
    assert(inputSizes.size()>0);
    std::vector<Eigen::VectorXd> startVec;
    
    startVec.push_back(firstInput);
    return GradientByFD(startVec, secondInput, args ...);
  }
  
  template<typename ... Args>
  Eigen::VectorXd GradientByFD(std::vector<Eigen::VectorXd> collectingVector,
                               Eigen::VectorXd const      & nextInput,
                               Eigen::VectorXd const      & secondInput,
                               Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return GradientByFD(collectingVector, secondInput, args ...);
  }
  
  
  template<typename ... Args>
  Eigen::VectorXd JacobianActionByFD(Eigen::VectorXd const& firstInput,  Eigen::VectorXd const & secondInput, Args ... args)
  {
    assert(inputSizes.size()>0);
    std::vector<Eigen::VectorXd> startVec;
    
    startVec.push_back(firstInput);
    return JacobianActionByFD(startVec, secondInput, args ...);
  }
  
  template<typename ... Args>
  Eigen::VectorXd JacobianActionByFD(std::vector<Eigen::VectorXd> collectingVector,
                               Eigen::VectorXd const      & nextInput,
                               Eigen::VectorXd const      & secondInput,
                               Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return JacobianActionByFD(collectingVector, secondInput, args ...);
  }
  
  
  
  ///A generic way to call Jacobian(vec1, vec2, ..., dimWrt)
  template<typename ... Args>
  Eigen::MatrixXd Jacobian(Eigen::VectorXd const& firstInput, Args ... args)
  {
    std::vector<Eigen::VectorXd> startVec;

    startVec.push_back(firstInput);
    return Jacobian(startVec, args ...);
  }

  template<typename ... Args>
  Eigen::MatrixXd Jacobian(std::vector<Eigen::VectorXd> collectingVector,
                           Eigen::VectorXd const      & nextInput,
                           Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return Jacobian(collectingVector, args ...);
  }
  
  
  ///A generic way to call GradientByFD(vec1, vec2, ..., sens, dimWrt)
  template<typename ... Args>
  Eigen::MatrixXd JacobianByFD(Eigen::VectorXd const& firstInput,  Args ... args)
  {
    assert(inputSizes.size()>0);
    std::vector<Eigen::VectorXd> startVec;
    
    startVec.push_back(firstInput);
    return JacobianByFD(startVec, args ...);
  }
  
  template<typename ... Args>
  Eigen::MatrixXd JacobianByFD(std::vector<Eigen::VectorXd> collectingVector,
                               Eigen::VectorXd const      & nextInput,
                               Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return JacobianByFD(collectingVector, args ...);
  }
  
  
  ///A generic way to call JacobianAction(vec1, vec2, ..., target, dimWrt)
  template<typename ... Args>
  Eigen::VectorXd JacobianAction(Eigen::VectorXd const& firstInput, Args ... args)
  {
    std::vector<Eigen::VectorXd> startVec;

    startVec.push_back(firstInput);
    return JacobianAction(startVec, args ...);
  }

  template<typename ... Args>
  Eigen::VectorXd JacobianAction(std::vector<Eigen::VectorXd> collectingVector,
                                 Eigen::VectorXd const      & nextInput,
                                 Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return JacobianAction(collectingVector, args ...);
  }

  ///A generic way to call Hessian(vec1, vec2, ..., dimWrt)
  template<typename ... Args>
  Eigen::MatrixXd Hessian(Eigen::VectorXd const& firstInput, Args ... args)
  {
    std::vector<Eigen::VectorXd> startVec;

    startVec.push_back(firstInput);
    return Hessian(startVec, args ...);
  }

  template<typename ... Args>
  Eigen::MatrixXd Hessian(std::vector<Eigen::VectorXd> collectingVector, Eigen::VectorXd const& nextInput,
                          Args ... args)
  {
    collectingVector.push_back(nextInput);
    return Hessian(collectingVector, args ...);
  }

  
  template<typename ... Args>
  Eigen::VectorXd HessianByFD(Eigen::VectorXd const& firstInput,  Eigen::VectorXd const & secondInput, Args ... args)
  {
    assert(inputSizes.size()>0);
    std::vector<Eigen::VectorXd> startVec;
    
    startVec.push_back(firstInput);
    return HessianByFD(startVec, secondInput, args ...);
  }
  
  template<typename ... Args>
  Eigen::VectorXd HessianByFD(std::vector<Eigen::VectorXd> collectingVector,
                              Eigen::VectorXd const      & nextInput,
                              Eigen::VectorXd const      & secondInput,
                              Args ...                     args)
  {
    collectingVector.push_back(nextInput);
    return HessianByFD(collectingVector, secondInput, args ...);
  }
  
  
  /**
   * Wipe the cache so that the Impl methods get called again. Necessary if the output changes silently, for example,
   * when a ModParameter has its value changed. Overrides should almost certainly call this one to avoid odd behavior.
   *Be
   * careful.
   **/
  virtual void      InvalidateCache();

  /** @brief Get the average run time for one of the implemented methods.
   *   @details This function returns the average wall clock time (in milliseconds) for the EvaluateImpl, GradientImpl,
   * JacobianImpl, JacobianActionImpl, or HessianImp functions.
   *            If the function was never called, -1 is returned.
   *   @param[in] method The implemented function of interest.  Possible options are "Evaluate", "Gradient", "Jacobian",
   * "JacobianAction", or "Hessian"
   *   @return The average wall clock time in seconds.
   */
  double            GetRunTime(const std::string& method) const;

  /** @brief get the number of times one of the implemented methods has been called.
   *   @details This function returns the number of times the EvaluateImpl, GradientImpl, JacobianImpl,
   * JacobianActionImpl, or HessianImp functions have been called.
   *   @param[in] method The implemented function of interest.  Possible options are "Evaluate", "Gradient", "Jacobian",
   * "JacobianAction", or "Hessian"
   *   @return An integer with the number of calls.
   */
  unsigned long int GetNumCalls(const std::string& method) const;

  /** @brief Resets the number of call and times.
   *   @details Sets the number of calls to each implemented function to zero and the recorded wall clock times to zero.
   */
  void              ResetCallTime();

private:

  virtual Eigen::MatrixXd EvaluateMultiImpl(std::vector<Eigen::MatrixXd> const& input);
  
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) = 0;

  /**
   * Say that you have an adjoint where you need to run the forward model first, then can compute a gradient. The one
   * step caching means that your implementation here may simply call Evaluate() as the first step, which will ensure
   * that it either has been run exactly once at this point, or else will do so. This concept gets strange if the
   * quantity is random.
   **/
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt);

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt);

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0);

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitvity,
                                      int const                           inputDimWrt);

  ///The one input cache of values. Users should never touch this.
  std::shared_ptr<ModPieceOneStepCache> oneStepCache;

  Eigen::VectorXd GradientByJacobian(std::vector<Eigen::VectorXd> const& input,
                                     Eigen::VectorXd const             & sensitivity,
                                     int const                           inputDimWrt);

protected:

  Eigen::MatrixXd AssembleJacobian(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt);

  ///Test with asserts that all the inputs are the right sizes
  void            CheckInputs(std::vector<Eigen::VectorXd> const& input) const;
  void            CheckMultiInputs(std::vector<Eigen::MatrixXd> const& input) const;
  
private:

  // The following variables keep track of how many times the Implemented functions, i.e. EvaluateImpl, GradientImpl,
  // etc... are called.
  unsigned long int numEvalCalls   = 0;
  unsigned long int numGradCalls   = 0;
  unsigned long int numJacCalls    = 0;
  unsigned long int numJacActCalls = 0;
  unsigned long int numHessCalls   = 0;

  // these variables keep track of the total wall-clock time spent in each of the Implemented functions.  They are in
  // units of nanoseconds
  unsigned long long int evalTime   = 0;
  unsigned long long int gradTime   = 0;
  unsigned long long int jacTime    = 0;
  unsigned long long int jacActTime = 0;
  unsigned long long int hessTime   = 0;

  /// The user defined name of the ModPiece (defaults to "" if no name has been defined)
  std::string myName;
  
  ///A unique ID number assigned by the constructor
  int id;
  
  // a boolean indicated whether one step caching should be used or not.  This is set with TurnCachingOn() and TurnCachingOff()
  bool useCache = true;
};

///Handles keeping a consistent cache. If a value is actually returned, it is a direct match.

/**
 * Only one step is kept at a time, and the matches are exact. They attempt to throw away as little as possible.
 **/
class ModPieceOneStepCache {
public:

  ModPieceOneStepCache()          = default;
  virtual ~ModPieceOneStepCache() = default;

  boost::optional<Eigen::VectorXd> GetCachedEvaluate(std::vector<Eigen::VectorXd> const& input) const;
  void                             SetCachedEvaluate(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & result);

  boost::optional<Eigen::VectorXd> GetCachedGradient(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & sensitivity,
                                                     int                                 inputDimWrt) const;
  void                             SetCachedGradient(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & sensitivity,
                                                     int                                 inputDimWrt,
                                                     Eigen::VectorXd const             & gradient);

  boost::optional<Eigen::MatrixXd> GetCachedJacobian(std::vector<Eigen::VectorXd> const& input, int inputDimWrt) const;
  void                             SetCachedJacobian(std::vector<Eigen::VectorXd> const& input,
                                                     int                                 inputDimWrt,
                                                     Eigen::MatrixXd const             & jacobian);

  boost::optional<Eigen::VectorXd> GetCachedJacobianAction(std::vector<Eigen::VectorXd> const& input,
                                                           Eigen::VectorXd const             & target,
                                                           int                                 inputDimWrt) const;
  void                             SetCachedJacobianAction(std::vector<Eigen::VectorXd> const& input,
                                                           Eigen::VectorXd const             & target,
                                                           int                                 inputDimWrt,
                                                           Eigen::VectorXd const             & action);

  boost::optional<Eigen::MatrixXd> GetCachedHessian(std::vector<Eigen::VectorXd> const&,
                                                    Eigen::VectorXd const           & sensitivity,
                                                    int                               inputDimWrt) const;
  void                             SetCachedHessian(std::vector<Eigen::VectorXd> const& input,
                                                    Eigen::VectorXd const             & sensitivity,
                                                    int                                 inputDimWrt,
                                                    Eigen::MatrixXd const             & hessian);

  void ClearCache();

private:

  bool initialized = false; //notice it is useless until something is done

  bool InputMatchesCache(std::vector<Eigen::VectorXd> const& input) const;

  std::vector<Eigen::VectorXd> cachedInput;
  boost::optional<Eigen::VectorXd> cachedOutput;

  std::map<int, std::pair<Eigen::VectorXd, Eigen::VectorXd> > gradientMap;

  std::map<int, Eigen::MatrixXd> jacobianMap;

  std::map<int, std::pair<Eigen::VectorXd, Eigen::VectorXd> > jacobianActionMap;

  std::map<int, std::pair<Eigen::VectorXd, Eigen::MatrixXd> > hessianMap;
};
} // namespace Modelling
} // namespace muq


#endif // ifndef ModPiece_h_
