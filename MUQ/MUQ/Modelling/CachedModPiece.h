#ifndef CACHEDMODPIECE_H
#define CACHEDMODPIECE_H


#include "MUQ/config.h"

#if MUQ_PYTHON == 1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

# include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include <list>
#include <memory>

#include <flann/flann.hpp>


#include "MUQ/Utilities/PointWrapper.h"
#include "MUQ/Modelling/ModPiece.h"

namespace muq {
namespace Modelling {
class ModPieceMultiStepCache;

/**
 * There are two main modes of interaction. First, to transparently return values previously computed, and to store
 *newly computed values. Secondly, to return the various kinds of values that might be in the cache near a particular
 *point.
 **/
class CachedModPiece : public ModPiece {
public:

  CachedModPiece(std::shared_ptr<ModPiece> sourceModPiece);

  CachedModPiece(Eigen::VectorXi const& inputSizes, int const outputSize);

  virtual ~CachedModPiece();


  ///Load the cache from a file.

  /**
   * It is the user's responsibility to ensure that the points in the cache actually came from the function of interest,
   *as they are blindly trusted.
   **/
  void LoadCache(std::string filename);

  ///Print the points stored in the cache.

  /**
   * Prints each entry as a row, first the point, then the results.
   **/
  void            SaveCache(std::string filename);


  int             CostOfEvaluations(Eigen::MatrixXd const& x);


  Eigen::MatrixXd FindKNearestNeighbors(std::vector<Eigen::VectorXd> const& input,
                                        unsigned const                      countToFind);

  Eigen::MatrixXd FindNeighborsWithinRadius(
    std::vector<Eigen::VectorXd> const& input,
    double const                        radius);

  std::list<std::shared_ptr<ModPieceMultiStepCache> > FindKNearestNeighborsCacheEntries(
    std::vector<Eigen::VectorXd> const& input,
    unsigned const                      countToFind);

  std::list<std::shared_ptr<ModPieceMultiStepCache> >
  FindNeighborsWithinRadiusCacheEntries(
    std::vector<Eigen::VectorXd> const& input,
    double const                        radius);


  Eigen::MatrixXd                                     FindKNearestNeighbors(Eigen::VectorXd const& input,
                                                                            unsigned const         countToFind);

  Eigen::MatrixXd                                     FindNeighborsWithinRadius(Eigen::VectorXd const& input,
                                                                                double const           radius);

  std::list<std::shared_ptr<ModPieceMultiStepCache> > FindKNearestNeighborsCacheEntries(
    Eigen::VectorXd const& input,
    unsigned const         countToFind);

  std::list<std::shared_ptr<ModPieceMultiStepCache> > FindNeighborsWithinRadiusCacheEntries(
    Eigen::VectorXd const& input,
    double const           radius);

  std::list<std::shared_ptr<ModPieceMultiStepCache> > FindKNearestNeighborsWithEvaluations(
    Eigen::VectorXd const& input,
    unsigned const         countToFind);

  ///Checks for a 0th input jacobian
  std::list<std::shared_ptr<ModPieceMultiStepCache> > FindKNearestNeighborsWithEvalJac(
    Eigen::VectorXd const& input,
    unsigned const         countToFind);

  ///Returns an existing entry or creates a new one
  std::shared_ptr<ModPieceMultiStepCache> FetchCachedResult(std::vector<Eigen::VectorXd> const& input);

  std::shared_ptr<ModPieceMultiStepCache> FetchCachedResult(Eigen::VectorXd const& input);

  void                                    RemoveFromCache(std::vector<Eigen::VectorXd> const& input);


  // bool                                    IsInputInCache(std::vector<Eigen::VectorXd> const& input) const;

  bool                      IsEntryInCache(Eigen::VectorXd const& pointToTest);

  bool                      IsEntryInCache(std::vector<Eigen::VectorXd> const& input);

  unsigned int              GetNumOfEvals();

  Eigen::MatrixXd           GetCachePoints();

  std::shared_ptr<ModPiece> GetSourceModPiece()
  {
    return sourceModPiece;
  }

#if MUQ_PYTHON == 1
  int                 PyCostOfEvaluations(boost::python::list const& x);

  boost::python::list PyFindKNearestNeighbors(boost::python::list const& input, unsigned const countToFind);

  boost::python::list PyFindNeighborsWithinRadius(boost::python::list const& input, double const radius);

  void                PyRemoveFromCache(boost::python::list const& input);

  bool                PyIsEntryInCacheMulti(boost::python::list const& input);

  bool                PyIsEntryInCacheSingle(boost::python::list const& input);

  boost::python::list PyGetCachePoints();
#endif // if MUQ_PYTHON == 1

protected:

  ///The ModPiece whose values are cached
  std::shared_ptr<ModPiece> sourceModPiece;

private:

  //basic version does nothing, it's for parallel. Should get called in anything that uses the cache.
  virtual void RefreshCache() {}

  int const totalInputSize;


  Eigen::VectorXd              CombineInputs(std::vector<Eigen::VectorXd> const& input) const;
  std::vector<Eigen::VectorXd> SeparateInputs(Eigen::VectorXd  const& input) const;

  int                          AddToNN(Eigen::VectorXd const& newPoints);

  void                         RemoveFromNN(int const idNN);

  ///A map from quadrature points to function evaluations, used to avoid ever calling fn twice on identical inputs
  std::map<muq::Utilities::PointWrapper, std::shared_ptr<ModPieceMultiStepCache>,
           muq::Utilities::PointComp> fnEvalCache;
  typedef std::map<muq::Utilities::PointWrapper, std::shared_ptr<ModPieceMultiStepCache> >
  ::const_iterator EvalCacheIter;

  ///The nearest neighbor index, used to perform searches
  std::shared_ptr<flann::Index<flann::L2<double> > > nnIndex;

  std::list<std::shared_ptr<flann::Matrix<double> > > nnData;

  int lastIdNN = 0;

  virtual Eigen::MatrixXd EvaluateMultiImpl(std::vector<Eigen::MatrixXd> const& input) override;
  
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

  /**
   * Say that you have an adjoint where you need to run the forward model first, then can compute a gradient. The one
   *step caching means that your implementation here may simply call Evaluate() as the first step, which will ensure
   *that it either has been run exactly once at this point, or else will do so. This concept gets strange if the
   *quantity is random.
   **/
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt);

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt);

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt);

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt);

  int removedCachePoints = 0;
};


class ModPieceMultiStepCache {
public:

  ModPieceMultiStepCache(std::vector<Eigen::VectorXd> const& input, int const idNN);
  virtual ~ModPieceMultiStepCache() = default;

  boost::optional<Eigen::VectorXd> GetCachedEvaluate() const;
  void                             SetCachedEvaluate(Eigen::VectorXd const& result);

  boost::optional<Eigen::VectorXd> GetCachedGradient(Eigen::VectorXd const& sensitivity, int inputDimWrt) const;
  void                             AddCachedGradient(Eigen::VectorXd const& sensitivity,
                                                     int                    inputDimWrt,
                                                     Eigen::VectorXd const& gradient);

  boost::optional<Eigen::MatrixXd> GetCachedJacobian(int inputDimWrt) const;
  void                             SetCachedJacobian(int inputDimWrt, Eigen::MatrixXd const& jacobian);

  boost::optional<Eigen::VectorXd> GetCachedJacobianAction(Eigen::VectorXd const& target, int inputDimWrt) const;
  void                             AddCachedJacobianAction(Eigen::VectorXd const& target,
                                                           int                    inputDimWrt,
                                                           Eigen::VectorXd const& action);

  boost::optional<Eigen::MatrixXd> GetCachedHessian(Eigen::VectorXd const& sensitivity, int const inputDimWrt) const;
  void                             AddCachedHessian(Eigen::VectorXd const& sensitivity,
                                                    int const              inputDimWrt,
                                                    Eigen::MatrixXd const& hessian);

  std::vector<Eigen::VectorXd> const cachedInput;
  int const idNN;

private:

  boost::optional<Eigen::VectorXd> cachedEvaluate;

  std::map<int, std::map<muq::Utilities::PointWrapper, Eigen::VectorXd, muq::Utilities::PointComp> >
  gradientMap;

  std::map<int, Eigen::MatrixXd> jacobianMap;

  std::map<int, std::map<muq::Utilities::PointWrapper, Eigen::VectorXd, muq::Utilities::PointComp> >
  jacobianActionMap;

  std::map<int, std::map<muq::Utilities::PointWrapper, Eigen::MatrixXd, muq::Utilities::PointComp> >
  hessianMap;
};
}
}

#endif // CACHEDMODPIECE_H
