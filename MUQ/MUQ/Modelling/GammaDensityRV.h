
#ifndef GAMMADENSITYRV_H
#define GAMMADENSITYRV_H


#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/RandVar.h"
#include "MUQ/Modelling/DensityRVPair.h"


namespace muq {
namespace Modelling {
class GammaSpecification {
public:

  ///Alpha is shape, beta is scale, according to c++ std docs
  GammaSpecification(double const alpha, double const beta, int length);
  virtual ~GammaSpecification() = default;


  Eigen::VectorXd Sample() const;

  double          LogDensity(Eigen::VectorXd const& input);
  const int length;

private:

  const double alpha;
  const double beta;
};

class GammaDensity : public muq::Modelling::Density {
public:

  GammaDensity(std::shared_ptr<GammaSpecification> spec);
  virtual ~GammaDensity() = default;

private:

  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override;

  std::shared_ptr<GammaSpecification> spec;
};

class GammaRV : public muq::Modelling::RandVar {
public:

  GammaRV(std::shared_ptr<GammaSpecification> spec);
  virtual ~GammaRV() = default;

private:

  std::shared_ptr<GammaSpecification> spec;

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;
};

typedef DensityRVPair<GammaRV, GammaDensity, GammaSpecification> GammaPair;
}
}
#endif // GAMMADENSITYRV_H
