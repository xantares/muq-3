
#ifndef DaeModPiece_h_
#define DaeModPiece_h_

#include <boost/property_tree/ptree_fwd.hpp>

// muq includes
#include "MUQ/Modelling/ModPiece.h"

#include <string>

namespace muq {
namespace Modelling {
	
/** @class DaeModPiece
 * @brief Provides a mechanism for defining differential-algebraic models from a DAE residual and an observation model.
 * @details This class allows ODE's to be defined from a right hand side function and an observation function.  This
 * class solves methods of the form
 * \f[
 * 0 = F(t,x,\frac{dx}{dt},p)
 * \f]
 * where x is the state vector, t is the scalar time, and p is a parameter vector.  The output of this modpiece is given
 * by an ``observation" function $g(x,t,p)$ that is evaluated on the state at fixed times.
 *
 * NOTE: DAEMODPIECE DOES NOT CURRENTLY IMPLEMENT SENSITIVITIES, EVEN THOUGH SUNDIALS SUPPORTS THEM 
 */
class DaeModPiece : public ModPiece {
public:

  /** Constructor
   * @param[in] fIn A modpiece defining the residual of the DAE.  Must take at least three inputs, the first being
   * a length 1 Eigen::VectorXd holding the time, the second holding an Eigen::VectorXd with the state, and the third,
   * holding an Eigen::VectorXd with dx/dt.
   * @param[in] gIn A modpiece defining the observation vector, must have the same inputs as fIn.
   * @param[in] evalTimes Times to evaluate the state and observation ModPiece.
   * @param[in] parameterSizes Holds the size of each parameter input.  Note that the first size must be the size of the
   * state vector passed to f and g.
   * @param[in] properties Holds integration properties such as integrator method (Forward Euler, Backward Euler,
   * etc...) as well as integration tolerances
   */
  DaeModPiece(std::shared_ptr<ModPiece>    fIn,
              std::shared_ptr<ModPiece>    gIn,
              Eigen::VectorXd const      & evalTimes,
              boost::property_tree::ptree& properties);


  DaeModPiece(std::shared_ptr<ModPiece> fIn, std::shared_ptr<ModPiece> gIn, Eigen::VectorXd const& evalTimes);

  DaeModPiece(std::shared_ptr<ModPiece> fIn, Eigen::VectorXd const& evalTimes, boost::property_tree::ptree& properties);

  DaeModPiece(std::shared_ptr<ModPiece> fIn, Eigen::VectorXd const& evalTimes);

protected:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  // virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
 //                                       Eigen::VectorXd const             & sensitivity,
 //                                       int const                           inputDimWrt) override;

  ///The jacobian is outputDim x inputDim
  //virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;


  //virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
  //                                           Eigen::VectorXd const             & target,
  //                                           int const                           inputDimWrt = 0) override;


  //virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
  //                                    Eigen::VectorXd const             & sensitvity,
  //                                    int const                           inputDimWrt) override;


  // the right hand side and observation modPieces
  std::shared_ptr<ModPiece> f, g;

  // observation times
  const Eigen::VectorXd obsTimes;

  // store the integration properties
  // boost::property_tree::ptree& integrateProperties;

private:

  std::string linSolver;
  double      abstol, reltol;
  std::string intMethod, solveMethod;
  double      maxStepSize;
  int checkPtGap;

  void RunIdasForward(std::vector<Eigen::VectorXd> const& inputs,
                        bool                                runSens,
                        int const                           inputDimWrt,
                        Eigen::VectorXd                   & eval,
                        Eigen::MatrixXd                   & jac) const;

  // void RunIdasAdjoint(std::vector<Eigen::VectorXd> const& inputs,
  //                       Eigen::VectorXd              const& outputSens,
  //                       int const                           inputDimWrt,
  //                       Eigen::VectorXd                   & gradient) const;
};

// DaeModPiece
} // namespace modelling
} // namespace muq


#endif // ifndef DaeModPiece_h_