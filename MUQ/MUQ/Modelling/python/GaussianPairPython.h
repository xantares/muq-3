#ifndef GAUSSIANPAIRPYTHON_H_
#define GAUSSIANPAIRPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/GaussianPair.h"

namespace muq {
namespace Modelling {
class GaussianPairPython : public GaussianPair, public boost::python::wrapper<GaussianPair> {
public:

  GaussianPairPython(unsigned int const dimIn);

  GaussianPairPython(unsigned int const dimIn, boost::python::list const& inputSizes);

  GaussianPairPython(unsigned int const dimIn, double const scaledCov);

  GaussianPairPython(boost::python::list const& mu, double const scaledCov);

  GaussianPairPython(boost::python::list const& mu,
                     boost::python::list const& diag);

  GaussianPairPython(Eigen::VectorXd const                         & mu,
                     Eigen::VectorXd const                         & diag,
                     GaussianSpecification::SpecificationMode const& mode);

  GaussianPairPython(Eigen::VectorXd const                         & mu,
                     Eigen::MatrixXd const                         & mat,
                     GaussianSpecification::SpecificationMode const& mode);

  GaussianPairPython(unsigned int const                              dim,
                     Eigen::VectorXd const                         & mu,
                     Eigen::MatrixXd const                         & mat,
                     GaussianSpecification::SpecificationMode const& mode);

  static std::shared_ptr<GaussianPairPython> Create(boost::python::list const                     & mu,
                                                    boost::python::list const                     & cov_or_prec,
                                                    GaussianSpecification::SpecificationMode const& mode);

  static std::shared_ptr<GaussianPairPython> CreateConditional(
    unsigned int const                              dim,
    boost::python::list const                     & mu,
    boost::python::list const                     & cov_or_prec,
    GaussianSpecification::SpecificationMode const& mode);

  std::shared_ptr<GaussianDensity> GetDensity() const;
  
  std::shared_ptr<GaussianRV> GetRV() const;

private:
};

void ExportGaussianPair();
} // namepsace Modelling
} // namespace muq

#endif // ifndef GAUSSIANPAIRPYTHON_H_
