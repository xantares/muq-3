#ifndef CACHEDMODPIECEPYTHON_H_
#define CACHEDMODPIECEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/CachedModPiece.h"

namespace muq {
namespace Modelling {
void ExportCachedModPiece();
} // namespace modelling
} // namespace muq

#endif // ifndef CACHEDMODPIECEPYTHON_H_
