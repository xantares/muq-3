#ifndef MODPIECEDENSITYPYTHON_H_
#define MODPIECEDENSITYPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/ModPieceDensity.h"

namespace muq { 
  namespace Modelling {
    void ExportModPieceDensity();
  } // namespace Modelling 
} // namespace muq

#endif
