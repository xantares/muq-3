
#ifndef RANDVARPYTHON_H_
#define RANDVARPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Modelling/RandVar.h"

namespace muq {
namespace Modelling {
/// A python wrapper around muq::Modelling::RandVar
class RandVarPython : public RandVar, public boost::python::wrapper<RandVar> {
public:

  RandVarPython(Eigen::VectorXi const& inputSizes,
                int const              outputSize,
                bool const             hasDirectGradient,
                bool const             hasDirectJacobian,
                bool const             hasDirectJacobianAction,
                bool const             hasDirectHessian);

  boost::python::list PySample(boost::python::list const& pyInput, int NumSamps);
};

/// Export muq::modelling::RandVar class to python
void ExportRandVar();
} // namespace modelling
} // namespace muq

#endif // ifndef RANDVARPYTHON_H_
