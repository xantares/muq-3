#ifndef POINTCACHEPYTHON_H_
#define POINTCACHEPYTHON_H_

#include "MUQ/Modelling/PointCache.h"

namespace muq {
  namespace Modelling {
    void ExportPointCache();
  } // namespace Modelling
} // namespace muq

#endif
