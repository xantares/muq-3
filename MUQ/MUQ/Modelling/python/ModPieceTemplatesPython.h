
#ifndef MODPIECETEMPLATESPYTHON_H_
#define MODPIECETEMPLATESPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/config.h"

#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Modelling/python/ModPiecePython.h"
#include "MUQ/Utilities/python/PythonTranslater.h"

namespace muq {
namespace Modelling {
/// Enable evaluation of OneInputNoDerivModPiece class in python
class OneInputNoDerivModPiecePython : virtual public ModPiecePython {
public:

  OneInputNoDerivModPiecePython(int const inputSize, int const outputSize);

  PYTHON_EVALUATE()

private:

  /// Call the python evaluation

  /**
   * Translate vectors to/from Eigen to python
   */
  virtual Eigen::VectorXd     EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

  /// Allow the user to call the gradient by finite difference function from python
  virtual boost::python::list PyGradientByFD(boost::python::list const& inputs,
                                             boost::python::list const& sensIn,
                                             int const                  inputDimWrt) override;

  /// Allow the user to call the Hessian by finite difference function from python
  virtual boost::python::list PyHessianByFD(boost::python::list const& inputs,
                                            boost::python::list const& sensIn,
                                            int const                  inputDimWrt) override;

  /// Specific gradient funciton if there is only one input
  boost::python::list PyGradientByFDOneInput(boost::python::list const& inputs,
                                             boost::python::list const& sensIn);

  /// Specific gradient funciton if there is only one input
  boost::python::list PyHessianByFDOneInput(boost::python::list const& inputs,
                                            boost::python::list const& sensIn);
};

/// Enable evaluation of OneInputAdjointModPiece class in python
class OneInputAdjointModPiecePython : virtual public OneInputNoDerivModPiecePython {
public:

  OneInputAdjointModPiecePython(int const inputSize, int const outputSize);

  PYTHON_GRADIENT()

private:

  /// Call the gradient evaluation

  /**
   * Translate vectors to/from Eigen to python
   */
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sens,
                                       int const                           inputDimWrt) override;
};

/// Enable evaluation of OneInputJacobianModPiece class in python
class OneInputJacobianModPiecePython : virtual public OneInputNoDerivModPiecePython {
public:

  OneInputJacobianModPiecePython(int const inputSize, int const outputSize);

  PYTHON_JACOBIAN()

private:

  /// Call the Jacobian from python
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int inputDimWrt) override;
};

/// Enable evaluation of OneInputAdjointJacobianModPiece class in python
class OneInputAdjointJacobianModPiecePython : virtual public OneInputAdjointModPiecePython,
                                              virtual public OneInputJacobianModPiecePython {
public:

  OneInputAdjointJacobianModPiecePython(int const inputSize, int const outputSize);

  PYTHON_JACOBIAN_ACTION()

private:

  /// Call the Jacobian action from python
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  /// Define a default function for Jacobian action to call if there is no python implementation

  /**
   * This will be called if the user as indicated there is a python implemenatation of the Jacobian but has not
   * implmented.  It is finite difference but requires extra python vector Eigen::VectorXd translations and therefore
   * has
   * extra overhead.
   */
  virtual boost::python::list PyJacobianActionImplDefault(boost::python::list const& inputs,
                                                          boost::python::list const& target,
                                                          int const                  inputDimWrt) override;
};

/// Enable evaluation of OneInputFullModPiece class in python
class OneInputFullModPiecePython : virtual public OneInputAdjointJacobianModPiecePython {
public:

  OneInputFullModPiecePython(int const inputSize, int const outputSize);

  PYTHON_HESSIAN()

private:

  /// Call the Hessian evaluation
  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sens,
                                      int const                           inputDimWrt) override;
};

// update python MUQmodelling module
void ExportModPieceTemplates();
} // namespace modelling
} // namespace muq

#endif // ifndef MODPIECETEMPLATESPYTHON_H_
