
#ifndef LINEARMODEL_H_
#define LINEARMODEL_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/LinearModel.h"

namespace muq {
namespace Modelling {
/// Python wrapper around muq::modelling::LinearModel
class LinearModelPython : public LinearModel, public boost::python::wrapper<LinearModel> {
public:

  LinearModelPython(boost::python::list const& Ain);

  LinearModelPython(boost::python::list const& bin, bool useId);

  LinearModelPython(boost::python::list const& bin, boost::python::list const& Ain);
};

/// Tell python interface about muq::modelling::LinearModel
void ExportLinearModel();
} // namespace muq
} // namespace modelling

#endif // ifndef LINEARMODEL_H_
