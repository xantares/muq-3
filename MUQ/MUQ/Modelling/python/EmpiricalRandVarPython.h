/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *  USA.
 *
 *  MIT UQ Library
 *  Copyright (C) 2013 MIT
 */

#ifndef EMPIRICALRANDVARPYTHON_H_
#define EMPIRICALRANDVARPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/EmpiricalRandVar.h"

namespace muq {
namespace Modelling {
/// Python wrapper around muq::modelling::EmpiricalRandVar
class EmpiricalRandVarPython : public EmpiricalRandVar, public boost::python::wrapper<EmpiricalRandVar> {
public:

  EmpiricalRandVarPython(int dim);

  /// Decide whether we are constructing with a vector or a matrix

  /**
   *  There is a vector and a matrix constructor; in python these are both lists.  This fuction takes the python list
   * with a flag indicating vector or matrix and calls the correct constructor.
   *  @param[in] input Vector or matrix python input
   *  @param[in] isVector True if vector; false if matrix
   */
  static std::shared_ptr<EmpiricalRandVarPython> Create(boost::python::list const& input, bool isVector);

  EmpiricalRandVarPython(Eigen::VectorXd const& input);

  EmpiricalRandVarPython(Eigen::MatrixXd const& input);
};

/// Tell python interface about muq::modelling::EmpiricalRandVar
void ExportEmpiricalRandVar();
} // namespace modelling
} // namespace muq

#endif // ifndef EMPIRICALRANDVARPYTHON_H_
