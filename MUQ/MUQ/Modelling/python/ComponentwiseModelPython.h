
#ifndef COMPONENTWISEMODEPYTHON_H_
#define COMPONENTWISEMODEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"
#include "MUQ/Modelling/AnalyticFunctions/ExpModel.h"
#include "MUQ/Modelling/AnalyticFunctions/SinModel.h"
#include "MUQ/Modelling/AnalyticFunctions/CosModel.h"
#include "MUQ/Modelling/AnalyticFunctions/TanModel.h"
#include "MUQ/Modelling/AnalyticFunctions/CscModel.h"
#include "MUQ/Modelling/AnalyticFunctions/SecModel.h"
#include "MUQ/Modelling/AnalyticFunctions/CotModel.h"
#include "MUQ/Modelling/AnalyticFunctions/Pow10Model.h"
#include "MUQ/Modelling/AnalyticFunctions/Pow2Model.h"
#include "MUQ/Modelling/AnalyticFunctions/AbsModel.h"
#include "MUQ/Modelling/AnalyticFunctions/LogModel.h"

namespace muq {
namespace Modelling {
/// Python wrapper around muq::Modelling::Componentwisemodel
/**
 *  Allows user to implement a componentwise model class in python
 */
class ComponentwiseModelPython : public ComponentwiseModel, public boost::python::wrapper<ComponentwiseModel> {
public:

  ComponentwiseModelPython(int dim) : ComponentwiseModel(dim) {}

  virtual double BaseFunc(double const& input) const override
  {
    return this->get_override("BaseFunc") (input);
  }

  virtual double BaseDeriv(double const& input) const override
  {
    return this->get_override("BaseDeriv") (input);
  }

  virtual double BaseSecondDeriv(double const& input) const override
  {
    return this->get_override("BaseSecondDeriv") (input);
  }
};

inline void ExportComponentwiseModel()
{
  // register override class to implement python version
  boost::python::class_<ComponentwiseModelPython, std::shared_ptr<ComponentwiseModelPython>,
                        boost::python::bases<OneInputFullModPiece>, boost::noncopyable> exportCompModel(
    "ComponentwiseModel",
    boost::python::init<int>());

  exportCompModel.def("BaseFunc", boost::python::pure_virtual(&ComponentwiseModelPython::BaseFunc));
  exportCompModel.def("BaseDeriv", boost::python::pure_virtual(&ComponentwiseModelPython::BaseDeriv));
  exportCompModel.def("BaseSecondDeriv", boost::python::pure_virtual(&ComponentwiseModelPython::BaseSecondDeriv));

  boost::python::implicitly_convertible<std::shared_ptr<ComponentwiseModelPython>, std::shared_ptr<ModPiece> >();

  // register Exp model
  boost::python::class_<ExpModel, std::shared_ptr<ExpModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportExp(
    "ExpModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<ExpModel>, std::shared_ptr<ModPiece> >();

  // register Sin model
  boost::python::class_<SinModel, std::shared_ptr<SinModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportSin(
    "SinModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<SinModel>, std::shared_ptr<ModPiece> >();

  // register Cos model
  boost::python::class_<CosModel, std::shared_ptr<CosModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportCos(
    "CosModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<CosModel>, std::shared_ptr<ModPiece> >();

  // register Tan model
  boost::python::class_<TanModel, std::shared_ptr<TanModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportTan(
    "TanModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<TanModel>, std::shared_ptr<ModPiece> >();

  // register Csc model
  boost::python::class_<CscModel, std::shared_ptr<CscModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportCsc(
    "CscModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<CscModel>, std::shared_ptr<ModPiece> >();

  // register Sec model
  boost::python::class_<SecModel, std::shared_ptr<SecModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportSec(
    "SecModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<SecModel>, std::shared_ptr<ModPiece> >();

  // register Cot model
  boost::python::class_<CotModel, std::shared_ptr<CotModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportCot(
    "CotModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<CotModel>, std::shared_ptr<ModPiece> >();

  // register Pow10 model
  boost::python::class_<Pow10Model, std::shared_ptr<Pow10Model>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportPow10(
    "Pow10Model",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<Pow10Model>, std::shared_ptr<ModPiece> >();

  // register Pow2 model
  boost::python::class_<Pow2Model, std::shared_ptr<Pow2Model>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportPow2(
    "Pow2Model",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<Pow2Model>, std::shared_ptr<ModPiece> >();

  // register Abs model
  boost::python::class_<AbsModel, std::shared_ptr<AbsModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportAbs(
    "AbsModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<AbsModel>, std::shared_ptr<ModPiece> >();

  // register Log model
  boost::python::class_<LogModel, std::shared_ptr<LogModel>,
                        boost::python::bases<ComponentwiseModel, OneInputFullModPiece>, boost::noncopyable> exportLog(
    "LogModel",
    boost::python::init<int>());

  boost::python::implicitly_convertible<std::shared_ptr<LogModel>, std::shared_ptr<ModPiece> >();
}
} // namespace Modelling
} // namespace muq

#endif // ifndef COMPONENTWISEMODEPYTHON_H_
