
#ifndef SCALEDMVNORMDENS_H_
#define SCALEDMVNORMDENS_H_

#include <memory>

#include <Eigen/Core>
#include <Eigen/Dense>

#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/ConstantMVNormSpecification.h"

namespace muq {
namespace Modelling {
class ScaledMvNormDens : public Density {
public:

  ///Constructor using an already provided specification. May only have one vector input
  ScaledMvNormDens(std::shared_ptr<ConstantMVNormSpecification> specification) :
    Density(Eigen::Vector2i(specification->dim, 1), true, true, true,
            specification->HasDirectInverseCovariance()), specification(specification)
  {}

  ///Constructs a specification from input arguments and calls the base constructor

  /**
   * Check the ConstantMVNormSpecification constructors for options
   **/
  template<typename ... Args>
  ScaledMvNormDens(Args ... args) : ScaledMvNormDens(std::make_shared<ConstantMVNormSpecification>(args ...)) {}

  std::shared_ptr<ConstantMVNormSpecification> const specification;

private:

  virtual double          LogDensityImpl(std::vector<Eigen::VectorXd> const& input)  override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override;
};
}
}

#endif // ifndef MvNormDens_h_
