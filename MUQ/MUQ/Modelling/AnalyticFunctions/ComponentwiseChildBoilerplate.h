
#ifndef _FUNCNAMEExpr_h
#define _FUNCNAMEExpr_h


#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"
#include "MUQ/Modelling/ModGraph.h"

namespace muq {
namespace Modelling {
/**
 *  @class FUNCNAMEModel
 *  @ingroup Modelling
 *  @brief Implemenation of componentwise FUNCNAME
 *
 */
class FUNCNAMEModel : public ComponentwiseModel {
public:

  FUNCNAMEModel(int dim) : ComponentwiseModel(dim) {}

  virtual ~FUNCNAMEModel() = default;
  
  /** Apply the base symbolic function
   *  @param[in] input  the value to be altered
   *  @return f(x) where f is a simple analytic functions such as exp(x), sin(x), etc...
   */
  virtual double BaseFunc(const double& x) const
  {
    return FUNCSTRING;
  }

  /** return the derivative of the base symbolic function evaluated at point
   *  @param[in] input  where we want to evaluate the derivative
   *  @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseDeriv(const double& x) const
  {
    return DERIVSTRING;
  }
  
   /** return the second derivative of the base symbolic function evaluated at point
   *   @param[in] input  where we want to evaluate the second derivative
   *   @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseSecondDeriv(const double& x) const
  {
    return DERIVSECONDSTRING;

   }
};

///Create direct operator-like calls
inline std::shared_ptr<ModGraph> FUNCNAME(const std::shared_ptr<ModGraph>& x)
{
  //copy the graph
  auto newGraph = x->DeepCopy();
  
  //Find out the size, asserting that the graph only has one output
  auto outputSize = newGraph->outputSize();
  
  //Create the new model
  auto newComponentwiseNode = std::make_shared<FUNCNAMEModel>(outputSize);

  //add it to the graph
  newGraph->AddNode(newComponentwiseNode, newComponentwiseNode->GetName());
  //connect the nodes
  newGraph->AddEdge(x->GetOutputNodeName(), newComponentwiseNode->GetName(), 0);

  return newGraph;
}

inline std::shared_ptr<ModGraph> FUNCNAME(const std::shared_ptr<ModPiece>& x)
{
  return FUNCNAME(std::make_shared<ModGraph>(x));
}
}
} // namespace muq


#endif // ifndef _FUNCNAMEExpr_h
