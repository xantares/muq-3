#ifndef POINTOBSERVER_H_
#define POINTOBSERVER_H_

#include "libmesh/numeric_vector.h"

#include "MUQ/Utilities/VectorTranslater.h"

#include "MUQ/Modelling/ModPiece.h"

#include "MUQ/Pde/GenericEquationSystems.h"

namespace muq {
namespace Pde {
/// Observer to compute a field at specific points
class PointObserver : public muq::Modelling::ModPiece {
public:

  /// Create a point sensor given an equation system, the system name, and points
  /**
   *  @param[in] inputSizes The number of inputs
   *  @param[in] points A vector of points.  Each Eigen::VectorXd must be the same size as the number of dimensions
   *  @param[in] stateIndex The index of the input we are observing
   *  @param[in] sysName The name of the system (it must already be in eqnsystem)
   *  @param[in] system The equation system that contains the specific system being sensed
   */
  PointObserver(Eigen::VectorXi const                       & inputSizes,
                std::vector<Eigen::VectorXd> const          & points,
                unsigned int const                            stateIndex,
                std::string const                           & sysName,
                std::shared_ptr<GenericEquationSystems> const eqnsystem);

private:

  /// Compute the field at specified points
  /**
   *  Projects the input onto PointObserver::sysName and then computes the solution at PointObserver::points. It returns
   *the solution at each sensor in the same order as the points are listed.  If there is more than one unknown in the
   *system the order is
   *  \f{equation}{
   *  \left[\begin{array}{ccccccccccc}
   *  v_1(\mathbf{x}_1) & v_2(\mathbf{x}_1) & ... & v_n(\mathbf{x}_1) & | & ... & | & v_1(\mathbf{x}_m) &
   * v_2(\mathbf{x}_m) & ... & v_n(\mathbf{x}_m)
   *  \end{array} \right]^T.
   *  \f}
   *  @param[in] inputs The same inputs as the associated system we are observing (muq::Pde::PDEBase)
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  /// The index of the input we are observing
  const unsigned int stateIndex;

  /// A list of points where there are sensors
  std::vector<libMesh::Point> points;

  /// The equation system where the field is stored
  std::shared_ptr<libMesh::EquationSystems> system;
};
} // namespace Pde
} // namespace muq


#endif // ifndef POINTOBSERVER_H_
