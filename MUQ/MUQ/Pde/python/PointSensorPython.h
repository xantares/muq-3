
#ifndef POINTSENSORPYTHON_H_
#define POINTSENSORPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Pde/PointSensor.h"

namespace muq {
namespace Pde {
/// Python wrapper around muq::Pde::PointSensor
class PointSensorPython : public PointSensor, public boost::python::wrapper<PointSensor> {
public:

  PointSensorPython(int const inputSize, boost::python::list const& points, std::string const& sysName,
                    std::shared_ptr<GenericEquationSystems> const eqnsystem) : PointSensor(inputSize,
                                                                                           muq::Utilities::
                                                                                           PythonListToVector(points),
                                                                                           sysName, eqnsystem) {}

  /// Allow user to call evaluate with a list (rather than a list of lists)
  inline boost::python::list PyEvaluate(boost::python::list const& inList)
  {
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(Evaluate(muq::Utilities::GetEigenVector<Eigen
                                                                                                                  ::
                                                                                                                  VectorXd>(
                                                                              inList)));
  }

private:
};

/// Export the point sensor to python
inline void ExportPointSensor()
{
  boost::python::class_<PointSensorPython, std::shared_ptr<PointSensorPython>, boost::noncopyable,
                        boost::python::bases<muq::Modelling::ModPiece> > exportPointSensor(
    "PointSensor",
    boost::python::init<int const, boost::python::list const&, std::string const&,
                        std::shared_ptr<GenericEquationSystems> const>());

  exportPointSensor.def("Evaluate", &PointSensorPython::PyEvaluate);

  boost::python::implicitly_convertible<std::shared_ptr<PointSensorPython>,
                                        std::shared_ptr<muq::Modelling::ModPiece> >();
}
} // namespace Pde
} // namespace muq

#endif // ifndef POINTSENSORPYTHON_H_
