#ifndef ELEMENTINFORMATION_H_
#define ELEMENTINFORMATION_H_

#include <Eigen/Core>

#include "Sacado.hpp"
#include "Sacado_Fad_DVFad.hpp"
#include "Sacado_Fad_SimpleFad.hpp"
#include "Sacado_CacheFad_DFad.hpp"
#include "Sacado_CacheFad_SFad.hpp"
#include "Sacado_CacheFad_SLFad.hpp"

#include <memory>
#include <unordered_map>

#include <boost/bimap.hpp>

#include "libmesh/mesh.h"
#include "libmesh/mesh_base.h"
#include "libmesh/equation_systems.h"
#include "libmesh/dof_map.h"

#include "libmesh/quadrature_gauss.h"

namespace muq {
  namespace Pde {
    
    /// Type def. for the bimap between unsigned int and string 
    typedef boost::bimap<unsigned int, std::string> bimap;
    
    /// forward AD type with fixed storage
    typedef Sacado::CacheFad::SLFad<double, 30> FadType;
    
    /// forward AD type with dynamic storage
    typedef Sacado::CacheFad::DFad<double> DFadType;
    
    /// Stored shape functions on an element
    /**
     *  Each element has a its own shape functions, this structure stores the shape functions \f$\phi(x)\f$ and their
     *derivatives  \f$\nabla \phi(x)\f$ at the quadrature points.
     */
    struct BasisInfo {
      /// DELETED: Do not allow default constructor
      BasisInfo() = delete;
      
      /// Store the basis information from a libMesh basis
      /**
       *  @param[in] fe The libMesh finite element basis on each element
       */
      BasisInfo(libMesh::AutoPtr<libMesh::FEBase> const& fe);
      
      /// Shape functions at each quadrature point
      std::vector<std::vector<libMesh::Real> > phi;
      
      /// Shape function gradient at each quadrature point
      /**
       *  Each element of the outer vector corresponds to a quadrature point.  The inner vector is the gradient.
       */
      std::vector<std::vector<libMesh::RealGradient> > dphi;
    };
    
    
    /// Store all information about and element
    /**
     *  Store the element Jacobian, mapping from the mesh element to the reference element, and quadrature points for each
     *element.
     *
     *  The element may have more than one FE type (e.g. linear and quadratic if one unknown has linear basis functions and
     *another has quadratic).  For each FE type store the shape functions and their derivatives at the element quadrature
     *points.
     */
    struct ElemInfo {
      /// DELETED: Do not allow default constructor
      ElemInfo() = delete;
      
      /// Store the element information from libMesh
      /**
       *  @param[in] feType A vector each field's FE type (linear or quadratic)
       *  @param[in] feVec The FE type information (shape functions, ect...) for each FE type
       */
      ElemInfo(std::vector<libMesh::FEType> const& feType, std::vector<libMesh::AutoPtr<libMesh::FEBase> > const& feVec);
      
      /// The element Jacobian (to map to and from the reference element)
      std::vector<libMesh::Real> JxW;
      
      /// The quadrature points
      std::vector<libMesh::Point> qPoint;
      
      /// The shape function information for each FE type
      /**
       *  Store the values and their derivatives at the quadrature points.  Stored as a map so if more than one unknown may
       *have the same FE type only one copy of the muq::Pde::BasisInfo is stored.
       */
      std::map<const libMesh::FEType, std::shared_ptr<BasisInfo> > basisMap;
    };
    
    /// Store all information about the element faces (for domain boundaries)
    /**
     *  Store the element Jacobina, mapping from the mesh element boundary to the reference element boundary, and quadrature
     * points for each face.
     *
     *  The faces can also have more than one FE type (e.g. linear or quadratic  if one unknown has linear basis functions
     * and another has quadratic).  For each FE type store the shape functions and their derivatives at the element boundary
     * quadrature points.
     */
    struct FaceInfo {
      /// DELETED: Do not allow default constructor
      FaceInfo() = delete;
      
      /// Store the element boundary information from libMesh
      /**
       *  @param[in] feType A vector each field's FE type (linear or quadratic)
       *  @param[in] feVec The FE type information (shape functions, ect...) for each FE type
       */
      FaceInfo(std::vector<libMesh::FEType> const& feType, std::vector<libMesh::AutoPtr<libMesh::FEBase> > const& feVec);
      
      /// The element Jacobian (to map to and from the reference element)
      std::vector<libMesh::Real> JxW;
      
      /// The quadrature points
      std::vector<libMesh::Point> qPoint;
      
      /// The normal vector at each quadrature point
      std::vector<libMesh::Point> normals;
      
      /// The shape function information for each FE type
      /**
       *  Store the values and their derivatives at the quadrature points.  Stored as a map so if more than one unknown may
       *have the same FE type only one copy of the muq::Pde::BasisInfo is stored.
       */
      std::map<const libMesh::FEType, std::shared_ptr<BasisInfo> > basisMap;
    };
    
    /// Store information about each element in a mesh for a specific element type
    class ElementInformation {
    public:
      
      ElementInformation() = delete;
      
      ElementInformation(std::vector<libMesh::FEType> const& feType, unsigned int quadOrder, libMesh::MeshBase const& mesh);
      
      /// Return the ith element information
      std::shared_ptr<ElemInfo> Get(unsigned int const i) const;
      
      /// Return the ith boundary element information
      std::shared_ptr<FaceInfo> GetFace(unsigned int const i) const;
      
    private:
      
      /// Information about elements
      std::vector<std::shared_ptr<ElemInfo> > elemVec;
      /// Information about boundary elements
      std::vector<std::shared_ptr<FaceInfo> > faceVec;
    };
    
    /// Store element DOFs and element matrix/vectors
    class ElementDOFs {
    public:
      
      ElementDOFs() = delete;
      
      ElementDOFs(unsigned int const nVars);
      
      /// reset element matrix and vector at an element
      /**
         Resize vectors and matrices to match the number of degrees of freedom on the element
         @param[in] el The current element
         @param[in] varNums The variable numbers in the equation system
         @param[in] dofMap local->global degree of freedom map
       */
      void Reset(libMesh::MeshBase::const_element_iterator const& el, bimap const& vars, libMesh::DofMap const& dofMap);
      
      /// Sum into the element vector given a variable and the element DOF
      void SumIntoElementVector(unsigned int const var, unsigned int const dof, double const val);
      
      /// Sum into the element vector given a variable and the element DOF
      void SumIntoElementVector(unsigned int const var, unsigned int const dof, FadType const val);
      
      /// Sum into the element stiffness matrix given the two variables interacting and their DOFs
      void SumIntoElementStiffnessMatrix(unsigned int const var1,
					 unsigned int const var2,
					 unsigned int const dof1,
					 unsigned int const dof2,
					 double const       val);
      
      /// Sum into the element mass matrix given the two variables interacting and their DOFs
      void SumIntoElementMassMatrix(unsigned int const var1,
				    unsigned int const var2,
				    unsigned int const dof1,
				    unsigned int const dof2,
				    double const       val);
      
      /// Get a global DOF given a variable and its local DOF
      libMesh::dof_id_type                  GetGlobalDOF(unsigned int const var, unsigned int const dof) const;
      
      /// Get the number of DOFs for a given variable
      unsigned int                          GetNumDOFs(unsigned int const var) const;
      
      /// Get the total number of DOFs
      unsigned int                          GetNumDOFs() const;
      
      /// Get element stiffness matrix
      libMesh::DenseMatrix<libMesh::Number> GetElementStiffnessMatrix() const;
      
      /// Get element mass matrix
      libMesh::DenseMatrix<libMesh::Number> GetElementMassMatrix() const;
      
      /// Get elment vector
      libMesh::DenseVector<libMesh::Number> GetElementVector() const;
      
      /// Get elment vector
      libMesh::DenseVector<FadType>         GetElementVectorAD() const;

      /// Get local to global DOF map
      std::vector<libMesh::dof_id_type>     GetLocalToGlobalMap() const;
      
    private:
      
      /// element stiffness matrix
      libMesh::DenseMatrix<libMesh::Number> Ke;
      /// unknown interaction submatrices (stiffness)
      std::vector<std::vector<std::shared_ptr<libMesh::DenseSubMatrix<libMesh::Number> > > > KeSub;
      
      /// element mass matrix
      libMesh::DenseMatrix<libMesh::Number> Me;
      /// unknown interaction submatrices (mass)
      std::vector<std::vector<std::shared_ptr<libMesh::DenseSubMatrix<libMesh::Number> > > > MeSub;
      
      /// element vector
      libMesh::DenseVector<libMesh::Number> Fe;
      /// unknown subvectors
      std::vector<std::shared_ptr<libMesh::DenseSubVector<libMesh::Number> > > FeSub;
      
      // element vector; sacado DFadType (used to compute derivative information)
      libMesh::DenseVector<FadType> FeFad;
      // sacado type subvectors
      std::vector<std::shared_ptr<libMesh::DenseSubVector<FadType> > > FeSubFad;
      
      /// all local -> global DOFs
      std::vector<libMesh::dof_id_type> dofIndices;
      /// each unknown -> global DOFs
      std::vector<std::vector<libMesh::dof_id_type> > dofIndicesVar;
    };
  } // namespace Pde
} // namespace MUQice

#endif // ifndef ELEMENTINFORMATION_H_
