#ifndef TRANSIENT_H_
#define TRANSIENT_H_

#include "MUQ/Pde/PDE.h"

namespace muq {
  namespace Pde {
    class Transient : public PDE {
    public:

      Transient(Eigen::VectorXi const& inSizes, std::shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para);

    protected:
      
    private:

      /// Compute the element mass matrix integrand at quadrature point
      /**
         Can be overridden by children.  Defaults to
         \f[
         M(x_q) = \phi_i(x_q) \phi_j(x_q) \mbox{ if the variable names match}
         \f]
         \noindent and is zero otherwise.  \f$x_q\f$ is the quadrature point.  (Note: \f$\phi_i(x_q)\f$ and \f$\phi_j(x_q)\f$ are both inputs)
         @param[in] varName1 First unknown block name
         @param[in] phiV1 Shape functions for first unknown block at a quadrature point \f$\phi_i(x_q)\f$
         @param[in] dphiV2 Shape function gradient for first unknown block at a quadrature point \f$\nabla \phi_i(x_q)\f$
         @param[in] varName1 Second unknown block name
         @param[in] phiV1 Shape functions for second unknown block at a quadrature point \f$\phi_j(x_q)\f$
         @param[in] dphiV2 Shape function gradient for second unknown block at a quadrature point \f$\nabla \phi_j(x_q)\f$
         \return Element mass matrix integrand evaluated at a quadrature point
      */
      virtual double MassMatrixImpl(std::string const& varName1, libMesh::Real const& phiV1, libMesh::RealGradient const& dphiV1, std::string const& varName2, libMesh::Real const& phiV2, libMesh::RealGradient const& dphiV2) const;

      /// Compute the mass matrix 
      /**
	 Compute and store the mass matrix.
       */
      virtual void ComputeMassMatrix() override;

      /// Compute the mass matrix on an element
      /**
         @param[in] elInfo The basis functions, basis function derivatives, quad points, and jacobian for the element
         @param[in] elemDOFs Map from local to global DOFs
      */
      void ComputeElementMassMatrix(std::shared_ptr<ElemInfo> const& elInfo, std::shared_ptr<ElementDOFs> const& elemDOFs);
      
      /// Compute the input sizes for the solver
      /**
	 @param[in] outSize The output size 
	 @param[in] paraInSizes The input sizes for the non initial guess or time parameters
	 \return The input sizes
      */
      Eigen::VectorXi InputSizes(unsigned int const outSize, Eigen::VectorXi const& paraInSizes) const;

    };
  } // namespace Pde
} // namespace muq

#define TRANSIENT_DIRICHLET(FUNCNAME)					\
  virtual double DirichletBCs(std::string const & varName, libMesh::Point const & point, \
                              double const time) const override		\
  {									\
    return FUNCNAME<double>(varName, point, time);			\
  }									\
  									\
  virtual muq::Pde::FadType DirichletBCs(std::string const & varName, libMesh::Point const & point, \
                                         muq::Pde::FadType const time) const override \
  {									\
    return FUNCNAME<muq::Pde::FadType>(varName, point, time);		\
  }

#endif
