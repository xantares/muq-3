#ifndef GENERICEQUATIONSYSTEM_H_
#define GENERICEQUATIONSYSTEM_H_

#include <boost/property_tree/xml_parser.hpp>

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "libmesh/equation_systems.h"
#include "libmesh/exodusII_io.h"

#include "MUQ/Pde/GenericMesh.h"

namespace muq {
namespace Pde {
  /// Store all the system of equations corresponding to a specific mesh
  /**
     Any system with a spatial component must be associated with a mesh.  This class stores a single mesh and all of they system associated with it.
   */
class GenericEquationSystems {
public:

  /// Construct an equation system from boost::ptree
  /**
     The ptree only needs to store the options for the mesh.  See muq::Pde::GenericMesh for the specific options.
     @param[in] params The options for the mesh.
   */
  GenericEquationSystems(boost::property_tree::ptree const& params);

#if MUQ_PYTHON == 1
  GenericEquationSystems(boost::python::dict const& dict);
#endif // MUQ_PYTHON == 1

  /// Construct an equation system from xml file
  /**
     @param[in] xmlFile The options stored in an xml file
   */
  GenericEquationSystems(std::string const& xmlFile);

  /// Get the equation systems pointer
  /**
     \return The pointer to the libMesh::Equation systems 
   */
  std::shared_ptr<libMesh::EquationSystems> GetEquationSystemsPtr() const;

  /// Get a pointer to the mesh 
  /**
     \return The pointer to the muq::Pde::GenericMesh associated with all of the systems
   */
  std::shared_ptr<GenericMesh> GetGenericMeshPtr() const;

  /// print the solution to an Exodus file
  /**
     Prints all of the fields in all of the systems to an exodus file.  See libMesh documentation for details.
     @param[in] file The path and file name
   */
  void PrintToExodus(std::string const& file);

private:

  /// Pointer to the libMesh equation system this class wraps around
  std::shared_ptr<libMesh::EquationSystems> eqnsystem;

  /// The mesh associated with this equation system
  std::shared_ptr<GenericMesh> mesh;
};
} // namespace Pde
} // namespace muq

#endif // ifndef GENERICEQUATIONSYSTEM_H_


