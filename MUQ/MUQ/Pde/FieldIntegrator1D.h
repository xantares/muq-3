#ifndef FIELDINTEGRATOR1D_H_
#define FIELDINTEGRATOR1D_H_

#include "MUQ/Pde/PDEModPiece.h"

namespace muq {
namespace Pde {
/// Integrate a field over the one dimensional domain.
/**
 *  Suppose \f$u(x)\f$ is a known field over a line mesh (could be nonregular, but must be one dimensional).  The
 *quantity of interest is
 *  \f[
 *  U(x) = \int_0^x u(x^\prime) \, dx^\prime,
 *  \f] which can be written as the ODE
 *  \f[
 *  \frac{d^2U}{dx^2} = \frac{du}{dx}
 *  \f] with boundary condtion
 *  \f{eqnarray}{
 *  U(0) &=& 0 \\
 *  \left. \frac{dU}{dx}\right|_{x=1} = u(1)
 *  \f}
 *  This class solves this equation given scalar u(x).
 */
class FieldIntegrator1D : public Nontransient {
public:

  /// Required constructor to make a PDE
  /**
   *  Also checks to make sure the unknown field \f$u(x)\f$ is a scalar and the mesh is one dimensional.
   */
  FieldIntegrator1D(Eigen::VectorXi const& inputSizes, std::shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para);

private:

  /// Implement the weak form
  /**
   *  The weak form of the PDE is
   *  \f[
   *  \int_0^L \frac{d \phi}{dx} \frac{dU}{dx} - \phi u \, dx = 0
   *  \f]
   */
  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline returnScalar ResidualImpl(std::                                           string const&,
                                   libMesh::Real const                             phi,
                                   libMesh::RealGradient const                   & dphi,
                                   std::vector<unknownScalar> const              & V,
                                   std::vector<std::vector<unknownScalar> > const& grad) const
  {
    parameterScalar u    = GetScalarParameter<parameterScalar>(fieldVariableName);
    parameterScalar dudx = GetScalarParameter<parameterScalar>("d" + fieldVariableName + "dx");

    return -dphi(0) * grad[0][0] - phi * dudx;
  }
  
  RESIDUAL(ResidualImpl)

  /// Implement the weak form
  /**
   *  The weak form of the PDE gives boundary condition
   *  \f[
   *  \left.\frac{dU}{dx}\right|_{x=1}=u(1)
   *  \f]
   */
  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline std::vector<returnScalar> NeumannBCs(std::                 string const&,
                                              libMesh::Point const& pt,
                                              std::                 vector<unknownScalar> const&,
                                              std::                 vector<std::vector<unknownScalar> > const&) const
  {
    std::vector<returnScalar> neumman(3, 0.0);

    parameterScalar u = GetScalarParameter<parameterScalar>(fieldVariableName);

    neumman[0] = u;

    return neumman;
  }

  NEUMANN(NeumannBCs)

  /// The solution is exactly zero at \f$x=0\f$.
  virtual bool IsDirichlet(libMesh::Point const& point) const override;

  /// The name of the unknown field we are integrating
  std::string fieldVariableName;
};
} // namespace Pde
} // namespace muq

#endif // ifndef FIELDINTEGRATOR1D_H_
