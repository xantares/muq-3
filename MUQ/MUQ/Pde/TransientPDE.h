
#ifndef TRANSIENTPDE_H_
#define TRANSIENTPDE_H_

#include "MUQ/Pde/PDEBase.h"

namespace muq {
namespace Pde {
/// Compute the residual of a time dependent partial differential equations
/**
 *  If the partial differential equation has the form
 *  \f{equation}{
 *  \frac{\partial u}{\partial t} = D(u, t; \theta_1) - f(u, t; \theta_2)
 *  \f}
 *  where \f$D(u, t; \theta_1)\f$ is a, potentially nonlinear, differential operator and \f$f(u, t; \theta_2)\f$ is a,
 *potentially nonlinear, forcing.  Using a finite element spatial discretization the weak form is
 *  \f{equation}{
 *  \mathbf{M}(t) \frac{d \mathbf{u}}{d t} = \mathbf{J}(\mathbf{u}, t; \theta_1)\mathbf{u} - \mathbf{f}(\mathbf{u}, t;
 * \theta_2),
 *  \f}
 *  where \f$\mathbf{M}\f$ is the mass matrix,
 *  \f$\mathbf{J} \equiv \nabla_{\mathbf{u}} \left(D(u, t; \theta_1) - f(u,t; \theta_2)\right)\f$ is the stiffness
 *matrix, and \f$\mathbf{f}\f$ is the discretized forcing.  The residual
 *  \f{equation}{
 *  \mathbf{r}(\mathbf{u}, t; \theta) \equiv \mathbf{M}_{dlmm}^{-1}\left( \mathbf{J}\mathbf{u} - \mathbf{f} \right),
 *  \f}
 *  where \f$\mathbf{M}_{dlmm}\f$ is the diagonal lumped mass matrix, is computed by evaluating children of this class.
 *Derivative information is computed using automatic differentiation.
 */
class TransientPDE : public PDEBase {
public:

protected:

  /// Protected construct called by children
  /**
   *  @param[in] inputSizes A vector determining the size of each input parameter
   *  @param[in] outputSize The output size
   *  @param[in] eqnsystem The libmesh::EquationSystems that holds the mesh for this problem
   *  @param[in] param A boost::ptree will all the information needed to make the PDEBase
   */
  TransientPDE(Eigen::VectorXi const& inputSizes, unsigned int const outputSize,
               std::shared_ptr<GenericEquationSystems> const& eqnsystem,
               boost::property_tree::ptree const& param);

private:

  /// Evaluate the residual
  /**
   *  Compute the residual
   *  \f{equation}{
   *  \mathbf{r}(\mathbf{u}, t; \theta) \equiv \mathbf{M}_{dlmm}^{-1}\left( \mathbf{J}\mathbf{u} - \mathbf{f} \right),
   *  \f}
   *  The first input is the time, the second input is the current solution (initial conditions in the case of
   * \f$t=0\f$), and the rest are inputs to the system.
   *  @param[in] inputs A vector of parameter inputs \f$\theta\f$
   *  \return The residual \f$\mathbf{r}\f$
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  /// Compute the Jacobian with respect to the state
  /**
   *  Compute the Jacobian
   *  \f{equation}{
   *  \nabla_{\mathbf{u}} \mathbf{r}(\mathbf{u}; \theta) \equiv \nabla_{\mathbf{u}}\left( \mathbf{J}\mathbf{u} -
   * \mathbf{f}\right)
   *  \f}
   *  @param[in] inputs A vector of parameter inputs \f$\theta\f$
   *  \return The Jacobian with respect to the state
   */
  virtual Eigen::MatrixXd JacobianWRTState(std::vector<Eigen::VectorXd> const& inputs) override;

  /// Compute the Jacobian with respect to an input parameter
  /**
   *  Compute the Jacobian
   *  \f{equation}{
   *  \nabla_{\theta_i} \mathbf{r}(\mathbf{u}; \theta) \equiv \nabla_{\theta_i}\left( \mathbf{J}\mathbf{u} -
   * \mathbf{f}\right)
   *  \f}
   *  @param[in] inputs A vector of parameter inputs \f$\theta\f$
   *  @param[in] inputDimWrt The index of the parameter we are taking the Jacobian with respect to
   *  \return The Jacobian with respect to the state
   */
  virtual Eigen::MatrixXd JacobianWRTParameter(std::vector<Eigen::VectorXd> const& inputs,
                                               unsigned int const                  inputDimWrt) override;

  /// The first input is an initial guess
  /**
   *  Add one input (the same size as outputSize) since we know the first input has to be an initial guess for the
   *solver.
   *  @param[in] inputSizes Size of each input (except initial guess)
   *  @param[in] outputSize The size of the solution vector
   */
  static Eigen::VectorXi UpdateInputSizes(Eigen::VectorXi const& inputSizes, unsigned int const outputSize);

  /// Compute the mass matrix for each element
  /**
   *  @param[in] elInfo Shape functions, shape function derivatives, ect... for the element
   *  @param[in] elemDOFs Local to global DOF mapping
   */
  void ComputeMassMatrix(std::shared_ptr<ElemInfo>           elInfo,
                         std::shared_ptr<ElementDOFs> const& elemDOFs);

  /// Compute the element mass matrix integrand at quadrature point
  /**
   *  Can be overridden by children.  Defaults to
   *  \f[
   *  M(x_q) = \phi_i(x_q) \phi_j(x_q) \mbox{ if the variable names match}
   *  \f]
   *  \noindent and is zero otherwise.  \f$x_q\f$ is the quadrature point.  (Note: \f$\phi_i(x_q)\f$ and \f$\phi_j(x_q)\f$ are both
   *inputs)
   *  @param[in] varName1 First unknown block name
   *  @param[in] phiV1 Shape functions for first unknown block at a quadrature point \f$\phi_i(x_q)\f$
   *  @param[in] dphiV2 Shape function gradient for first unknown block at a quadrature point \f$\nabla \phi_i(x_q)\f$
   *  @param[in] varName1 Second unknown block name
   *  @param[in] phiV1 Shape functions for second unknown block at a quadrature point \f$\phi_j(x_q)\f$
   *  @param[in] dphiV2 Shape function gradient for second unknown block at a quadrature point \f$\nabla \phi_j(x_q)\f$
   *  \return Element mass matrix integrand evaluated at a quadrature point
   */
  virtual double ElementMassMatrix(std::string const          & varName1,
                                   libMesh::Real const        & phiV1,
                                   libMesh::RealGradient const& dphiV1,
                                   std::string const          & varName2,
                                   libMesh::Real const        & phiV2,
                                   libMesh::RealGradient const& dphiV2) const;

  /// Enforce Dirichlet conditions on the residual of the transient problem
  /**
   *  At a Dirichlet boundary point the right hand side of the equtaion because
   *  \f{eqnarray*}{
   *  \frac{\partial u}{\partial t} = \frac{\partial g(\mathbf{x}, t)}{\partial t}
   *  \f}
   *  where \f$g(\mathbf{x}, t)\f$ is a function determining the Dirichlet boundary condition.
   *  @param[out] residual Modified residual at DOFs on the Dirichlet boundary
   */
  void EnforceDirichletBCs(Eigen::VectorXd& residual) const;

  /// Return the Dirichlet boundary condition at a point
  /**
   *  Can be overridden by children but returns zero (homogenous) by default
   *  @param[in] varName The name of the unknown
   *  @parem[in] point A Dirichlet point
   *  @param[in] time The current time
   *  \return The Dirichlet bounary condition at the current time
   */
  virtual FadType DirichletBCs(std::string const& varName, libMesh::Point const& point, FadType const time) const;
};
} // namespace Pde
} // namespace muq

#define TRANSIENT_DIRICHLET(FUNCNAME)					\
  virtual double DirichletBCs(std::string const & varName, libMesh::Point const & point, \
                              double const time) const override		\
  {									\
    return FUNCNAME<double>(varName, point, time);			\
  }									\
  									\
  virtual muq::Pde::FadType DirichletBCs(std::string const & varName, libMesh::Point const & point, \
                                         muq::Pde::FadType const time) const override \
  {									\
    return FUNCNAME<muq::Pde::FadType>(varName, point, time);		\
  }

#endif // ifndef TRANSIENTPDE_H_
