#ifndef PDE_H_
#define PDE_H_

#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>

#include "libmesh/libmesh.h"
#include "libmesh/libmesh_common.h"
#include "libmesh/system.h"
#include "libmesh/dof_map.h"
#include "libmesh/fe_interface.h"

#include "MUQ/Utilities/LibMeshTranslater.h"
#include "MUQ/Utilities/VariableNameManipulation.h"
#include "MUQ/Utilities/RegisterClassNameHelper.h"

#include "MUQ/Modelling/ModPiece.h"

#include "MUQ/Pde/GenericEquationSystems.h"
#include "MUQ/Pde/ElementInformation.h"

namespace muq {
  namespace Pde {
    class PDE : public muq::Modelling::ModPiece {
    public:
      
      PDE(Eigen::VectorXi const& inputSizes, std::shared_ptr<GenericEquationSystems> const& eqnsystem, boost::property_tree::ptree const& para);
      
      /// A function that can construct a muq::Pde::PDE
      typedef std::function<std::shared_ptr<PDE>(Eigen::VectorXi const&, std::shared_ptr<GenericEquationSystems> const&, boost::property_tree::ptree const&)> PdeConstructor;
      
      /// A map that holds all of the muq::Pde::PDE that can be created 
      typedef std::map<std::string, PdeConstructor> PdeMap;
      
      /// Get a map that holds all the muq::Pde::PDE that can be created 
      /**
	 \return a muq::Pde::PDE::PdeMap that has all of the children's constructors
      */
      static std::shared_ptr<PdeMap> GetPdeMap();
      
      static std::shared_ptr<PDE> Create(Eigen::VectorXi const& inputSizes, std::shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree& para);

      /// Set the Dirichlet conditions
      /**
         The current solution (or initial condition) needs to satisfy the Dirichlet conditions.  This function updates the nodes on the Dirichlet boundary so this is true and does not change anything else.
         @param[in] solution The current solution (may not satisfy Dirichlet conditions)
         @param[in] time The current time (is set to zero by default)
         \return The current solution (satisfies Dirichlet conditions)
      */
      Eigen::VectorXd SetDirichletConditions(Eigen::VectorXd const& solution,
					     double const           time = 0.0);

      /// Store the inputs 
      /**
	 Inputs are either stored as parameters (scalar or constant sized vector inputs) or projected onto the mesh.  
	 @param[in] inputs The inputs to the PDE (does NOT include initial conditions)
       */
      void StoreInputs(std::vector<Eigen::VectorXd> const& inputs);

      /// Get the libMesh equation systems pointer 
      std::shared_ptr<libMesh::EquationSystems> GetEquationSystemsPtr() const;

    protected:

      /// Compute the residual at a quadrature point (double)
      /**
         The residual for the PDE. Needs to be implemented by child.
         @param[in] varName The name of the unkonwn
         @param[in] phi Shape function at the quadrature point
         @param[in] dhpi Shape function gradient at the quadrature point
         @param[in] soln The solution for all unknowns at the quadrature point
         @param[in] grad The solution gradient for all unknowns at the quadrature point
      */
      virtual double ResidualImpl(std::string const& varName, double const phi, libMesh::RealGradient const& dphi, std::vector<double> const& soln, std::vector<std::vector<double> > const& grad) const = 0;

      /// Compute the residual at a quadrature point (muq::Pde::FadType)
      /**
         The residual for the PDE. Needs to be implemented by child.
         @param[in] varName The name of the unkonwn
         @param[in] phi Shape function at the quadrature point
         @param[in] dhpi Shape function gradient at the quadrature point
         @param[in] soln The solution for all unknowns at the quadrature point
         @param[in] grad The solution gradient for all unknowns at the quadrature point
      */
      virtual FadType ResidualImpl(std::string const& varName, double const phi, libMesh::RealGradient const& dphi, std::vector<FadType> const& soln, std::vector<std::vector<FadType> > const& grad) const = 0;

      /// Compute the residual at a quadrature point (muq::Pde::FadType)
      /**
	 Needs to be implemented by child.  Computes the residual but now the derivative is wrt to a parameter.
	 @param[in] varName The name of the unkonwn
	 @param[in] phi shape function at the quadrature point
	 @param[in] dhpi shape function gradient at the quadrature point
	 @param[in] soln The solution for all unknowns at the quadrature point
	 @param[in] grad The solution gradient for all unknowns at the quadrature point
      */
      virtual FadType ResidualImplPara(std::string const& varName, double const phi, libMesh::RealGradient const& dphi, std::vector<double> const& soln, std::vector<std::vector<double> > const& grad) const = 0;

      /// Return the Neumann boundary condition at a point (double)
      /**
	 Can be overridden by children but returns zero (homogenous) by default
	 @param[in] varName The name of the unknown
	 @parem[in] point A boundary point (not Dirichlet)
	 @param[in] soln The solution for all unknowns at the quadrature point
	 @param[in] grad The solution gradient for all unknowns at the quadrature point
	 \return The Neumann bounary condition at the current time
      */
      virtual std::vector<double> NeumannBCs(std::string const& varName, libMesh::Point const& point, std::vector<double> const& soln, std::vector<std::vector<double> > const& grad) const;

      /// Return the Neumann boundary condition at a point (muq::Pde::FadType)
      /**
         Can be overridden by children but returns zero (homogenous) by default
         @param[in] varName The name of the unknown
         @parem[in] point A boundary point (not Dirichlet)
         @param[in] soln The solution for all unknowns at the quadrature point
         @param[in] grad The solution gradient for all unknowns at the quadrature point
         \return The Neumann bounary condition at the current time
      */
      virtual std::vector<FadType> NeumannBCs(std::string const& varName, libMesh::Point const& point, std::vector<FadType> const& soln, std::vector<std::vector<FadType> > const& grad) const;

      /// Return the Neumann boundary condition at a point (muq::Pde::FadType)
      /**
         Can be overridden by children but returns zero (homogenous) by default.  Returns the derivative with respect to the parameters.
	 @param[in] varName The name of the unknown
	 @parem[in] point A boundary point (not Dirichlet)
	 @param[in] soln The solution for all unknowns at the quadrature point
	 @param[in] grad The solution gradient for all unknowns at the quadrature point
	 \return The Neumann bounary condition at the current time
      */
      virtual std::vector<FadType> NeumannBCsPara(std::string const& varName, libMesh::Point const& point, std::vector<double> const& soln, std::vector<std::vector<double> > const& grad) const;

      /// Is a point on the Dirichlet boundary?
      /**
         Can be overridden by children but returns false by default.  Check happens at all DOFs, including the interior so if this function is overridden it should return false unless the point is explicitly on the (Dirichlet) boundary.
         @param[in] point A point in the domain
         \return True if the point is on the Dirichlet boundary, false otherwise
      */
      virtual bool IsDirichlet(libMesh::Point const& point) const;

      /// Return the Dirichlet boundary condition at a point
      /**
	 Can be overridden by children but returns zero (homogenous) by default
	 @param[in] varName The name of the unknown
	 @parem[in] point A Dirichlet point
	 @param[in] time The current time
	 \return The Dirichlet bounary condition at the current time
      */
      virtual double DirichletBCs(std::string const& varName, libMesh::Point const& point, double const time) const;

      /// Return the Dirichlet boundary condition at a point
      /**
         Can be overridden by children but returns zero (homogenous) by default
	 @param[in] varName The name of the unknown
	 @parem[in] point A Dirichlet point
	 @param[in] time The current time
	 \return The Dirichlet bounary condition at the current time
      */
      virtual FadType DirichletBCs(std::string const& varName, libMesh::Point const& point, FadType const time) const;

      /// Compute the mass matrix 
      /**
	 By default this function does nothing.  The transient child overloads it; the nontransient child does not need it.
       */
      virtual void ComputeMassMatrix() {}

      /// Enforce the Dirichlet boundary conditions in a matrix 
      /**
	 Can be applied to either the mass matrix or the Jacobian.
	 @param[in] matrix The matrix
	 @param[in] diag True: Set the diagonal to one
      */
      void EnforceMatrixBCs(Eigen::MatrixXd& matrix, bool const diag = true);

      /// Get the parameter (used to compute residual and boundary conditions)
      /**
	 For scalar parameters return the double (or FadType); for mesh dependent parameters each variable is a double (or FadType)
	 @param[in] name The parameter name 
	 \return The stored value for the parameter
       */
      template<typename scalar>
	scalar GetScalarParameter(std::string const& name) const
	{
	  // failed 
	  assert(false);
	  return scalar();
	}

      /// Get the parameter (used to compute residual and boundary conditions)
      /**
	 For vector parameters; gets either and Eigen::VectorXd or Eigen::Matrix<FadType, Eigen::Dynamic, 1>
	 @param[in] name The parameter name 
	 \return The stored value for the parameter
       */
      template<typename vec>
	vec GetVectorParameter(std::string const& name) const
	{
	  // failed 
	  assert(false);
	  return vec();
	}

      /// Get the point where ComputeParametersAtPoint() was most recently called 
      /**
	 \return The point
       */
      libMesh::Point GetCurrentPoint() const;

      /// Map scalar parameters to double
      std::map<std::string, double> scalarParameters;

      /// Map scalar parameters to FadType
      std::map<std::string, FadType> scalarParametersFad;

      /// Map vector parameters to Eigen::VectorXd
      std::map<std::string, Eigen::VectorXd> vectorParameters;

      /// Map vector parameters to Eigen::Matrix<FadType, Eigen::Dynamic, 1>
      std::map<std::string, Eigen::Matrix<FadType, Eigen::Dynamic, 1> > vectorParametersFad;

      /// The variable names and numbers
      /**
	 Each variable has a name and a number.  A bimap maps from the name to the number or the number to the name.  
       */
      bimap vars;

      /// The type of variable 
      enum ParameterType {
	/// A scalar parameter 
	scalarParameter,
	/// A vector, non-mesh dependent parameter 
	vectorParameter, 
	/// A mesh dependent parameter 
	meshDependent
      };
      
      /// The names of the inputs 
      /**
	 Each input parameter is a pair.  The first element of the pair is true if it is a scalar and false if it is not.  The second is the parameter's name.   If the parameter name matches a system in muq::Pde::PDE::system it is mesh dependent.
       */
      std::vector<std::pair<ParameterType, std::string> > inputNames;

      /// The muq::Pde::GenericEquationSystems that stores the mesh for this set of equations
      std::shared_ptr<GenericEquationSystems> eqnsystem;

      /// Are we solving a transient problem?
      /**
	 True: transient problem, False: nontransient problem 
       */
      bool isTransient;

      /// Finite element basis information for each unknown
      std::vector<libMesh::FEType> feType;

      /// Information about each element
      /**
	 We need to store basis functions, basis function derivatives, elemental jacobians, and the quadrature points for
	 each element.  All unknowns us the same quadrature points but can use different basis functions (i.e. one can be
	 linear and the other quadratic).
      */
      std::shared_ptr<ElementInformation> elemInfo;

      /// The mass matrix for transient problmes 
      /**
	 Assume the mass matrix is not time-dependent.  If the problem is transient, it is precomputed.  Since the mass matrix is often singular, the lumped sum mass matrix is used.
       */
      boost::optional<Eigen::VectorXd> lumpedSumMassMatrix;

      /// A vector of parameter systems where we need to compute the spatial gradient at the quadrature points
      /**
	 The spatial gradients (for the parameters) are not computed by default (they are expensive).  These are the names of the parameter systems the user needs to compute spatial gradients.
       */
      std::vector<std::string> needGradient;

      /// A map to store the parameter names and numbers for mesh-dependent parameter systems
      /**
	 Mesh dependent parameters have variables; this maps the parameter (system) name to a bimap that stores the names and numbers of the variables
       */
      std::map<std::string, bimap> paraVars;

    private:

      /// Compute the element DOF to global DOF mapping for non-state parameters 
      /**
	 @param[in] el The current element iterator
	 @param[in] inputDimWrt The input we are taking the derivative wrt
       */
      boost::optional<std::vector<unsigned int> > ParameterElementDOFs(libMesh::MeshBase::const_element_iterator const& el, int const inputDimWrt) const;

      /// Compute the residual on an element
      /**
         @param[in] elInfo The basis functions, basis function derivatives, quad points, and jacobian for the element
         @param[in] elemDOFs Map from local to global DOFs
         @param[in] soln The current solution
      */
      void ComputeResidual(std::shared_ptr<ElemInfo> const& elInfo, std::shared_ptr<ElementDOFs> const& elemDOFs, Eigen::VectorXd const& soln);

      /// Compute the residual on an element as a FadType wrt a parameter
      /**
         @param[in] elInfo The basis functions, basis function derivatives, quad points, and jacobian for the element
         @param[in] elemDOFs Map from local to global DOFs
         @param[in] soln The current solution
	 @param[in] inputDimWrt The parameter we are taking the derivative wrt
      */
      void ComputeResidual(std::shared_ptr<ElemInfo> const& elInfo, std::shared_ptr<ElementDOFs> const& elemDOFs, Eigen::VectorXd const& soln, int const inputDimWrt);

      /// Compute the Jacobian on an element
      /**
         @param[in] elInfo The basis functions, basis function derivatives, quad points, and jacobian for the element
         @param[in] elemDOFs Map from local to global DOFs
         @param[in] soln The current solution
      */
      void ComputeJacobian(std::shared_ptr<ElemInfo> const& elInfo, std::shared_ptr<ElementDOFs> const& elemDOFs, Eigen::VectorXd const& soln);

      /// Compute the residual 
      /**
	 @param[in] inputs The input parameters
       */
      virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

      /// Compute the residual 
      /**
	 @param[in] inputs The input parameters
	 @param[in] inputDimWrt The dimension we are taking the Jacobian wrt
       */
      virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt) override;

      /// Compute the parameters at a point
      /**
         Ignores non mesh dependent parameters.  If the parameter name has an associated system, however, save the paramter compute its value and derivatives in the equation system parameter tree.  If the parameter is named <EM>p</EM>, the tree saves the parameter under the name "p" and its derivatives as "dpdx", "dpdy", and "dpdz", respectively.  

	 Also, store the parameter in the map of points where the parameter has been computed.  This saves having to reinterpolate onto the quadrature points every point in the non linear solver.
       @param[in] point The point we want to evaluate the parameters at
       */
      void ComputeParametersAtPoint(libMesh::Point const& point);

      /// Compute the parameters at a point as a muq::Pde::FadType
      /**
         Ignores non mesh dependent parameters.  If the parameter name has an associated system, however, save the paramter compute its value and derivatives in the equation system parameter tree.  If the parameter is named <EM>p</EM>, the tree saves the parameter under the name "p" and its derivatives as "dpdx", "dpdy", and "dpdz", respectively.  

	 Also, store the parameter in the map of points where the parameter has been computed.  This saves having to reinterpolate onto the quadrature points every point in the non linear solver.
       @param[in] point The point we want to evaluate the parameters at
       @param[in] inputDimWrt The parameter we are taking the derivative wrt
       */
      void ComputeParametersAtPoint(libMesh::Point const& point, int const inputDimWrt);

      /// Store a parameter value/gradient at a point 
      /**
	 Store the mesh-dependent parameter value and gradient.  Updates the libMesh parameters to be storing the these values.  Also, puts the value and gradient into the map so we don't need to reinterpolate.
	 @param[in] storeGrad Do we need to store the gradient?
	 @param[in] var The name of the parameter
	 @param[in] point The point were we have computed the parameters
	 @param[in] val The parameter value 
	 @param[in] grad The parameter gradient
       */
      void StoreParameters(bool const storeGrad, std::string const& var, libMesh::Point const& point, double const val, libMesh::RealGradient const& grad = libMesh::RealGradient());

      /// Store a parameter value/gradient at a point 
      /**
	 Store the mesh-dependent parameter value and gradient.  Updates the libMesh parameters to be storing the these values.  Also, puts the value and gradient into the map so we don't need to reinterpolate.
	 @param[in] storeGrad Do we need to store the gradient?
	 @param[in] var The name of the parameter
	 @param[in] point The point were we have computed the parameters
	 @param[in] val The parameter value 
	 @param[in] grad The parameter gradient
       */
      void StoreParameters(bool const storeGrad, std::string const& var, libMesh::Point const& point, FadType const val, std::vector<FadType> const& grad = std::vector<FadType>());

      /// Project a parameter onto the mesh 
      /**
	 Project mesh-dependent parameters onto the mesh.  Replace the libMesh::System solution vector with the input.
	 @param[in] paraName The name of the parameter system 
	 @param[in] paraVec The parameter vector; each element is the value at a DOF
       */
      void ProjectParameter(std::string const& paraName, Eigen::VectorXd const& paraVec);

      /// Extract system name, unkonwns, and the order of the unknowns
      /**
	 @param[out] vars A list of the unknowns
	 @param[out] orders The order of the unknowns on the elements
	 @param[in] params A parameter tree that contains all the information to make the PDEBase class
	 \return The name of the system
      */
      static std::string ExtractSystemInformation(std::vector<std::string>         & vars,
						  std::vector<unsigned int>        & orders,
						  boost::property_tree::ptree const& params);
      
      /// Exatract the parameters and determine if they are scalars
      /**
	 @param[in] params Contains a list of the parameters and if they are scalars
      */
      void ExtractParameterList(boost::property_tree::ptree const& params);

      /// Get each solution variable (double)
      /**
	 Given the current solution extract the solution for each variable 
	 @param[out] solnDOF A vector of vectors; each element is the solution vector for a specific variable
	 @param[in] soln The current solution 
         @param[in] elemDOFs Map from local to global DOFs
       */
      void ExtractVariableSolutions(std::vector<std::vector<double> > & solnDOF, Eigen::VectorXd const& soln, std::shared_ptr<ElementDOFs> const& elemDOFs) const;

      /// Get each solution variable (muq::Pde::FadType)
      /**
	 Given the current solution extract the solution for each variable 
	 @param[out] solnDOF A vector of vectors; each element is the solution vector for a specific variable
	 @param[in] soln The current solution 
         @param[in] elemDOFs Map from local to global DOFs
       */
      void ExtractVariableSolutions(std::vector<std::vector<FadType> > & solnDOF, Eigen::VectorXd const& soln, std::shared_ptr<ElementDOFs> const& elemDOFs) const;
      /// Set the residual to zero at the boundary nodes
      /**
	 @param[in,out] residual The residual 
       */
      void EnforceDirichletBCs(Eigen::VectorXd& residual) const;

      /// Interpolate the solution onto quadrature points
      /**
	 \return The first is the solution at each quadrature points; the second is the gradients at each quadrature point.  Can be called with any scalar (double or FadType) and basises (element or boundary).
	 @param[in] qp The quadrature point
	 @param[in] var The variable number
	 @param[in] varSoln The solution vector for the variable
         @param[in] elInfo The basis functions, basis function derivatives, quad points, and jacobian for the element
       */
      template<typename scalar, typename basis>
	std::pair<scalar, std::vector<scalar> > InterpolateToQP(unsigned int const qp, unsigned int const var, std::vector<scalar> const& varSoln, std::shared_ptr<basis> elInfo)
	{
	  // basis functions on the element (const reference to avoid the copy)
	  const std::vector<std::vector<libMesh::Real> >& phiV = elInfo->basisMap[feType[var]]->phi;

	  // basis function derivatives on the element (const reference to avoid the copy)
	  const std::vector<std::vector<libMesh::RealGradient> >& dphiV = elInfo->basisMap[feType[var]]->dphi;

	  // the solution 
	  scalar soln = 0.0;

	  // the gradient 
	  std::vector<scalar> grad(3);

	  // loop through the basis functions 
	  for( unsigned int p=0; p<phiV.size(); ++p ) {
	    // sum into the solution 
	    soln += phiV[p][qp] * varSoln[p];

	    // loop through the dimensions 
	    for( unsigned int d=0; d<3; ++d ) {
	      // sum into the gradient 
	      grad[d] += dphiV[p][qp](d) * varSoln[p];
	    }
	  }
	  
	  return std::pair<scalar, std::vector<scalar> >(soln, grad);
	}

      /// Apply boundary condtions to the residual
      /**
         @param[in] elemDOFs Map from local to global DOFs
         @param[in] faceInfo The basis functions, basis function derivatives, quad points, and jacobian for the element
         @param[in] soln The current solution
	 @param[in] inputDimWrt The parameter we are taking the derivative wrt (is -1 if we are not taking a derivative)
       */
      void ResidApplyBCs(std::shared_ptr<ElementDOFs> const elemDOFs, std::shared_ptr<FaceInfo> const faceInfo, Eigen::VectorXd const& soln, int const inputDimWrt = -1);

      /// Apply boundary condtions to the Jacobian
      /**
         @param[in] elemDOFs Map from local to global DOFs
         @param[in] faceInfo The basis functions, basis function derivatives, quad points, and jacobian for the element
         @param[in] soln The current solution
       */
      void JacApplyBCs(std::shared_ptr<ElementDOFs> const elemDOFs, std::shared_ptr<FaceInfo> const faceInfo, Eigen::VectorXd const& soln);

      /// Compute FadType parameters at a point 
      /**
	 Compute and store the mesh dependent parameters at a point using FadType objects.
	 @param[in] point The point where we need the value
	 @param[in] paraSysName The name of the parameter system
	 @param[in] active True: this parameter needs to be active FadTypes; False: this parameter needs to be passive FadTypes
	 @param[in] elNum The element number
       */
      void ComputeFadTypeAtPoint(libMesh::Point const& point, std::string const& paraSysName, bool const active, int const elNum);

      /// The point where ComputeParametersAtPoint() was most recently called 
      libMesh::Point pnt;
      
      /// The points were a parameter value has been computed (double)
      std::map<std::pair<std::string, libMesh::Point>, double> paraVals;

      /// The points were a parameter value has been computed (FadType)
      std::map<std::pair<std::string, libMesh::Point>, FadType> paraValsFad;
      
      /// The points were a parameter gradient has been computed
      std::map<std::pair<std::string, libMesh::Point>, libMesh::RealGradient> paraGrads;

      /// The points were a parameter gradient has been computed
      std::map<std::pair<std::string, libMesh::Point>, std::vector<FadType> > paraGradsFad;
      
      /// Is the mass matrix time dependent
      const bool timeDependentMassMatrix;
    };

template<>
  double PDE::GetScalarParameter(std::string const& name) const;

template<>
  FadType PDE::GetScalarParameter(std::string const& name) const;

template<>
  Eigen::VectorXd PDE::GetVectorParameter(std::string const& name) const;

template<>
  Eigen::Matrix<FadType, Eigen::Dynamic, 1> PDE::GetVectorParameter(std::string const& name) const;

  } // namespace Pde 
} // namespace muq

#define REGISTER_PDE(NAME) static auto NAME##_ =			\
    muq::Pde::PDE::GetPdeMap()->insert(std::make_pair(#NAME, boost::shared_factory<NAME>()));

#define RESIDUAL(FUNCNAME)						\
  virtual double ResidualImpl(std::string const& varName, double const phi, libMesh::RealGradient const& dphi, std::vector<double> const& soln, std::vector<std::vector<double> > const& grad) const override { \
    return FUNCNAME<double, double, double>(varName, phi, dphi, soln, grad); \
  }									\
									\
  virtual muq::Pde::FadType ResidualImpl(std::string const& varName, double const phi, libMesh::RealGradient const& dphi, std::vector<muq::Pde::FadType> const& soln, std::vector<std::vector<muq::Pde::FadType> > const& grad) const override { \
    return FUNCNAME<muq::Pde::FadType, muq::Pde::FadType, double>(varName, phi, dphi, soln, grad); \
  }									\
  									\
  virtual muq::Pde::FadType ResidualImplPara(std::string const& varName, double const phi, libMesh::RealGradient const& dphi, std::vector<double> const& soln, std::vector<std::vector<double> > const & grad) const override { \
    return FUNCNAME<muq::Pde::FadType, double, muq::Pde::FadType>(varName, phi, dphi, soln, grad); \
  }

#define NEUMANN(FUNCNAME)                                                                                     \
  virtual std::vector<double> NeumannBCs(std::string const& varName, libMesh::Point const& point, std::vector<double> const& soln, std::vector<std::vector<double> > const& grad) const override \
  {									\
    return FUNCNAME<double, double, double>(varName, point, soln, grad); \
  }									\
  									\
  virtual std::vector<muq::Pde::FadType> NeumannBCsPara(std::string const& varName, libMesh::Point const& point, std::vector<double> const& soln, std::vector<std::vector<double> > const& grad) const override \
  {									\
    return FUNCNAME<muq::Pde::FadType, double, muq::Pde::FadType>(varName, point, soln, grad); \
  }									\
  									\
  virtual std::vector<muq::Pde::FadType> NeumannBCs(std::string const& varName, libMesh::Point const& point, std::vector<muq::Pde::FadType> const& soln, std::vector<std::vector<muq::Pde::FadType> > const& grad) const override \
  {									\
    return FUNCNAME<muq::Pde::FadType, muq::Pde::FadType, double>(varName, point, soln, grad); \
  }

#endif


