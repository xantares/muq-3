#!/usr/bin/python

import os
import sys

# open the header file
f = open('MUQ.h','w');
f.write('\n\n#ifndef _MUQ_H_\n')
f.write('#define _MUQ_H_\n\n\n\n')

# generate a list of all the files in the include/ directory
fileList = []
rootdir = "./"
for root, subFolders, files in os.walk(rootdir):
    for file in files:
        fileList.append(os.path.join(root,file))

# print all the files we want to include to the header file
for path in fileList:
    if(path[len(path)-1]=='h'):
        parts = path.split('/');
        if(len(parts)>2)&(parts[1]!=".svn"):
            if(len(parts)>3):
                if((parts[3]!="ComponentwiseChildBoilerplate.h")&(parts[2]!="Eigen")&(parts[2]!="Macros")):
                    f.write('#include "' + path[2:] + '"\n')
            elif((parts[2]!="Eigen")&(parts[2]!="Macros")):
                f.write('#include "' + path[2:] + '"\n')
        

f.write('\n\n\n#endif')

f.close()
