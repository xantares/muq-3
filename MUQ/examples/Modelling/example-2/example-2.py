# Load the modelling library
import libmuqModelling

# As in example-1, this class describes the right hand side of our predator prey model.
class PredPreyModel(libmuqModelling.ModPiece):
    # Unlike the previous example, our Model constructor needs to call the ModPiece constructor directly.  Like the
    # templates, the ModPiece constructor requires the input dimensions and output dimensions.  However, the ModPiece
    # constructor also requires us to tell it a bit about our model. Specifically, the ModPiece class needs to knoow
    # what kind of derivative information this class provides, and whether or not the output of the Model is stochastic.
    # Also note that now we have to define multiple input sizes.  This is done with an python list object passed to the PredPreyModel constructor. 
   # construct the vector of input sizes.  The first input containes the current predator and prey populations, while
   # the second input contains the growth/death parameters.
    def __init__(self):
        libmuqModelling.ModPiece.__init__(self, 
                                          [2, 6], # input sizes 
                                          2,      # output size
                                          False,  # there is no GradientImpl
                                          False,  # there is no JacobianImpl
                                          False,  # there is no JacobianActionImpl
                                          False,  # there is no HessianImpl
                                          False)  # it is not random

        # Even with more than one input, the EvaluateImpl function performs the forward model computations.  However, when a model has
        # multiple inputs, EvaluateImpl takes a container of vector valued inputs instead of a single Eigen::VectorXd.
    def EvaluateImpl(self, ins):
        # for clarity, we grab the prey and predator populations from the input vector
        # notice the indexing here, the [0] extracts the first vector valued input
        # and the [0][0] grabs the first entry in the first input vector
        preyPop = ins[0][0] 
        predPop = ins[0][1]

        # extract the model parameters from the input vector
        preyGrowth      = ins[1][0]
        preyCapacity    = ins[1][1]
        predationRate   = ins[1][2]
        predationHunger = ins[1][3]
        predatorLoss    = ins[1][4]
        predatorGrowth  = ins[1][5]

        # create a vector to hold the ModPiece output, this will hold the time derivatives of population
        output = [None] * self.outputSize

        # compute the growth rates as we did in example-1
        output[0] = preyPop * (preyGrowth * (1.0 - preyPop / preyCapacity) - predationRate * predPop / (predationHunger + preyPop))
        output[1] = predPop * (predatorGrowth * preyPop / (predationHunger + preyPop) - predatorLoss)

        # return the output
        return output;

if __name__ == '__main__':
    # create an instance of the modpiece defining the predator prey model
    predatorPreyModel = PredPreyModel()

    # set the population sizes
    populations = [50.0, 5.0]

    # set the interaction parameters
    growthParams = [0.8, 100.0, 4.0, 15.0, 0.6, 0.8] # preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;

    # Evaluate the predator prey growth rate ModPiece as we did before, but now include the second input
    growthRates = predatorPreyModel.Evaluate([populations, growthParams])

    # print the growth rates
    print
    print "Model output with multiple argument Evaluate call:"
    print "  The prey growth rate evaluated at     (", populations[0], ",", populations[1], ") is ", growthRates[0]
    print "  The predator growth rate evaluated at (", populations[0], ",", populations[1], ") is ", growthRates[1]


