/** OVERVIEW:
This example demonstrates how to define the EvaluateMulti function in a one input ModPiece.  The EvaluateMulti member function provides an alterantive to the standard Evaluate function when efficient methods are available for evaluating the ModPiece at more than one point at a time.  Examples include multiplying the input by a matrix (when more than one input are used, more efficient matrix-matrix multiplication can be used), or when the ModPiece can evaluate each input simultaneously by launching a parallel MPI job.

The EvaluateMulti function is defined just like the Evaluate function, except that a Matrix of inputs are used instead of a single vector.  Each column of the input matrix corresponds to a point that needs to be evaluated.
*/


#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Utilities/RandomGenerator.h"

using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace std;

/** Here we create a simple model with one vector-valued input.  This model takes a vector [x,y] and returns a vector [sin(x)+cos(y), cos(x)+sin(y)].  No derivative information is provided (hence why we inherit from OneInputNoDerivModPiece).  The EvaluateMultiImpl function is implemented here as a for loop.  In this example, the advantage of explicitly implementing EvaluateMultiImpl instead of using the default, is that the for loop can be parallelized with OPENMP.
*/
class ExampleModel : public OneInputNoDerivModPiece
{
public:
  ExampleModel() : OneInputNoDerivModPiece(2,2){};
  
private:

  virtual Eigen::MatrixXd EvaluateMultiImpl(Eigen::MatrixXd const& input) override
  {
    const int numPts = input.cols();
    
    Eigen::MatrixXd output(outputSize,numPts);
    
    #pragma omp parallel for
    for(int pt=0; pt<numPts; ++pt)
    {
      output(0,pt) = sin(input(0,pt)) + cos(input(1,pt));
      output(1,pt) = cos(input(0,pt)) + sin(input(1,pt));
    }
    
    return output; 
  }

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    return EvaluateMultiImpl(input);
  }

};


int main()
{
  /** Construct the model.  */
  shared_ptr<ModPiece> model = make_shared<ExampleModel>();
  
  /** Evaluate the model with a single input in the usual way. */
  Eigen::VectorXd singleInput = RandomGenerator::GetUniformRandomVector(2);
  singleInput << 2, 1;
  
  Eigen::VectorXd singleOutput = model->Evaluate(singleInput);
  std::cout << "\nSingle output vector = \n" << singleOutput << std::endl << std::endl;
  
  /** Now, evaluate the model with several inputs.  This use of EvaluateMulti replaces the use of a for loop with the standard Evaluate function.*/
  int numPts = 10;
  Eigen::MatrixXd multipleInputs = RandomGenerator::GetUniformRandomMatrix(2,numPts);
  
  Eigen::MatrixXd multipleOutput = model->EvaluateMulti(multipleInputs);
  std::cout << "Multiple output matrix = \n" << multipleOutput << std::endl << std::endl;
  
}