/** \example modelling-example-4.cpp
 *
 *  <h3> About this example </h3>
 *  <p>This example builds on example-2.  example-2 showed how to define and evaluate a ModPiece with multiple inputs.
 *This example takes it a step further.  This example illustrates how Jacobian information is defined in a ModPiece and
 *uses the Jacobian to find a fixed point of the RHS model with a simple Newton iteration. Thus, the goal of this
 *example is to solve
 *  \f{eqnarray*}{
 *  0 & = & rP\left(1-\frac{P}{K}\right) - s\frac{PQ}{a+P}\\
 *  0 & = & u\frac{PQ}{a+P} - vQ
 *  \f}
 *  using Newton's method.
 *  </p>
 *
 *  <h3>Line by line explanation</h3>
 *  <p>See example-4.cpp for finely documented code.</p>
 *
 */
// Just as before, we will need Eigen and std::cout
#include <Eigen/Dense>
#include <iostream>
#include <iomanip>

// In this example, we will work directly from the ModPiece base class instead of the single input templates.  This line
// includes the ModPiece base directly.
#include <MUQ/Modelling/ModPiece.h>
#include <MUQ/Modelling/ModGraphPiece.h>
#include <MUQ/Modelling/OdeModPiece.h>
#include <MUQ/Modelling/VectorPassthroughModel.h>

// as before, we'll work in the muq::Modelling and std namespaces
using namespace muq::Modelling;
using namespace std;

class PreyGrowthModel : public ModPiece {
public:

  PreyGrowthModel() : ModPiece(Eigen::Vector3i(1, 2, 6), 1, false, false, false, false, false) {}

private:

  Eigen::VectorXd EvaluateImpl(vector<Eigen::VectorXd> const& inputs)
  {
    const double preyPop      = inputs[1](0);
    const double preyGrowth   = inputs[2](0);
    const double preyCapacity = inputs[2](1);

    return preyGrowth * (1.0 - preyPop / preyCapacity) * Eigen::VectorXd::Ones(1);
  }
};

class PreyDeathModel : public ModPiece {
public:

  PreyDeathModel() : ModPiece(Eigen::Vector3i(1, 2, 6), 1, false, false, false, false, false) {}

private:

  Eigen::VectorXd EvaluateImpl(vector<Eigen::VectorXd> const& inputs)
  {
    const double preyPop         = inputs[1](0);
    const double predationRate   = inputs[2](2);
    const double predationHunger = inputs[2](3);

    return -1.0 * predationRate  / (predationHunger + preyPop) * Eigen::VectorXd::Ones(1);
  }
};

class PredatorGrowthModel : public ModPiece {
public:

  PredatorGrowthModel() : ModPiece(Eigen::Vector3i(1, 2, 6), 1, false, false, false, false, false) {}

private:

  Eigen::VectorXd EvaluateImpl(vector<Eigen::VectorXd> const& inputs)
  {
    const double preyPop         = inputs[1](0);
    const double predPop         = inputs[1](1);
    const double predationHunger = inputs[2](3);
    const double predatorGrowth  = inputs[2](4);

    return predatorGrowth * predPop / (predationHunger + preyPop) * Eigen::VectorXd::Ones(1);
  }
};

class PredatorDeathModel : public ModPiece {
public:

  PredatorDeathModel() : ModPiece(Eigen::Vector3i(1, 2, 6), 1, false, false, false, false, false) {}

private:

  Eigen::VectorXd EvaluateImpl(vector<Eigen::VectorXd> const& inputs)
  {
    const double predatorLoss = inputs[2](5);

    return -1.0 *predatorLoss *Eigen::VectorXd:: Ones(1);
  }
};

class PredPreyResidual : public ModPiece {
public:

  PredPreyResidual() : ModPiece(InputSize(), 2, false, false, false, false, false) {}

private:

  Eigen::VectorXd EvaluateImpl(vector<Eigen::VectorXd> const& inputs)
  {
    const double gp = inputs[0](0);
    const double dp = inputs[1](0);
    const double gq = inputs[2](0);
    const double dq = inputs[3](0);
    const double P  = inputs[4](0);
    const double Q  = inputs[4](1);


    Eigen::Vector2d resid;

    resid(0) = P * gp + Q * dp;
    resid(1) = P * gq +  Q * dq;

    return resid;
  }

  static Eigen::VectorXi InputSize()
  {
    Eigen::VectorXi ins(5);

    ins << 1, 1, 1, 1, 2;

    return ins;
  }
};

/** By providing Jacobian information in the the PredPreyModel class, much for involved analysis of the system can be
 * performed.  Below, the Jacobian is used within a simple Newton iteration to compute an equilibrium point of the ODE
 * model.
 */
int main()
{
  // make a graph
  auto predPreyGraph = std::shared_ptr<muq::Modelling::ModGraph>();

  predPreyGraph->AddNode(make_shared<PredatorGrowthModel>(), "Predator Growth Model");
  predPreyGraph->AddNode(make_shared<PredatorDeathModel>(), "Predator Death Model");
  predPreyGraph->AddNode(make_shared<PreyGrowthModel>(), "Prey Growth Model");
  predPreyGraph->AddNode(make_shared<PreyDeathModel>(), "Prey Death Model");
  predPreyGraph->AddNode(make_shared<PredPreyResidual>(), "Pred-Prey Residual");
  predPreyGraph->AddNode(make_shared<VectorPassthroughModel>(2), "Pred Prey Populations");
  predPreyGraph->AddNode(make_shared<VectorPassthroughModel>(1), "Time");
  predPreyGraph->AddNode(make_shared<VectorPassthroughModel>(6), "Parameters");

  predPreyGraph->AddEdge("Parameters", "Prey Growth Model", 2);
  predPreyGraph->AddEdge("Parameters", "Prey Death Model", 2);
  predPreyGraph->AddEdge("Parameters", "Predator Growth Model", 2);
  predPreyGraph->AddEdge("Parameters", "Predator Death Model", 2);

  predPreyGraph->AddEdge("Time", "Prey Growth Model", 0);
  predPreyGraph->AddEdge("Time", "Prey Death Model", 0);
  predPreyGraph->AddEdge("Time", "Predator Growth Model", 0);
  predPreyGraph->AddEdge("Time", "Predator Death Model", 0);

  predPreyGraph->AddEdge("Pred Prey Populations", "Prey Growth Model", 1);
  predPreyGraph->AddEdge("Pred Prey Populations", "Predator Growth Model", 1);

  predPreyGraph->AddEdge("Pred Prey Populations", "Prey Death Model", 1);
  predPreyGraph->AddEdge("Pred Prey Populations", "Predator Death Model", 1);

  predPreyGraph->AddEdge("Prey Growth Model", "Pred-Prey Residual", 0);
  predPreyGraph->AddEdge("Prey Death Model", "Pred-Prey Residual", 1);
  predPreyGraph->AddEdge("Predator Growth Model", "Pred-Prey Residual", 2);
  predPreyGraph->AddEdge("Predator Death Model", "Pred-Prey Residual", 3);
  predPreyGraph->AddEdge("Pred Prey Populations", "Pred-Prey Residual", 4);

  predPreyGraph->writeGraphViz("PredPreyGraph.pdf");

  vector<string> inputOrder(3);
  inputOrder[0] = "Time"; inputOrder[1] = "Pred Prey Populations"; inputOrder[2] = "Parameters";

  auto predatorPreyRhs = predPreyGraph->ConstructModPiece("Pred-Prey Residual", inputOrder);

  boost::property_tree::ptree odeParams;

  odeParams.put("CVODES.ATOL", 1e-13);           // absolute tolerance
  odeParams.get("CVODES.RTOL", 1e-13);           // relative tolerance
  odeParams.put("CVODES.Method", "Adams");         // either BDF or Adams
  odeParams.put("CVODES.LinearSolver", "Dense"); // either Dense, SPGMR, SPBCG, or SPTFQMR
  odeParams.put("CVODES.SolveMethod", "Iter"); // either Newton or Iter
  odeParams.put("CVODES.MaxStepSize", 8e-4);     // maximum step size, should be small for accurate discrete adjoint

  /** The ODE solver needs to know when to stop integrating the system and while integrating, when to store the state.
   * All of this information is contained in the obsTimes vector.  This vector is an increasing list of times when the
   * state should be stored.  The solver assumes the integration starts at 0 and goes until the last time in this
   * vector.
   */
  Eigen::VectorXd obsTimes;
  obsTimes.setLinSpaced(50, 0.0, 55.0);


  auto predatorPreyModel = make_shared<OdeModPiece>(predatorPreyRhs, obsTimes, odeParams);

  Eigen::VectorXd initialCondition(2);
  initialCondition << 50.0, 5.0;

  Eigen::VectorXd parameters(6);
  parameters << 0.8, 100.0, 4.0, 15.0, 0.8, 0.6; // preyGrowth, preyCapacity, predationRate, predationHunger,
                                                 // predatorGrowth, predatorLoss


  Eigen::VectorXd populations = predatorPreyModel->Evaluate(initialCondition, parameters);

  // print the prey population and predator population
  cout << "\nPrey Population  |  Predator Population\n";
  for (int i = 0; i < populations.size(); i += 2) {
    std::cout << setw(10) << setprecision(2) << fixed << populations(i) << "                " <<
    populations(i + 1) << endl;
  }
  cout << endl;
}

