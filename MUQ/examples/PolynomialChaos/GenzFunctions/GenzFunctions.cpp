
//Standard includes
#include <Eigen/Core>
#include <iostream>
#include <math.h>
#include <MUQ/Modelling/ModPieceTemplates.h>
#include "MUQ/Utilities/RandomGenerator.h"

#include "GenzFunctions.hpp"

/* use the muq::utilities namespace
 */
using namespace muq::Utilities;

/* use the muq::modelling namespace
 */
using namespace muq::Modelling;

/* use the std namespace for cout and endl
 */
using namespace std;


/**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
 **/
GenzFunction::GenzFunction(
                           unsigned int dim,
                           Eigen::VectorXd const& w,
                           Eigen::VectorXd const& c) 
  : OneInputNoDerivModPiece(dim, 1), dim(dim), w(w), c(c)
{};

GenzOscillatory::GenzOscillatory(unsigned int dim,
				 Eigen::VectorXd const& w,
				 Eigen::VectorXd const& c) 
  : GenzFunction(dim, w, c) {};

Eigen::VectorXd GenzOscillatory::EvaluateImpl(Eigen::VectorXd const& x)
{
  Eigen::VectorXd scaled_x = ( x.array() / 2.0 ) + 0.5; 
  Eigen::VectorXd out(1);
  out(0) = cos( 2.0 * M_PI * w(0) + c.dot(scaled_x) );
  return out;
};

GenzProductPeak::GenzProductPeak(unsigned int dim,
				 Eigen::VectorXd const& w,
				 Eigen::VectorXd const& c) 
  : GenzFunction(dim, w, c) {};

Eigen::VectorXd GenzProductPeak::EvaluateImpl(Eigen::VectorXd const& x)
{
  Eigen::VectorXd scaled_x = ( x.array() / 2.0 ) + 0.5; 
  Eigen::VectorXd out(1);
  out(0) = 1.0 / ( (this->c).array().pow(-2.0) + (scaled_x - this->w).array().pow(2.0) ).prod();
  return out;
};

GenzCornerPeak::GenzCornerPeak(unsigned int dim,
				 Eigen::VectorXd const& w,
				 Eigen::VectorXd const& c) 
  : GenzFunction(dim, w, c) {};

Eigen::VectorXd GenzCornerPeak::EvaluateImpl(Eigen::VectorXd const& x)
{
  Eigen::VectorXd scaled_x = ( x.array() / 2.0 ) + 0.5; 
  Eigen::VectorXd out(1);
  out(0) = pow( 1.0 + c.dot(scaled_x), -static_cast<double>(this->dim) - 1.0 );
  return out;
};

GenzGaussian::GenzGaussian(unsigned int dim,
				 Eigen::VectorXd const& w,
				 Eigen::VectorXd const& c) 
  : GenzFunction(dim, w, c) {};

Eigen::VectorXd GenzGaussian::EvaluateImpl(Eigen::VectorXd const& x)
{
  Eigen::VectorXd scaled_x = ( x.array() / 2.0 ) + 0.5; 
  Eigen::VectorXd out(1);
  auto csq = c.array().pow(2).matrix();
  auto xwsq = (scaled_x - w).array().pow(2).matrix();
  out(0) = exp( - csq.dot( xwsq )  );
  return out;
};

GenzContinuous::GenzContinuous(unsigned int dim,
				 Eigen::VectorXd const& w,
				 Eigen::VectorXd const& c) 
  : GenzFunction(dim, w, c) {};

Eigen::VectorXd GenzContinuous::EvaluateImpl(Eigen::VectorXd const& x)
{
  Eigen::VectorXd scaled_x = ( x.array() / 2.0 ) + 0.5; 
  Eigen::VectorXd out(1);
  auto xwabs = (scaled_x - w).array().abs().matrix();
  out(0) = exp( - c.dot( xwabs ) );
  return out;
};

GenzDiscontinuous::GenzDiscontinuous(unsigned int dim,
				 Eigen::VectorXd const& w,
				 Eigen::VectorXd const& c) 
  : GenzFunction(dim, w, c) {};

Eigen::VectorXd GenzDiscontinuous::EvaluateImpl(Eigen::VectorXd const& x)
{
  Eigen::VectorXd scaled_x = ( x.array() / 2.0 ) + 0.5; 
  Eigen::VectorXd out(1);
  
  if ( scaled_x(0) > w(0) || scaled_x(1) > w(1) )
    out(0) = 0. ;
  else
    out(0) = exp( c.dot( scaled_x ) );
  
  return out;
};

/**
   Draw random parameters which then define the returned Genz function
   \param ftype standard (0) or modified (1) Genz Functions
   \param fnum (1-6) function number
   \param dim number of dimensions
   \return the required Genz function with random parameters
 **/
shared_ptr<OneInputNoDerivModPiece> GetRandomGenzFunction( unsigned int ftype,
                                                           unsigned int fnum, 
                                                           unsigned int dim )
{
  Eigen::VectorXd c = RandomGenerator::GetUniformRandomVector(dim).array() / 2.0 + 0.5;
  Eigen::VectorXd w = RandomGenerator::GetUniformRandomVector(dim).array() / 2.0 + 0.5;
  
  if (ftype == 0)
    {      
      double c_norm;
      switch(fnum)
        {
        case 1: c_norm = 1.5; break;
        case 2: c_norm = (double) dim; break;
        case 3: c_norm = 1.85; break;
        case 4: c_norm = 7.03; break;
        case 5: c_norm = 20.4; break;
        case 6: c_norm = 4.3; break;
        case 7: c_norm = 1.7; break;
        default:
          assert(false);
        };

      //rescale c for the right norm
      c = c*c_norm/c.sum();
  
      //rescale w
      w = w/w.sum();
    }
  // else if (fnum == 6)
  //   {
  //     double beta = 1.;
  //     double alpha = exp( log(1./2.) / static_cast<double>(dim) ) / ( 1. - exp( log(1./2.) / static_cast<double>(dim) ) ) * beta;
  //     double ga;
  //     double gb;
  //     for (unsigned int i = 0; i < dim; ++i) {
  //       ga = RandomGenerator::GetGamma(alpha,1.);
  //       gb = RandomGenerator::GetGamma(beta,1.);
  //       w(i) = ga / (ga + gb);
  //     }
  //   }
  
  shared_ptr<OneInputNoDerivModPiece> outfun;
  switch(fnum)
    {
    case 1:
      outfun = make_shared<GenzOscillatory>(dim, w, c);
      break;
    case 2:
      outfun = make_shared<GenzProductPeak>(dim, w, c);
      break;
    case 3:
      outfun = make_shared<GenzCornerPeak>(dim, w, c);
      break;
    case 4:
      outfun = make_shared<GenzGaussian>(dim, w, c);
      break;
    case 5:
      outfun = make_shared<GenzContinuous>(dim, w, c);
      break;
    case 6:
      // if (ftype == 0)
      outfun = make_shared<GenzDiscontinuous>(dim, w, c);
      // else
      //   outfun = make_shared<ModGenzDiscontinuous>(dim, w, c);
      break;
    default:
      assert(false);
    };
  return outfun;
};
