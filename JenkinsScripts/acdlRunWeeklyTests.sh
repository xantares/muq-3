#!/bin/bash

if [[ `hostname` == "macys.mit.edu" ]]; then
  export DYLD_LIBRARY_PATH=/Users/jenkins/util/gtest/lib/:$WORKSPACE/MUQ/install/lib:$WORKSPACE/MUQ/install/muq_external/lib
fi

build/modules/RunAllTests --gtest_output=xml:results/tests/TestResults.xml
build/modules/RunAllLongTests --gtest_output=xml:results/tests/LongTestResults.xml

exit 0
